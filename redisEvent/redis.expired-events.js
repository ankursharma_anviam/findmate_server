const PubSub = require("./pubsub");
const prospectDb = require('../models/prospect');
const ObjectID = require('mongodb').ObjectID


function RedisExpiredEvents() {
  PubSub.subscribe("__keyevent@0__:expired");
  // PubSub.subscribe("__key*__:*");
  PubSub.on("message",  function(channel, message) {

    console.log('ooooooooooooooooooooooo',message);
    console.log('split',  message.split(',') );
    let users =  message.split(',') 

    //update for first user
    prospectDb.Update( {_id:ObjectID(users[0]) },{coolOffTimeStampExpired:true},(err,result)=>{

      if(err){
        console.log(err);
      }else{
        console.log('coolofftimeexpired is set to true for user',users[0] )
      }

    })
    //update for second user
    prospectDb.Update( {_id:ObjectID(users[1]) },{coolOffTimeStampExpired:true},(err,result)=>{

      if(err){
        console.log(err);
      }else{
        console.log('coolofftimeexpired is set to true for user',users[1] )
      }

    })

    
  });

}

module.exports = RedisExpiredEvents