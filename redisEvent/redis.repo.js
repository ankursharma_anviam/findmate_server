var Redis = require('redis');

const config = { port: '6379', host: 'localhost',  db: 0 };

 class RedisRepo {
  constructor() {
    this.redis = new Redis.createClient(config);
    this.redis.on("ready", () => {
      this.redis.config("SET", "notify-keyspace-events", "Ex");
    });
    
  }
  get(key) {
    return this.redis.get(key);
  }

  setReminder(key, value, expire) {
    this.redis
      .multi()
      .set(key, value)
      .set(`reminder:${key}`, 1)
      .expire(`reminder:${key}`, expire)
      .exec();
  }

}

let redisRepo = new RedisRepo();

module.exports = redisRepo