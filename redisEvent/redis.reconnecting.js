var Redis = require('redis');

var restoreExpiryToRedis = require('./../../models/expireTransfersAfterServerRestart');

const config = { port: process.env.REDIS_PORT, host: process.env.REDIS_IP, password:process.env.REDIS_PASS,  db: 0 };

function checkConnection(){
    this.redis = new Redis.createClient(config);
    this.ReconnectionAttempt = false;

    this.redis.on('connect'     , ()=>{ resetKeysIfRestared() });
    
    
    function resetKeysIfRestared(){       
        
            restoreExpiryToRedis();
            this.ReconnectionAttempt = false;
        
    }   
    
}

module.exports = checkConnection