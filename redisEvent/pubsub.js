var Redis = require('redis');
const conf = require('../config');

const config = { port: 6379, host: conf.redis.REDIS_IP };
const subscriber = new Redis.createClient(config);
const publisher = new Redis.createClient(config);

  class PubSub {
  publish(channel, message) {
    publisher.publish(channel, message);
  }
  subscribe(channel) {
    subscriber.subscribe(channel);
  }

  on(event, callback) {
    subscriber.on(event, (channel, message) => {
      callback(channel, message);
    });
  }
};

var pubSub = new PubSub();

module.exports = pubSub;