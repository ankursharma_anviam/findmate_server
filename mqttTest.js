"use strict"
const mqtt = require('mqtt');

const config = require('./config');

var os = require('os');
var interfaces = os.networkInterfaces();
var addresses = [];
var calls = {};
for (var k in interfaces) {
    for (var k2 in interfaces[k]) {
        var address = interfaces[k][k2];
        if (address.family === 'IPv4' && !address.internal) {
            addresses.push(address.address + "_DatumClient");
        }
    }
}

var MqttClientId = "MQTT_CLIENT_12_" + addresses[0];
/**
 * options:
 *  - clientId
 *  - username
 *  - password
 *  - keepalive: number
 *  - clean:
 *  - will: 
 */
var mqtt_options = {
    clientId: MqttClientId,
    keepalive: 0,
    clean: true,
    // username: config.mqtt.MQTT_USERNAME,
    // password: config.mqtt.MQTT_PASSWORD
    will: { topic: "test1", payload: "offline", qos: 1, retain: 1 }
};

const client = mqtt.connect(config.mqtt.MQTT_URL, mqtt_options);

// debuggin code
client.on("error", function (error) {
    console.log("ERROR: ", error);
});

client.on('offline', function (error) {
    console.log("offline", error);
});

client.on('reconnect', function (error) {
    console.log("reconnect", error);
});
/**
 * here client is connect to server as assign
 */
client.on('connect', () => {
    console.log('MQTT Connected');
    client.subscribe("searchResult/#", {
        qos: 2
    });
    client.subscribe("match/user/#", {
        qos: 1
    });
    client.subscribe("GetChats/#", {
        qos: 2
    });
    client.subscribe("unMatch/#", {
        qos: 2
    });
    client.subscribe("OnlineStatus/#", {
        qos: 2
    });
    client.subscribe("Acknowledgement/#", {
        qos: 2
    });
    client.subscribe("Message/#", {
        qos: 1
    });
    client.subscribe("GetMessages/#", {
        qos: 2
    });
    client.subscribe("boost/#", {
        qos: 1
    });
    client.subscribe("#", {
        qos: 1
    });
    client.subscribe("5cb88964de7f135118e20c24",{
        qos:1
    })
    client.subscribe("test1",{
        qos:1
    })
    // client.subscribe("#", { qos: 1 });
    console.log("Client Connected...");
});
/**
 * here client listin chennels which is subscribe
 * In any channel user must be send fId : fiberBaseUserId 
 */
client.on('message', (topic, message) => {
    try {
        var targetId = "";
        message = JSON.parse(message.toString());
        switch (message.messageType) {
            case "onlineStatus":
                {

                   console.log('online Status')
                    break;
                }
        }
        switch (topic) {
            case String(topic.match(/^OnlineStatus.*/)):
                {
                    // console.log('fff',topic)
                    break;
                }
            case String(topic.match(/^\/match\/user\/.*/)):
                {
                    console.log("got a msg ", message.toString())
                    break;
                }
            case String(topic.match(/^Acknowledgement.*/)):
                {
                    console.log('topic',topic)
                    break;
                }
            case String(topic.match(/^5cb88964de7f135118e20c24.*/)):
                {
                    console.log('topic',message)
                    break;
                }

            case String(topic.match(/^CallsAvailability.*/)):
            case String(topic.match(/^Calls.*/)):
                {

                  console.log('ggg',topic)
                    break;
                }
            case String(topic.match(/^Message.*/)):
                {
                    console.log("received message  topic : " + topic);
                    break;
                }
            case String(topic.match(/^test1.*/)):
                {
                    console.log("test1test1test1test1test1 : ");
                    break;
                }
        }
        // console.log("message ", JSON.stringify(message))
        console.log("received message  topic : " + topic);
    } catch (e) {
        console.log("Exception : ", e);
    }
});

