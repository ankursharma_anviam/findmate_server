
const redisClient = require('../redisDB').client;
//redis functions
let changeUserStatusInRedis = (id, status) => {
    return new Promise((resolve, reject) => {

        /* if (typeof id == "object") {
            for (let i of id) {
                redisClient.HMSET('onlineUsersList', i, status, (err, result) => {
                    if (err) {
                        console.log('here is the eerror====', err);
                        reject(err)
                    }
                });
            }
            resolve(true);
        } else { */
            redisClient.HMSET('onlineUsersList', id, status, (err, result) => {
                if (err) console.log('here is the eerror====', err);

                else return resolve(result)
            });
        // }



    })
}

let searchUsersByStatusInRedis = () => {
    return new Promise((resolve, reject) => {

        redisClient.HGETALL('onlineUsersList', (err, result) => {
            if (err) console.log('here error=', { err, message: "redis error, searching user" });

            else return resolve(result);
        })
    })
}
//last will from broker in mqtt
let checkIfUserIsInProspectInRedis = (userId) => {
    return new Promise((resolve, reject) => {
        redisClient.get(userId[0] + ',' + userId[1], function (err, reply) {
            if (err) console.log('ehrer error==', err);
            if (reply) {
                return resolve(reply);
            } else {
                redisClient.get(userId[1] + ',' + userId[0], function (err, reply) {
                    if (err) console.log('ehrer error==', err);
                    return resolve(reply);
                });
            }
        });
    })
}

let insertUserInProspectRedis = (userId) => {
    return new Promise((resolve, reject) => {
        redisClient.get(userId[0] + ',' + userId[1], function (err, reply) {
            if (err) console.log('ehrer error==', err);
            if (reply) {
                redisClient.set(userId[0] + ',' + userId[1], Date.now());
                redisClient.expire(userId[0] + ',' + userId[1], 3600);
                return resolve(true);
            } else {
                redisClient.set(userId[1] + ',' + userId[0], Date.now());
                redisClient.expire(userId[1] + ',' + userId[0], 3600);
                return resolve(true);
            }
        });
    })
}

module.exports = {
    insertUserInProspectRedis, checkIfUserIsInProspectInRedis, searchUsersByStatusInRedis, changeUserStatusInRedis
};