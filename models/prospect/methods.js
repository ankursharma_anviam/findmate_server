'use strict'

var db = require('../mongodb')
var logger = require('winston');
var ObjectID = require('mongodb').ObjectID;

var tablename = 'prospect';
function Select(data, callback) {
     //console.log(data)
    db.get().collection(tablename)
        .find(data)
        .toArray((err, result) => {
          // console.log(result)
            return callback(err, result);
        });

}
var getPendingRequest = (_id, callback) => {
    console.log(_id, typeof (_id))
    db.get().collection(tablename)
        .aggregate([
            // { $match: { friendRequestSentTo: ObjectID("5c514b6c82a3fd4e93d18375"), friendRequestStatus: "Pending" } },
            { $match: { friendRequestSentTo: _id, friendRequestStatus: "Pending" } },//match with userid with _id
            {
                $lookup:
                {
                    from: 'userList',
                    localField: 'friendRequestSentByerId',//from shortlist
                    foreignField: '_id',// from postList
                    as: 'senderdetails'
                },
            },
            { $unwind: "$senderdetails" },

            {
                $project: {
                    "senderdetails.firstName": 1,
                    "senderdetails.profilePic": 1,
                    "senderdetails.findMateId": 1,
                    "senderdetails._id": 1,
                    "_id": 0
                }
            },

        ]).toArray((err, result) => {
            //  console.log(result);
            return callback(err, result);
        });

}
var OtherLikeDetail = (_id, callback) => {
    db.get().collection(tablename)
    .aggregate([
        { $match:{_id:_id }},//match with userid with _id
        { $lookup:
           {
             from: 'userList',
             localField: 'ProfileLikedBy',//from shortlist
             foreignField: '_id',// from postList
             as: 'userdetails'
           }
         },
         { $unwind:"$userdetails" }, 
          { $project: {
            "userdetails.firstName": 1,
            "userdetails.profilePic": 1,
            "userdetails.address.country":1,
            "userdetails.address.countryCode":1,
            "userdetails.address.city":1,
            "userdetails._id":1,
            "_id":0
         }},
          
        ]).toArray ((err, result) => {
          //  console.log(result);
            return callback(err, result);
        });

}
var SelectByAggregate = (_id, callback) => {
    //console.log(_id, typeof (_id))
    db.get().collection(tablename)
        .aggregate([
            // { $match: { friendRequestSentTo: ObjectID("5c514b6c82a3fd4e93d18375"), friendRequestStatus: "Pending" } },
            { $match: { _id:_id } },//match with userid with _id
            {
                $lookup:
                {
                    from: 'userList',
                    localField: 'friendShipId',//from shortlist
                    foreignField: '_id',// from postList
                    as: 'senderdetails'
                },
            },
            { $unwind: "$senderdetails" },

            {
                $project: {
                    "senderdetails.firstName": 1,
                    "senderdetails.profilePic": 1,
                    "senderdetails.findMateId": 1,
                    "senderdetails._id": 1,
                    "senderdetails.address.country":1,
                    "senderdetails.address.countryCode":1,
                    "senderdetails.address.city":1,
                    "_id": 0
                }
            },

        ]).toArray((err, result) => {
            //  console.log(result);
            return callback(err, result);
        });

}
var getAcceptedRequest = (_id, callback) => {
    db.get().collection(tablename)
        .aggregate([
            { $match: { $or: [{ friendRequestSentByerId: _id, friendRequestStatus: "Accepted" },
             { friendRequestSentTo: _id, friendRequestStatus: "Accepted" }] } },//match with userid with _id
            {
                $lookup:
                {
                    from: 'userList',
                    localField: 'friendRequestSentByerId',//from shortlist
                    foreignField: '_id',// from postList
                    as: 'senderdetails'
                }
            },
            { $unwind: "$senderdetails" },

            {
                $project: {
                    "senderdetails.firstName": 1,
                    "senderdetails.profilePic": 1,
                    "senderdetails.findMateId": 1,
                    "senderdetails._id": 1,
                    "_id": 0
                }
            },

        ]).toArray((err, result) => {
            //  console.log(result);
            return callback(err, result);
        });

}

var getPendingRequest1 = (_id, callback) => {
    console.log(_id, typeof (_id))
    db.get().collection(tablename)
        .aggregate([
            // { $match: { friendRequestSentTo: ObjectID("5c514b6c82a3fd4e93d18375"), friendRequestStatus: "Pending" } },
            { $match: { friendRequestSentTo: _id, friendRequestStatus: "Pending" } },//match with userid with _id
            {
                $lookup:
                {
                    from: 'userList',
                    localField: 'friendRequestSentByerId',//from shortlist
                    foreignField: '_id',// from postList
                    as: 'senderdetails'
                },
            },
            { $unwind: "$senderdetails" },

            {
                $project: {
                    "senderdetails.firstName": 1,
                    "senderdetails.profilePic": 1,
                    "senderdetails.findMateId": 1,
                    "senderdetails._id": 1,
                    "_id": 0
                }
            },

        ]).toArray((err, result) => {
            //  console.log(result);
            return callback(err, result);
        });

}
var getAcceptedRequest1 = (_id, callback) => {
    db.get().collection(tablename)
        .aggregate([
            { $match: { $or: [{ friendRequestSentByerId: _id, friendRequestStatus: "Accepted" },
             { friendRequestSentTo: _id, friendRequestStatus: "Accepted" }] } },//match with userid with _id
            {
                $lookup:
                {
                    from: 'userList',
                    localField: 'friendRequestSentByerId',//from shortlist
                    foreignField: '_id',// from postList
                    as: 'senderdetails'
                }
            },
            { $unwind: "$senderdetails" },

            {
                $project: {
                    "senderdetails.firstName": 1,
                    "senderdetails.profilePic": 1,
                    "senderdetails.findMateId": 1,
                    "senderdetails._id": 1,
                    "_id": 0
                }
            },

        ]).toArray((err, result) => {
            //  console.log(result);
            return callback(err, result);
        });

}
function SelectWithSort(data, sortBy = {}, porject = {}, skip = 0, limit = 20, callback) {
    db.get().collection(tablename)
        .find(data)
        .sort(sortBy)
        .project(porject)
        .skip(skip)
        .limit(limit)
        .toArray((err, result) => {
            return callback(err, result);
        });

}
function matchList(data, callback) {
    db.get().collection(tablename)
        .aggregate(data, (err, result) => {
            return callback(err, result);
        });
}

function SelectOne(data, callback) {
    db.get().collection(tablename)
        .findOne(data, (err, result) => {
           // console.log(result);
            return callback(err, result);
        });
}

function FindMateIdOne(data, callback) {
    db.get().collection(tablename)
        .findOne({ _id: ObjectID(data) }, { findMateId: 1 }, (err, result) => {
            return callback(err, result);
        });
}
function FindUserByFindMateId(data, callback) {
    db.get().collection(tablename)
        .findOne(data, { firstName: 1, profilePic: 1 }, (err, result) => {
            return callback(err, result);
        });
}
function SelectById(condition, requiredFeild, callback) {
    condition["_id"] = ObjectID(condition._id);
    db.get().collection(tablename)
        .findOne(condition, requiredFeild, ((err, result) => {
            return callback(err, result);
        }));
}


function Insert(data, callback) {
    db.get().collection(tablename)
        .insert(data, (err, result) => {
            return callback(err, result);
        });
}
function Update(condition, data, callback) {
   // console.log(condition, data)
    db.get().collection(tablename)
        .updateOne(condition, { $set: data }, (err, result) => {
//console.log("result---",result);
            return callback(err, result);
        });
}
function FindAndModify(condition, data, callback) {
    //  console.log(condition, data)
    db.get().collection(tablename)
        .updateOne(condition, { $set: data }, { upsert: true }, (err, result) => {
            return callback(err, result);
        });
}
var findOneAndUpdate = (condition, data, callback) => {
    db.get().collection(tablename)
        .update(
            condition,
            data, { upsert: true },
            ((err, result) => {
                return callback(err, result);
            }));
}

function UpdateWithIncrease(condition, data, callback) {
    db.get().collection(tablename)
        .update(condition, { $inc: data }, (err, result) => {
            return callback(err, result);
        });
}

function UpdateFriendWithPull(condition, data, callback) {
    db.get().collection(tablename)
        .update(condition, { $pull: data },{multi:true}, (err, result) => {
            return callback(err, result);
        });
}
function UpdateById(_id, data, callback) {
    db.get().collection(tablename)
        .updateOne({ _id: ObjectID(_id) }, { $set: data }, (err, result) => {
            return callback(err, result);
        });
}
function UpdateById1(_id, data, callback) {
    db.get().collection(tablename)
        .updateOne({ _id: ObjectID(_id) }, { $addToSet: data }, (err, result) => {
            return callback(err, result);
        });
}

function UpdateByIdWithAddToSet(condition, data, callback) {
    condition["_id"] = ObjectID(condition._id);
    db.get().collection(tablename)
        .update(condition, { $addToSet: data }, (err, result) => {
            return callback(err, result);
        });
}

// function updateWithAddToSet(condition, data, callback) {
//     condition["_id"] = ObjectID(condition._id);
//     db.get().collection(tablename)
//         .update(condition, { $addToSet: data }, (err, result) => {
//             return callback(err, result);
//         });
// }
const updateWithAddToSet = (id, data, callback) => {
    db.get().collection(tablename)
        .update({ _id: id }, { "$addToSet": data }, ((err, result) => {
            return callback(err, result);
        }));

}

function updateLikeAddToSet(condition, data, callback) {
    db.get().collection(tablename)
        .update(condition, { $addToSet: data }, (err, result) => {
            return callback(err, result);
        });
}

function UpdateByIdWithPush(condition, data, callback) {
   // condition["_id"] = ObjectID(condition._id);
    db.get().collection(tablename)
        .update(condition, { $push: data }, (err, result) => {
            return callback(err, result);
        });
}
function UpdateByIdWithPull(condition, data, callback) {
    condition["_id"] = ObjectID(condition._id);
    db.get().collection(tablename)
        .update(condition, { $pull: data }, (err, result) => {
            return callback(err, result);
        });
}

function Delete(condition, callback) {
    db.get().collection(tablename)
        .remove(condition, (err, result) => {
            return callback(err, result);
        });
}
function Aggregate(condition, callback) {
    db.get().collection(tablename)
        .aggregate(condition, (err, result) => {
            return callback(err, result);
        });
}

function Unset(condition, data, callback) {
    db.get().collection(tablename)
        .update(condition, { $unset: data }, (err, result) => {
            return callback(err, result);
        });
}


module.exports = {
    Aggregate, SelectWithSort, Unset, UpdateWithIncrease,
    Select, matchList, SelectOne, Insert, Update, SelectById, UpdateById,
    Delete, UpdateByIdWithAddToSet, UpdateByIdWithPush, UpdateByIdWithPull,
    FindMateIdOne,FindUserByFindMateId,UpdateById1,UpdateFriendWithPull,
    updateWithAddToSet,SelectByAggregate,OtherLikeDetail,findOneAndUpdate,FindAndModify,
    updateLikeAddToSet
};
