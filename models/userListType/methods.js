'use strict'
const logger = require('winston');
const elasticClient = require('../elasticSearch');
const tablename = 'userList';
const indexName = "azar";
const version = 382;

function findMatch(data, callback) {
    let condition = {
        "from": data.skip, "size": data.limit,
        "query": {
            "bool": {
                "must": [
                    {
                        "match": {
                            "gender": data.gender
                        }
                    },
                    {
                        "range": {
                            "height": {
                                "gte": data.heightMin,
                                "lte": data.heightMax
                            }
                        }
                    },
                    {
                        "range": {
                            "dob": {
                                "gte": data.dobMin,
                                "lte": data.dobMax
                            }
                        }
                    }
                ],
                "must_not": data.must_not,
                "filter": {
                    "geo_distance": {
                        "distance": data.distanceMax,
                        "location": {
                            "lat": data.latitude,
                            "lon": data.longitude
                        }
                    }
                }
            }
        },
        "sort": [
            { "boostTimeStamp": "asc" },
            {
                "_geo_distance": {
                    "location": {
                        "lat": data.latitude,
                        "lon": data.longitude
                    },
                    "order": "asc",
                    "unit": "km",
                    "distance_type": "plane"
                }
            },

        ],
        "_source": [
            "firstName", "contactNumber", "gender", "registeredTimestamp",
            "profilePic", "otherImages", "email", "profileVideo", "dob", "about",
            "instaGramProfileId", "onlineStatus", "height", "location", "firebaseTopic",
            "deepLink", "profileVideoThumbnail", "myPreferences", "dontShowMyAge", "dontShowMyDist",
            "profileVideoWidth", "profileVideoHeight", "boostTimeStamp"
        ]
    }

    // logger.silly("condition : ", JSON.stringify(condition))
    elasticClient.get().search({
        index: indexName,
        type: tablename,
        body: condition
    }, function (err, result) {
        callback(err, result);
    });
}
function findLiveMatch(data, callback) {
    let condition = {
        "from": 0, "size": 1,
        "query": {
            "bool": {
                "must": data.must,
                "must_not": data.must_not,
                "should": data.should
            }
        },
        "_source": [
            "firstName", "contactNumber", "gender", "registeredTimestamp",
            "profilePic", "otherImages", "email", "profileVideo", "dob", "about",
            "instaGramProfileId", "onlineStatus", "height", "location", "firebaseTopic",
            "deepLink", "profileVideoThumbnail", "myPreferences", "dontShowMyAge", "dontShowMyDist",
            "profileVideoWidth", "profileVideoHeight", "boostTimeStamp", "address"
        ]
    }

    logger.silly("condition : ", JSON.stringify(condition))
    elasticClient.get().search({
        index: indexName,
        type: tablename,
        body: condition
    }, function (err, result) {
        callback(err, result);
    });
}
function Select(data, callback) {
    elasticClient.get().search({
        index: indexName,
        type: tablename,
        body: {
            "query": {
                "match": data
            }
        }

    }, function (err, result) {
        callback(err, result);
    });
}
function SelectActiveUser(data, callback) {
    elasticClient.get().search({
        index: indexName,
        type: tablename,
        body: {
            "query": {
                "bool": {
                    "must": { "match": data },
                    "must_not": [
                        {
                            "exists": {
                                "field": "deleteStatus"
                            }
                        }
                    ]
                }
            },
            "_source": ["firstName",
                "profilePic", "findMateId", "address"
            ]
        }
    }, function (err, result) {
        callback(err, result);
    });
}


function SelectAll(callback) {
    elasticClient.get().search({
        index: indexName,
        type: tablename,
        body: {
            from: 0, size: 200,
            "query": {
                "match_all": {}
            }
        }
    }, function (err, result) {
        callback(err, result);
    });
}

function CustomeSelect(data, callback) {
    elasticClient.get().search({
        index: indexName,
        type: tablename,
        body: data
    }, function (err, result) {
        callback(err, result);
    });
}
function Insert(data, callback) {
    console.log("es inster 1  ", data)
    let _id = "" + data._id;
    delete data._id;
    elasticClient.get().index({
        index: indexName,
        type: tablename,
        id: _id,
        body: data
    }, (err, result) => {
        console.log("es inster ", err)
        return callback(err, result);
    });
}

function UpdateWithPush(_id, field, value, callback) {


    elasticClient.get().update({
        index: indexName,
        type: tablename,
        id: _id,
        retry_on_conflict: 5,
        body: {
            "script": "ctx._source." + field + ".add('" + value + "')"
        }

    }, (err, results) => {
        callback(err, results)
    })
}

function UpdateWithPull(_id, field, value, callback) {

    elasticClient.get().update({
        index: indexName,
        type: tablename,
        id: _id,
        retry_on_conflict: 5,
        body: {
            "script": "ctx._source." + field + ".remove(ctx._source." + field + ".indexOf('" + value + "'))"
        }
    }, (err, results) => {
        callback(err, results);
    })
}

function Update(_id, data, callback) {

    elasticClient.get().update({
        index: indexName,
        type: tablename,
        id: _id,
        retry_on_conflict: 5,
        body: {
            doc: data,
            doc_as_upsert: true
        }
    }, (err, results) => {

        callback(err, results)
    })
}

function updateByQuery(condition, fieldName, fieldValue, callback) {
    elasticClient.get().updateByQuery({
        index: indexName,
        type: tablename,
        version: version,
        body: {
            query: { "match": condition },
            "script": { "source": `ctx._source.${fieldName}=${fieldValue}` }
        }
    }, (err, results) => {
        callback(err, results)
    })
}

function updateByQuery(condition, fieldName, fieldValue, callback) {
    elasticClient.get().updateByQuery({
        index: indexName,
        type: tablename,
        version: version,
        body: {
            query: { "match": condition },
            "script": { "source": `ctx._source.${fieldName}=${fieldValue}` }
        }
    }, (err, results) => {
        callback(err, results)
    })
}
function removeField(_id, fieldName, callback) {
    elasticClient.get().updateByQuery({
        index: indexName,
        type: tablename,
        version: version,
        body: {
            "script": `ctx._source.remove("${fieldName}")`,
            "query": {
                "bool": {
                    "must": [
                        {
                            "exists": {
                                "field": `${fieldName}`
                            }
                        }
                    ]
                }
            }
        }
    }, (err, results) => {
        callback(err, results)
    })
}

function setOffline(timestamp, callback) {
    elasticClient.get().updateByQuery({
        index: indexName,
        type: tablename,
        version: version,
        body: {
            query: {
                "range": {
                    "lastOnline": {
                        "lt": timestamp
                    }
                },
                // "match":{"onlineStatus":1}
            },
            "script": { "source": "ctx._source.onlineStatus=0" }
        }
    }, (err, results) => {
        callback(err, results)
    })
}

function Delete(tablename, condition, callback) {
    elasticClient.get().deleteByQuery({
        index: indexName,
        type: tablename,
        version: version,
        body: {

            query: {
                match: condition
            }
        }
    }, (err, results) => {
        callback(err, results)
    })
}
function Bulk(dataInArray, callback) {
    elasticClient.get().bulk({
        body: dataInArray
    }, (err, results) => {
        callback(err, results)
    })
}
function updateByQueryWithArray(condition, callback) {
    elasticClient.updateByQuery({
        index: indexName,
        type: tablename,
        version: version,
        body: condition
    }, (err, results) => {
        callback(err, results)
    })
}


/* function searchWithPrefrencesInES(searchingUsers, data, callback) {		
    let searchPreferences = [];		
    for (let searchPref of data.searchPreferences){		
        searchPreferences.push({		
            "terms": {		
            "searchPreferences.selectedValue":  searchPref		
          }		
        });		
    }		
    elasticClient.get().search({		
        index: indexName,		
        type: tablename,		
        body:{		
           "query" : {		
             "bool": {		
              "should": [		
                searchPreferences,		
                {		
                  "term": {		
                    "gender.keyword": {		
                      "value": data.gender		
                    }		
                  }		
                }		
              ],		
              "must_not": [		
                {		
                  "term": {		
                    "_id": data._id		
                  }		
                }		
              ],		
              "must": [		
                {		
                  "terms": {		
                    "_id": searchingUsers		
                  }		
                }		
              ]		
            }		
          }		
        }		
        		
    }, function (err, result) {		
        callback(err, result);		
    });		
}		 */
//latest		
async function searchWithPrefrencesInES(searchingUsers, data, payload) {

    try {
        let condition =  {
            "query": {
                "bool": {
                    "must": [
                        {
                            "term": {
                                "gender.keyword": {
                                    "value": data.gender
                                }
                            },
                        },
                        {
                            "terms":{
                                "_id":searchingUsers
                            }
                        }
                    ],
                    "must_not": [
                        {
                            "term": {
                                "_id": data._id
                            }
                        },
                        {
                            "terms": {
                              "_id": data.myBlock
                            }
                          },
                        {
                            "terms": {
                              "_id": data.blockedBy
                            }
                          }
                    ],
                   
                    "should": [
                        {
                            "terms":{
                                "searchPreferences.pref_id":payload.interests
                            }
                        },
                        {
        
                            "range": {
                                "dob": {
                                    "gte": payload.rangeOfAge[0],
                                    "lte": payload.rangeOfAge[1]
                                }
                            }
                        },
                        {
                            "range": {
                                "height": {
                                    "gte": payload.rangeOfHeight[0],
                                    "lte": payload.rangeOfHeight[1]
                                }
                            }
                        }
                    ]                   
                   
                }
            }
        }

        console.log('sdfsdf',JSON.stringify(condition));

    /* elasticClient.get().search({
        index: indexName,
        type: tablename,
        body:condition
        
    }, function (err, result) {
       return callback(err, result);
    }); */

      let result = await elasticClient.get().search({
        index: indexName,
        type: tablename,
        body:condition
      });

      if(result)
      return result;
    } catch (error) {
        console.log("es query ===> ", error);
        
        throw error 
    }




      
}
/* function searchUsersForChatInES(searchingUsers, data, callback) {
    elasticClient.get().search({
        index: indexName,
        type: tablename,
        body: {
            "query": {
                "bool": {
                    "should": [
                        {
                            "term": {
                                "gender.keyword": {
                                    "value": data.gender
                                }
                            }
                        }
                    ],
                    "must_not": [
                        {
                            "term": {
                                "_id": data._id
                            }
                        }
                    ],
                    "must": [
                        {
                            "terms": {
                                "_id": searchingUsers
                            }
                        }
                    ]
                }
            }
        }
    }, function (err, result) {
        callback(err, result);
    });
} */
module.exports = {
    updateByQueryWithArray, setOffline, Bulk, removeField,
    CustomeSelect, Select, Insert, Update, updateByQuery, Delete, findMatch,
    UpdateWithPush, UpdateWithPull, SelectAll, SelectActiveUser, findLiveMatch,

     searchWithPrefrencesInES
};