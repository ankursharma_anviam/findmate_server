const path = require('path');
const ipc = require('node-ipc');
const fork = require('child_process').fork;
let logger = require('winston');
let amqp = require('./rabbitMq');

var cpus = true

function InsertQueue(channel, queue, data, callback) {

    if (channel) {
        channel.assertQueue(queue, { durable: false }, function (err, queueList) {


            channel.sendToQueue(queue, Buffer.from(JSON.stringify(data)));
            if (cpus) {
                cpus = false;
                startIPCServerForRabbitMQWorker(data);
            }
        });
    }
}


function startIPCServerForRabbitMQWorker(data) {
    var file = path.join(__dirname, '../../worker/latLongToCity/worker.js');
    var child_process = fork(file);

  
}

module.exports = { InsertQueue };