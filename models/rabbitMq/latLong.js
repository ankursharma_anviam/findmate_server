const path = require('path');
const ipc = require('node-ipc');
const fork = require('child_process').fork;
let logger = require('winston');
let amqp = require('./rabbitMq');

const cluster = require('cluster');
ipc.config.id = 'rabbitmqserverIptoLatLong';
ipc.config.silent = true;
var cpus = {};

function InsertQueue(channel, queue, data, callback) {

    if (channel) {
        channel.assertQueue(queue, { durable: false }, function (err, queueList) {

            let messageCount = queueList.messageCount;
            let consumerCount = queueList.consumerCount;
            channel.sendToQueue(queue, Buffer.from(JSON.stringify(data)));
            if (!cpus[cluster.worker.id]) {
                cpus[cluster.worker.id] = true;
                startIPCServerForRabbitMQWorker(data);
            }
        });
    }

}


function startIPCServerForRabbitMQWorker(data) {

    var file = path.join(__dirname, '../../worker/ipToLatLong/worker.js');
    var child_process = fork(file);

    ipc.serve(() => {
        ipc.server.on('consumer.connected', (data, socket) => {
        });

        ipc.server.on('message.consumed', (data, socket) => {
        });

        ipc.server.on('consumer.exiting', (data, socket) => {
            ipc.server.stop();
        });

    });

    try {
        ipc.server.start();
    } catch (err) {
        logger.error(err);
    }
}


module.exports = { InsertQueue };