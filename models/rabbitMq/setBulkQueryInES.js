const path = require('path');
let logger = require('winston');
const cluster = require('cluster');
const fork = require('child_process').fork;
var cpus = true;
var bulkQueryArray = [];

function InsertQueue(channel, queue, data, callback) {
    if (channel) {
        channel.assertQueue(queue, { durable: false }, function (err, queueList) {

            channel.sendToQueue(queue, Buffer.from(JSON.stringify(data)));

            if (cpus) {
                cpus = false;
                startIPCServerForRabbitMQWorker(data);
            }
        });
    } else {
        logger.error("channal not found...1");
    }

}
function startIPCServerForRabbitMQWorker(data) {
    var file = path.join(__dirname, '../../worker/bulk/worker.js');
    var child_process = fork(file);
}

module.exports = { InsertQueue };