const path = require('path');
const ipc = require('node-ipc');
const fork = require('child_process').fork;
let logger = require('winston');
let amqp = require('./rabbitMq');
const cluster = require('cluster');
var cpus = {};

ipc.config.id = 'rabbitmqserverSMS';
ipc.config.silent = true;


function InsertQueue(channel, queue, data, callback) {
    if (channel) {
        channel.assertQueue(queue, { durable: false }, function (err, queueList) {

            let messageCount = queueList.messageCount;
            let consumerCount = queueList.consumerCount;
            channel.sendToQueue(queue, Buffer.from(JSON.stringify(data)));
            if (!cpus[cluster.worker.id]) {
                cpus[cluster.worker.id] = true;
                startIPCServerForRabbitMQWorker(data);
            }

        });
    }
    else {
        logger.error("channal not found...");
    }
}


function startIPCServerForRabbitMQWorker(data) {

    var file = path.join(__dirname, '../../worker/sms/worker.js');
    var child_process = fork(file);
    ipc.serve(() => {
        ipc.server.on('consumer.connected', (data, socket) => {
            logger.info('RabbitMQ consumer is connected and ready to execute', data);
        });

        ipc.server.on('message.consumed', (data, socket) => {
            logger.info('RabbitMQ message consumed callback', data);
            smsLog.Insert(data, (err, response) => {
            });//insert  log in database

        });

        ipc.server.on('consumer.exiting', (data, socket) => {
            logger.info('RabbitMQ message consumer exiting', data);
            ipc.server.stop();
        });

    });

    try {
        ipc.server.start();
    } catch (err) {
        logger.error(err);

    }

}


module.exports = { InsertQueue };