const path = require('path');
const ipc = require('node-ipc');
const fork = require('child_process').fork;
let logger = require('winston');
let amqp = require('./rabbitMq');
const mqtt = require('../../library/mqtt');
const cluster = require('cluster');
var cpus = false;



function InsertQueue(channel, queue, data, callback) {

    if (channel) {

        channel.sendToQueue(queue, Buffer.from(JSON.stringify(data)));
        if (!cpus) {
            startIPCServerForRabbitMQWorker(data);
        }
    }
    else {
        logger.error("no have channales")
    }
}


function startIPCServerForRabbitMQWorker(data) {

    var file = path.join(__dirname, '../../worker/email/worker.js');
    var child_process = fork(file);

}


module.exports = { InsertQueue };