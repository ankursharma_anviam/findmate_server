const path = require('path');
const fork = require('child_process').fork;
let logger = require('winston');
const cluster = require('cluster');
var cpus = {};

function InsertQueue(channel, queue, data, callback) {

    if (channel) {
        channel.assertQueue(queue, { durable: false }, function (err, queueList) {
            channel.sendToQueue(queue, Buffer.from(JSON.stringify(data)));
            if (!cpus[cluster.worker.id]) {
                cpus[cluster.worker.id] = true;
                startIPCServerForRabbitMQWorker(data);
            }

        });
    } else {
        logger.error("channal not found...1");
        const amqpConn = require('./rabbitMq');
        amqpConn.connect(() => {
            if (channel) {
                logger.error("channal found...1");
                channel.assertQueue(queue, { durable: false }, function (err, queueList) {
                    channel.sendToQueue(queue, Buffer.from(JSON.stringify(data)));
                    if (!cpus[cluster.worker.id]) {
                        cpus[cluster.worker.id] = true;
                        startIPCServerForRabbitMQWorker(data);
                    }

                });
            } else {
                logger.error("channal not found...2");
            }
        });

    }

}
function startIPCServerForRabbitMQWorker(data) {
    var file = path.join(__dirname, '../../worker/searchResult/worker.js');
    var child_process = fork(file);
}

module.exports = { InsertQueue };