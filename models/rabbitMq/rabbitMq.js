'use strict'
/** 
 * This const requires the mongo url
 * @const
 * @requires module:config 
 */
const logger = require('winston');
const ipc = require('node-ipc');
const fork = require('child_process').fork;
const url = require('../../config/components/rebitMQ');
const amqp = require('amqplib/callback_api');
//For the connection the amqp server
//For the chhanel corresponding to the high traffic apis
let channelEmail = null;
let channelLatLong = null;
let channelSearchResult = null;
let channelSMS = null;
let channelLatLongToCity = null;
let channelForBulkQuerys = null;
//let channelTwilio = null;
//For the queues corresponding to the high traffic apis
let queueEmail = 'QueueEmail';
let queueLatLong = 'QueueLatLong';
let queueSearchResult = 'QueueSearchResult';
let queueSMS = 'QueueSMS';
let queueLatLongToCity = 'QueueLatLongToCity';
let queueForBulkQuerys = 'QueueForBulkQuerys';


//For the threshold of messages that can be executed by one worker corresponding to the high traffic api call
let thresholdEmail = 100;
let thresholdLatLong = 100;
let thresholdSearchResult = 100;
let thresholdSMS = 100;
let thresholdLatLongToCity = 100;
let thresholdBulkQuery =100;
//set up a connection
/**
 * @amqpConn will hold the connection and channels will be set up in the connection.
 */
var state = { amqpConn: null };


/**
 * Method to connect to the mongodb
 * @param {*} url
 * @returns connection object
 */
exports.connect = (callback) => {
    if (state.amqpConn) return callback();

    amqp.connect(url + "?heartbeat=60", (err, conn) => {
        if (err) {
            logger.error("[AMQP] error", err.message);
            return callback(err);
        }
        conn.on("error", (err) => {
            logger.error("[AMQP] conn error", err.message);
            if (err.message !== "Connection closing") {
                return callback(err);
            }
        });
        conn.on("close", () => {
            logger.error("[AMQP] reconnecting");
        });
        logger.info("[AMQP] Connected");
        state.amqpConn = conn;
        preparePublisher();
        return callback();
    });
}

/**
 * Method to get the connection object of the mongodb
 * @returns db object
 */
exports.get = () => {
    return state.amqpConn;
}


function preparePublisher() {

    channelForBulkQuerys = state.amqpConn.createChannel((err, ch) => {
        if (closeOnErr(err)) return;

        ch.on("error", (err) => {
            logger.error("[AMQP] channel error", err.message);
        });
        ch.on("close", () => {
            logger.error("[AMQP] channel closed");
        });
    });

    channelEmail = state.amqpConn.createChannel((err, ch) => {

        if (closeOnErr(err)) return;

        ch.on("error", (err) => {
            logger.error("[AMQP] channel error", err.message);
        });
        ch.on("close", () => {
            logger.error("[AMQP] channel closed");
        });
    });

    channelLatLong = state.amqpConn.createChannel((err, ch) => {

        if (closeOnErr(err)) return;

        ch.on("error", (err) => {
            logger.error("[AMQP] channel error", err.message);
        });
        ch.on("close", () => {
            logger.error("[AMQP] channel closed");
        });

    });
    channelSearchResult = state.amqpConn.createChannel((err, ch) => {

        if (closeOnErr(err)) return;

        ch.on("error", (err) => {
            logger.error("[AMQP] channel error", err.message);
        });
        ch.on("close", () => {
            logger.error("[AMQP] channel closed");
        });
    });

    channelSMS = state.amqpConn.createChannel((err, ch) => {

        if (closeOnErr(err)) return;

        ch.on("error", (err) => {
            logger.error("[AMQP] channel error", err.message);
        });
        ch.on("close", () => {
            logger.error("[AMQP] channel closed");
        });
    });

    channelLatLongToCity = state.amqpConn.createChannel((err, ch) => {

        if (closeOnErr(err)) return;

        ch.on("error", (err) => {
            logger.error("[AMQP] channel error", err.message);
        });
        ch.on("close", () => {
            logger.error("[AMQP] channel closed");
        });
    });
}

function closeOnErr(err) {
    if (!err) return false;

    state.amqpConn.close();
    return true;
}

exports.getChannelForBulkQuerys = () => {
    return channelForBulkQuerys;
}
exports.getChannelEmail = () => {
    return channelEmail;
}
exports.getChannelLatLong = () => {
    return channelLatLong;
}
exports.getChannelSearchResult = () => {
    return channelSearchResult;
}
exports.getChannelSMS = () => {
    return channelSMS;
}
exports.getChannelLatLongToCity = () => {
    return channelLatLongToCity;
}


exports.queueEmail = queueEmail;
exports.thresholdEmail = thresholdEmail;

exports.queueLatLong = queueLatLong;
exports.thresholdLatLong = thresholdLatLong;

exports.queueSearchResult = queueSearchResult;
exports.thresholdSearchResult = thresholdSearchResult;

exports.queueSMS = queueSMS;
exports.thresholdSMS = thresholdSMS;

exports.queueLatLongToCity = queueLatLongToCity;
exports.thresholdLatLongToCity = thresholdLatLongToCity;

exports.queueForBulkQuerys = queueForBulkQuerys;
exports.thresholdBulkQuery = thresholdBulkQuery;