'use strict'
const redis = require("redis");
const logger = require('winston');
const config = require('../../config');

var client = redis.createClient(6379, config.redis.REDIS_IP);
/* let client = redis.createClient({
    url:'redis://redis-19107.c81.us-east-1-2.ec2.cloud.redislabs.com:19107',
    no_ready_check:true,
    password:'uXq8Nu3KaVoTog6IZQqJknaST4vaFjYQ'
}) */

/**
 * Method to connect to the redis
 * @param {*} url
 * @returns connection object
 */
// client.auth("3embed")
client.on('connect', function () {
    logger.info('Redis connected');
});

module.exports={client}