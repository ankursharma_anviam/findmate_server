'use strict'

const tablename = 'profileLikes';
const db = require('../mongodb')
const logger = require('winston');
const moment = require('moment');//date-time
const ObjectID = require('mongodb').ObjectID;//to convert stringId to mongodb's objectId
/**
 * @function read
 * @param {object} data 
 * @param {*} callback 
 */
const insert = (data, callback) => {
    db.get().collection(tablename)
        .insert(data, ((err, result) => {
            return callback(err, result);
        }));

}
function Select(data, callback) {
     console.log(data)
     db.get().collection(tablename)
         .find(data)
         .toArray((err, result) => {
             console.log(result)
             return callback(err, result);
         });
 
 }
const MyLikeDetail = (_id, callback) => {
    db.get().collection(tablename)
    .aggregate([
        { $match:{actionBy:_id }},//match with userid with _id
        { $lookup:
           {
             from: 'userList',
             localField: 'actionOnProfile',//from shortlist
             foreignField: '_id',// from postList
             as: 'userdetails'
           }
         },
         { $unwind:"$userdetails" }, 
          { $project: {
            "userdetails.firstName": 1,
            "userdetails.profilePic": 1,
            "userdetails._id":1,
            "userdetails.address.country":1,
            "userdetails.address.countryCode":1,
            "userdetails.address.city":1,
            "_id":0
         }},
          
        ]).toArray ((err, result) => {
          //  console.log(result);
            return callback(err, result);
        });

}
const MyLikeDetailWithPagination = (_id,limit,offset, callback) => {
    db.get().collection(tablename)
    .aggregate([
        { $match:{actionBy:_id }},//match with userid with _id
        { $lookup:
           {
             from: 'userList',
             localField: 'actionOnProfile',//from shortlist
             foreignField: '_id',// from postList
             as: 'userdetails'
           }
         },
         { $unwind:"$userdetails" }, 
          { $project: {
            "userdetails.firstName": 1,
            "userdetails.profilePic": 1,
            "userdetails._id":1,
            "userdetails.address.country":1,
            "userdetails.address.countryCode":1,
            "userdetails.address.city":1,
            "_id":0
         }},
         { "$skip": offset },
         { "$limit": limit }
          
        ]).toArray ((err, result) => {
          //  console.log(result);
            return callback(err, result);
        });

}
const OtherLikeDetail = (_id, callback) => {
    db.get().collection(tablename)
    .aggregate([
        { $match:{actionOnProfile:_id }},//match with userid with _id
        { $lookup:
           {
             from: 'userList',
             localField: 'actionOnProfile',//from shortlist
             foreignField: '_id',// from postList
             as: 'userdetails'
           }
         },
         { $unwind:"$userdetails" }, 
          { $project: {
            "userdetails.firstName": 1,
            "userdetails.profilePic": 1,
            "userdetails._id":1,
            "userdetails.address.country":1,
            "userdetails.address.countryCode":1,
            "userdetails.address.city":1,
            "_id":0
         }},
          
        ]).toArray ((err, result) => {
          //  console.log(result);
            return callback(err, result);
        });

}
const updateById = (_id,data, callback) => {
    db.get().collection(tablename)
        .update({_id:_id},{"$set":{data}}, ((err, result) => {
            return callback(err, result);
        }));

}
const getCategory = (id, callback) => {
    db.get().collection(tablename)
        .findOne({_id:id}, ((err, result) => {
           // console.log("category",result);
           // console.log("err",err)
            return callback(err, result);
        }));    

}

const findOneAndUpdate = (condition, data, callback) => {
    db.get().collection(tablename)
        .update(condition,data,{upsert: true},
            ((err, result) => {
                return callback(err, result);
            }));
}

const readByAggregate = (condition, callback) => {
    db.get().collection(tablename)
        .aggregate(condition, ((err, result) => { // aggregate method
            return callback(err, result);
        }));
}
// const deleteById = (id, callback) => {
//     db.get().collection(tablename)
//         .updateOne({ _id: new ObjectID(id) }, { $set: { 'deactive': true, loggedIn: false, accessCode: "", deletedOn: moment().unix() } }, ((err, result) => {
//             return callback(err, result);
//         }));

// }
const deleteById = (condition, callback) => {
    db.get().collection(tablename)
    .deleteOne(condition,((err, result) => {
        return callback(err, result);
    }));

}
const readAll = (data, callback) => {
    db.get().collection(tablename)
        .find(data).toArray((err, result) => {
            return callback(err, result);
        });
}

//promocode

const getUserDetails = (userId, callBack) => {
    db.get().collection(tablename)
        .find({
            "_id": new ObjectID(userId),
        }).toArray((err, result) => {
            return callBack(err, result);
        });
}
//droping the collection
const remove = ({}, callback) => {
    //console.log("data",condition);
    db.get().collection(tablename)
        .drop(data, ((err, result) => {
            return callback(err, result);
        }));

}



module.exports = {
    updateById,
    insert,
    getCategory,
    findOneAndUpdate,       
    readByAggregate,
    deleteById,
    readAll,
    getUserDetails,
    remove,
    Select,
    MyLikeDetail,
    OtherLikeDetail,
    MyLikeDetailWithPagination
};
