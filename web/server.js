
var cron = require('node-cron');
var logger = require('winston');
var cluster = require('cluster');
const trace = require("node-trace");
var config = require('../config');
var db = require('../models/mongodb');
var middleware = require('./middleware');
var userListType = require('../models/userListType');
var numCPUs = require('os').cpus().length;
var express = require("express");

var redis = require("redis");

const redisEvent = require('../redisEvent/redis.expired-events');

var subscriber = redis.createClient(6379,config.redis.REDIS_IP);
subscriber.auth("3embed");
var cronJobTask = require('../cronJobTask');
var amqpConn = require('../models/rabbitMq');
var elasticSearchDB = require('../models/elasticSearch');


if (cluster.isMaster) {

    redisEvent()

    db.connect(() => { });
    elasticSearchDB.connect(() => { });
    cron.schedule('0 0 0 * * *', function () {
        cronJobTask.removeBoostIfRequired();
    });
    cron.schedule('0 * * * * *', function () {
        cronJobTask.updateOnlineUsers();
        cronJobTask.findAndRemoveExpiredInAppPurchasePlans();
        // cronJobTask.sendWellComeMailOnSignUp(11);
    });
    for (let i = 0; i < numCPUs; i++) {
        cluster.fork();
    }
/*

1008888
*/

     var metricsServer = express();
     metricsServer.listen(3003, () => { });
     metricsServer.use('', trace.MetricsCluster(metricsServer, express));
     metricsServer.use('', trace.SnapshotExpress(metricsServer, express));

    //  const trace_endpoint = require('node-trace').Endpoint;

    //  Server.on('response', (req) => {

    //      trace_endpoint.onComplete(req.info.received, req.route.method.toUpperCase(), req.route.path, req.response.statusCode)
    //  });



    subscriber.on("message", function (channel, key) {

        if (key.match(cronJobTask.likeNotificationAt)) {
            cronJobTask.sendLikeNotification(key.replace(cronJobTask.likeNotificationAt, ""));
        } else if (key.match(cronJobTask.redisKeyForBoost)) {
            cronJobTask.onBoostExpire(key.replace(cronJobTask.redisKeyForBoost, ""));
        }

        // console.log("\n\n\n\nMessage '" + key + "' on channel '" + channel + "' arrived!")
    });
    subscriber.subscribe("__keyevent@0__:expired");


    cluster.on('exit', function (worker) {
        logger.error(`worker ${worker.process.pid} died`);
        cluster.fork();
    });

} else {
    var Hapi = require('hapi');
    var Server = new Hapi.Server();

    Server.connection({
        port: config.server.port,
        routes: {
            cors: 
            {
                origin: ['*'],
                additionalHeaders: ['cache-control', 'x-requested-with', 'authorization', 'token','language','content-type']
            }
        }
    });
    Server.register(
        [
            middleware.good,
            middleware.swagger.inert,
            middleware.swagger.vision,
            middleware.swagger.swagger,
            middleware.auth.HAPI_AUTH_JWT,
            middleware.localization.i18n
        ], function (err) {
            if (err) Server.log(['error'], 'hapi-swagger load error: ' + err)
        });

    Server.auth.strategy('refJwt', 'jwt', middleware.auth.refJWTConfig);
    Server.auth.strategy('userJWT', 'jwt', middleware.auth.userJWTConfig);
    Server.route(require('./routes/index'));



     const trace_endpoint = require('node-trace').Endpoint;

    Server.on('response', (req) => {

         trace_endpoint.onComplete(req.info.received, req.route.method.toUpperCase(), req.route.path, req.response.statusCode)
    });

// }

// const initialize = () => {
    Server.start(() => {
        logger.info(`Server is listening on port `, config.server.port)
        db.connect(() => { });//create a connection to mongodb
        amqpConn.connect(() => { });
        elasticSearchDB.connect(() => { }); // create a connection to elasticSearchDB 
    });// Add the route
// }

// module.exports = { initialize };
}
