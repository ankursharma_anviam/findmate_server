'use strict'
let headerValidator = require('../../middleware/validator');
let getAPI = require('./Get');

module.exports = [
    {
        method: 'Get',
        path: '/currentCoinBalance',
        handler: getAPI.handler,
        config: {
            description: 'This API will be used',
            tags: ['api', 'plans'],
            auth: 'userJWT',
            validate: {
                headers: headerValidator.headerAuthValidator,
                query:getAPI.validator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            }
        }
    }
];