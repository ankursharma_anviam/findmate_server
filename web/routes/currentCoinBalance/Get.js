'use strict'
var Joi = require('joi');
const logger = require('winston');
var jsonwebtoken = require('jsonwebtoken');
var ObjectID = require('mongodb').ObjectID
const local = require('../../../locales');
const coinWalletCollection = require('../../../models/coinWallet');

let handler = (req, res) => {

    let _id = req.auth.credentials._id;
    coinWalletCollection.SelectOne({ _id: ObjectID(_id) }, (err, result) => {
        if (err) return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);

        if (result == null) {
            result = {
                "_id": _id,
                "coins": {
                    "Coin": 0
                }
            };
            return res({ message: req.i18n.__('GetCurrentCoinBalance')['200'], data: result }).code(200);
        } else {
            return res({ message: req.i18n.__('GetCurrentCoinBalance')['200'], data: result }).code(200);
        }



    });
}

let response = {
    status: {
        200: { message: Joi.any().default(local['GetCurrentCoinBalance']['200']), data: Joi.any() },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) },
    }
}
module.exports = { handler, response }