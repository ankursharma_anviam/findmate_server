'use strict'
let headerValidator = require('../../middleware/validator');
let searchFriend= require('./Get');
module.exports = [
    {
        method: 'GET',
        path: '/LiveUsersByPreferences',
        handler: searchFriend.APIHandler,
        config: {
            description: 'This API will search for users on the app by using the username / findmateid as the input variable',
            tags: ['api', 'searchFriend'],
            auth: 'userJWT',
            validate: {
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            // response: postUnMatchAPI.response
        }
    }
];