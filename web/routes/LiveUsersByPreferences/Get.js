'use strict'
const Joi = require("joi");
const async = require("async");
const logger = require('winston');
const moment = require("moment");
const cronJobTask = require('../../../cronJobTask');
const userListType = require('../../../models/userListType');
const userListCollection = require('../../../models/userList');
const recentVisitorsCollection = require('../../../models/recentVisitors');
const redisClient = require('../../../models/redisDB').client;
var ObjectID = require('mongodb').ObjectID


/**
 * @method GET searchResult
 * @description This API use to get searchResult.
 
 * @param {*} req 
 * @param {*} res 
 
 * @property {*} authorization in header
 * @property {*} lang in header

 */
let APIHandler = (req, res) => {
    let _id = req.auth.credentials._id;
    let favoritePreferences = [], dataToSend = [], matcheResult = [];
    let gender = "Male,Female",contactNumber = "123", latitude,longitude,fbId="-1111111", must_not = [];
    let must = [],should = []; 

    try {

        async.series([
            function (callback) {
                if (req.user) {
                    favoritePreferences = req.user.searchPreferences;
                    latitude = req.user.location.latitude || 77;
                    longitude = req.user.location.longitude || 13;
                    contactNumber = req.user.contactNumber || req.user.fbId;
                    fbId = req.user.fbId || "not have a fbId";                
                    
                } else {
                    return res({ message: req.i18n.__('genericErrMsg')['401'] }).code(401);
                }
                return callback(null, true);
            },
            function (callback) {
                if (favoritePreferences) {
                    for (let index = 0; index < favoritePreferences.length; index++) {
                        if (favoritePreferences[index].pref_id == "57231014e8408f292d8b4567") {
                            gender = favoritePreferences[index].selectedValue.toString();
                        }else if (favoritePreferences[index].pref_id == "55d48f9bf6523cc552c51c52") {
                            distanceMin = (favoritePreferences[index].selectedValue[0] || 0) + "km";
                            distanceMax = (favoritePreferences[index].selectedValue[1] || 100) + "km";
                        }else if (favoritePreferences[index].pref_id == "5c5af0df2f705d135ad72f81") {
                           should.push({"match": {"searchPrefObj.5c5af0df2f705d135ad72f81":favoritePreferences[index]["selectedValue"].join()}})
                        }
                    }
                }
                return callback(null, 1);
            },
           
            function (callback) {
                must_not.push({ "match": { "contactNumber": contactNumber || "12345" } });
                must_not.push({ "match": { "profileStatus": 1 } });
                must_not.push({ "match": { "deleteStatus": 1 } });
                // must_not.push({ "match": { "fbId": fbId } });
                
                must.push({ "match": { "onlineStatus": 1 } });
                if(gender!="both") must.push({ "match": { "gender": gender } });

                return callback(null, true);
            },
            function (callback) {
                userListType.findLiveMatch({
                must:must,
                must_not: must_not,
                should:should
                }, (err, result) => {
                    logger.error("matcheResult ", err);
                    matcheResult = (result.hits) ? result.hits.hits : [];
                    // logger.silly("matcheResult total ", JSON.stringify(matcheResult))
                    logger.silly("matcheResult total ", matcheResult.length)
                    callback(err, result);
                });
            },
            function (callback) {
                for (let index = 0; index < matcheResult.length; index++) {
                    
                    dataToSend.push({
                        "opponentId": matcheResult[index]["_id"],
                        "firstName": matcheResult[index]["_source"]["firstName"],
                        "mobileNumber": matcheResult[index]["_source"]["contactNumber"],
                        "emailId": matcheResult[index]["_source"]["email"],
                        "height": matcheResult[index]["_source"]["height"],
                        "gender": matcheResult[index]["_source"]["gender"],
                        "dateOfBirth": matcheResult[index]["_source"]["dob"],
                        "onlineStatus": matcheResult[index]["_source"]["onlineStatus"],
                        "about": matcheResult[index]["_source"]["about"],
                        "profilePic": matcheResult[index]["_source"]["profilePic"] || "https://res.cloudinary.com/demo/image/upload/ar_4:3,c_fill/c_scale,w_auto,dpr_auto/sample.jpg",
                        "profileVideo": matcheResult[index]["_source"]["profileVideo"],
                        "otherImages": (Array.isArray(matcheResult[index]["_source"]["otherImages"])) ? matcheResult[index]["_source"]["otherImages"] : [],
                        "firebaseTopic": matcheResult[index]["_source"]["firebaseTopic"],
                        "instaGramProfileId": matcheResult[index]["_source"]["instaGramProfileId"] || "",
                        "instaGramToken": matcheResult[index]["_source"]["instaGramToken"] || "",
                        "address":( matcheResult[index]["_source"]["address"] )?matcheResult[index]["_source"]["address"]:{"country":"india","countryCode":"IN","city":"Bangalore"}
                 })
                }
                if(dataToSend[0] && dataToSend[0]["opponentId"]){

                    let dataToInsert = {
                        "userId" : ObjectID(_id), 
                        "targetUserId" : ObjectID(dataToSend[0]["opponentId"]), 
                        "createdTimestamp" : new Date().getTime(), 
                        "creationDate" : new Date()
    
                    };
                    recentVisitorsCollection.Insert(dataToInsert,(e,r)=>{if(e)logger.error("ASDASD : ",e)})
                    userListCollection.UpdateByIdWithAddToSet({_id : ObjectID(dataToSend[0]["opponentId"])},{recentVisitors: ObjectID(_id)},(e,r)=>{if(e)logger.error("SDFSDFSDFSDF : ",e)})
                }
                callback(null, true);
            }
        ], (err, result) => {

          
            return res({
                message: "success",
                data: dataToSend,
              }).code(200);
        })
    } catch (error) {
        logger.error("error : ", error);
        return res({ message: "success", data: [] }).code(200);
    }
};


module.exports = { APIHandler }