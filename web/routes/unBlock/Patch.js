'use strict'
const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../locales');
const userListType = require('../../../models/userListType');
const userListCollection = require('../../../models/userList');
const userBlocksCollection = require('../../../models/userBlocks');
const mqttClient = require("../../../library/mqtt");

let validator = Joi.object({
    targetUserId: Joi.string().min(24).max(24).required().description("targetUserId").error(new Error('targetUserId is missing'))
}).required();

/**
 * @method PATCH unblock
 * @description This API use to block a user.
 
 * @param {*} req 
 * @param {*} res 
 
 * @property {*} authorization in header
 * @property {*} lang in header
 * @property {*} lang targetUserId
 
 * @returns  400 : Value in any of the  mandatory field's is missing.
 * @returns  500 : An unknown error has occurred.
 */

let APIHandler = (req, res) => {
    try {
        var _id = req.auth.credentials._id;
        var targetUserId = req.payload.targetUserId;

        if (_id === targetUserId) {
            return res({ message: req.i18n.__('PatchUnBlock')['422'] }).code(422);
        }
        unBlockUser()
            .then(function (value) {
                return res({ message: req.i18n.__('PatchUnBlock')['200'] }).code(200);
            }).catch(function (err) {
                console.log("error : ", err);
                return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
            });
    } catch (error) {
        logger.error("in unblockUser : ", error);
        return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
    }

    function unBlockUser() {
        return new Promise(function (resolve, reject) {
            mqttClient.publish(targetUserId, JSON.stringify({ "messageType": "unBlock", userId: _id }), { qos: 0 }, (e, r) => { if (e) logger.error("OAKSOAKS  : ", e) })
            /**
             * remove from elastic search
             */
            userListType.UpdateWithPull(targetUserId, "blockedBy", _id, (err, result) => {
                if (err) return reject(new Error('Ooops, something broke!'));
            });
            userListType.UpdateWithPull(_id, "myBlock", targetUserId, (err, result) => {
                if (err) return reject(new Error('Ooops, something broke!'));
            });
            /**
            * remove from mongoDB
            */
            userListCollection.UpdateByIdWithPull({ _id: targetUserId }, { "blockedBy": ObjectID(_id) }, (err, result) => {
                if (err) return reject(new Error('Ooops, something broke!'));
            });
            userListCollection.UpdateByIdWithPull({ _id: _id }, { "myBlock": ObjectID(targetUserId) }, (err, result) => {
                if (err) return reject(new Error('Ooops, something broke!'));
            });

            let condition = {
                userId: ObjectID(_id),
                targetUserId: ObjectID(targetUserId)
            };
            userBlocksCollection.Delete(condition, () => { });

            return resolve("---");
        });
    }
};

let response = {
    status: {
        200: { message: Joi.any().default(local['PatchUnBlock']['200']) },
        422: { message: Joi.any().default(local['PatchUnBlock']['422']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) },
    }
}
module.exports = { APIHandler, validator, response }