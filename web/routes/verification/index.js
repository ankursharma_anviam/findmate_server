'use strict'
var patchEmailIdExistsVerificationAPI = require('./PatchEmailIdExists');
let patchPhoneNumberExistsVerificationAPI = require("./PatchPhoneNumberExists");
var headerValidator = require('../../middleware/validator');

module.exports = [
    {
        method: 'PATCH',
        path: '/emailIdExistsVerification',
        handler: patchEmailIdExistsVerificationAPI.APIHandler,
        config: {
            description: 'This API confirms if the email id entered is already registered or not ',
            tags: ['api', 'verification'],
            auth: false,
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: patchEmailIdExistsVerificationAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: patchEmailIdExistsVerificationAPI.APIResponse
        }
    }, {
        method: 'PATCH',
        path: '/phoneNumberExistsVerification',
        handler: patchPhoneNumberExistsVerificationAPI.APIHandler,
        config: {
            description: 'This API confirms if the phone number entered is already registered or not.',
            tags: ['api', 'verification'],
            auth: false,
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: patchPhoneNumberExistsVerificationAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: patchPhoneNumberExistsVerificationAPI.APIResponse
        }
    }
];