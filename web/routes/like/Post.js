'use strict'
const Joi = require("joi");
const async = require('async');
const moment = require("moment");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;
const Timestamp = require('mongodb').Timestamp;

const local = require('../../../locales');
const fcmPush = require("../../../library/fcm");
const mqttClient = require("../../../library/mqtt");
const userListType = require('../../../models/userListType');
const messagesCollection = require('../../../models/messages');
const userListCollection = require('../../../models/userList');
const chatLikesCollection = require('../../../models/chatList');
const userLikesCollection = require('../../../models/userLikes');
const userMatchCollection = require('../../../models/userMatch');
const rabbitMqClass = require('../../../models/rabbitMq/rabbitMq');
const userUnlikesCollection = require('../../../models/userUnlikes');
const userBoostCollection = require('../../../models/userBoost');
const cronJobTask = require('../../../cronJobTask');
const redisClient = require('../../../models/redisDB').client;
const rabbitMqBulkQueryInES = require('../../../models/rabbitMq/setBulkQueryInES');

let validator = Joi.object({
    targetUserId: Joi.string().required().description("targetUserId").error(new Error('targetUserId is missing'))
}).required();
/**
 * @method POST like
 * @description This API use to like a user.
 * @param {*} req 
 * @param {*} res 
 * @property {*} authorization in header
 * @property {*} lang in header
 * @property {*} targetUserId targetUserId,Eg. 5a14013b7b334c19349aec7b
 */
let APIHandler = (req, res) => {

    // const TimeToLiveInHours = cronJobTask.likeRefreshInMillisecond;
    const likeRefreshInMillisecond = cronJobTask.likeRefreshInMillisecond;
    let nextLikeTime = 0;
    let _id = req.auth.credentials._id;
    let targetUserId = req.payload.targetUserId;
    let isTargetLikedMe = false;
    let isTargetBoostProfile = false;
    let targetBoostId = "";
    let targetBoostDetails = {};
    let firstLikedByName = "";
    let firstLikedByPhoto = "";
    let myName = req.user.firstName;
    let bulkArray = [];
    let remainsLikesInString = "0"


    if (req.user.matchedWith && req.user.matchedWith.map(id => id.toString()).includes(targetUserId)) {
        return res({ message: req.i18n.__('genericErrMsg')['405'] }).code(405);
    } else if (req.user.myLikes && req.user.myLikes.map(id => id.toString()).includes(targetUserId)) {
        return res({ message: req.i18n.__('genericErrMsg')['405'] }).code(405);
    } else if (req.user.mySupperLike && req.user.mySupperLike.map(id => id.toString()).includes(targetUserId)) {
        return res({ message: req.i18n.__('genericErrMsg')['405'] }).code(405);
    }

    userListType.UpdateWithPull(_id, "myunlikes", targetUserId, (err, result) => { });
    userListCollection.UpdateByIdWithPull({ _id: _id }, { "myunlikes": ObjectID(targetUserId) }, (e, r) => { })
    try {

        req.user.lastTimestamp.like = (req.user.lastTimestamp && req.user.lastTimestamp.like) ? req.user.lastTimestamp.like : new Date().getTime();
    } catch (error) {
        req.user["lastTimestamp"]["like"] = new Date().getTime();
    }
    var a = moment();
    var b = moment(req.user.lastTimestamp.like);

    // if (req.user.subscription[0]["likeCount"] != "unlimited" &&
    //     req.user.subscription &&
    //     req.user.lastTimestamp &&
    //     req.user.count.like >= req.user.subscription[0]["likeCount"] &&
    //     a.diff(b, "ms") < likeRefreshInMillisecond) {
    //     nextLikeTime = b.add(likeRefreshInMillisecond, 'ms').valueOf();
    //     return res({ message: req.i18n.__('genericErrMsg')['409'], nextLikeTime: nextLikeTime }).code(409);
    // }

    if (req.user.subscription[0]["likeCount"] != "unlimited" &&
        req.user.subscription &&
        req.user.count.like >= req.user.subscription[0]["likeCount"] &&
        a.diff(b, 'ms') >= likeRefreshInMillisecond) {

        req.user.count.like = 0;
        remainsLikesInString = parseInt(req.user.subscription[0]["likeCount"]) - 1;
        userListCollection.UpdateById(_id, { "count.like": 0 }, (err, result) => {
            if (err) return reject(new Error('Ooops, something broke!', err));
        });
    } else {
        remainsLikesInString = (req.user.subscription[0]["likeCount"] == "unlimited") ? "unlimited" : parseInt(req.user.subscription[0]["likeCount"]) - req.user.count.like - 1;
    }

    if (remainsLikesInString == "0" || parseInt(remainsLikesInString) < 0) {
        //for send refresh notification on like
        redisClient.hmset(cronJobTask.likeNotificationAt + _id, { _id: _id }, (e, r) => {
            if (e) logger.error("at set hmset in redis with " + cronJobTask.likeNotificationAt + _id, e);
            redisClient.expire(cronJobTask.likeNotificationAt + _id, (likeRefreshInMillisecond / 1000), (e, r) => {
                if (e) logger.error("at set hmset  expire in redis with " + cronJobTask.likeNotificationAt + _id, e);
            });
        });
    }
    b = moment(req.user.lastTimestamp.like);
    nextLikeTime = (remainsLikesInString == "0" || parseInt(remainsLikesInString) < 0)
        ? (b.add(likeRefreshInMillisecond, 'ms').valueOf())
        : 0;

    let condition = { "_id": ObjectID(_id), "myLikes": ObjectID(targetUserId) };
    userListCollection.Select(condition, (err, result) => {
        if (err) {
            logger.error("Error : ", err);
            return res({ message: req.i18n.__('PostLike')['412'] }).code(412);
        } else if (result.length) {
            logger.silly("already Liked " + targetUserId);
            userListCollection.UpdateByIdWithPush({ _id: _id }, { myLikes: ObjectID(targetUserId) }, (err, result) => { });
            userListCollection.UpdateByIdWithPush({ _id: targetUserId }, { likedBy: ObjectID(_id) }, (err, result) => { });
            return res({ message: req.i18n.__('PostLike')['200'] }).code(200);
        } else {
            targetUserExists()
                .then(function (value) {
                    return isTargetLikedMeFun();
                }).then(function (value) {
                    return processLike();
                }).then(function (value) {
                    rabbitMqBulkQueryInES.InsertQueue(rabbitMqClass.getChannelForBulkQuerys(), rabbitMqClass.queueForBulkQuerys,
                        bulkArray, (err, result) => { if (err) logger.error(err) });
                    if (isTargetLikedMe) {
                        return res({ message: req.i18n.__('PostLike')['201'], nextLikeTime: nextLikeTime, data: value, remainsLikesInString: remainsLikesInString }).code(201);
                    } else {
                        return res({ message: req.i18n.__('PostLike')['200'], nextLikeTime: nextLikeTime, remainsLikesInString: remainsLikesInString }).code(200);
                    }
                }).catch(function (err) {
                    logger.silly('Caught an error!', err);
                    return res({ message: req.i18n.__('PostLike')['412'] }).code(412);
                });
        }
    });

    function targetUserExists() {
        return new Promise(function (resolve, reject) {
            userListCollection.SelectById({ _id: targetUserId }, { _id: 1, firstName: 1, profilePic: 1, currentLoggedInDevices: 1 }, (err, result) => {
                if (err) return reject(new Error('Ooops, something broke!'));

                if (result) {
                    firstLikedByName = result.firstName;
                    firstLikedByPhoto = result.profilePic || "";
                    return resolve(result);
                } else {
                    userListType.Delete("userList", { _id: targetUserId }, () => { });
                    return reject(new Error('Ooops, something broke! userNot found' + targetUserId));
                }
            });
        });
    }
    function isTargetLikedMeFun() {
        return new Promise(function (resolve, reject) {

            isTargetLikedMe = (req.user.likedBy.map(id => id.toString()).includes(targetUserId))
            isTargetLikedMe = (isTargetLikedMe) ? isTargetLikedMe : req.user.supperLikeBy.map(id => id.toString()).includes(targetUserId);

            userListCollection.SelectOne({ _id: ObjectID(targetUserId) }, (err, result) => {
                if (result && result.boost && result.boost.expire >= new Date().getTime()) {
                    isTargetBoostProfile = true;
                    targetBoostId = result.boost._id;
                    targetBoostDetails = result.boost;
                }
                return resolve(true);
            });
        });
    }
    function processLike() {
        return new Promise(function (resolve, reject) {

            redisClient.hmset(_id, { [targetUserId]: targetUserId }, (e, r) => { if (e) logger.error("insert in redis ", e); });
            redisClient.expire(_id, 10, (e, r) => { if (e) logger.error("insert  expire in redis ", e); });

            condition = {
                "$or": [
                    { "userId": ObjectID(targetUserId), "targetUserId": ObjectID(_id) },
                    { "userId": ObjectID(_id), "targetUserId": ObjectID(targetUserId) }]
            };
            userUnlikesCollection.Delete(condition, (err, result) => {
                if (err) return reject(new Error('Ooops, something broke!', err));
            });
            userListCollection.UpdateById(_id, { "count.like": req.user.count.like + 1 || 1, "lastTimestamp.like": new Date().getTime() }, (err, result) => {
                if (err) return reject(new Error('Ooops, something broke!', err));
            });
            bulkArray.push({ "update": { "_id": `${_id}`, "_type": "userList", "_index": "azar", "retry_on_conflict": 3 } },
                { "script": { "source": "ctx._source.disLikedUSers.remove(ctx._source.disLikedUSers.indexOf('" + targetUserId + "'))", "lang": "painless" } }
            );
            if (isTargetLikedMe) {

                let chatId = new ObjectID();
                let msg_id = new ObjectID();

                /**Update In Elastic Search */
                bulkArray.push({ "update": { "_id": `${_id}`, "_type": "userList", "_index": "azar", "retry_on_conflict": 3 } },
                    { "script": { "source": "ctx._source.likedBy.remove(ctx._source.likedBy.indexOf('" + targetUserId + "'))", "lang": "painless" } },

                    { "update": { "_id": `${_id}`, "_type": "userList", "_index": "azar", "retry_on_conflict": 3 } },
                    { "script": { "source": "ctx._source.supperLikeBy.remove(ctx._source.supperLikeBy.indexOf('" + targetUserId + "'))", "lang": "painless" } },

                    { "update": { "_id": `${_id}`, "_type": "userList", "_index": "azar", "retry_on_conflict": 3 } },
                    { "script": { "source": "ctx._source.matchedWith.add('" + targetUserId + "')", "lang": "painless" } },

                    { "update": { "_id": `${targetUserId}`, "_type": "userList", "_index": "azar", "retry_on_conflict": 3 } },
                    { "script": { "source": "ctx._source.myLikes.remove(ctx._source.myLikes.indexOf('" + _id + "'))", "lang": "painless" } },

                    { "update": { "_id": `${targetUserId}`, "_type": "userList", "_index": "azar", "retry_on_conflict": 3 } },
                    { "script": { "source": "ctx._source.mySupperLike.remove(ctx._source.mySupperLike.indexOf('" + _id + "'))", "lang": "painless" } },

                    { "update": { "_id": `${targetUserId}`, "_type": "userList", "_index": "azar", "retry_on_conflict": 3 } },
                    { "script": { "source": "ctx._source.matchedWith.add('" + _id + "')", "lang": "painless" } });

                /**Update In MongoDB */
                userListCollection.UpdateByIdWithPull({ _id: _id }, { "supperLikeBy": ObjectID(targetUserId) }, (err, result) => {
                    if (err) return reject(new Error('Ooops, something broke!'));
                });
                userListCollection.UpdateByIdWithPull({ _id: targetUserId }, { "mySupperLike": ObjectID(_id) }, (err, result) => {
                    if (err) return reject(new Error('Ooops, something broke!'));
                });
                userListCollection.UpdateByIdWithPull({ _id: _id }, { "likedBy": ObjectID(targetUserId) }, (err, result) => {
                    if (err) return reject(new Error('Ooops, something broke!'));
                });
                userListCollection.UpdateByIdWithPull({ _id: targetUserId }, { "myLikes": ObjectID(_id) }, (err, result) => {
                    if (err) return reject(new Error('Ooops, something broke!'));
                });
                let condition = { "userId": ObjectID(targetUserId), "targetUserId": ObjectID(_id), isMatch: { $exists: false } };
                userLikesCollection.Update(condition, { "isMatch": 1 }, (err, result) => {
                    if (err) return reject(new Error('Ooops, something broke!'));
                });
                userListCollection.UpdateByIdWithAddToSet({ _id: _id }, { "matchedWith": ObjectID(targetUserId) }, (err, result) => {
                    if (err) return reject(new Error('Ooops, something broke!'));
                });
                userListCollection.UpdateByIdWithAddToSet({ _id: targetUserId }, { "matchedWith": ObjectID(_id) }, (err, result) => {
                    if (err) return reject(new Error('Ooops, something broke!'));
                });
                userListCollection.SelectOne({ _id: ObjectID(targetUserId) }, (err, result) => {
                    let deviceType = "1";
                    if (result && result._id) {
                        deviceType = result.deviceType || "1";
                    }
                    let payloadUser1 = {
                        notification: {
                            body: "You got matched with " + myName + ". Please incroduce your self."
                            , title: "Match"
                        },
                        data: { type: "1", target_id: _id, deviceType: deviceType }
                    }
                    fcmPush.sendPushToTopic("/topics/" + targetUserId, payloadUser1, (e, r) => { });

                });
                let payloadUser2 = {
                    notification: {
                        body: "You got matched with " + firstLikedByName + ". Please incroduce your self."
                        , title: "Match"
                    },
                    data: { type: "1", target_id: targetUserId, deviceType: req.user.deviceType } // values must be string
                }
                fcmPush.sendPushToTopic("/topics/" + _id, payloadUser2, (e, r) => { });
                if (isTargetBoostProfile) {
                    userBoostCollection.UpdateByIdWithPush({ _id: targetBoostId },
                        { "match": { userId: ObjectID(_id), timestamp: new Date().getTime() } },
                        (e, r) => { if (e) logger.error(" userBoostCollection.UpdateByIdWithPush : ", e); });

                    userBoostCollection.UpdateWithIncrease({ _id: targetBoostId }, { "count.match": 1 },
                        (e, r) => { if (e) logger.error(" userBoostCollection.UpdateWithIncrease : ", e); });

                    userListCollection.UpdateWithIncrease({ _id: ObjectID(targetUserId) }, { "boost.match": 1 },
                        (e, r) => { if (e) logger.error(" userListCollection.UpdateWithIncrease : ", e); });

                    targetBoostDetails["match"] = targetBoostDetails["match"] + 1
                    mqttClient.publish(targetUserId, JSON.stringify({ "messageType": "boost", boost: targetBoostDetails }), { qos: 0 }, (e, r) => { if (e) logger.error("/boost/match", e) })
                }
                req.user.supperLikeByHistory = (req.user.supperLikeByHistory) ? req.user.supperLikeByHistory.map(id => id.toString()) : [];
                userListCollection.SelectById({ _id: _id }, { _id: 1, firstName: 1, profilePic: 1 }, (err, result) => {
                    if (result._id) {
                        let dataToSend = {
                            "matchDate": new Date().getTime(),
                            "firstLikedBy": targetUserId,
                            "firstLikedByName": firstLikedByName,
                            "firstLikedByPhoto": firstLikedByPhoto,
                            "isFirstSupperLiked": (req.user && req.user.supperLikeByHistory && req.user.supperLikeByHistory.includes(targetUserId)) ? 1 : 0,
                            "secondLikedBy": _id,
                            "SecondLikedByName": result.firstName,
                            "secondLikedByPhoto": result.profilePic,
                            "isSecondSupperLiked": 0,
                        };
                        let dataToInsert = {
                            "initiatedBy": ObjectID(targetUserId),
                            "opponentId": ObjectID(_id),
                            "createdTimestamp": new Date().getTime(),
                            "chatId": chatId,
                            "creationTs": new Timestamp(),
                            "creationDate": new Date()
                        }
                        if (targetBoostId != "") dataToInsert["boostId"] = targetBoostId;
                        if (targetBoostId != "") dataToInsert["boostByUser"] = targetUserId;
                        userMatchCollection.Insert(dataToInsert, (err, result) => {
                            if (err) return reject(new Error('Ooops, something broke!'));
                        });

                        let condition = {
                            ["members." + _id]: { "$exists": true },
                            ["members." + targetUserId]: { "$exists": true },
                            "chatType": "NormalChat",
                            "$or": [{ "isUnMatched": { "$ne": 1 } }, { "isFriend": 1 }]
                        };
                        chatLikesCollection.SelectOne(condition, (err, result) => {
                            if (err) return reject(new Error('Ooops, something broke!'));

                            if (result) {
                                chatLikesCollection.UpdateById(result._id.toString(), { "isMatchedUser": 1, "isUnMatched": 0 }, (err, result) => {
                                    if (err) return reject(new Error('Ooops, something broke!'));
                                })
                                dataToSend["chatId"] = result._id
                                dataToSend["messageType"] = "match/user";
                                mqttClient.publish(_id, JSON.stringify(dataToSend), { qos: 1 }, (e, r) => { });
                                mqttClient.publish(targetUserId, JSON.stringify(dataToSend), { qos: 1 }, (e, r) => { });
                            } else {
                                dataToSend["chatId"] = chatId;
                                dataToSend["messageType"] = "match/user";
                                mqttClient.publish(_id, JSON.stringify(dataToSend), { qos: 1 }, (e, r) => { });
                                mqttClient.publish(targetUserId, JSON.stringify(dataToSend), { qos: 1 }, (e, r) => { });


                                let dataToInsert = {
                                    "_id": chatId,
                                    "message": [msg_id],
                                    "members": {
                                        [_id]: {
                                            "status": "NormalMember"
                                        },
                                        [targetUserId]: {
                                            "status": "NormalMember"
                                        }
                                    },
                                    "initiatedBy": ObjectID(_id),
                                    "createdAt": new Date().getTime(),
                                    "chatType": "NormalChat",
                                    "isMatchedUser": 1,
                                    "secretId": "",
                                    creationTs: new Timestamp(),
                                    creationDate: new Date()
                                };
                                if (targetBoostId != "") dataToInsert["boostId"] = targetBoostId;
                                if (targetBoostId != "") dataToInsert["boostByUser"] = targetUserId;
                                chatLikesCollection.Insert(dataToInsert, (e, r) => { });
                                let newMessage = {
                                    "_id": msg_id,
                                    "messageId": "" + new Date().getTime(),
                                    "secretId": "",
                                    "dTime": -1,
                                    "senderId": ObjectID(_id),
                                    "receiverId": ObjectID(targetUserId),
                                    "payload": "3embed test",
                                    "messageType": "0",
                                    "timestamp": new Date().getTime(),
                                    "expDtime": 0,
                                    "chatId": chatId,
                                    "userImage": firstLikedByPhoto,
                                    "toDocId": "test message",
                                    "name": firstLikedByName,
                                    "dataSize": null,
                                    "thumbnail": null,
                                    "mimeType": null,
                                    "extension": null,
                                    "fileName": null,
                                    "members": {
                                        [_id]: {

                                        },
                                        [targetUserId]: {
                                            "status": 0
                                        }
                                    },
                                    creationTs: new Timestamp(),
                                    creationDate: new Date(),
                                }
                                messagesCollection.Insert(newMessage, (e, r) => { })
                            }
                        })
                        return resolve(dataToSend);
                    } else {
                        return reject(new Error('Ooops, something broke!'));
                    }
                });
            } else {
                userListCollection.SelectOne({ _id: ObjectID(targetUserId) }, (err, result) => {
                    if (err) logger.error(err)

                    if (result && result._id) {
                        let deviceType = result.deviceType || "1";

                        let payload = {
                            notification: { body: myName + " is interested in you. ", title: "findmate" },
                            data: { type: '2', target_id: _id, deviceType: deviceType } // values must be string  data: { type: "1", target_id: _id }
                        }
                        fcmPush.sendPushToTopic("/topics/" + targetUserId, payload, (e, r) => { if (err) logger.error(err) });
                    }
                })
                rabbitMqBulkQueryInES.InsertQueue(rabbitMqClass.getChannelForBulkQuerys(), rabbitMqClass.queueForBulkQuerys,
                    [
                        { "update": { "_id": `${targetUserId}`, "_type": "userList", "_index": "azar", "retry_on_conflict": 3 } },
                        { "script": { "source": "ctx._source.likedBy.add('" + _id + "')", "lang": "painless" } },
                        { "update": { "_id": `${_id}`, "_type": "userList", "_index": "azar", "retry_on_conflict": 3 } },
                        { "script": { "source": "ctx._source.myLikes.add('" + targetUserId + "')", "lang": "painless" } },
                    ], (err, result) => { if (err) logger.error(err) });
                /**Update in MongoDB */
                userListCollection.UpdateByIdWithAddToSet({ _id: targetUserId }, { "likedBy": ObjectID(_id) }, (err, result) => {
                    if (err) {
                        logger.error(err);
                        return reject(new Error('Ooops, something broke!'));
                    }
                });
                /**Update in MongoDB */
                userListCollection.UpdateByIdWithAddToSet({ _id: _id }, { "myLikes": ObjectID(targetUserId) }, (err, result) => {
                    if (err) {
                        logger.error(err);
                        return reject(new Error('Ooops, something broke!'));
                    }
                });
                if (isTargetBoostProfile) {
                    userBoostCollection.UpdateByIdWithPush({ _id: targetBoostId },
                        { likes: { userId: ObjectID(_id), timestamp: new Date().getTime() } },
                        (e, r) => { if (e) logger.error(" userBoostCollection.UpdateByIdWithPush : ", e); });

                    userBoostCollection.UpdateWithIncrease({ _id: targetBoostId }, { "count.likes": 1 },
                        (e, r) => { if (e) logger.error(" userBoostCollection.UpdateWithIncrease : ", e); });

                    userListCollection.UpdateWithIncrease({ _id: ObjectID(targetUserId) }, { "boost.likes": 1 },
                        (e, r) => { if (e) logger.error(" userListCollection.UpdateWithIncrease : ", e); });

                    targetBoostDetails["likes"] = targetBoostDetails["likes"] + 1
                    mqttClient.publish(targetUserId, JSON.stringify({ "messageType": "boost", boost: targetBoostDetails }), { qos: 0 }, (e, r) => { if (e) logger.error("/boost/like", e) })
                }
                let dataToInsert = {
                    userId: ObjectID(_id),
                    targetUserId: ObjectID(targetUserId),
                    timestamp: new Date().getTime(),
                    creationTs: new Timestamp(),
                    creationDate: new Date(),
                }
                if (targetBoostId != "") dataToInsert["boostId"] = targetBoostId
                userLikesCollection.Insert(dataToInsert, (err, result) => {
                    if (err) {
                        logger.error(err);
                        return reject(new Error('Ooops, something broke!'));
                    }
                    return resolve("---");
                });
            }
        });
    }
};

let response = {
    status: {
        201: { message: Joi.any().default(local['PostLike']['201']), data: Joi.any(), remainsLikesInString: Joi.any(), nextLikeTime: Joi.any() },
        200: { message: Joi.any().default(local['PostLike']['200']), remainsLikesInString: Joi.any(), nextLikeTime: Joi.any() },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        402: { message: Joi.any().default(local['genericErrMsg']['402']) },
        409: { message: Joi.any().default(local['genericErrMsg']['409']), nextLikeTime: Joi.any() },
        412: { message: Joi.any().default(local['PostLike']['412']) },
    }
}

module.exports = { APIHandler, validator, response }