'use strict'
let headerValidator = require('../../middleware/validator');
let postLikeAPI = require('./Post');
let getLikeAPI = require('./Get');
let GetMyAPI = require('./GetMy');

module.exports = [
    {
        method: 'POST',
        path: '/like',
        handler: postLikeAPI.APIHandler,
        config: {
            description: 'This API is used to like user',
            tags: ['api', 'like'],
            auth: 'userJWT',
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: postLikeAPI.validator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: postLikeAPI.response
        }
    },
    {
        method: 'GET',
        path: '/likesBy',
        handler: getLikeAPI.handler,
        config: {
            description: 'This API is used to like user',
            tags: ['api', 'like'],
            auth: 'userJWT',
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: getLikeAPI.queryValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: getLikeAPI.response
        }
    },
    {
        method: 'GET',
        path: '/myLikes',
        handler: GetMyAPI.handler,
        config: {
            description: 'This API is used to like user',
            tags: ['api', 'like'],
            auth: 'userJWT',
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: GetMyAPI.queryValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetMyAPI.response
        }
    },
];