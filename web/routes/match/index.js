'use strict'
let headerValidator = require('../../middleware/validator');
let getMatchAPI = require('./Get');

module.exports = [
    {
        method: 'GET',
        path: '/match',
        handler: getMatchAPI.handler,
        config: {
            description: 'This API will be used to getmatch List',
            tags: ['api', 'match'],
            auth: 'userJWT',
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: getMatchAPI.queryValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: getMatchAPI.response
        }
    },
];