'use strict'
const logger = require('winston');
const Promise = require('promise');
const userMatchCollection = require('../../../models/userMatch');
const chatListCollection = require('../../../models/chatList');
const ObjectID = require('mongodb').ObjectID;
const Joi = require("joi");

const local = require('../../../locales');

/**
 * @method GET match
 * @description This API use to get matchList.
 
 * @param {*} req 
 * @param {*} res 
 
 * @property {*} authorization in header
 * @property {*} lang in header
 
 * @returns  200 : Matchlist sent successfully.
 * @returns  400 : Value in any of the  mandatory field's is missing.
 * @returns  412 : Matchlist is empty. 
 * @returns  500 : An unknown error has occurred.
 */
let handler = (req, res) => {

    let _id = req.auth.credentials._id.toString();
    let limit = parseInt(req.query.limit) || 20;
    let skip = parseInt(req.query.offset) || 0;
    let matchedUsers = [], withoutMatchUsers = [];
    if (req.user.supperLikeByHistory) req.user.supperLikeByHistory.map(id => id.toString());

    getMatchdata()
        .then((value) => { return getWithoutMatchdata(); })
        .then((value) => {
            return res({ message: req.i18n.__('GetMatch')['200'], data: { matchedUsers: matchedUsers, withoutMatchUsers: withoutMatchUsers } }).code(200);
        }).catch(function (err) {
            logger.error(err)
            return res({ message: req.i18n.__('GetMatch')['412'] }).code(412);
        })

    function getMatchdata() {
        return new Promise(function (resolve, reject) {
            let condition = [
                {
                    "$match": {
                        "unMatchedBy": { "$exists": false },
                        "$or": [
                            { "initiatedBy": ObjectID(_id) },
                            { "opponentId": ObjectID(_id) }
                        ]
                    }
                },
                {
                    "$project": {
                        "userId": {
                            "$cond": {
                                if: { "$eq": ["$initiatedBy", ObjectID(_id)] }, then: "$initiatedBy",
                                else: "$opponentId"
                            }
                        },
                        "targetId": {
                            "$cond": {
                                if: { "$eq": ["$initiatedBy", ObjectID(_id)] }, then: "$opponentId",
                                else: "$initiatedBy"
                            }
                        },
                        "chatId": 1
                    }
                },
                { "$lookup": { "from": "chatList", "localField": "chatId", "foreignField": "_id", "as": "chatData" } },
                { "$unwind": "$chatData" },
                {
                    "$project": {
                        "userId": 1, "targetId": 1, "chatId": 1,
                        "isConvertationStart": { "$cond": { if: { "$eq": [{ "$size": "$chatData.message" }, 1] }, then: 0, else: 1 } }
                    }
                },
                { "$lookup": { "from": "userList", "localField": "targetId", "foreignField": "_id", "as": "targetUser" } },
                { "$unwind": "$targetUser" },
                {
                    "$project": {
                        "chatId": 1, "_id": 0,
                        "isConvertationStart": 1, "opponentId": "$targetUser._id", "firstName": "$targetUser.firstName",
                        "profilePic": "$targetUser.profilePic", "lastName": "$targetUser.lastName", "height": "$targetUser.height",
                        "gender": "$targetUser.gender", "profileVideo": "$targetUser.profileVideo", "dateOfBirth": "$targetUser.dob",
                        "onlineStatus": "$targetUser.onlineStatus"
                    }
                },
                { "$skip": skip }, { "$limit": limit }
            ];

            userMatchCollection.Aggregate(condition, (err, result) => {

                if (result.length) {
                    result.forEach(element => {
                        let birthdate = new Date(element["dateOfBirth"]);
                        let cur = new Date();
                        let diff = cur - birthdate; // This is the difference in milliseconds
                        let age = Math.floor(diff / 31557600000); // Divide by 1000*60*60*24*365.25
                        element["age"] = age;
                        element["isMatched"] = true;
                        element["isSupperLikedMe"] = (req.user && req.user.supperLikeByHistory &&
                            req.user.supperLikeByHistory.includes(element["opponentId"].toString())) ? 1 : 0;

                    });
                }
                matchedUsers = result;
                return resolve(true);
            })
        });
    }
    function getWithoutMatchdata() {
        return new Promise(function (resolve, reject) {
            let condition = [
                { "$match": { ["members." + _id]: { "$exists": true }, "isMatchedUser": 0 } },
                {
                    "$project": {
                        "opponentId": { "$cond": { if: ["$senderId", ObjectID(_id)], then: "$targetId", else: "$senderId" } },
                        "isConvertationStart": { "$cond": { if: { "$eq": [{ "$size": "$message" }, 1] }, then: 0, else: 1 } },
                        "isMatchedUser": 1,
                    }
                },
                { "$lookup": { "from": "userList", "localField": "opponentId", "foreignField": "_id", "as": "oppenetData" } },
                { "$unwind": "$oppenetData" },
                {
                    "$project": {
                        "chatId": 1, "opponentId": 1, "isConvertationStart": 1, "isMatchedUser": 1, "firstName": "$oppenetData.firstName",
                        "profilePic": "$oppenetData.profilePic", "lastName": "$oppenetData.lastName", "height": "$oppenetData.height",
                        "gender": "$oppenetData.gender", "profileVideo": "$oppenetData.profileVideo",
                        "dateOfBirth": "$oppenetData.dob", "onlineStatus": "$oppenetData.onlineStatus"
                    }
                }
            ];

            chatListCollection.Aggregate(condition, (err, result) => {

                if (result.length) {
                    result.forEach(element => {
                        let birthdate = new Date(element["dateOfBirth"]);
                        let cur = new Date();
                        let diff = cur - birthdate; // This is the difference in milliseconds
                        let age = Math.floor(diff / 31557600000); // Divide by 1000*60*60*24*365.25
                        element["age"] = age;
                        element["isMatched"] = false;
                        element["isSupperLikedMe"] = (req.user && req.user.supperLikeByHistory &&
                            req.user.supperLikeByHistory.includes(element["opponentId"])) ? 1 : 0;

                    });
                }
                withoutMatchUsers = result;
                return resolve(true);
            })
        });
    }
};

const queryValidator = Joi.object({
    offset: Joi.number().description("offset"),
    limit: Joi.number().description("limit"),
}).required();


let response = {
    status: {
        200: { message: Joi.any().default(local['GetMatch']['200']), data: Joi.any() },
        412: { message: Joi.any().default(local['GetMatch']['412']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
}

module.exports = { handler, response, queryValidator }