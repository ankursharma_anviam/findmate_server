'use strict'
const Joi = require("joi");
const async = require("async");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;
const local = require('../../../locales');
const GeoPoint = require('geopoint')
const userListCollection = require('../../../models/userListType');
var data2
/**
 * @method GET like
 * @description This API is used to get likedlist.
 * @param {*} req 
 * @param {*} res 
 * @property {*} authorization in header
 * @property {*} lang in header
 * @returns  200 : User found in database.
 * @returns  500 : An unknown error has occurred.
 */
let validator = Joi.object({
    findMateId: Joi.string().required().description("findMateId").error(new Error('findMateId is missing'))
}).required();

const handler = (req, reply) => {
    let _id = new ObjectID(req.auth.credentials._id);
    var dataToSend=[];
    
    const dbErrResponse = { message: req.i18n.__('genericErrMsg')['500'], code: 500 };
    let getFindMatchId = () => {
        return new Promise((resolve, reject) => {
            let condition = { "findMateId": req.payload.findMateId.toLowerCase() };

            userListCollection.Select1(condition, (err, result) => {
                if (err) {
                    logger.error("get profile by FindMatchId API : ", err)
                    return reject(dbErrResponse);
                }
                else {
                    console.log(result.hits.hits);
                    for (var i = 0; i < (result.hits.hits.length); i++) {
                        dataToSend.push({
                            _id:result.hits.hits[i]._id,
                            profilePic:result.hits.hits[i]._source["profilePic"],
                            firstName:result.hits.hits[i]._source["firstName"],
                            findMateId:result.hits.hits[i]._source["findMateId"]
                        })
                    }
                    console.log("======================>>>",dataToSend[0]
                )
                   // data2={"_id":result.hits.hits[0]._id};
                   // console.log(data2)
                    //console.log(data1.push(data2))

                  //  data1=result.hits.hits[0]._source;
                //    console.log(data1.push(data2))
                    return resolve(dataToSend[0]);

                }
            })
        });
    };

    getFindMatchId()
        .then((data) => { return reply({ message: req.i18n.__('genericErrMsg')['200'], data: dataToSend[0] }).code(200); })
        .catch((err) => {
            logger.err("get profile by FindMatchId API error : ", err);
            return reply({ message: err.message }).code(err.code);
        });
};




let response = {
    status: {
        200: { message: Joi.any().default(local['genericErrMsg']['200']), data: Joi.any() },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) },

    }
}

module.exports = { handler, response, validator }