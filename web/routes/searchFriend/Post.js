'use strict'
const Joi = require("joi");
const async = require("async");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;
const local = require('../../../locales');
const GeoPoint = require('geopoint')
const userListCollection = require('../../../models/userListType');
const userListMongoCollection = require('../../../models/userList');
var data2
/**
 * @method GET like
 * @description This API is used to get likedlist.
 * @param {*} req 
 * @param {*} res 
 * @property {*} authorization in header
 * @property {*} lang in header
 * @returns  200 : User found in database.
 * @returns  500 : An unknown error has occurred.
 */
let validator = Joi.object({
    findMateId: Joi.string().required().description("findMateId").error(new Error('findMateId is missing'))
}).required();

const handler = (req, reply) => {
    let _id = new ObjectID(req.auth.credentials._id);
    var dataToSend = [];

    const dbErrResponse = { message: req.i18n.__('genericErrMsg')['500'], code: 500 };
    let isPenddingFriend = false

    let isPenddingFriendRequest = () => {
        return new Promise((resolve, reject) => {
            let condition = { "findMateId": req.payload.findMateId.toLowerCase(),"deleteStatus":{"$exists":false} };

            userListMongoCollection.SelectOne(condition, (err, result) => {
                if (err) {
                    logger.error("get profile by FindMatchId API : ", err)
                    return reject(dbErrResponse);
                }
                else if (result) {

                    isPenddingFriend = (result && Array.isArray(result.pendingFriendShipId)) ? result.pendingFriendShipId.includes(req.auth.credentials._id) : false;

                }

                return resolve(true);


            })
        });
    };
    let getFindMatchId = () => {
        return new Promise((resolve, reject) => {
            let condition = {"findMateId": req.payload.findMateId.toLowerCase()};

            userListCollection.SelectActiveUser(condition, (err, result) => {
                if (err) {
                    logger.error("get profile by FindMatchId API : ", err);
                    return reject(dbErrResponse);
                }
                else {

                    result.hits.hits.forEach(element => {
                        dataToSend.push({
                            _id: element._id,
                            profilePic: element._source["profilePic"],
                            firstName: element._source["firstName"],
                            findMateId: element._source["findMateId"],
                            country:element._source["address"]?element._source["address"].country:"",
                            countryShortName:element._source["address"]?element._source["address"].countryCode:"",
                            city:element._source["address"]?element._source["address"].city:"",
                            isPenddingFriendRequest: isPenddingFriend,
                            isFriend: (req.user && Array.isArray(req.user.friendShipId)) ? req.user.friendShipId.includes(element._id) : false

                        })
                    });

                }

                return resolve(dataToSend[0]);


            })
        });
    };

    isPenddingFriendRequest()
        .then((data) => { return getFindMatchId() })
        .then((data) => { return reply({ message: req.i18n.__('genericErrMsg')['200'], data: dataToSend[0] }).code(200); })
        .catch((err) => {
            logger.err("get profile by FindMatchId API error : ", err);
            return reply({ message: err.message }).code(err.code);
        });
};




let response = {
    status: {
        200: { message: Joi.any().default(local['genericErrMsg']['200']), data: Joi.any() },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) },

    }
}

module.exports = { handler, response, validator }