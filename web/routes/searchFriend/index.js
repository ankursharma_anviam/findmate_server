'use strict'
let headerValidator = require('../../middleware/validator');
let searchFriend= require('./Post');
module.exports = [
    {
        method: 'POST',
        path: '/searchFriend',
        handler: searchFriend.handler,
        config: {
            description: 'This API will search for users on the app by using the username / findmateid as the input variable',
            tags: ['api', 'searchFriend'],
            auth: 'userJWT',
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: searchFriend.validator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            // response: postUnMatchAPI.response
        }
    }
];