'use strict'
const Joi = require("joi");
const async = require("async");
const logger = require('winston');
const ObjectID = require('mongodb').ObjectID;
var moment = require("moment");
const local = require('../../../locales');
const userListCollection = require("../../../models/userList");

/**
 * @function GET onlineUsers
 * @description This API is used to get Profile.

 * @property {string} authorization - authorization
 * @property {string} lang - language
  
 * @returns  200 : User profile sent successfully.
 * @example {
  "message": "User profile sent successfully.",
  "data": {
    "firstName": "Rahul Sharma",
    "countryCode": "+91",
    "contactNumber": "+919182736451",
    "gender": "Male",
    "email": "example@domain.com",
    "dob": 1234567,
    "about": "You want app call to me",
    "height": 200
  }
}
 * @returns  400 : Value in any of the  mandatory field's is missing.
 * @returns  412 : Entered User Id(token) doesnot exist,please check the entered token. 
 * @returns  500 : An unknown error has occurred.
 **/
let APIHandler = (req, res) => {

    let _id = req.auth.credentials._id;
    let offset = parseInt(req.query.offset) || 0;
    let limit = parseInt(req.query.limit) || 20;

    let condition = [
        {
            $geoNear: {
                near: {
                    "longitude": req.user.location.longitude,
                    "latitude": req.user.location.latitude
                },
                distanceField: "dist",
                num: 500,
                spherical: true
            }
        },
        { $match: { _id: { "$ne": ObjectID(req.user._id) }, "onlineStatus": 1, "lastOnline": { "$gte": moment().add(-4, "minute").valueOf() }, "deleteStatus": { "$ne": 1 } } },
        { "$sort": { "dist": 1 } },
        {
            "$project": {
                "dist": "$dist",
                "opponentId": "$_id",
                // "creation": 1,
                "firstName": 1,
                "profilePic": 1,
                "otherImages": 1,
                "profileVideo": 1,
                "otherVideos": 1,
                "countryCode": 1,
                "mobileNumber": "$contactNumber",
                "emailId": "$email",
                "dateOfBirth": "$dob",
                "dob": 1,
                "about": 1,
                "firebaseTopic": 1,
                "height": 1,
                "myPreferences": "$myPreferences",
                "location": 1,
                "dontShowMyDist": 1,
                "dontShowMyAge": 1
            }
        },
        { "$skip": offset }, { "$limit": limit }
    ];


    userListCollection.Aggregate(condition, (err, result) => {
        if (err) return res({ message: req.i18n.__('genericErrMsg')['412'] }).code(412);

        if (result.length) {
            for (let index = 0; index < result.length; index++) {
                result[index]["isMatched"] = false;
                result[index]["work"] = "";
                result[index]["job"] = "";
                result[index]["education"] = "";

                if (result[index]["myPreferences"]) {
                    for (let pref_index = 0; pref_index < result[index]["myPreferences"].length; pref_index++) {
                        if (result[index]["myPreferences"][pref_index]["pref_id"] == "5a30fda027322defa4a14638"
                            && result[index]["myPreferences"][pref_index]["isDone"]) {
                            result[index]["work"] = result[index]["myPreferences"][pref_index]["selectedValues"][0];
                        }
                        if (result[index]["myPreferences"][pref_index]["pref_id"] == "5a30fdd127322defa4a14649"
                            && result[index]["myPreferences"][pref_index]["isDone"]) {
                            result[index]["job"] = result[index]["myPreferences"][pref_index]["selectedValues"][0];
                        }
                        if (result[index]["myPreferences"][pref_index]["pref_id"] == "5a30fdfa27322defa4a14653"
                            && result[index]["myPreferences"][pref_index]["isDone"]) {
                            result[index]["education"] = result[index]["myPreferences"][pref_index]["selectedValues"][0];
                        }
                    }
                }

                let cur = new Date();
                let diff = cur - result[index].dob; // This is the difference in milliseconds
                let age = Math.floor(diff / 31557600000); // Divide by 1000*60*60*24*365.25

                result[index]["age"] = { value: age, isHidden: result[index].dontShowMyAge || 0 };
                result[index]["distance"] = { value: parseFloat(result[index]["dist"].toFixed(2)), isHidden: result[index].dontShowMyDist || 0 };

                delete result[index]["myPreferences"];
                delete result[index]["location"];
                delete result[index]["dist"];
            }
            return res({ message: req.i18n.__('GetOnlineUsers')['200'], data: result }).code(200);
        } else {
            return res({ message: req.i18n.__('GetOnlineUsers')['412'], data: [] }).code(412);
        }
    });
};


const queryValidator = Joi.object({
    offset: Joi.number().description("offset"),
    limit: Joi.number().description("limit"),
}).required();


let APIResponse = {
    status: {
        200: {
            message: Joi.any().default(local['GetOnlineUsers']['200']), data: Joi.any()
        },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        // 500: { message: Joi.any().default(local['genericErrMsg']['500']) },
    }
}

module.exports = { APIHandler, APIResponse, queryValidator }