'use strict'
let headerValidator = require('../../middleware/validator');
let GetAPI = require('./Get');


module.exports = [
    {
        method: 'Get',
        path: '/onlineUsers',
        handler: GetAPI.APIHandler,
        config: {
            description: 'This API will be used to block user',
            tags: ['api', 'unLike'],
            auth: 'userJWT',
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: GetAPI.queryValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.APIResponse
        }
    }
];