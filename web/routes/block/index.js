'use strict'
let headerValidator = require('../../middleware/validator');
let patchAPI = require('./Patch');

module.exports = [
    {
        method: 'PATCH',
        path: '/block',
        handler: patchAPI.APIHandler,
        config: {
            description: 'This API will be used to block user',
            tags: ['api', 'block'],
            auth: 'userJWT',
            response: patchAPI.response,
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: patchAPI.validator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            }
        }
    }
];