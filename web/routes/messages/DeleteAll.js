'use strict'
const Joi = require("joi");
const moment = require("moment");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../locales');
const messagesCollection = require('../../../models/messages');

/**
 * @method DELETE Messages
 * @description This API use to  Delete All Messages.
 
 * @param {*} req 
 * @param {*} res 
 
 * @property {*} authorization in header
 * @property {*} lang in header
 * @property {*} targetUserId targetUserId
 
 * @returns  200 : User deleted successfully.
 * @returns  400 : Value in any of the  mandatory field's is missing.
 * @returns  500 : An unknown error has occurred.
 */
let handler = (req, res) => {

    let _id = ObjectID(req.auth.credentials._id);
    let chatId = ObjectID(req.params.chatId);

    deleteAllMessages()
        .then(function (value) { return res({ message: req.i18n.__('PostCoinPlans')['200'] }).code(200); })
        .catch(function (err) {
            logger.error('Caught an error!', err);
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        });


    function deleteAllMessages() {
        return new Promise((resolve, reject) => {

            messagesCollection.Update({ chatId: chatId }, { ["members." + chatId + ".del"]: new Date().getTime() }, (err, result) => {
                if (err) {
                    return reject("have some issue in delete all messages")
                } else {
                    return resolve(true);
                }
            })
        });
    }
};

let response = {
    status: {
        200: { message: local['PostCoinPlans']['200'] },
        412: { message: local['PostCoinPlans']['412'] },
        400: { message: local['genericErrMsg']['400'] },
    }
}

let validator = Joi.object({
    chatId: Joi.string().required().min(24).max(24).description("chatId, Eg. 5b08158770c84b13a29c95cf ").error(new Error('chatId is missing')),
}).unknown();

module.exports = { handler, response, validator }