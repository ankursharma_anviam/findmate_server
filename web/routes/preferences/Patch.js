'use strict'
const Joi = require("joi");
const logger = require('winston');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../locales');
const userListCollection = require('../../../models/userList');
const userListType = require("../../../models/userListType");
const cronJObFile = require("../../../cronJobTask");

let payloadValidator = Joi.object({
    pref_id: Joi.string().required().description("enter pref_id").example("5a2fd81901ba4b2ea9c6314c").error(new Error('pref_id is missing')),
    values: Joi.array().required().description("enter values").example(["value1", "value2"]).error(new Error('values is missing')),
}).required();


let APIHandler = (req, res) => {

    let _id = req.auth.credentials._id;
    let pref_id = req.payload.pref_id;
    let values = req.payload.values;
    let heightInCMObj = cronJObFile.heightInCMObj;
    let heightInFeetObj = cronJObFile.heightInFeetObj;


    let condition = { "_id": ObjectID(_id), "myPreferences": { "$elemMatch": { "pref_id": ObjectID(pref_id) } } };
    let dataToUpdate = { "myPreferences.$.selectedValues": values, "myPreferences.$.isDone": true };

    if (pref_id == "5a30fa6d27322defa4a14550") {

        dataToUpdate["height"] = heightInCMObj[values[0]] || 160;
        dataToUpdate["heightInFeet"] = heightInFeetObj[values[0]] || `5'3"`;

        let Data = { "height": dataToUpdate["height"], "heightInFeet": dataToUpdate["heightInFeet"] };
        userListType.Update(_id, Data, () => { });
    }


    userListCollection.Update(condition, dataToUpdate, (err, result) => {
        if (err) {
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else {
            userListCollection.SelectOne({ _id: ObjectID(_id) }, (err, result) => {
                if (result) {
                    userListType.Update(_id, { myPreferences: result.myPreferences }, () => { });
                }
                return res({ message: req.i18n.__('PatchPreference')['200'] }).code(200);
            });
        }
    });
};

module.exports = { APIHandler, payloadValidator }