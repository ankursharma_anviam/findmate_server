'use strict'
let headerValidator = require('../../middleware/validator');
let PostAPI = require('./Post');

module.exports = [
    {
        method: 'POST',
        path: '/messageWithoutMatch',
        handler: PostAPI.APIHandler,
        config: {
            description: 'This API will be used to messageWithoutMatch user',
            tags: ['api', 'chat'],
            auth: 'userJWT',
            response: PostAPI.response,
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PostAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            }
        }
    }
];