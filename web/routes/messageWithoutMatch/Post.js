'use strict'
const Joi = require("joi");
const moment = require("moment");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../locales');
const mqtt = require('../../../library/mqtt');
const chatListCollection = require('../../../models/chatList');
const userListCollection = require('../../../models/userList');
const coinConfigCollection = require('../../../models/coinConfig');
const coinWalletCollection = require('../../../models/coinWallet');
const walletCustomerCollection = require('../../../models/walletCustomer');


/**
 * @method post messageWithoutMatch
 * @description This API use to rewind a user.
 
 * @param {*} req 
 * @param {*} res 
 
 * @property {*} authorization in header
 * @property {*} lang in header
 * @property {*} lang targetUserId
 
 * @returns  400 : Value in any of the  mandatory field's is missing.
 * @returns  500 : An unknown error has occurred.
 */
const payloadValidator = Joi.object({
    // from: Joi.string().required().min(24).max(24).description("from").error(new Error('from is missing')),
    to: Joi.string().required().min(24).max(24).description("to").error(new Error('to is missing')),
    // chatId: Joi.string().required().min(24).max(24).description("chatId").error(new Error('chatId is missing')),
    payload: Joi.string().allow(["", null]).description("payload").error(new Error('payload is missing')),
    type: Joi.string().allow(["", null]).description("messsage type").error(new Error('type is missing')),
    id: Joi.string().allow(["", null]).description("messageId ").error(new Error('id is missing')),
    secretId: Joi.string().allow(["", null]).description("secretId").error(new Error('secretId is missing')),
    dTime: Joi.string().allow(["", null]).description("dTime").error(new Error('dTime is missing')),
    thumbnail: Joi.string().allow(["", null]).description("thumbnail").error(new Error('thumbnail is missing')),
    extension: Joi.string().allow(["", null]).description("extension").error(new Error('extension is missing')),
    fileName: Joi.string().allow(["", null]).description("fileName").error(new Error('fileName is missing')),
    previousReceiverIdentifier: Joi.string().allow(["", null]).description("previousReceiverIdentifier").error(new Error('previousReceiverIdentifier is missing')),
    previousPayload: Joi.string().allow(["", null]).description("previousPayload").error(new Error('previousPayload is missing')),
    previousType: Joi.string().allow(["", null]).description("previousType").error(new Error('previousType is missing')),
    previousId: Joi.string().allow(["", null]).description("previousId").error(new Error('previousId is missing')),
    replyType: Joi.string().allow(["", null]).description("replyType").error(new Error('replyType is missing')),
    previousFileType: Joi.string().allow(["", null]).description("previousFileType").error(new Error('previousFileType is missing')),
    userImage: Joi.string().allow(["", null]).description("userImage").error(new Error('userImage is missing')),
    toDocId: Joi.string().allow(["", null]).description("toDocId").error(new Error('toDocId is missing')),
    name: Joi.string().allow(["", null]).description("name").error(new Error('name is missing')),
    dataSize: Joi.string().allow(["", null]).description("dataSize").error(new Error('dataSize is missing'))
}).unknown();

let APIHandler = (req, res) => {
    try {
        var _id = req.auth.credentials._id;
        var timestamp = new Date().getTime();
        var trigger = "perMsgWithoutMatch";
        var data = {};
        req.payload["from"] = _id.toString();
        req.payload["isMatchedUser"] = 0;

        getRequiredCoinForTrigger()
            .then((value) => {
                return getUsersCoinFromCoinWalletAndValidate(value);
            })
            .then(() => {
                return sendMessage();
            })
            .then(() => {
                return insertInWalletCusetomerAndUpdateInCoinWallet();
            })
            .then(() => {
                return res({
                    message: req.i18n.__('PostMessageWithoutMatch')['200'],
                    chatId: req.payload["chatId"],
                    coinWallet: data
                }).code(200);
            }).catch((err) => {
                logger.error(err);
                return res({
                    message: err.message
                }).code(err.code);
            });

    } catch (error) {
        logger.error("in PatchBoost : ", error);
        return res({
            message: req.i18n.__('genericErrMsg')['500']
        }).code(500);
    }

    function getRequiredCoinForTrigger() {
        return new Promise((resolve, reject) => {
            coinConfigCollection.SelectOne({}, (err, result) => {
                if (err) return reject(new Error('Ooops, no have data in coinConfig!'));

                return resolve(result[trigger] || {});
            });
        });
    }

    function getUsersCoinFromCoinWalletAndValidate(requiredCoin) {
        return new Promise((resolve, reject) => {
            coinWalletCollection.SelectOne({
                _id: ObjectID(_id)
            }, (err, result) => {
                if (err) return reject(new Error('Ooops, no have data in coinConfig!'));

                logger.silly("requiredCoin ", requiredCoin);
                for (let key in requiredCoin) {
                    if (requiredCoin[key] == 0 || (result && result["coins"][key] && requiredCoin[key] <= result["coins"][key])) {} else {
                        return reject({
                            code: 402,
                            message: req.i18n.__('genericErrMsg')['402']
                        });
                    }
                }
                data["coinWallet"] = result["coins"] || {};
                data["requiredCoin"] = requiredCoin;
                return resolve(true);
            });
        });
    }

    function sendMessage() {
        return new Promise(function (resolve, reject) {
            chatListCollection.SelectOne({
                ["members." + _id]: {
                    "$exists": true
                },
                ["members." + req.payload.to]: {
                    "$exists": true
                },
                "chatType": "NormalChat",
                "secretId": "",
                "deletedUser":{"$exists":false}, 
                "isUnMatched": {
                    "$exists": false
                }
            }, (err, result) => {
                req.payload["chatId"] = (result && result._id) ? result._id : new ObjectID();
                if (result == null) {
                    var chatDB = {
                        "_id": ObjectID(req.payload["chatId"]),
                        "message": [],
                        "members": {
                            [_id]: {
                                "memberId": ObjectID(_id),
                                "status": "NormalMember"
                            },
                            [req.payload.to]: {
                                "memberId": ObjectID(req.payload.to),
                                "status": "NormalMember"
                            }
                        },
                        "senderId": ObjectID(_id),
                        "targetId": ObjectID(req.payload.to),
                        "initiatedBy": ObjectID(_id),
                        "createdAt": new Date().getTime(),
                        "chatType": "NormalChat",
                        "secretId": "",
                        "isMatchedUser": 0
                    };
                    chatListCollection.Insert(chatDB, () => {});
                } else {
                    var chatDB = {
                        "_id": ObjectID(req.payload["chatId"]),
                        "members": {
                            [_id]: {
                                "memberId": ObjectID(_id),
                                "status": "NormalMember"
                            },
                            [req.payload.to]: {
                                "memberId": ObjectID(req.payload.to),
                                "status": "NormalMember"
                            }
                        },
                        "senderId": ObjectID(_id),
                        "targetId": ObjectID(req.payload.to),
                        "initiatedBy": ObjectID(_id),
                        "createdAt": new Date().getTime(),
                        "chatType": "NormalChat",
                        "secretId": "",
                        "isMatchedUser": 0
                    };
                    chatListCollection.Update({
                        "_id": ObjectID(req.payload["chatId"])
                    }, chatDB, () => {});

                }
                mqtt.publish(`Message/${req.payload.to}`, JSON.stringify(req.payload), {
                    qos: 1
                }, () => {});
                req.payload["isSuperLikedMe"] = req.user.mySupperLike.map(id => id.toString()).includes(req.payload.to);
                return resolve(true);
            });
        });
    }

    function insertInWalletCusetomerAndUpdateInCoinWallet() {
        return new Promise((resolve, reject) => {
            let coinWallet = data.coinWallet;
            let requiredCoin = data.requiredCoin;

            let insertData = [];
            let txnId = "TXN-ID-" + Math.floor(Math.random() * 10000000000);
            for (let key in requiredCoin) {

                if (coinWallet[key]) {
                    insertData.push({
                        "txnId": txnId,
                        "userId": ObjectID(_id),
                        "docType": "on message witout match",
                        "docTypeCode": 10,
                        "txnType": "DEBIT",
                        "txnTypeCode": 2,
                        "trigger": `Spent ${requiredCoin[key]} coins on a chat before getting matched`,
                        "currency": "N/A",
                        "currencySymbol": "N/A",
                        "coinType": key,
                        "coinOpeingBalance": coinWallet[key],
                        "cost": 0,
                        "coinAmount": requiredCoin[key],
                        "coinClosingBalance": (coinWallet[key] - requiredCoin[key]),
                        "paymentType": "N/A",
                        "timestamp": timestamp,
                        "transctionTime": timestamp,
                        "transctionDate": timestamp,
                        "paymentTxnId": "N/A",
                        "initatedBy": "customer"
                    });
                    coinWallet[key] = coinWallet[key] - requiredCoin[key];
                }
            }


            walletCustomerCollection.InsertMany(insertData, () => {});
            coinWalletCollection.Update({
                _id: ObjectID(_id)
            }, {
                "coins": coinWallet
            }, () => {});
            data = coinWallet;
            return resolve("---");
        });
    }
};

let response = {
    status: {
        200: {
            message: Joi.any().default(local['PostMessageWithoutMatch']['200']),
            chatId: Joi.any(),
            coinWallet: Joi.any()
        },
        400: {
            message: Joi.any().default(local['genericErrMsg']['400'])
        },
        402: {
            message: Joi.any().default(local['genericErrMsg']['402'])
        },
    }
}
module.exports = {
    APIHandler,
    response,
    payloadValidator
}