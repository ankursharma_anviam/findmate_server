'use strict'
let headerValidator = require('../../middleware/validator');
let postAPI = require('./Post');

module.exports = [
    {
        method: 'POST',
        path: '/dateResponse',
        handler: postAPI.APIHandler,
        config: {
            description: 'This API will be used',
            tags: ['api', 'Date'],
            auth: 'userJWT',
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: postAPI.validator,
                failAction: (req, reply, source, error) => {
                    console.log(reply)
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: postAPI.response
        }
    }
];