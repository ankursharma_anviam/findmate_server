'use strict'
let headerValidator = require('../../middleware/validator');
let friends = require('./get');
module.exports = [
    {
        method: 'GET',
        path: '/friends',
        handler: friends.APIHandler,
        config: {
            description: 'This API will be used to pull the accepted friends for that user',
            tags: ['api', 'friends'],
            auth: 'userJWT',
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: friends.validator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: friends.response
        }
    },
    
];