'use strict';
/**
 * @method GET pending friendrquest
 * @description This API allows user to get the pending frindrequest
 * @author ikshita@mobifyi.com
 * @property {string} lan - in header
 * @property {string} authorization - in header
 * @returns 400:Mandatory field missing.
 * @returns 500:Internal server error.
 * @returns 200:Got The Details..
 */
const Joi = require('joi');
const logger = require('winston');
const ObjectID = require('mongodb').ObjectID;
const local = require('../../../locales');
const userListCollection = require('../../../models/userList');
const friendshipCollection = require('../../../models/friendship');
const fcmPush = require("../../../library/fcm");

var count;

const APIHandler = (req, reply) => {
    const dbErrResponse = { message: req.i18n.__('genericErrMsg')['500'], code: 500 };
    let userId = new ObjectID(req.auth.credentials._id);

    let getPendingRequest = () => {
        return new Promise((resolve, reject) => {
            var datoTosend = [];

            //  console.log(userId);
            userListCollection.SelectByAggregate(userId, (err, result) => {
                result.forEach(e => {
                    if (!e.senderdetails.address) {
                        e.senderdetails.address = { "country": "India", "countryCode": "IN", "city": "Bangalore" }
                        // e.senderdetails["adderss"]["country"] = "India"
                        // e.senderdetails["adderss"]["countryCode"] = "IN"
                        // e.senderdetails["adderss"]["city"] = "Bangalore"
                    }
                    datoTosend.push(e.senderdetails);
                })
                count = result.length;

                if (err) {
                    logger.error("get friends API : ", err)
                    return reject(dbErrResponse);
                }
                else {
                    return resolve(datoTosend);
                }

            })
        });
    };



    getPendingRequest()
        .then((data) => { return reply({ message: req.i18n.__('genericErrMsg')['200'], data: data, "count": count }).code(200); })
        .catch((err) => {
            logger.error("get sortedlist error : ", err);
            return reply({ message: err.message }).code(err.code);
        });
};


const response = {
    status: {
        200: { message: Joi.any().default(local['genericErrMsg']['200']), data: Joi.any(), count: Joi.any() },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) },

    }
}
//swagger response code

module.exports = { APIHandler, response };
