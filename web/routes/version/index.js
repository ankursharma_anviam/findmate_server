'use strict'
var GetAPI = require('./Get');

module.exports = [
    {
        method: 'GET',
        path: '/version',
        handler: GetAPI.APIHandler,
        config: {
            description: 'This API will return latest version of the APP',
            tags: ['api', 'version'],
            auth: false,
            response: GetAPI.APIResponse
        }
    }
];