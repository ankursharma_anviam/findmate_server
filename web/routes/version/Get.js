'use strict'
const Joi = require("joi");
const local = require('../../../locales');
const config = require("../../../config")
/**
 * @method GET version
 * @description This API use to GET version.
 
 * @param {*} req 
 * @param {*} res 
 
 * @returns  200 : retun latest version.
 * @returns  500 : An unknown error has occurred.
 */
let APIHandler = (req, res) => {
    return res({ message: req.i18n.__('GETLatestVersion')['200'], data: { version: config.server.LATEST_VERSION } }).code(200);
};

let APIResponse = {
    status: {
        200: { message: local['GETLatestVersion']['200'], data: Joi.any()},
        500: { message: local['genericErrMsg']['500'] }
    }
}

module.exports = { APIHandler, APIResponse }