'use strict'
var Joi = require('joi');
var logger = require('winston');
var ObjectID = require('mongodb').ObjectID

const local = require('../../../locales');
const moment = require('moment-timezone');
const dateSchedule_TTLCollection = require('../../../models/dateSchedule_TTL');

let handler = (req, res) => {

    let _id = req.auth.credentials._id;
    let dataToSend = { pendingDates: {}, upcomingDates: {} };
    let befor = {
        initiatedId: "",
        initiatedName: "",
        opponentId: "",
        opponentName: ""
    };
    const dateTypeObj = { "videoDate": 1, "physicalDate": 2, "audioDate": 3 };
    const limit = parseInt(req.query.limit) || 20;
    const offset = parseInt(req.query.offset) || 0;
    const TimeToLive = -5;

    getPanddingDates()
        .then((value) => { return getUpcomingDates(); })
        .then((value) => { return res({ message: req.i18n.__('GetCurrentDates')['200'], data: dataToSend }).code(200); })
        .catch(function (err) {
            logger.error('Caught an error!', err);
            return res({ message: req.i18n.__('genericErrMsg')['412'] }).code(412);
        });

    function getPanddingDates() {
        return new Promise(function (resolve, reject) {

            let condition = [
                {
                    "$match": {
                        "$or": [
                            { "opponentId": ObjectID(_id) },
                            { "initiatedBy": ObjectID(_id) }],
                        "negotiations": {
                            "$elemMatch": {
                                "proposedOn": {
                                    "$gte": new Date().getTime()
                                },
                                "opponentResponse": ""
                            }
                        },
                        "deActivate": { "$exists": false }
                    }
                },
                {
                    "$project": {
                        "matchId": 1, "initiatedBy": 1, "initiatorName": 1, "iniatorProfilePic": 1,
                        "opponentId": 1, "opponentProfilePic": 1, "opponentName": 1, "createdTimestamp": 1,
                        "chatId": 1, "status": 1, "acceptedDateTimeStamp": 1, "dateFeedback": 1,
                        "callLogId": 1, "createdTimestamp": 1, "negotiationsLength": { "$size": "$negotiations" },
                        "negotiations": { "$slice": ["$negotiations", -1] },
                    }
                },
                { "$unwind": "$negotiations" },
                {
                    "$project": {
                        "data_id": "$_id",
                        "opponentId": {
                            "$cond": {
                                "if": { "$eq": ["$initiatedBy", ObjectID(_id)] },
                                "then": "$opponentId", "else": "$initiatedBy"
                            }
                        },
                        "opponentName": {
                            "$cond": {
                                "if": { "$eq": ["$initiatedBy", ObjectID(_id)] },
                                "then": "$opponentName", "else": "$initiatorName"
                            }
                        },
                        "opponentProfilePic": {
                            "$cond": {
                                "if": { "$eq": ["$initiatedBy", ObjectID(_id)] },
                                "then": "$opponentProfilePic", "else": "$iniatorProfilePic"
                            }
                        },
                        "proposedOn": "$negotiations.proposedOn",
                        "placeName": "$negotiations.placeName",
                        "longitude": "$negotiations.longitude",
                        "latitude": "$negotiations.latitude",
                        "requestedFor": "$negotiations.requestedFor",
                        "createdTimestamp": 1,
                        "userId": ObjectID(_id),
                        "isInitiatedByMe": {
                            "$cond": {
                                "if": { "$eq": ["$negotiations.proposerId", ObjectID(_id)] },
                                "then": 1, "else": 0
                            }
                        },
                        "negotiationsLength": 1
                    }
                },
                { "$lookup": { "from": "userList", "localField": "userId", "foreignField": "_id", "as": "data" } },
                { "$unwind": "$data" },
                {
                    "$project": {
                        "opponentId": 1, "opponentName": 1, "opponentProfilePic": 1,
                        "data_id": 1, "proposedOn": 1, "isInitiatedByMe": 1, "createdTimestamp": 1,
                        "timeZone": "$data.timeZone", "placeName": 1, "longitude": 1, "latitude": 1,
                        "requestedFor": 1, "onlineStatus": "$data.onlineStatus", "negotiationsLength": 1,
                        "gender": "$data.gender"
                    }
                },
                { "$sort": { "_id": -1 } },
                { "$skip": offset },
                { "$limit": limit },
            ];

            dateSchedule_TTLCollection.Aggregate(condition, (err, result) => {
                if (err) return reject(new Error('Ooops, something broke!'));

                if (result[0] && result[0]["timeZone"]) {
                    result.forEach(function (element) {
                        let timeZone = element["timeZone"].trim();
                        let proposedOnInMyTimeZone = moment.tz(element["proposedOn"], timeZone).format('MMMM Do YYYY, h:mm:ss a');
                        element["proposedOnInMyTimeZone"] = proposedOnInMyTimeZone;
                        element["dateType"] = dateTypeObj[element["requestedFor"]] || 0;
                        element["isReschedule"] = (element["negotiationsLength"] > 1);
                        switch (dateTypeObj[element["requestedFor"]]) {
                            case 1: {
                                element["requestedFor"] = "Video Date ";
                                break;
                            }
                            case 2: {
                                element["requestedFor"] = "Phisical Date ";
                                break;
                            }
                            case 3: {
                                element["requestedFor"] = "Audio Date ";
                                break;
                            }
                        }

                    }, this);
                }
                dataToSend["pendingDates"] = result;
                
                return resolve("--");
            });
        });
    }
    function getUpcomingDates() {
        return new Promise(function (resolve, reject) {

            let condition = [
                {
                    "$match": {
                        "$or": [
                            { "initiatedBy": ObjectID(_id) },
                            { "opponentId": ObjectID(_id) }
                        ],
                        "acceptedDateTimeStamp": { "$ne": 0 },
                        "isComplete": { "$ne": true },
                        "negotiations": {
                            "$elemMatch": {
                                "proposedOn": {
                                    "$gte": moment().add(TimeToLive, "minutes").valueOf()//new Date().getTime()
                                },
                                "opponentResponse": "accepted"
                            }
                        },
                        "deActivate": { "$exists": false },
                    }
                },
                {
                    "$project": {
                        "matchId": 1, "initiatedBy": 1, "initiatorName": 1, "iniatorProfilePic": 1,
                        "opponentId": 1, "opponentProfilePic": 1, "opponentName": 1, "createdTimestamp": 1,
                        "chatId": 1, "status": 1, "acceptedDateTimeStamp": 1, "dateFeedback": 1,
                        "callLogId": 1, "createdTimestamp": 1, "negotiationsLength": { "$size": "$negotiations" },
                        "negotiations": { "$slice": ["$negotiations", -1] }
                    }
                },
                { "$unwind": "$negotiations" },
                {
                    "$project": {
                        "opponentId": {
                            "$cond": {
                                "if": { "$eq": ["$initiatedBy", ObjectID(_id)] },
                                "then": "$opponentId", "else": "$initiatedBy"
                            }
                        },
                        "opponentName": {
                            "$cond": {
                                "if": { "$eq": ["$initiatedBy", ObjectID(_id)] },
                                "then": "$opponentName", "else": "$initiatorName"
                            }
                        },
                        "opponentProfilePic": {
                            "$cond": {
                                "if": { "$eq": ["$initiatedBy", ObjectID(_id)] },
                                "then": "$opponentProfilePic", "else": "$iniatorProfilePic"
                            }
                        },
                        "placeName": "$negotiations.placeName",
                        "longitude": "$negotiations.longitude",
                        "latitude": "$negotiations.latitude",
                        "requestedFor": "$negotiations.requestedFor",
                        "data_id": "$_id",
                        "createdTimestamp": 1,
                        "proposedOn": "$negotiations.proposedOn",
                        "userId": ObjectID(_id),
                        "negotiationsLength": 1, "dateFeedback": 1
                    }
                },
                { "$lookup": { "from": "userList", "localField": "userId", "foreignField": "_id", "as": "data" } },
                { "$unwind": "$data" },
                {
                    "$project": {
                        "opponentId": 1, "opponentName": 1, "opponentProfilePic": 1,
                        "data_id": 1, "proposedOn": 1, "createdTimestamp": 1, "requestedFor": 1,
                        "timeZone": "$data.timeZone", "placeName": 1, "longitude": 1, "latitude": 1,
                        "onlineStatus": "$data.onlineStatus", "negotiationsLength": 1, "dateFeedback": 1,
                        "gender": "$data.gender"
                    }
                },
                { "$sort": { "_id": -1 } },
                { "$skip": offset },
                { "$limit": limit }
            ];
            dateSchedule_TTLCollection.Aggregate(condition, (err, result) => {
                if (err) return reject(new Error('Ooops, something broke!'));

                if (result[0] && result[0]["timeZone"]) {

                    result.forEach(function (element) {
                        let timeZone = element["timeZone"].trim();
                        let proposedOnInMyTimeZone = moment.tz(element["proposedOn"], timeZone).format('MMMM Do YYYY, h:mm:ss a');
                        element["proposedOnInMyTimeZone"] = proposedOnInMyTimeZone;
                        element["dateType"] = dateTypeObj[element["requestedFor"]] || 0;
                        element["isLiveDate"] = ((element["proposedOn"] - (1000 * 30)) <= new Date().getTime());
                        element["isReschedule"] = (element["negotiationsLength"] > 1);

                        switch (dateTypeObj[element["requestedFor"]]) {
                            case 1: {
                                element["requestedFor"] = "Video Date ";
                                break;
                            }
                            case 2: {
                                element["requestedFor"] = "Phisical Date ";
                                break;
                            }
                            case 3: {
                                element["requestedFor"] = "Audio Date ";
                                break;
                            }
                        }
                    }, this);
                }
                dataToSend["upcomingDates"] = result;
                return resolve("--");
            })
        });
    }
}

let validator = Joi.object({
    offset: Joi.number().default(0).description("0").error(new Error('offset is missing')),
    limit: Joi.number().default(20).description("0").error(new Error('limit is missing'))
})

let response = {
    status: {
        200: { message: Joi.any().default(local['GetCurrentDates']['200']), data: Joi.any() },
        412: { message: Joi.any().default(local['genericErrMsg']['412']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
}

module.exports = { handler, validator }