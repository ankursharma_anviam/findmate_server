'use strict'
let headerValidator = require('../../middleware/validator');
let getAPI = require('./Get');

module.exports = [
    {
        method: 'Get',
        path: '/coinConfig',
        handler: getAPI.handler,
        config: {
            description: 'This API will be used to get coinConfig',
            tags: ['api', 'coinConfig'],
            auth: 'userJWT',
            response: getAPI.response,
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: getAPI.validator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            }
        }
    }
];