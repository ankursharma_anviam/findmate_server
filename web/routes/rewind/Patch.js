'use strict'
const Joi = require("joi");
const moment = require("moment");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../locales');
const cronJobTask = require('../../../cronJobTask');
const userListType = require('../../../models/userListType');
const userListCollection = require('../../../models/userList');
const rabbitMqClass = require('../../../models/rabbitMq/rabbitMq');
const userUnlikesCollection = require('../../../models/userUnlikes');
const rabbitMqBulkQueryInES = require('../../../models/rabbitMq/setBulkQueryInES');

/**
 * @method PATCH rewind
 * @description This API use to rewind a user.
 
 * @param {*} req 
 * @param {*} res 
 
 * @property {*} authorization in header
 * @property {*} lang in header
 * @property {*} lang targetUserId
 
 * @returns  400 : Value in any of the  mandatory field's is missing.
 * @returns  500 : An unknown error has occurred.
 */

let APIHandler = (req, res) => {
    let dataToSend = {};
    let remainsRewindsInString = "0"
    let bulkArray = [];
    let nextRewindTime = 0;
    const likeRefreshInMillisecond = cronJobTask.likeRefreshInMillisecond;
    try {
        var _id = req.auth.credentials._id;
        var differenceInHours = 12;
        var a = moment();
        var b = moment(req.user.lastTimestamp.rewind || new Date().getTime());

        getUserIdForRewind()
            .then((value) => { return rewindUser(value); })
            .then((value) => {
                rabbitMqBulkQueryInES.InsertQueue(rabbitMqClass.getChannelForBulkQuerys(), rabbitMqClass.queueForBulkQuerys,
                    bulkArray, (err, result) => { if (err) logger.error(err) });
                nextRewindTime = (parseInt(remainsRewindsInString) <= 0) ? (b.add(differenceInHours, 'hours').valueOf()) : 0;
                return res({ message: req.i18n.__('PatchRewind')['200'], nextRewindTime: nextRewindTime, data: dataToSend, remainsRewindsInString: remainsRewindsInString }).code(200);
            }).catch((err) => {
                logger.error(err);
                return res({ message: err.message }).code(err.code);
            });
    } catch (error) {
        logger.error("in PatchRewind : ", error);
        return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
    }

    function getUserIdForRewind() {
        return new Promise(function (resolve, reject) {

            if (req.user.subscription &&
                req.user.subscription[0]["rewindCount"] != "unlimited"
                && req.user.count.rewind >= req.user.subscription[0]["rewindCount"]
                && a.diff(b, 'hours') >= differenceInHours
            ) {
                req.user.count.rewind = 0;
                userListCollection.UpdateById(_id, { "count.rewind": 0 }, (err, result) => {
                    if (err) return reject(new Error('Ooops, something broke!', err));
                });
            }
            if (req.user && req.user.subscription && req.user.subscription[0]["rewindCount"] == "unlimited" 
                // && a.diff(b, 'hours') < 12
            ) {
                return resolve(req.user.lastUnliked.userId.toString());
            } else {
                return reject({ code: 409, message: req.i18n.__('genericErrMsg')['409'] });
            }

        });
    }
    function rewindUser(targetUserId) {
        return new Promise(function (resolve, reject) {

            if (req.user.subscription && req.user.count.rewind >= req.user.subscription[0]["rewindCount"] &&
                a.diff(b, 'hours') >= differenceInHours) {
                userListCollection.UpdateById(_id, { "count.rewind": 0 }, (err, result) => {
                    if (err) return reject(new Error('Ooops, something broke!', err));
                });
                remainsRewindsInString = (req.user.subscription[0]["rewindCount"] == "unlimited") ? "unlimited" : parseInt(req.user.subscription[0]["rewindCount"]) - 1;
            }
            /**
             * remove from elastic search
             */
            // bulkArray.push(
            //     { "update": { "_id": `${_id}`, "_type": "userList", "_index": "azar", "retry_on_conflict": 3 } },
            //     { "script": { "source": "ctx._source.myunlikes.remove(ctx._source.myunlikes.indexOf('" + targetUserId + "'))", "lang": "painless" } },
            //     { "update": { "_id": `${targetUserId}`, "_type": "userList", "_index": "azar", "retry_on_conflict": 3 } },
            //     { "script": { "source": "ctx._source.disLikedUSers.remove(ctx._source.disLikedUSers.indexOf('" + _id + "'))", "lang": "painless" } }

            // );
            userListType.UpdateWithPull(targetUserId, "disLikedUSers", _id, (err, result) => {
                if (err) { logger.error(err); return reject(new Error('Ooops, something broke!')); }
            });
            userListType.UpdateWithPull(_id, "myunlikes", targetUserId, (err, result) => {
                if (err) { logger.error(err); return reject(new Error('Ooops, something broke!')); }
            });
            /**
            * remove from mongoDB
            */
            userListCollection.UpdateByIdWithPull({ _id: targetUserId }, { "disLikedUSers": ObjectID(_id) }, (err, result) => {
                if (err) { logger.error(err); return reject(new Error('Ooops, something broke!')); }
            });
            userListCollection.UpdateByIdWithPull({ _id: _id }, { "myunlikes": ObjectID(targetUserId) }, (err, result) => {
                if (err) { logger.error(err); return reject(new Error('Ooops, something broke!')); }
            });

            userListCollection.UpdateWithIncrease({ _id: ObjectID(_id) }, { "count.rewind": 1 }, (err, result) => {
                if (err) { logger.error(err); return reject(new Error('Ooops, something broke!')); }
            });
            userListCollection.Update({ _id: ObjectID(_id) }, { "lastTimestamp.rewind": new Date().getTime() }, (err, result) => {
                if (err) { logger.error(err); return reject(new Error('Ooops, something broke!')); }
            });

            let condition = {
                userId: ObjectID(_id),
                targetUserId: ObjectID(targetUserId)
            };
            userUnlikesCollection.Delete(condition, () => { });

            userListCollection.Unset({ _id: ObjectID(_id) }, { "lastUnliked": "" }, (err, result) => {
                if (err) {
                    logger.error(err);
                    return reject(new Error('Ooops, something broke!'));
                }
            });

            userListCollection.SelectOne({ _id: ObjectID(targetUserId) }, (err, result) => {
                if (result && result._id) {
                    let birthdate = new Date(result["dob"]);
                    let cur = new Date();
                    let diff = cur - birthdate; // This is the difference in milliseconds
                    let age = Math.floor(diff / 31557600000); // Divide by 1000*60*60*24*365.25
                    let work = "", job = "", education = "";

                    if (result["myPreferences"]) {
                        for (let pref_index = 0; pref_index < result["myPreferences"].length; pref_index++) {
                            if (result["myPreferences"][pref_index]["pref_id"] == "5a30fda027322defa4a14638"
                                && result["myPreferences"][pref_index]["isDone"]) {
                                work = result["myPreferences"][pref_index]["selectedValues"][0];
                            }
                            if (result["myPreferences"][pref_index]["pref_id"] == "5a30fdd127322defa4a14649"
                                && result["myPreferences"][pref_index]["isDone"]) {
                                job = result["myPreferences"][pref_index]["selectedValues"][0];
                            }
                            if (result["myPreferences"][pref_index]["pref_id"] == "5a30fdfa27322defa4a14653"
                                && result["myPreferences"][pref_index]["isDone"]) {
                                education = result["myPreferences"][pref_index]["selectedValues"][0];
                            }
                        }
                        dataToSend = {
                            "opponentId": result["_id"],
                            "firstName": result["firstName"],
                            "mobileNumber": result["contactNumber"],
                            "emailId": result["email"],
                            "height": result["height"],
                            "gender": result["gender"],
                            "dateOfBirth": result["dob"],
                            "onlineStatus": result["onlineStatus"],
                            "about": result["about"],
                            "profilePic": result["profilePic"] || "https://res.cloudinary.com/demo/image/upload/ar_4:3,c_fill/c_scale,w_auto,dpr_auto/sample.jpg",
                            "profileVideo": result["profileVideo"],
                            "otherImages": (Array.isArray(result["otherImages"])) ? result["otherImages"] : [],
                            "firebaseTopic": result["firebaseTopic"],
                            "instaGramProfileId": result["instaGramProfileId"] || "",
                            "instaGramToken": result["instaGramToken"] || "",
                            "deepLink": result["deepLink"],
                            "profileVideoThumbnail": (result["profileVideoThumbnail"] && !Array.isArray(result["profileVideoThumbnail"])) ? result["profileVideoThumbnail"] : "",
                            "distance": { value: 0, isHidden: result.dontShowMyDist || 0 },
                            "age": { value: age, isHidden: result.dontShowMyAge || 0 },
                            "work": work,
                            "job": job,
                            "education": education
                        }
                    }
                }
                return resolve(true);
            });
        });
    }
};

let response = {
    status: {
        200: { message: Joi.any().default(local['PatchRewind']['200']), nextRewindTime: Joi.any(), remainsRewindsInString: Joi.any(), data: Joi.any() },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        402: { message: Joi.any().default(local['genericErrMsg']['402']) },
        409: { message: Joi.any().default(local['genericErrMsg']['409']) },
    }
}

module.exports = { APIHandler, response }