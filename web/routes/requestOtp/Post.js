'use strict'
const Joi = require("joi");
const logger = require('winston');
const local = require('../../../locales');
const SMS = require("../../../library/twilio")
const otps_mongoDB = require("../../../models/otps");

const Timestamp = require('mongodb').Timestamp;

const validator = Joi.object({
    phoneNumber: Joi.string().required().description("Country Code and Phonenumber(without space), Eg. +919620826142").error(new Error('phoneNumber is missing')),
    type: Joi.number().required().min(1).max(3).default(1).description("Type-1(New registration || Login), Type-2(Recovery password),Type-3 update MobileNo Eg. 1/2").error(new Error('type is missing')),
    deviceId: Joi.string().required().description("deviceId ").error(new Error('deviceId is missing')),
}).required();

const handler = (req, res) => {

    try {
        console.log("req.payload ", req.payload)
        let phoneNumber = req.payload.phoneNumber;
        let randomCode = (process.env.NODE_ENV == "development") ? 111111 : Math.floor(100000 + Math.random() * 9000);

        let dataTosave = {
            "type": req.payload.type,
            "phone": phoneNumber,
            "otp": randomCode,
            "time": new Date().getTime(),
            "deviceId": req.payload.deviceId,
            "creationTs": new Timestamp(),
            "creationDate": new Date()
        };
        let smsData = { "to": phoneNumber, "body": "your findmate verification code is : " + randomCode };
        // if (process.env.NODE_ENV != "development") {
        //     SMS.sendSms(smsData, (haveError, result) => {
        //         console.log("haveError ", haveError);
        //         console.log("result ", result);
        //     });
        // }
        otps_mongoDB.Insert(dataTosave, (haveError, result) => {
            if (haveError) {
                return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
            }
            return res({ message: req.i18n.__('PostRequestOTP')['200'] }).code(200);
        });
    } catch (error) {
        logger.error("error ", error)
        return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
    }
};

module.exports = { handler, validator }