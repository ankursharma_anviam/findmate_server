'use strict'
const Joi = require("joi");
const logger = require('winston');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../locales');
const userList = require('../../../models/userList');
const userListType = require('../../../models/userListType');


let APIHandler = (req, res) => {

    let _id = req.auth.credentials._id;

    setInMongo()
        .then(function (value) {
            return res({ message: req.i18n.__('PostDeepLink')['200'] }).code(200);
        })
        .catch(function (err) {
            return res({ message: req.i18n.__('PostDeepLink')['412'] }).code(412);
        });


    function setInMongo() {
        return new Promise(function (resolve, reject) {

            let dataToUpdate = {
                deepLink: req.payload.deepLink,
            }
            userList.UpdateById(_id, dataToUpdate, (err, result) => {
                if (err) {
                    return reject(new Error('Ooops, something broke!'));
                } else {
                    userListType.Update(_id, dataToUpdate, (err, result) => {
                        if (err) {
                            return reject(new Error('Ooops, something broke from ESearch!'));
                        } else {
                            return resolve("---");
                        }
                    });
                }
            });
        });
    }
};

let validator = Joi.object({
    deepLink: Joi.string().required().description("ex : deepLink........").example("deepLink.....").error(new Error('deepLink is missing')),
}).required();

let response = {
    status: {
        200: { message: Joi.any().default(local['PostDeepLink']['200']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        412: { message: Joi.any().default(local['PostDeepLink']['412']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}

module.exports = { APIHandler, validator,response }