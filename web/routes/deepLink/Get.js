'use strict'
const Joi = require("joi");
const async = require("async");
const Cryptr = require('cryptr');
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require("mongodb").ObjectID

const local = require('../../../locales');
const Auth = require("../../middleware/authentication.js");
const userListCollection = require('../../../models/userList');
const userListType = require('../../../models/userListType');
const recentVisitorsCollection = require('../../../models/recentVisitors');

/**
 * @function GET profileById
 * @description This API is used to get Profile by Id.

 * @property {string} authorization - authorization
 * @property {string} lang - language
 * @property {string}  targetUserId- targetUserId
  
 * @returns  200 : User found in database.
 * @example {
  "message": "User found in database.",
  "data": {
    "firstName": "Rahul",
    "countryCode": "+91",
    "contactNumber": "+919019810918",
    "gender": "Male",
    "profilePic": "................",
    "otherImages": [
      "1.......",
      "2..........",
      "3......."
    ],
    "email": "example@domain.com",
    "profileVideo": "..............",
    "otherVideos": [
      "1.......",
      "2..........",
      "3......."
    ],
    "dob": 1504867670000,
    "about": "about user",
    "currentLoggedInDevices": {
      "deviceId": "yourDeviceId",
      "pushToken": "yourPushToken"
    },
    "height": 190,
    "heightInFeet": "5'6\"",
    "firebaseTopic": "yourDeviceId",
    "userId": "5a0d494e2c501c37b0f3067a"
  }
}
 * @returns  400 : Value in any of the  mandatory field's is missing.
 * @returns  412 : User doesnot exist in the database. 
 * @returns  500 : An unknown error has occurred.
 **/
let handler = (req, res) => {

    let inMylanguage = req.headers.lang;
    let _id = req.auth.credentials._id;
    let targetUserId = req.params.targetUserId;
    let requiredField = {
        _id: 0, deepLink: 1
    };
    let dataToSend = {};

    getUser()
        .then(function (value) {
            return res({ message: req.i18n.__('GetDeepLink')['200'], data: dataToSend }).code(200);
        })
        .catch(function (err) {
            logger.info('Caught an error!', err);
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        });


    function getUser() {
        return new Promise(function (resolve, reject) {
            userListCollection.SelectById({ _id: targetUserId }, requiredField, (err, result) => {
                if (err) return reject("-");

                dataToSend = result;
                return resolve("--");
            })
        });
    }
};

let validator = Joi.object({
    targetUserId: Joi.string().required().description("enter targetUserId").error(new Error('targetUserId is missing')),
}).required();

let response = {

    status: {
        200: {
            message: Joi.any().default(local['GetDeepLink']['200']),
            data: Joi.any().example({
                "deepLink": "deepLink......",
            })
        },
        412: { message: Joi.any().default(local['GetDeepLink']['412'])},
        400: { message: Joi.any().default(local['genericErrMsg']['400'])},
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}

module.exports = { handler, response, validator }