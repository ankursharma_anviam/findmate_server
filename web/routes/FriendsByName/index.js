'use strict'
let headerValidator = require('../../middleware/validator');
let FriendsByName= require('./Post');
module.exports = [
    {
        method: 'POST',
        path: '/FriendsByName',
        handler: FriendsByName.handler,
        config: {
            description: ' This API returns the list of friends for each user.',
            tags: ['api', 'FriendsByName'],
            auth: 'userJWT',
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: FriendsByName.validator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            // response: postUnMatchAPI.response
        }
    }
];