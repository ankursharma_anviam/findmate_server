'use strict'
var Joi = require('joi');
const logger = require('winston');
var ObjectID = require('mongodb').ObjectID
const local = require('../../../locales');
const walletCustomerCollection = require('../../../models/walletCustomer');

let handler = (req, res) => {

    let _id = req.auth.credentials._id;
    let limit = parseInt(req.query.limit) || 20;
    let skip = parseInt(req.query.offset) || 0;

    walletCustomerCollection.SelectWithSort({ userId: ObjectID(_id) }, { _id: -1 }, {
        "txnId": 1, "txnType": 1, "txnTypeCode": 1, "trigger": 1, "currency": 1, "currencySymbol": 1,
        "coinType": 1, "coinOpeingBalance": 1, "cost": 1, "coinAmount": 1, "coinClosingBalance": 1,
        "paymentType": 1, "timestamp": 1, "paymentTxnId": 1, "userPurchaseTime": 1, "_id": 0
    }, skip, limit, (err, result) => {
        if (err) return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);

        let allCoins = [], coinIn = [], coinOut = [];

        result.forEach(element => {

            allCoins.push(element);
            if (element["txnType"] == "DEBIT") {
                coinOut.push(element);
            } else {
                coinIn.push(element);
            }

        });

        return res({ message: req.i18n.__('GetCoinHostory')['200'], data: { allCoins: allCoins, coinIn: coinIn, coinOut: coinOut } }).code(200);
    });
}

let validator = Joi.object({
    offset: Joi.number().default(0).description("0").error(new Error('offset is missing')),
    limit: Joi.number().default(20).description("0").error(new Error('limit is missing'))
}).required();

let response = {
    status: {
        200: { message: Joi.any().default(local['GetCoinHostory']['200']), data: Joi.any() },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) },
    }
}
module.exports = { handler, response, validator }