'use strict'
const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;
const userListType = require("../../../models/userListType");

const local = require('../../../locales');
const userListCollection = require('../../../models/userList');

/**
 * @method PUT User
 * @description This API use to  Delete User.
 
 * @param {*} req 
 * @param {*} res 
 
 * @property {*} authorization in header
 * @property {*} lang in header
 * @property {*} targetUserId targetUserId
 
 * @returns  200 : User Delete successfully.
 * @returns  400 : Value in any of the  mandatory field's is missing.
 * @returns  422 : targetUserId  doesnot exist in the database,try with the different ID.
 * @returns  500 : An unknown error has occurred.
 */
let handler = (req, res) => {


    let _id = req.auth.credentials._id;


    let data = {
        deleteStatus: 1,
        deleteTimeStamp: new Date().getTime(),

    }
    DeleteUser()
        .then(function (value) {
            return res({ message: req.i18n.__('PutUser')['200'], data: value }).code(200);
        }).catch(function (err) {
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        });

    function DeleteUser() {
        return new Promise(function (resolve, reject) {
            userListCollection.UpdateById(_id, data, (err, result) => {
                if (err) return reject(new Error('Ooops, something broke!'));

                return resolve(result);
            })
            userListType.Update(_id, data, (err, result) => { });
        });
    }
};

let response = {
    // status: {
    //     200: { message: local['PutUser']['200'], data: Joi.any() },
    //     412: { message: local['PutUser']['412'] },
    //     400: { message: local['genericErrMsg']['400'] },
    //     500: { message: local['genericErrMsg']['500'] },
    // }
}

module.exports = { handler, response }