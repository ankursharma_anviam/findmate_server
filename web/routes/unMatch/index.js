'use strict'
let headerValidator = require('../../middleware/validator');
let postUnMatchAPI = require('./Post');

module.exports = [
    {
        method: 'POST',
        path: '/unMatch',
        handler: postUnMatchAPI.handler,
        config: {
            description: 'This API will be used to unmatch user',
            tags: ['api', 'unMatch'],
            auth: 'userJWT',
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: postUnMatchAPI.validator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            // response: postUnMatchAPI.response
        }
    }
];