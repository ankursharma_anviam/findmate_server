'use strict'
const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;
const async = require("async");
const local = require('../../../locales');

const mqttClient = require("../../../library/mqtt");
const dateSchedule = require('../../../models/dateSchedule');
const userListType = require("../../../models/userListType");
const userListCollection = require('../../../models/userList');
const chatListCollection = require('../../../models/chatList');
const messagesCollection = require('../../../models/messages');
const userMatchCollection = require("../../../models/userMatch");
const rabbitMqClass = require('../../../models/rabbitMq/rabbitMq');
const rabbitMqBulkQueryInES = require('../../../models/rabbitMq/setBulkQueryInES');


let validator = Joi.object({
    targetUserId: Joi.string().required().description("targetUserId").error(new Error('targetUserId is missing'))
}).required();

/**
 * @method POST unmatch
 * @description This API use to  POST unmatch.
 
 * @param {*} req 
 * @param {*} res 
 
 * @property {*} authorization in header
 * @property {*} lang in header
 * @property {*} targetUserId targetUserId
 
 * @returns  200 : User unmatched successfully.
 * @returns  400 : Value in any of the  mandatory field's is missing.
 * @returns  422 : targetUserId  doesnot exist in the database,try with the different ID.
 * @returns  500 : An unknown error has occurred.
 */
let handler = (req, res) => {

    let _id = req.auth.credentials._id;
    let targetUserId = req.payload.targetUserId;
    checkIsMatched()
        .then(() => { return unMatchFromMongo(); })
        .then(() => { return unMatchFromES(); })
        .then(() => { return removeChats(); })
        .then(() => { return updateInUnMatch(); })
        .then(() => { return res({ message: req.i18n.__('PostUnMatch')['200'] }).code(200); })
        .catch((err
        ) => { return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500); })


    function checkIsMatched() {
        return new Promise(function (resolve, reject) {
            let condition = { "_id": ObjectID(_id), "matchedWith": ObjectID(targetUserId) };
            userListCollection.SelectOne(condition, (err, result) => {
                if (err) return reject(new Error('Ooops, something broke!'));

                mqttClient.publish(_id, JSON.stringify({ "messageType": "unMatch", targetId: targetUserId }), { qos: 1 }, (e, r) => { });
                mqttClient.publish(targetUserId, JSON.stringify({ "messageType": "unMatch", targetId: _id }), { qos: 1 }, (e, r) => { });
                return resolve(true)
            });
        });
    }

    function unMatchFromMongo() {
        return new Promise(function (resolve, reject) {

            async.series([
                function (callback) {
                    let condition = { "_id": _id };
                    let data = { "matchedWith": ObjectID(targetUserId) };

                    userListCollection.UpdateByIdWithPull(condition, data, (err, result) => {
                        if (err) return reject(new Error('Ooops, something broke!'));

                        return callback(err, result)
                    });
                },
                function (callback) {
                    let condition = { "_id": targetUserId };
                    let data = { "matchedWith": ObjectID(_id) };

                    userListCollection.UpdateByIdWithPull(condition, data, (err, result) => {
                        if (err) return reject(new Error('Ooops, something broke!'));

                        return callback(err, result);
                    });
                },
                function (callback) {
                    dateSchedule.deActivateDates({ _id: _id, reason: "unMatch" }, (err, result) => {
                        if (err) return reject(new Error('Ooops, something broke!'));

                        return callback(err, result);
                    });
                },
                function (callback) {
                    let condition = { "_id": targetUserId };
                    let data = { "supperLikeByHistory": ObjectID(_id) };

                    userListCollection.UpdateByIdWithPull(condition, data, (err, result) => {
                        if (err) return reject(new Error('Ooops, something broke!'));

                        return callback(err, result);
                    });
                },
                function (callback) {
                    let condition = { "_id": _id };
                    let data = { "supperLikeByHistory": ObjectID(targetUserId) };
                    userListCollection.UpdateByIdWithPull(condition, data, (err, result) => {
                        if (err) return reject(new Error('Ooops, something broke!'));

                        return callback(err, result);
                    });
                },
                function (callback) {
                    let bulkArray = [
                        { "update": { "_id": `${_id}`, "_type": "userList", "_index": "azar", "retry_on_conflict": 3 } },
                        { "script": { "source": "ctx._source.matchedWith.remove(ctx._source.matchedWith.indexOf('" + targetUserId + "'))", "lang": "painless" } },

                        { "update": { "_id": `${targetUserId}`, "_type": "userList", "_index": "azar", "retry_on_conflict": 3 } },
                        { "script": { "source": "ctx._source.matchedWith.remove(ctx._source.matchedWith.indexOf('" + _id + "'))", "lang": "painless" } }];
                    rabbitMqBulkQueryInES.InsertQueue(rabbitMqClass.getChannelForBulkQuerys(), rabbitMqClass.queueForBulkQuerys,
                        bulkArray, (err, result) => { logger.error((err) ? err : ""); });
                    return callback(null, 1);
                }
            ], () => {

                return resolve(true)
            })
        });
    }

    function unMatchFromES() {
        return new Promise(function (resolve, reject) {

            userListType.UpdateWithPull(_id, "matchedWith", targetUserId, (err, result) => {
                if (err) return reject(new Error('Ooops, something broke!'));
            });

            userListType.UpdateWithPull(targetUserId, "matchedWith", _id, (err, result) => {
                if (err) return reject(new Error('Ooops, something broke!'));
            });

            return resolve(true);
        });
    }
    function removeChats() {
        return new Promise(function (resolve, reject) {

            let condition = {
                ["members." + _id + ".status"]: "NormalMember",
                ["members." + targetUserId + ".status"]: "NormalMember",
                "chatType": "NormalChat",
                "secretId": ""
            };
            chatListCollection.Update(condition, { isUnMatched: 1, "isMatchedUser": 0 }, (err, result) => { });

            return resolve(true);
        });
    }
    function updateInUnMatch() {
        return new Promise(function (resolve, reject) {

            let condition = {
                "$or": [
                    { "initiatedBy": ObjectID(_id), "opponentId": ObjectID(targetUserId) },
                    { "initiatedBy": ObjectID(targetUserId), "opponentId": ObjectID(_id) }
                ]
            };
            let data = { "unMatchedBy": ObjectID(_id), "unMatchedTimestamp": new Date().getTime() };

            userMatchCollection.Update(condition, data, (err, result) => {
                if (err) return reject(new Error('Ooops, something broke!'));

                return resolve("----");
            });
        });
    }
};

let response = {
    status: {
        200: { message: local['PostUnMatch']['200'] },
        422: { message: local['PostUnMatch']['412'] },
        400: { message: local['genericErrMsg']['400'] },
    }
}
module.exports = { handler, response, validator }