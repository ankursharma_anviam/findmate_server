'use strict'
var Joi = require('joi');
var async = require("async");
var conf = process.env;
var ObjectId = require('mongodb').ObjectID
const logger = require('winston');
const callLogsCollection = require('../../../models/callLogs')
const userListCollection = require('../../../models/userList')

let handler = (req, res) => {

    let _id = req.auth.credentials._id;
    let JWTDecoded = { userId: _id };
    let dataToSend = { available: [], missed: [] };

    async.series([
        function (callback) {
            const limit = parseInt(req.query.limit) || 20;
            const offset = parseInt(req.query.offset) || 0;

            var timeStamp = parseInt(req.params.timeStamp);
            var userId = JWTDecoded.userId;
            var userParams = "members." + userId + ".deleteAt";

            var condition = [
                { $match: { $or: [{ "userId": ObjectId(userId) }, { "targetId": ObjectId(userId) }] } },
                { $match: { [userParams]: { $exists: false }, "creation": { $gt: 1 } } },
                {
                    $project: {
                        "opponetId": {
                            "$cond": {
                                if: { $eq: ["$userId", ObjectId(userId)] },
                                then: "$targetId", else: "$userId"
                            }
                        },
                        "callInitiated": {
                            "$cond": {
                                if: { $eq: ["$userId", ObjectId(userId)] },
                                then: true, else: false
                            }
                        },
                        "_id": 0, "calltime": "$creation", "callType": "$typeOfCall",
                        "callId": "$callId", "secondsDiff": "$secondsDiff"
                    }
                },
                { "$lookup": { "from": "userList", "localField": "opponetId", "foreignField": "_id", "as": "user" } },
                { $unwind: "$user" },
                {
                    $project: {
                        "calltime": "$calltime",
                        "callInitiated": 1,
                        "callType": "$callType",
                        "callId": "$callId",
                        "callDuration": "$secondsDiff",
                        "opponentId": "$user._id",
                        "opponentProfilePic": "$user.profilePic",
                        "opponentName": "$user.firstName",
                        "opponentNumber": "$user.contactNumber"
                    }
                },
                { $sort: { calltime: -1 } },
                { "$skip": offset },
                { "$limit": limit },
            ];

            callLogsCollection.Aggregate(condition, function (err, result) {
                if (err) {
                    return res({ message: "success", response: [] }).code(503);
                } else {


                    for (let index = 0; index < result.length; index++) {
                        if (result[index]["callDuration"] && result[index]["calltime"] != 1) {
                            dataToSend.available.push(result[index]);
                        } else {
                            dataToSend.missed.push(result[index]);
                        }
                    }
                }
                callback(err, result);
            });
        },
        function (callback) {

            let condition = [
                { "$match": { _id: ObjectId(_id) } },
                { "$project": { "matchedWith": 1 } },
                { "$unwind": "$matchedWith" },
                { "$lookup": { "from": "userList", "localField": "matchedWith", "foreignField": "_id", "as": "Data" } },
                { "$unwind": "$Data" },
                { "$match": { "Data.onlineStatus": 1 } },
                {
                    "$project": {
                        "_id": 0, "opponentId": "$Data._id", "opponentProfilePic": "$Data.profilePic", "opponentName": "$Data.firstName",
                        "contactNumber": "$Data.contactNumber", "countryCode": "$Data.countryCode", "lastOnline": "$Data.lastOnline",
                    }
                },
                { "$sort": { "lastOnline": -1 } },
                { "$skip": offset },
                { "$limit": limit },
            ];

            userListCollection.Aggregate(condition, (err, result) => {
                dataToSend["available"] = result;
                callback(err, result);
            });
        }

    ], (err, result) => {

        return res({ message: "success", response: dataToSend }).code(200);
    });
}

let validator = Joi.object({
    timeStamp: Joi.string().required().description("0").error(new Error('timeStamp is missing'))
}).required();

const queryValidator = Joi.object({
    offset: Joi.number().description("offset"),
    limit: Joi.number().description("limit"),
}).required();

module.exports = { handler, validator, queryValidator }