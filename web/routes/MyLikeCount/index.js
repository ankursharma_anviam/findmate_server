
'use strict';
const getAPI= require('./get');
let headerValidator = require("../../middleware/validator");

module.exports = [
    /**
    * api to add new card
    */
    
    {
        method: 'GET',
        path: '/MyProfileLikeCount',
        handler: getAPI.APIHandler,
        config: {
            tags: ['api', 'profileLike'],
            description: 'This API returns the number of unique likes on the logged in user’s profile',
            auth: "userJWT",
            response: getAPI.responseCode,
            validate: {
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
   
]