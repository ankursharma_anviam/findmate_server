
let headerValidator = require('../../middleware/validator');
let getReportUserReasonsAPI = require('./GetReasons');
let postReportUserAPI = require('./Post');

module.exports = [
    {
        method: 'GET',
        path: '/reportUserReasons',
        handler: getReportUserReasonsAPI.handler,
        config: {
            description: 'This API will be used by the passport feature to update the user’s location so that the future search runs off this new location',
            tags: ['api', 'reportUser'],
            auth: 'userJWT',
            response: getReportUserReasonsAPI.response,
            validate: {
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            }            
        }
    },
    {
        method: 'POST',
        path: '/reportUser',
        handler: postReportUserAPI.handler,
        config: {
            description: 'This API will be used to post Report User ',
            tags: ['api', 'reportUser'],
            auth: 'userJWT',
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: postReportUserAPI.validator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: postReportUserAPI.response
        }
    }
];