'use strict'
const Config = process.env;
const Joi = require("joi");
const async = require("async");
const Cryptr = require('cryptr');
const logger = require('winston');
const Promise = require('promise');
const local = require('../../../locales');
const cryptr = new Cryptr(Config.SECRET_KEY);
const ObjectID = require('mongodb').ObjectID;//to convert stringId to mongodb's objectId
const Timestamp = require('mongodb').Timestamp;
const Auth = require("../../middleware/authentication.js");
const unCompleteUsers = require('../../../models/unCompleteUsers');


const validator = Joi.object({
    contactNumber: Joi.string().required().description("contactNumber with contrycode,Eg. +919871653650").example("+919871653650").error(new Error('contactNumber missing')),
    dob: Joi.number().allow(null).description("Date of Birth(miliseconds),Eg. 1504867670000").example(1504867670000).error(new Error('dob is missing')),
    email: Joi.string().allow("").email().description("Email id,Eg. example@domain.com").example("example@domain.com").error(new Error('email is missing')),
    gender: Joi.string().allow("").description("gender,Eg. Male || Female").example("example@domain.com").error(new Error('gender is missing')),
    name: Joi.string().allow("").description("name ,Eg.Rahul").example("Rahul").error(new Error('name is missing')),
}).required();

const handler = (req, res) => {
    try {
        let contactNumber = req.payload.contactNumber;
        let dob = req.payload.dob;
        let email = req.payload.email;
        let gender = req.payload.gender;
        let name = req.payload.name;

        let condition = { contactNumber: contactNumber };
        let data = {
            dob: dob,
            email: email,
            gender: gender,
            name: name,
            creationTs: new Timestamp(1,Date.now()), 
            creationDate: new Date()
        };

        unCompleteUsers.Upsert(condition, data, (err, result) => {
            if (err) {
                return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
            } else {
                return res({ message: req.i18n.__('UnCompleteUser')['200'] }).code(200);
            }
        });
    } catch (error) {
        return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
    }

};


let response = {
    status: {
        200: { message: local['UnCompleteUser']['200'] },
        400: { message: local['genericErrMsg']['400'] },
        500: { message: local['genericErrMsg']['500'] }
    }
}


module.exports = { validator, handler, response };