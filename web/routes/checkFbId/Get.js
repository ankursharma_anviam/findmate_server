'use strict'
var Joi = require('joi');
var logger = require('winston');

const local = require('../../../locales');
const userListCollection = require('../../../models/userList');

let handler = (req, res) => {


    GetCheckFbId()
        .then((value) => { return res({ message: req.i18n.__('GetCurrentDates')['200'], data: dataToSend }).code(200); })
        .catch(function (err) {
            logger.error('Caught an error!', err);
            return res({ message: req.i18n.__('genericErrMsg')['412'] }).code(412);
        });

    function GetCheckFbId() {
        return new Promise(function (resolve, reject) {

            let condition = { fbId: req.params.fbId, deleteStatus: { $ne: 1 } };
            userListCollection.SelectOne(condition, (err, result) => {
                if (err) {
                    return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
                } else if (result) {
                    return res({ message: req.i18n.__('GetCheckFbId')['200'] }).code(200);
                } else {
                    return res({ message: req.i18n.__('GetCheckFbId')['412'] }).code(412);
                }
            });
        });
    }

}

let validator = Joi.object({
    fbId: Joi.string().description("fbId").error(new Error('fbId is missing')),
})

let response = {
    status: {
        200: { message: Joi.any().default(local['GetCheckFbId']['200']) },
        412: { message: Joi.any().default(local['GetCheckFbId']['412']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
}

module.exports = { handler, validator }