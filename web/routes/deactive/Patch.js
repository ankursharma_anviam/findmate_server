'use strict'
const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../locales');
const fcmPush = require("../../../library/fcm");
const userListType = require('../../../models/userListType');
const dateSchedule = require('../../../models/dateSchedule');
const userListCollection = require('../../../models/userList');

let APIHandler = (req, res) => {

    let _id = req.auth.credentials._id;

    setInMongo()
        .then(function (value) {
            return setInElasticSearch();
        })
        .then(function (value) {
            return deActivateDates();
        })
        .then(function (value) {
            return res({ message: req.i18n.__('PatchDeactivate')['200'] }).code(200);
        })
        .catch(function (err) {
            return res({ message: req.i18n.__('PatchDeactivate')['412'] }).code(412);
        });

    function deActivateDates() {
        return new Promise(function (resolve, reject) {
            let condition = { _id: _id, reason: "Deactive" };
            dateSchedule.deActivateDates(condition, (err, result) => {
                if (err) return reject(new Error('Ooops, something broke!'));

                return resolve("----");
            });
        });
    }

    function setInMongo() {
        return new Promise(function (resolve, reject) {

            userListCollection.UpdateById(_id, {
                isDeactive: true,
                deactivateTime: new Date().getTime(),
                deactivateReason: req.payload.reason
            }, (err, result) => {
                if (err) {
                    return reject(new Error('Ooops, something broke!'));
                }
                else {
                    return resolve(result);
                }
            });
        });
    }

    function setInElasticSearch() {
        return new Promise(function (resolve, reject) {
            userListType.Update(_id, { isDeactive: true }, (err, result) => {
                if (err) {
                    return reject(new Error('Ooops, something broke!'));
                }
                else {
                    return resolve(result);
                }
            });
        });
    }

};

let validator = Joi.object({
    reason: Joi.string().required().error(new Error('reason is missing')),
}).required();

let response = {
    status: {
        200: {
            message: Joi.any().default(local['PatchDeactivate']['200'])
        },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        412: { message: Joi.any().default(local['PatchDeactivate']['412']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}


module.exports = { APIHandler, validator, response }