'use strict'
let headerValidator = require('../../middleware/validator');
let PatchAPI = require('./Patch');

module.exports = [
    {
        method: 'PATCH',
        path: '/deactive',
        handler: PatchAPI.APIHandler,
        config: {
            description: 'This API will be used',
            tags: ['api', 'login'],
            auth: 'userJWT',
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PatchAPI.validator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PatchAPI.response
        }
    }
];