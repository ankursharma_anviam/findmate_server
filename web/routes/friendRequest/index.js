'use strict'
let headerValidator = require('../../middleware/validator');
let friendRequest= require('./Post');
let getAPI = require('./get');
module.exports = [
    {
        method: 'POST',
        path: '/friendRequest',
        handler: friendRequest.handler,
        config: {
            description: 'This API will be used to send the friend request to the user.',
            tags: ['api', 'friendRequest'],
            auth: 'userJWT',
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: friendRequest.validator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);                }
            },
             response: friendRequest.response
        }
    },
    {
        method: 'GET',
        path: '/friendRequest',
        handler: getAPI.APIHandler,
        config: {
            description: 'This API will be used to pull the pending friends requests for that user.',
            tags: ['api', 'friendRequest'],
            auth: 'userJWT',
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: getAPI.validator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: getAPI.response
        }
    },
    
];