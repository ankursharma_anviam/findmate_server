'use strict'
const Joi = require("joi");
const async = require("async");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;
const local = require('../../../locales');
const GeoPoint = require('geopoint')
const userListCollection = require('../../../models/userList');
const friendshipCollection = require('../../../models/friendship');
const fcmPush = require("../../../library/fcm");

/**
 * @method GET like
 * @description This API is used to post friendRequest.
 * @param {*} req 
 * @param {*} res 
 * @property {*} authorization in header
 * @property {*} lang in header
 * @returns  200 : User found in database.
 * @returns  500 : An unknown error has occurred.
 */
let validator = Joi.object({
    targetUserId: Joi.string().required().description("targetUserId").error(new Error('targetUserId is missing'))
}).required();

const handler = (req, reply) => {
    const friendRequest = { message: req.i18n.__('friendRequest')['200'], code: 200 };
    const PendingFriendRequest = { code: 409, message: req.i18n.__('friendRequest')['409'] };
    const AcceptedFriendRequest = { code: 412, message: req.i18n.__('friendRequest')['412'] };
    const dbErrResponse = { message: req.i18n.__('genericErrMsg')['500'], code: 500 };
    var _id = req.auth.credentials._id;
    var targetUserId = req.payload.targetUserId;

    if (req.user && req.user.pendingFriendShipId) {
        if (req.user.pendingFriendShipId.map(e => e.toString()).includes(targetUserId)) return reply({ message: PendingFriendRequest.message }).code(PendingFriendRequest.code);
    }
    if (req.user && req.user.friendShipId) {
        if (req.user.friendShipId.map(e => e.toString()).includes(targetUserId)) return reply({ message: AcceptedFriendRequest.message }).code(AcceptedFriendRequest.code);
    }


    var data = {
        friendRequestSentByerId: new ObjectID(_id),
        friendRequestSentTo: new ObjectID(targetUserId),
        friendRequestSentOn: new Date().getTime(),
        friendRequestSentOnDate: new Date(),
        friendRequestStatus: "Pending"
    }
    //add the friendship detail in friendship collection
    let checkFriendShipStatus = () => {
        return new Promise((resolve, reject) => {

            friendshipCollection.SelectOne({
                $or: [{ friendRequestSentByerId: ObjectID(_id), friendRequestSentTo: ObjectID(targetUserId) },
                { friendRequestSentByerId: ObjectID(targetUserId), friendRequestSentTo: ObjectID(_id) }]
            }, (err, result) => {
                console.log("log user", result);
                if (err) {
                    logger.error("post friendRequest API : ", err);
                    return reject(dbErrResponse);
                }
                if (result && result._id) {
                    if (result.friendRequestStatus === "Pending") {
                        return reject(PendingFriendRequest);

                    }
                    if (result.friendRequestStatus === "Accepted") {
                        return reject(AcceptedFriendRequest);

                    }
                    if (result.friendRequestStatus === "Denied") {
                        return resolve("true");

                    }
                    if (result.friendRequestStatus === "Unfriend") {
                        return resolve("true");
                    }
                }
                else {
                    return resolve(true);
                }

            })
        });
    };
    //add the friendship detail in friendship collection
    let postFriendship = () => {
        return new Promise((resolve, reject) => {

            friendshipCollection.findOneAndUpdate({
                $or: [{ friendRequestSentByerId: ObjectID(_id), friendRequestSentTo: ObjectID(targetUserId) },
                { friendRequestSentByerId: ObjectID(targetUserId), friendRequestSentTo: ObjectID(_id) }]
            }, data, (err, result) => {
                if (err) {
                    logger.error("err: ", err)
                    return reject(dbErrResponse);
                }
                else {
                    return resolve(true);
                }
            })
        });
    };
    //updating the targer user id in the userlist 
    let updateSentRequest = () => {
        return new Promise((resolve, reject) => {
            let dataToUpdate = { pendingFriendShipId: new ObjectID(targetUserId) };
            userListCollection.UpdateById1(_id, dataToUpdate, (err, result) => {
                if (err) return reject(new Error('Ooops, something broke!'));
                if (result) {
                    return resolve(true);
                } else {
                    return reject(new Error('Ooops, something broke!'));
                }
            });
        });
    };
    //updating the own user in the userlist
    let updateFriendRequest = () => {
        return new Promise((resolve, reject) => {
            let dataToUpdate = { pendingFriendShipId: new ObjectID(_id) };
            userListCollection.UpdateById1(targetUserId, dataToUpdate, (err, result) => {
                if (err) return reject(new Error('Ooops, something broke!'));
                if (result) {
                    let deviceType = "1";

                    let payloadUser1 = {
                        notification: {
                            body: "Hey you just got a new friend request from " + req.user.firstName
                            , title: "New friend request"
                        },
                        data: { type: "23", target_id: _id, deviceType: deviceType }
                    }
                    fcmPush.sendPushToTopic("/topics/" + req.payload.targetUserId, payloadUser1, (e, r) => {
                        console.log("e----", e);
                        console.log("r-----", r);
                    });

                    return resolve(true);
                } else {
                    return reject(new Error('Ooops, something broke!'));
                }
            });
        });
    };

    checkFriendShipStatus()
        .then((data) => { return postFriendship() })
        .then((data) => { return updateSentRequest() })
        .then((data) => { return updateFriendRequest() })
        .then((data) => { return reply({ message: req.i18n.__('friendRequest')['200'] }).code(200); })
        .catch((err) => {
            console.log("post friendRequest API error : ", err);
            return reply({ message: err.message }).code(err.code);
        });
};

let response = {
    status: {
        200: { message: Joi.any().default(local['friendRequest']['200']) },
        409: { message: Joi.any().default(local['friendRequest']['409']) },
        412: { message: Joi.any().default(local['friendRequest']['412']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },

    }
}

module.exports = { handler, response, validator }
