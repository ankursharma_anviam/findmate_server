'use strict'
const Joi = require("joi");
const moment = require("moment");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;
const local = require('../../../locales');
const userListCollection = require('../../../models/userList');
const coinWalletCollection = require('../../../models/coinWallet');
const coinConfigCollection = require('../../../models/coinConfig');
const walletCustomerCollection = require('../../../models/walletCustomer');

/**
 * @method POST trigger
 * @description This API use to  POST unmatch.
 
 * @param {*} req 
 * @param {*} res 
 
 * @property {*} authorization in header
 * @property {*} lang in header
 * @property {*} targetUserId targetUserId
 
 * @returns  200 : User unmatched successfully.
 * @returns  400 : Value in any of the  mandatory field's is missing.
 * @returns  422 : targetUserId  doesnot exist in the database,try with the different ID.
 * @returns  500 : An unknown error has occurred.
 */
let handler = (req, res) => {

    let _id = req.auth.credentials._id;
    let timestamp = new Date().getTime();
    let trigger = "";

    switch (req.payload.triggerCode) {
        case 1: {
            trigger = "perMsgWithoutMatch";
            break;
        }
        case 2: {
            trigger = "boostProfileForADay";
            break;
        }
        case 3: {
            trigger = "callDateInitiated";
            break;
        }
        case 4: {
            trigger = "inPersonDateInitiated";
            break;
        }

    }
    function getRequiredCoinForTrigger() {
        return new Promise((resolve, reject) => {
            coinConfigCollection.SelectOne({}, (err, result) => {
                if (err) return reject(new Error('Ooops, no have data in coinConfig!'));

                return resolve(result[trigger] || {});
            });
        });
    }
    function getUsersCoinFromCoinWalletAndValidate(requiredCoin) {
        return new Promise((resolve, reject) => {
            coinWalletCollection.SelectOne({ _id: ObjectID(_id) }, (err, result) => {
                if (err) return reject(new Error('Ooops, no have data in coinConfig!'));

                for (let key in requiredCoin) {
                    if (requiredCoin[key] == 0 || (result && result["coins"][key] && requiredCoin[key] <= result["coins"][key])) {
                    } else {
                        return reject(new Error('Ooops, insufficient balance!'));
                    }
                }
                return resolve({ coinWallet: result["coins"] || {}, requiredCoin: requiredCoin });
            });
        });
    }
    function insertInWalletCusetomerAndUpdateInCoinWallet(data) {
        return new Promise((resolve, reject) => {
            let coinWallet = data.coinWallet;
            let requiredCoin = data.requiredCoin;

            let insertData = [];
            let txnId = "TXN-ID-" + Math.floor(Math.random() * 10000000000);
            for (let key in requiredCoin) {

                if (coinWallet[key]) {
                    insertData.push({
                        "txnId": txnId,
                        "userId": ObjectID(_id),
                        "txnType": "DEBIT",
                        "trigger": trigger,
                        "currency": "N/A",
                        "currencySymbol": "N/A",
                        "coinType": key,
                        "coinOpeingBalance": coinWallet[key],
                        "cost": 0,
                        "coinAmount": requiredCoin[key],
                        "coinClosingBalance": (coinWallet[key] - requiredCoin[key]),
                        "paymentType": "N/A",
                        "timestamp": timestamp,
                        "transctionTime": timestamp,
                        "transctionDate": timestamp,
                        "paymentTxnId": "N/A",
                        "initatedBy": "customer"
                    });
                    coinWallet[key] = coinWallet[key] - requiredCoin[key];
                }
            }


            walletCustomerCollection.InsertMany(insertData, () => { });
            coinWalletCollection.Update({ _id: ObjectID(_id) }, { "coins": coinWallet }, () => { });
            return resolve("---");
        });
    }

    getRequiredCoinForTrigger()
        .then(function (value) { return getUsersCoinFromCoinWalletAndValidate(value) })
        .then(function (coinWallet, requiredCoin) { return insertInWalletCusetomerAndUpdateInCoinWallet(coinWallet, requiredCoin) })
        .then(function (value) { return res({ message: req.i18n.__('PostTrigger')['200'] }).code(200); })
        .catch(function (err) {
            logger.error('Caught an error!', err);
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        });

}
let response = {
    status: {
        200: { message: local['PostTrigger']['200'] },
        400: { message: local['genericErrMsg']['400'] },
        500: { message: local['genericErrMsg']['500'] },
    }
}


let validator = Joi.object({
    triggerCode: Joi.number().required().allow([1, 2, 3, 4]).description("eg. 1:perMsgWithoutMatch, 2:boostProfileForADay, 3:callDateInitiated, 4:inPersonDateInitiated").error(new Error('triggerCode is missing')),
}).required();

module.exports = { handler, response, validator }