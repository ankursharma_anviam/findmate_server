const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const local = require('../../../locales');
const ObjectID = require('mongodb').ObjectID;
const fcmPush = require("../../../library/fcm");
const makeASuggestion = require('../../../models/makeASuggestion');


let APIHandler = (req, res) => {

    let _id = req.auth.credentials._id;

    setInMongo()
        .then(function (value) {
            return res({ message: req.i18n.__('PostMakeASuggestion')['200'] }).code(200);
        })
        .catch(function (err) {
            logger.error('Caught an error!', err);
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        });


    function setInMongo() {
        return new Promise(function (resolve, reject) {

            let dataToInsert = {
                userId: ObjectID(_id),
                suggestion: req.payload.description
            }
            makeASuggestion.Insert(dataToInsert, (err, result) => {
                if (err) {
                    return reject(new Error('Ooops, something broke!'));
                }
                else {
                    return resolve(result);
                }
            });
        });
    }
};

let validator = Joi.object({
    description: Joi.string().required().description("ex : Rahul is not a spam user").example("Rahul is not a spam user").error(new Error('description is missing')),
}).required();

let response = {
    status: {
        200: { message: Joi.any().default(local['PostAskAQuestion']['200']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) },
    }
}

module.exports = { APIHandler, validator,response }