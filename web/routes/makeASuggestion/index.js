
let headerValidator = require('../../middleware/validator');
let PostAPI = require('./Post');

module.exports = [
    {
        method: 'POST',
        path: '/makeASuggestion',
        handler: PostAPI.APIHandler,
        config: {
            description: 'This API will be used',
            tags: ['api', 'contactUs'],
            auth: 'userJWT',
            response: PostAPI.response,
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PostAPI.validator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            }
        }
    }
];