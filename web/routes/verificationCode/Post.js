'use strict'
const Joi = require("joi");
const async = require("async");
const logger = require('winston');

const local = require('../../../locales');
const ObjectID = require('mongodb').ObjectID;
const otps_mongoDB = require('../../../models/otps');
const coinConfig = require('../../../models/coinConfig');
const coinWallet = require('../../../models/coinWallet');
const Auth = require("../../middleware/authentication.js");
const userList_mongoDB = require('../../../models/userList');
const userListType_ES = require('../../../models/userListType');
const userPrefrances = require('../../../models/userPrefrances');
const userDevicesCollection = require('../../../models/userDevices');
const searchPreferencesCollection = require('../../../models/searchPreferences');
const rabbitMq = require('../../../models/rabbitMq');
const fcm = require('../../../library/fcm');
const rabbitLatLongToCity = require('../../../models/rabbitMq/latLongToCity');

const Config = process.env;
const Timestamp = require('mongodb').Timestamp;

let validator = Joi.object({
    phoneNumber: Joi.string().required().description("Country Code and Phonenumber(without space), Eg. +919620826142").error(new Error('phoneNumber is missing')),
    type: Joi.number().required().min(1).max(2).description("Type-1(New registration || login), Type-2(Recovery password), Eg. 1/2").error(new Error('type is missing')),
    otp: Joi.number().required().description("Enter OTP(4-digit), Eg. 1111").error(new Error('otp is missing')),
    pushToken: Joi.string().allow("").description("Push Token, Eg. dY0xOny_uEA:APA91bE_HZQiTq").error(new Error('pushToken is missing')),
    deviceId: Joi.string().allow("").description("Device Id, Eg. 157875de315458000000000000000").error(new Error('deviceId is missing')),
    deviceMake: Joi.string().allow("").description("Device Make/Company, Eg. Samsung/Apple").error(new Error('deviceMake is missing')),
    deviceModel: Joi.string().allow("").description("Device Model Number, Eg. SM-N920T").error(new Error('deviceModel is missing')),
    deviceType: Joi.string().allow("").description("Device Type(1-IOS and 2-Android), Eg. 2").error(new Error('deviceType is missing')),
    deviceOs: Joi.string().allow("").description("deviceOs, Eg. Oreo").error(new Error('deviceOs is missing')),
    appVersion: Joi.string().allow("").description("appVersion, Eg. 1.0.0").error(new Error('appVersion is missing')),
    longitude: Joi.number().description("longitude").error(new Error('longitude is missing or invalid')),
    latitude: Joi.number().description("latitude").error(new Error('latitude is missing or invalid')),

});

let handler = (req, res) => {

    let phoneNumber = req.payload.phoneNumber;
    let randomCode = req.payload.otp;
    let dataToSend = { isNewUser: true };
    let coinWalletObj = {};

    try {
        if (req.payload.type == 1) {
            checkOTP()
                .then((value) => { return checkUser(); })
                .then((value) => { return getCoinBalance(); })
                .then((value) => { return getCoinConfig(); })
                .then((value) => {
                    return res({
                        message: req.i18n.__('PostVerificationCode')['200'], 
                        data: dataToSend,
                        coinConfig: value.coinConfig || {},
                        coinWallet: coinWalletObj[0].coins
                    }).code(200);
                })
                .catch((err) => {
                    logger.error('Caught an error!', err);
                    return res({ message: err.message });
                });
        } else if (req.payload.type == 2) {
            return res({ message: req.i18n.__('PostVerificationCode')['412'] }).code(412);
        }

    } catch (error) {
        return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
    }
    function checkOTP() {
        return new Promise(function (resolve, reject) {
            let condition = [
                { "$match": { "phone": phoneNumber, "type": req.payload.type, "otp": randomCode } },
                { "$sort": { time: -1 } }, { "$limit": 1 }
            ];

            otps_mongoDB.Aggregate(condition, (err, result) => {
                if (err) return reject({ message: req.i18n.__('genericErrMsg')['500'], code: 500 });

                if (result.length) {
                    return resolve(result[0]);
                } else {
                    return reject({ message: req.i18n.__('PostVerificationCode')['412'], code: 412 });
                }
            });
        });
    }
    function checkUser() {
        return new Promise(function (resolve, reject) {

            let condition = { "contactNumber": phoneNumber, deleteStatus: { "$ne": 1 } };

            userList_mongoDB.Select(condition, (err, result) => {
                if (err) return reject({ message: req.i18n.__('genericErrMsg')['500'], code: 500 });

                if (result && result[0] && result[0].profileStatus) {
                    return reject({ message: req.i18n.__('genericErrMsg')['403'], code: 403 });
                } else if (result && result[0]) {

                    let cur = new Date();
                    let diff = cur - result[0].dob; // This is the difference in milliseconds
                    let age = Math.floor(diff / 31557600000); // Divide by 1000*60*60*24*365.25
                    sendFCMNotificationForLogOutFromOtherDevices({ _id: result[0]._id.toString(), deviceId: req.payload.deviceId })
                    dataToSend["_id"] = result[0]._id;
                    dataToSend["countryCode"] = result[0].countryCode;
                    dataToSend["contactNumber"] = result[0].contactNumber;
                    dataToSend["firstName"] = result[0].firstName;
                    dataToSend["dob"] = result[0].dob;
                    dataToSend["profilePic"] = result[0].profilePic;
                    dataToSend["gender"] = result[0].gender;
                    dataToSend["findMateId"] = result[0].findMateId;
                    let token = Auth.SignJWT({ _id: result[0]._id, key: 'acc' }, 'user', 60000000);
                    dataToSend["token"] = token;
                    dataToSend["isNewUser"] = false;
                    dataToSend["height"] = result[0].height;
                    dataToSend["email"] = result[0].email;
                    dataToSend["profileVideo"] = result[0].profileVideo;
                    dataToSend["profileVideoThumbnail"] = result[0].profileVideoThumbnail;
                    dataToSend["otherImages"] = result[0].otherImages;
                    dataToSend["about"] = result[0].about;
                    dataToSend["instaGramProfileId"] = result[0].instaGramProfileId || "";
                    dataToSend["instaGramToken"] = result[0].instaGramToken || "";
                    dataToSend["eduation"] = "";
                    dataToSend["work"] = "";
                    dataToSend["job"] = "";
                    dataToSend["age"] = { value: age, isHidden: result[0].dontShowMyAge || false };
                    dataToSend["distance"] = { value: 0, isHidden: result[0].dontShowMyDist || false };
                    dataToSend["subscription"] = result[0]["subscription"] || [];
                    dataToSend["count"] = result[0]["count"] || {};
                    dataToSend["isPassportLocation"] = result[0]["isPassportLocation"] || false;
                    dataToSend["location"] = result[0]["location"];
                    dataToSend["country"] = result[0].address ? result[0].address.country : "";
                    dataToSend["countryShortName"] = result[0].address ? result[0].address.countryCode : "";
                    dataToSend["city"] = result[0].address ? result[0].address.city : "";

                    let myPrefrances = [];
                    let prefrances = {}
                    let _id = result[0]._id;
                    let searchPreferencesObj = {};
                    let searchPreferences = [];

                    if (typeof req.payload.latitude == 'undefined' || typeof req.payload.longitude == 'undefined') {

                    } else {
                        rabbitLatLongToCity.InsertQueue(rabbitMq.getChannelLatLongToCity(), rabbitMq.queueLatLongToCity,
                            { lat: req.payload.latitude, lon: req.payload.longitude, _id: result[0]._id },
                            (err, doc) => { });
                    }

                    async.series([
                        function (callback) {
                            userPrefrances.Select({}, (err, data) => {
                                myPrefrances = data;
                                for (let index = 0; index < result[0].myPreferences.length; index++) {
                                    prefrances[result[0].myPreferences[index]["pref_id"]] = result[0].myPreferences[index];

                                }

                                for (let index = 0; index < myPrefrances.length; index++) {
                                    myPrefrances[index]["pref_id"] = myPrefrances[index]["_id"];

                                    myPrefrances[index]["isDone"] = (prefrances[myPrefrances[index]["_id"]]) ? prefrances[myPrefrances[index]["_id"]]["isDone"] : false
                                    myPrefrances[index]["selectedValues"] = (prefrances[myPrefrances[index]["_id"]]) ? prefrances[myPrefrances[index]["_id"]]["selectedValues"] : [];

                                    if (myPrefrances[index]["_id"] == "5a30fda027322defa4a14638"
                                        && myPrefrances[index]["isDone"]) {
                                        dataToSend["work"] = myPrefrances[index]["selectedValues"][0];
                                    }
                                    if (myPrefrances[index]["_id"] == "5a30fdd127322defa4a14649"
                                        && myPrefrances[index]["isDone"]) {
                                        dataToSend["job"] = myPrefrances[index]["selectedValues"][0];
                                    }
                                    if (myPrefrances[index]["_id"] == "5a30fdfa27322defa4a14653"
                                        && myPrefrances[index]["isDone"]) {
                                        dataToSend["eduation"] = myPrefrances[index]["selectedValues"][0];
                                    }
                                }

                                let myPreferencesByGroupObj = {};
                                for (let index = 0; index < myPrefrances.length; index++) {
                                    if (myPreferencesByGroupObj[myPrefrances[index]["title"]]) {
                                        myPreferencesByGroupObj[myPrefrances[index]["title"]].push(myPrefrances[index]);
                                    } else {
                                        myPreferencesByGroupObj[myPrefrances[index]["title"]] = [myPrefrances[index]];
                                    }
                                }
                                let myPreferencesByGroupArray = [];
                                for (let key in myPreferencesByGroupObj) {
                                    myPreferencesByGroupArray.push({
                                        "title": key,
                                        "data": myPreferencesByGroupObj[key]
                                    });
                                }

                                myPrefrances = myPreferencesByGroupArray;

                                callback(err, data);
                            })
                        },
                        function (callback) {
                            searchPreferencesCollection.Select({}, (err, result) => {
                                if (result[0]) {
                                    let pref_id = "";
                                    for (let index = 0; index < result.length; index++) {
                                        pref_id = result[index]._id;
                                        searchPreferencesObj[pref_id] = result[index];
                                        searchPreferencesObj[pref_id]["selectedValue"] = searchPreferencesObj[pref_id]["OptionsValue"];
                                        delete searchPreferencesObj[pref_id]["ActiveStatus"];
                                        if (result[index]["TypeOfPreference_User"]) {
                                            delete searchPreferencesObj[pref_id]["TypeOfPreference_User"];
                                        }
                                    }
                                }
                                callback(null, 1);
                            })
                        },
                        function (callback) {
                            for (let index = 0; index < result[0].searchPreferences.length; index++) {
                                let pref_id = result[0].searchPreferences[index].pref_id;
                                if (searchPreferencesObj[pref_id]) {
                                    searchPreferencesObj[pref_id]["selectedValue"] = result[0].searchPreferences[index]["selectedValue"];
                                    if (result[0].searchPreferences[index]["selectedUnit"]) {
                                        searchPreferencesObj[pref_id]["selectedUnit"] = result[0].searchPreferences[index]["selectedUnit"];
                                    } else if (searchPreferencesObj[pref_id]["TypeOfPreference"] == "3" || searchPreferencesObj[pref_id]["TypeOfPreference"] == "4") {
                                        searchPreferencesObj[pref_id]["selectedUnit"] = result[0].searchPreferences[index]["selectedUnit"];
                                    }
                                }
                            }
                            for (let pref_id in searchPreferencesObj) {
                                searchPreferences.push(searchPreferencesObj[pref_id])
                            }
                            callback(null, 1);
                        },
                        function (callback) {
                            let deviceData = {
                                userId: _id,
                                creationDate: new Date(),
                                deviceId: req.payload.deviceId || "",
                                pushToken: req.payload.pushToken || "",
                                deviceMake: req.payload.deviceMake || "",
                                deviceModel: req.payload.deviceModel || "",
                                deviceType: req.payload.deviceType || "",
                                deviceOs: req.payload.deviceOs || "",
                                appVersion: req.payload.appVersion || "",
                                DevicetypeMsg: (req.payload.deviceType == "1") ? "iOS" : "Android",

                            };
                            /* insert into userDevices collection*/

                            userDevicesCollection.Insert(deviceData, (err, result) => {
                                callback(err, result);
                            })

                        },
                        function (callback) {
                            let deviceData = {
                                currentLoggedInDevices: {
                                    deviceId: req.payload.deviceId || "",
                                    pushToken: req.payload.pushToken || "",
                                    creationDate: new Date(),
                                    deviceMake: req.payload.deviceMake || "",
                                    deviceModel: req.payload.deviceModel || "",
                                    deviceType: req.payload.deviceType || "",
                                    deviceOs: req.payload.deviceOs || "",
                                    appVersion: req.payload.appVersion || "",
                                    DevicetypeMsg: (req.payload.deviceType == "1") ? "iOS" : "Android",
                                }
                            };
                            /* update  into userList type in elastic search*/
                            userListType_ES.Update(_id, deviceData, () => { });
                            /* update  into userList  collection*/

                            userList_mongoDB.UpdateById(_id, deviceData, (err, result) => {
                                callback(err, result);
                            });
                        }
                    ], () => {
                        dataToSend["myPreferences"] = myPrefrances;
                        dataToSend["searchPreferences"] = searchPreferences;
                        return resolve("--");
                    })

                } else {
                    return resolve("--");
                }
            });
        });
    }
    function sendFCMNotificationForLogOutFromOtherDevices(data) {
        
        let payloadData = {
            notification: {
                body: "you just login with other device.",
                title: "Logout"
            },
            data: { type: "22", deviceId: data.deviceId }
        }
        fcm.sendPushToTopic("/topics/" + data._id, payloadData, (e, r) => {
            if (e) logger.error(e);
            //  console.log("r-----", r);
        });
    }
    function getCoinConfig() {
        return new Promise(function (resolve, reject) {
            coinConfig.Select({}, (err, result) => {
                if (err) return reject(new Error('Ooops, something broke! at getCoinConfig'));

                return resolve({ coinConfig: result[0] || { coinConfig: {} } });
            });
        });
    }
    function getCoinBalance() {
        return new Promise(function (resolve, reject) {
            coinWallet.Select({ _id: ObjectID(dataToSend["_id"]) }, (err, result) => {
                if (err) return reject(new Error('Ooops, something broke! at getCoinConfig'));

                coinWalletObj = (dataToSend.isNewUser) ? [{ coins: { "Coin": 0 } }] : result;
                return resolve(true);
            });
        });
    }
};

module.exports = { handler, validator }