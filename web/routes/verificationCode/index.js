'use strict'
let postAPI = require('./Post');
let headerValidator = require("../../middleware/validator");

module.exports = [
    {
        method: 'POST',
        path: '/verificationCode',
        handler: postAPI.handler,
        config: {
            description: 'This API will check otp is correct or not.',
            tags: ['api', 'verificationCode'],
            auth: false,
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: postAPI.validator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: postAPI.response
        }
    }
];