'use strict'
const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;
const Timestamp = require('mongodb').Timestamp;
const local = require('../../../locales');
const messagesCollection = require('../../../models/messages');
const userListCollection = require('../../../models/userList');
const walletCustomerCollection = require('../../../models/walletCustomer');

const chatLikesCollection = require('../../../models/chatList');
const userMatchCollection = require('../../../models/userMatch');
const coinWalletCollection = require('../../../models/coinWallet');
const coinConfigCollection = require('../../../models/coinConfig');


let validator = Joi.object({
    targetUserId: Joi.string().required().min(24).max(24).description("targetUserId").error(new Error('targetUserId is missing')),
    msgToSend: Joi.string().required().description("msgToSend").error(new Error('msgToSend is missing')),
}).required();
/**
 * @method POST like
 * @description This API use to like a user.
 * @param {*} req 
 * @param {*} res 
 * @property {*} authorization in header
 * @property {*} lang in header
 * @property {*} targetUserId targetUserId,Eg. 5a14013b7b334c19349aec7b
 */
let APIHandler = (req, res) => {

    let _id = req.auth.credentials._id;
    let targetUserId = req.payload.targetUserId;
    var trigger = "perMsgWithoutFriend",data={};

    targetUserExists()
    .then(function (value) { return getRequiredCoinForTrigger(); })
    .then(function (value) { return getUsersCoinFromCoinWalletAndValidate(value); })    
    .then(function (value) { return processToMatch(); })
    .then(function (value) { return insertInWalletCusetomerAndUpdateInCoinWallet(value); })
        .then(function (value) {
            return res({ message: req.i18n.__('PostChatWithoutMatch')['200'], data: value }).code(200);
        }).catch(function (err) {
            logger.silly('Caught an error!', err);
            return res({ message: err.message }).code(err.code);
        });


    function targetUserExists() {
            return new Promise(function (resolve, reject) {
                userListCollection.SelectById({ _id: targetUserId }, { _id: 1, firstName: 1, profilePic: 1, currentLoggedInDevices: 1 }, (err, result) => {
                    if (err) return reject(new Error('Ooops, something broke!'));
    
                    if (result) {
                        return resolve(result);
                    } else {
                        userListType.Delete("userList", { _id: targetUserId }, () => { });
                        return reject({"message":req.i18n.__('genericErrMsg')['204'],code:204});
                    }
                });
            });
        }
    function getRequiredCoinForTrigger() {
        return new Promise((resolve, reject) => {
            coinConfigCollection.SelectOne({}, (err, result) => {
                if (err) return reject(new Error('Ooops, no have data in coinConfig!'));
                return resolve(result[trigger] || {});
            });
        });
    }
    function getUsersCoinFromCoinWalletAndValidate(requiredCoin) {
        return new Promise((resolve, reject) => {
            coinWalletCollection.SelectOne({
                _id: ObjectID(_id)
            }, (err, result) => {
                if (err) return reject({
                    code: 500,
                    message: req.i18n.__('genericErrMsg')['500']
                });

                logger.silly("requiredCoin ", requiredCoin)
                logger.silly("result   ", result)
                for (let key in requiredCoin) {
                    if (requiredCoin[key] == 0 || (result && result["coins"][key] && requiredCoin[key] <= result["coins"][key])) {} else {
                        return reject({
                            code: 402,
                            message: req.i18n.__('genericErrMsg')['402']
                        });
                    }
                }
                data["coinWallet"] = result["coins"] || {};
                data["requiredCoin"] = requiredCoin;
                return resolve(true);
            });
        });
    }      
    function processToMatch() {
        return new Promise(function (resolve, reject) {

            let userId = "members."+_id;
            let targetId = "members."+targetUserId
            let condition = {
                "isMatchedUser": 3,
                [userId]:{"$exists" : true},
                [targetId]:{"$exists" : true}
            };

chatLikesCollection.SelectOne(condition,(err,result)=>{
    if (err) return reject(new Error('Ooops, something broke!'));

    

    var chatId = new ObjectID();
    let msg_id = new ObjectID();

    if(result && result._id){
        chatId = ObjectID(result._id);
        
        let condition = { "_id": chatId};
        let dataToPush = {
            "message": msg_id,
        };
        chatLikesCollection.UpdateByIdWithPush(condition,dataToPush, (e, r) => { if(e)logger.error("SODJKOSD ",e) });
        let userId = "members."+_id+".inactive";
        let targetId = "members."+targetUserId+".inactive";
        let unsetInactiveStatus = {
           [userId]:"",
          [targetId]:""
        }
        chatLikesCollection.UpdatewithUnset(condition,unsetInactiveStatus, (e, r) => { if(e)logger.error("SODJSDSSD ",e) });
        
    }else{

        let dataToInsert = {
            "_id": chatId,
            "message": [msg_id],
            "members": {
                [_id]: {
                    "status": "NormalMember"
                },
                [targetUserId]: {
                    "status": "NormalMember"
                }
            },
            "initiatedBy": ObjectID(_id),
            "createdAt": new Date().getTime(),
            "chatType": "NormalChat",
            "secretId": "",
            "isMatchedUser": 3,
            "matchUserStatus":"chatWithoutFriend",
            creationTs: new Timestamp(),
            creationDate: new Date()
        };
        chatLikesCollection.Insert(dataToInsert, (e, r) => { });
        
    }
    let newMessage = {
        "_id": msg_id,
        "messageId": "" + new Date().getTime(),
        "secretId": "",
        "dTime": -1,
        "senderId": ObjectID(_id),
        "receiverId": ObjectID(targetUserId),
        "payload": req.payload.msgToSend,
        "messageType": "0",
        "timestamp": new Date().getTime(),
        "expDtime": 0,
        "chatId": chatId,
        "userImage": req.user.profilePic,
        "toDocId": "test message",
        "name": req.user.firstName,
        "dataSize": null,
        "thumbnail": null,
        "mimeType": null,
        "extension": null,
        "fileName": null,
        "isWithoutMatch": true,
        "members": {
            [_id]: {

            },
            [targetUserId]: {
                "status": 0
            }
        },
        creationTs: new Timestamp(),
        creationDate: new Date(),
    }
    messagesCollection.Insert(newMessage, (e, r) => { })
    return resolve({ chatId: chatId });
})
        });
    }
    function insertInWalletCusetomerAndUpdateInCoinWallet(value) {
        return new Promise((resolve, reject) => {
            let coinWallet = data.coinWallet;
            let requiredCoin = data.requiredCoin;

            let insertData = [];
            let txnId = "TXN-ID-" + Math.floor(Math.random() * 10000000000);
            var docTypeCode = 0;
            var docType = "";
            for (let key in requiredCoin) {

                if (coinWallet[key]) {
                        trigger = `Spent ${requiredCoin[key]} coins on message without friend`;
                        docTypeCode = 6;
                        docType = "on message without friend";
                    
                    insertData.push({
                        "txnId": txnId,
                        "userId": ObjectID(_id),
                        "txnType": "DEBIT",
                        "txnTypeCode": 2,
                        "trigger": trigger,
                        "docType": docType,
                        "docTypeCode": docTypeCode,
                        "currency": "N/A",
                        "currencySymbol": "N/A",
                        "coinType": key,
                        "coinOpeingBalance": coinWallet[key],
                        "cost": 0,
                        "coinAmount": requiredCoin[key],
                        "coinClosingBalance": (coinWallet[key] - requiredCoin[key]),
                        "paymentType": "N/A",
                        "timestamp": new Date().getTime(),
                        "transctionTime": new Date().getTime(),
                        "transctionDate": new Date().getTime(),
                        "paymentTxnId": "N/A",
                        "initatedBy": "customer"
                    });
                    coinWallet[key] = coinWallet[key] - requiredCoin[key];
                }
            }
            walletCustomerCollection.InsertMany(insertData, () => {});
            coinWalletCollection.Update({
                _id: ObjectID(_id)
            }, {
                "coins": coinWallet
            }, () => {});
            coinWallet = data;
            return resolve(value);
        });
    }
};

let response = {
    status: {
        200: { message: Joi.any().default(local['PostChatWithoutMatch']['200']), data: Joi.any() },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        402: { message: Joi.any().default(local['genericErrMsg']['402']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) },
    }
}

module.exports = { APIHandler, validator, response }