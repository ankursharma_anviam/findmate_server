'use strict'
const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../locales');
const userBoostCollection = require('../../../models/userBoost');

/**
 * @method PATCH boost
 * @description This API use to rewind a user.
 
 * @param {*} req 
 * @param {*} res 
 
 * @property {*} authorization in header
 * @property {*} lang in header
 * @property {*} lang targetUserId
 
 * @returns  400 : Value in any of the  mandatory field's is missing.
 * @returns  500 : An unknown error has occurred.
 */
let APIHandler = (req, res) => {
    try {
        var _id = req.auth.credentials._id;
        if (req.user.boost == null) return res({ message: req.i18n.__('GetWithLike')['204'] }).code(204);
        var boostId = ObjectID(req.user.boost._id);

        getBoostData()
            .then((data) => { return res({ message: data.message, data: data.data }).code(data.code); })
            .catch((err) => {
                logger.error(err);
                return res({ message: err.message }).code(err.code);
            });

    } catch (error) {
        logger.error("in Get Boost : ", error);
        return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
    }

    function getBoostData() {
        return new Promise((resolve, reject) => {

            let condition = [
                { "$match": { "_id": boostId } },
                { "$unwind": "$likes" },
                { "$lookup": { "from": "userList", "localField": "likes.userId", "foreignField": "_id", "as": "userData" } },
                { "$unwind": "$userData" },
                {
                    "$project": {
                        "_id": "$userData._id", "firstName": "$userData.firstName", "countryCode": "$userData.countryCode",
                        "contactNumber": "$userData.contactNumber", "profilePic": "$userData.profilePic", "email": "$userData.email",
                    }
                }
            ]
            userBoostCollection.Aggregate(condition, (err, result) => {
                if (err) {
                    console.log("err ", err)
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                }

                return (result && result.length)
                    ? resolve({ code: 200, message: req.i18n.__('GetWithLike')['200'], data: result })
                    : reject({ code: 204, message: req.i18n.__('GetWithLike')['204'] });
            });
        });
    }
};

let response = {
    status: {
        200: { message: Joi.any().default(local['GetWithLike']['200']), data: Joi.any() },
        204: { message: Joi.any().default(local['GetWithLike']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
}
module.exports = { APIHandler, response }