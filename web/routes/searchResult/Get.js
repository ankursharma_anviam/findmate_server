'use strict'
const Joi = require("joi");
const async = require("async");
const logger = require('winston');
const moment = require("moment");
const cronJobTask = require('../../../cronJobTask');
const userListType = require('../../../models/userListType');
const redisClient = require('../../../models/redisDB').client;

const myLibrary = require("../../../library/myLibrary");

/**
 * @method GET searchResult
 * @description This API use to get searchResult.
 
 * @param {*} req 
 * @param {*} res 
 
 * @property {*} authorization in header
 * @property {*} lang in header

 */
let APIHandler = (req, res) => {

    let _id = req.auth.credentials._id;
    const likeRefreshInMillisecond = cronJobTask.likeRefreshInMillisecond;
    let limit = parseInt(req.query.limit) || 20;
    let skip = parseInt(req.query.offset) || 0;
    let remainsLikesInString = "0", remainsRewindsInString = "0";

    let favoritePreferences = [], dataToSend = [], matcheResult = [];
    let gender = "Male,Female", heightMin = 100, heightMax = 300, dobMin = 1504867670000, dobMax = 1804867670000, distanceMin = 1,
        distanceMax = 500, latitude = 77.589864, longitude = 13.028712, contactNumber = "123", fbId = "123", must_not = [];
    let myLikes = [], matches = [], myDisLikes = [], myBlock = [], mySupperLike = [];

    try {

        async.series([
            function (callback) {
                if (req.user) {
                    favoritePreferences = req.user.searchPreferences;
                    latitude = req.user.location.latitude || 77;
                    longitude = req.user.location.longitude || 13;
                    contactNumber = req.user.contactNumber || req.user.fbId;
                    fbId = req.user.fbId;
                    myLikes = req.user.myLikes || [];
                    mySupperLike = req.user.mySupperLike || [];
                    matches = req.user.matchedWith || [];
                    myDisLikes = req.user.myunlikes || [];
                    // myBlock = req.user.myBlock || [];
                } else {
                    return res({ message: req.i18n.__('genericErrMsg')['401'] }).code(401);
                }
                return callback(null, true);
            },
            function (callback) {
                if (favoritePreferences) {
                    for (let index = 0; index < favoritePreferences.length; index++) {
                        if (favoritePreferences[index].pref_id == "57231014e8408f292d8b4567") {
                            gender = favoritePreferences[index].selectedValue.toString();
                        } else if (favoritePreferences[index].pref_id == "58bfb210849239b062ca9db4") {
                            heightMin = (favoritePreferences[index].selectedValue[0] < 100) ? 100 : favoritePreferences[index].selectedValue[0];
                            heightMax = (favoritePreferences[index].selectedValue[1] < 100) ? 300 : favoritePreferences[index].selectedValue[1];
                        } else if (favoritePreferences[index].pref_id == "55d486a5f6523c582bc51c53") {
                            dobMin = favoritePreferences[index].selectedValue[0];
                            dobMax = favoritePreferences[index].selectedValue[1];

                            let MaxTimeStamp = moment().add(-dobMin, "y").valueOf();
                            let MinTimeStamp = moment().add(-(dobMax + 1), "y").valueOf() - 1;

                            dobMin = (favoritePreferences[index].selectedValue[1] == 55) ? -2179594491000 : MinTimeStamp;
                            dobMax = MaxTimeStamp
                        } else if (favoritePreferences[index].pref_id == "55d48f9bf6523cc552c51c52") {
                            distanceMin = (favoritePreferences[index].selectedValue[0] || 0) + "km";
                            distanceMax = (favoritePreferences[index].selectedValue[1] || 100) + "km";
                        }
                    }
                }
                return callback(null, 1);
            },
            function (callback) {
                redisClient.hgetall(_id, (err, data) => {
                    for (let _id in data) {
                        must_not.push({ "match": { "_id": _id } });
                    }
                    logger.silly("must_not redis ", must_not.length)
                    callback(null, true)
                })
            },
            function (callback) {
                must_not.push({ "match": { "contactNumber": contactNumber || "12345" } });
                must_not.push({ "match": { "fbId": fbId || "-11111" } });
                must_not.push({ "match": { "profileStatus": 1 } });
                must_not.push({ "match": { "deleteStatus": 1 } });
                if (myLikes) myLikes.map(id => { must_not.push({ "match": { "_id": id } }) })
                if (matches) matches.map(id => { must_not.push({ "match": { "_id": id } }) })
                if (myDisLikes) myDisLikes.map(id => { must_not.push({ "match": { "_id": id } }) })
                if (myBlock) myBlock.map(id => { must_not.push({ "match": { "_id": id } }) })
                if (mySupperLike) mySupperLike.map(id => { must_not.push({ "match": { "_id": id } }) })
                return callback(null, true);
            },
            function (callback) {
                userListType.findMatch({
                    gender: gender, heightMin: heightMin, heightMax: heightMax, dobMin: dobMin,
                    dobMax: dobMax, longitude: longitude, latitude: latitude, distanceMin: distanceMin,
                    distanceMax: distanceMax, contactNumber: contactNumber, must_not: must_not,
                    skip: skip, limit: limit
                }, (err, result) => {
                    logger.error("matcheResult ", err);
                    matcheResult = (result.hits) ? result.hits.hits : [];
                    // logger.silly("matcheResult total ", JSON.stringify(matcheResult))
                    logger.silly("matcheResult total ", matcheResult.length)
                    callback(err, result);
                });
            },
            function (callback) {
                for (let index = 0; index < matcheResult.length; index++) {
                    let birthdate = new Date(matcheResult[index]["_source"]["dob"]);
                    let cur = new Date();
                    let diff = cur - birthdate; // This is the difference in milliseconds
                    let age = Math.floor(diff / 31557600000); // Divide by 1000*60*60*24*365.25
                    let work = "", job = "", education = "";

                    if (matcheResult[index]["_source"]["myPreferences"]) {
                        for (let pref_index = 0; pref_index < matcheResult[index]["_source"]["myPreferences"].length; pref_index++) {
                            if (matcheResult[index]["_source"]["myPreferences"][pref_index]["pref_id"] == "5a30fda027322defa4a14638"
                                && matcheResult[index]["_source"]["myPreferences"][pref_index]["isDone"]) {
                                work = matcheResult[index]["_source"]["myPreferences"][pref_index]["selectedValues"][0];
                            }
                            if (matcheResult[index]["_source"]["myPreferences"][pref_index]["pref_id"] == "5a30fdd127322defa4a14649"
                                && matcheResult[index]["_source"]["myPreferences"][pref_index]["isDone"]) {
                                job = matcheResult[index]["_source"]["myPreferences"][pref_index]["selectedValues"][0];
                            }
                            if (matcheResult[index]["_source"]["myPreferences"][pref_index]["pref_id"] == "5a30fdfa27322defa4a14653"
                                && matcheResult[index]["_source"]["myPreferences"][pref_index]["isDone"]) {
                                education = matcheResult[index]["_source"]["myPreferences"][pref_index]["selectedValues"][0];
                            }
                        }
                    }
                    dataToSend.push({
                        "opponentId": matcheResult[index]["_id"],
                        "firstName": matcheResult[index]["_source"]["firstName"],
                        "mobileNumber": matcheResult[index]["_source"]["contactNumber"],
                        "emailId": matcheResult[index]["_source"]["email"],
                        "height": matcheResult[index]["_source"]["height"],
                        "gender": matcheResult[index]["_source"]["gender"],
                        "dateOfBirth": matcheResult[index]["_source"]["dob"],
                        "onlineStatus": matcheResult[index]["_source"]["onlineStatus"],
                        "about": matcheResult[index]["_source"]["about"],
                        "profilePic": matcheResult[index]["_source"]["profilePic"] || "https://res.cloudinary.com/demo/image/upload/ar_4:3,c_fill/c_scale,w_auto,dpr_auto/sample.jpg",
                        "profileVideo": matcheResult[index]["_source"]["profileVideo"],
                        "otherImages": (Array.isArray(matcheResult[index]["_source"]["otherImages"])) ? matcheResult[index]["_source"]["otherImages"] : [],
                        "firebaseTopic": matcheResult[index]["_source"]["firebaseTopic"],
                        "instaGramProfileId": matcheResult[index]["_source"]["instaGramProfileId"] || "",
                        "instaGramToken": matcheResult[index]["_source"]["instaGramToken"] || "",
                        "deepLink": matcheResult[index]["_source"]["deepLink"],
                        "profileVideoThumbnail": (matcheResult[index]["_source"]["profileVideoThumbnail"] && !Array.isArray(matcheResult[index]["_source"]["profileVideoThumbnail"])) ? matcheResult[index]["_source"]["profileVideoThumbnail"] : "",
                        "distance": { value: parseFloat(matcheResult[index]["sort"][1].toFixed(2)), isHidden: matcheResult[index].dontShowMyDist || 0 },
                        "age": { value: age, isHidden: matcheResult[index].dontShowMyAge || 0 },
                        "work": work,
                        "job": job,
                        "education": education,
                        "superliked": (req.user && req.user.supperLikeByHistory &&
                            req.user.supperLikeByHistory.map(id => id.toString()).includes(matcheResult[index]["_id"])) ? 1 : 0,
                        "profileVideoWidth": matcheResult[index]["_source"]["profileVideoWidth"],
                        "profileVideoHeight": matcheResult[index]["_source"]["profileVideoHeight"],
                        "isBoostProfile": (matcheResult[index]["_source"]["boostTimeStamp"] && matcheResult[index]["_source"]["boostTimeStamp"] > 0)
                    })
                }
                callback(null, true);
            }
        ], (err, result) => {

            if (req.user.lastTimestamp) {
                req.user.lastTimestamp.like = (req.user.lastTimestamp && req.user.lastTimestamp.like) ? req.user.lastTimestamp.like : new Date().getTime();
            } else {
                req.user["lastTimestamp"] = { "like": 0 }
                // req.user["lastTimestamp"]["like"] = 0;
            }

            var a = moment();
            var b = moment(req.user.lastTimestamp.like);
            if (req.user.subscription[0]["likeCount"] != "unlimited" &&
                req.user.subscription &&
                req.user.count.like >= req.user.subscription[0]["likeCount"] &&
                a.diff(b, 'ms') >= likeRefreshInMillisecond) {
                remainsLikesInString = parseInt(req.user.subscription[0]["likeCount"]);
            } else {
                remainsLikesInString = (req.user.subscription[0]["likeCount"] == "unlimited") ? "unlimited" : parseInt(req.user.subscription[0]["likeCount"]) - req.user.count.like;
            }

            b = moment(req.user.lastTimestamp.rewind);
            if (req.user.subscription[0]["rewindCount"] != "unlimited" &&
                req.user.subscription &&
                req.user.count.like >= req.user.subscription[0]["rewindCount"] &&
                a.diff(b, 'ms') >= likeRefreshInMillisecond) {
                remainsRewindsInString = parseInt(req.user.subscription[0]["rewindCount"]);
            } else {
                remainsRewindsInString = (req.user.subscription[0]["rewindCount"] == "unlimited") ? "unlimited" : parseInt(req.user.subscription[0]["rewindCount"]) - req.user.count.rewind;
            }
            b = moment(req.user.lastTimestamp.like);
            let nextLikeTime = (remainsLikesInString == "0" || parseInt(remainsLikesInString) < 0)
                ? (b.add(likeRefreshInMillisecond, 'ms').valueOf())
                : 0;
            let nextRewindTime = (remainsRewindsInString == "0" || parseInt(remainsRewindsInString) < 0)
                ? (b.add(likeRefreshInMillisecond, 'ms').valueOf())
                : 0;

            // console.log("=====>>>>", JSON.stringify(dataToSend))

            return res({
                message: "success",
                data: dataToSend,
                remainsLikesInString: remainsLikesInString,
                remainsRewindsInString: remainsRewindsInString,
                nextLikeTime: nextLikeTime,
                nextRewindTime: nextRewindTime,
                boost: (req.user && req.user.boost) ? req.user.boost : {}
            }).code(200);
        })
    } catch (error) {
        logger.error("error : ", error);
        return res({ message: "success", data: [] }).code(200);
    }
};

let validator = Joi.object({
    offset: Joi.number().default(0).description("0").error(new Error('offset is missing')),
    limit: Joi.number().default(20).description("0").error(new Error('limit is missing'))
}).required({ Unknown: true });

module.exports = { APIHandler, validator }