'use strict'
const Joi = require("joi");
const async = require("async");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require("mongodb").ObjectID
const userListCollection = require('../../../models/userList');


let handler = (req, res) => {

    let _id = req.auth.credentials._id;
    let targetUserId = req.params.targetUserId;
    let requiredField = {
        _id: 1, firstName: 1, gender: 2, profilePic: 1, otherImages: 1, email: 1,
        profileVideo: 1, otherVideos: 1,
        dob: 1, about: 1, height: 1, heightInFeet: 1, firebaseTopic: 1
    };
    let dataToSend = {};
    let isMatched = 0;


    isMatchedUser()
        .then(function (value) {
            return getUser();
        })
        .then(function (value) {
            return res({ message: "success", data: dataToSend }).code(200);
        })
        .catch(function (err) {
            logger.info('Caught an error!', err);
            return res({ message: "not found" }).code(412);
        });


    function isMatchedUser() {
        return new Promise(function (resolve, reject) {

            if (_id == targetUserId) {
                return reject("-");
            }

            userListCollection.SelectOne({ _id: ObjectID(_id), matchedWith: ObjectID(targetUserId) }, (err, result) => {
                if (err) return reject("-");

                if (result && result._id != null) {
                    isMatched = 1;
                }
                return resolve("--");
            })
        });
    }

    function getUser() {
        return new Promise(function (resolve, reject) {
            userListCollection.SelectById({ _id: targetUserId }, requiredField, (err, result) => {
                if (err) return reject("-");

                if (result._id) {
                    result["userId"] = result._id;
                    result["emailId"] = result.email;
                    result["dateOfBirth"] = result.dob;

                    delete result._id;
                    delete result.dob;
                    delete result.email;

                    let birthdate = new Date(result["dateOfBirth"]);
                    let cur = new Date();
                    let diff = cur - birthdate; // This is the difference in milliseconds
                    let age = Math.floor(diff / 31557600000); // Divide by 1000*60*60*24*365.25

                    dataToSend = result;
                    dataToSend["isMatched"] = isMatched;
                    dataToSend["age"] = age;

                    return resolve("--");
                } else {
                    return reject("--");
                }

            })
        });
    }

};

let validator = Joi.object({
    targetUserId: Joi.string().required().description("enter targetUserId").error(new Error('targetUserId is missing')),
}).required();

let response = {

    // status: {
    //     200: {
    //         message: allMessage.GetSearchResultById["200"]["1"],
    //         data: Joi.any().example({
    //             "firstName": "Rajesh",
    //             "gender": "Male",
    //             "profilePic": "..................................................",
    //             "otherImages": ["1.......", "2..........", "3......."],
    //             "profileVideo": "..................................................",
    //             "otherVideos": ["1.......", "2..........", "3......."],
    //             "about": "",
    //             "height": 182.88,
    //             "heightInFeet": "182.88",
    //             "firebaseTopic": "566D5CA9-FE43-4B38-B70B-AA42816D9CFD",
    //             "userId": "5a4b967e12179b28442d5d01",
    //             "emailId": "rajesh@gmail.com",
    //             "dateOfBirth": 567887400000,
    //             "isMatched": 0,
    //             "age": 30
    //         })
    //     },
    //     412: { message: allMessage.GetSearchResultById["412"]["1"] },
    //     400: { message: allMessage.genericErrMsg["400"]["1"] },
    //     500: { message: allMessage.genericErrMsg["500"]["1"] }
    // }
}

module.exports = { handler, validator, response }