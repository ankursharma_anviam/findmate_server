'use strict'
let headerValidator = require('../../middleware/validator');
let GetSearchResultAPI = require('./Get');
let GetById = require('./GetById');

module.exports = [
    {
        method: 'GET',
        path: '/searchResult',
        handler: GetSearchResultAPI.APIHandler,
        config: {
            description: 'This API is used to find users nearby me',
            tags: ['api', 'searchResult'],
            auth: 'userJWT',
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: GetSearchResultAPI.validator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            }
        }
    },

    {
        method: 'GET',
        path: '/searchResult/{targetUserId}',
        handler: GetById.handler,
        config: {
            description: 'This API is used to find users nearby me',
            tags: ['api', 'searchResult'],
            auth: 'userJWT',
            validate: {
                headers: headerValidator.headerAuthValidator,
                params: GetById.validator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            // response: GetById.response
        }
    },
];