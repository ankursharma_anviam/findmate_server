'use strict'
let headerValidator = require('../../middleware/validator');
let unfriendUser= require('./Post');
module.exports = [
    {
        method: 'POST',
        path: '/unfriendUser',
        handler: unfriendUser.handler,
        config: {
            description: 'This API will be used to un-friend the already be-friended user.',
            tags: ['api', 'unfriendUser'],
            auth: 'userJWT',
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: unfriendUser.validator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
             response: unfriendUser.response
        }
    }
    
];