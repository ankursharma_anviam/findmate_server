'use strict'
const Joi = require("joi");
const async = require("async");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;
const local = require('../../../locales');
const mqttClient = require("../../../library/mqtt");
const GeoPoint = require('geopoint')
const userListCollection = require('../../../models/userList');
const friendshipCollection = require('../../../models/friendship');
const chatListCollection = require('../../../models/chatList');

const fcmPush = require("../../../library/fcm");
var findMateId;

/**
 * @method GET like
 * @description This API is used to get likedlist.
 * @param {*} req 
 * @param {*} res 
 * @property {*} authorization in header
 * @property {*} lang in header
 * @returns  200 : User found in database.
 * @returns  500 : An unknown error has occurred.
 */
let validator = Joi.object({
    targetUserId: Joi.string().required().description("targetUserId").error(new Error('targetUserId is missing')),
    statusCode: Joi.number().integer().default(3).required().description("statusCode 3 for unfriend").error(new Error('statusCode is missing'))
}).required();

const handler = (req, reply) => {
    console.log("========>", req.payload)
    const dbErrResponse = { message: req.i18n.__('genericErrMsg')['500'], code: 500 };
    const successResponse = { message: req.i18n.__('unfriend')['200'], code: 200 };

    var _id = new ObjectID(req.auth.credentials._id);
    var targetUserId = req.payload.targetUserId;
    var friendRequestStatus = (req.payload.statusCode) == parseInt(3) ? "Unfriend" : "";

    //checking whether friend or not
    let checkFriend = () => {
        return new Promise((resolve, reject) => {
            friendshipCollection.Select({
                $or: [{ "friendRequestSentTo": _id, "friendRequestSentByerId": new ObjectID(targetUserId) },
                { "friendRequestSentTo": new ObjectID(targetUserId), "friendRequestSentByerId": _id }]
            }, (err, result) => {
                if (err) {
                    logger.error("post shortList API : ", err)
                    return reject(dbErrResponse);
                }
                if (result[0]) {
                    return resolve(true);
                }
                else {
                    return reject(new Error('you are not a friend'));
                }

            })
        })
    };
    //update the friendship detail in friendship collection
    let postFriendship = () => {
        return new Promise((resolve, reject) => {
            //if ignored request
            if (req.payload.statusCode === parseInt(3)) {

                friendshipCollection.Update({
                    $or: [
                        { "friendRequestSentByerId": _id, "friendRequestSentTo": new ObjectID(targetUserId) },
                        { "friendRequestSentByerId": new ObjectID(targetUserId), "friendRequestSentTo": _id }
                    ]
                }, {
                        friendUnfriendedOn: new Date().getTime(),
                        friendUnfriendedOnDate: new Date(),
                        friendRequestStatus: friendRequestStatus
                    }, (err, result) => {
                        if (err) {
                            logger.error("post unfriend API : ", err)
                            return reject(dbErrResponse);
                        }

                        //remove userId from sentReqeuests[]
                        console.log("===========remove userId from sentReqeuests")
                        userListCollection.UpdateFriendWithPull({ _id: _id }, { friendShipId: new ObjectID(targetUserId) }, (err, result) => {
                            if (err) {
                                logger.error("patch userlist1 API : ", err);
                                return reject(dbErrResponse);
                            }
                            else {
                                //remove userId from friendRequest[]
                                console.log("=========>remove userId from friendRequest")
                                userListCollection.UpdateFriendWithPull({ _id: new ObjectID(targetUserId) }, { friendShipId: _id }, (err, result) => {
                                    if (err) {
                                        logger.error("patch userlist2 API : ", err)
                                        return reject(dbErrResponse);
                                    }
                                    else {
                                        //   console.log("su1---");
                                        let userId = req.auth.credentials._id;
                                        let targetId = req.payload.targetUserId;
                                        let dataToUpdate1 = "members." + userId;
                                        let dataToUpdate2 = "members." + targetId;
                                        let dataToUpdate3 = "members." + userId + ".inactive";
                                        let dataToUpdate4 = "members." + targetId + ".inactive";

                                        chatListCollection.Update({
                                            [dataToUpdate1]: { "$exists": true },
                                            [dataToUpdate2]: { "$exists": true },
                                            "isMatchedUser": 2,
                                            //"isMatchedUser": 0
                                        }, {
                                                [dataToUpdate3]: true,
                                                [dataToUpdate4]: true,
                                                "isMatchedUser": 0,
                                                "isUnMatched":0
                                            }, (err, result) => {
                                                if (err) {
                                                    logger.error("post unfriend API : ", err);
                                                    return reject(dbErrResponse);
                                                }
                                            });
                                        chatListCollection.Update({
                                            [dataToUpdate1]: { "$exists": true },
                                            [dataToUpdate2]: { "$exists": true },
                                            "isMatchedUser": 2,
                                           // $or: [{ isMatchedUser: { "$in": [1, 2] } }, { isMatchedUser: { $exists: false } }]

                                        },
                                            { "isMatchedUser": 0, isUnMatched: 0 },
                                            (err, result) => {
                                                if (err) {
                                                    logger.error("post unfriend API : ", err);
                                                    return reject(dbErrResponse);
                                                }
                                            });
                                        userListCollection.SelectOne({ _id: ObjectID(targetUserId) }, (err, result) => {
                                            if (err) {
                                                logger.error("post unfriend API : ", err);
                                                return reject(dbErrResponse);
                                            }

                                            if (result && result._id) {

                                                let dataToSend = {
                                                    "unfrinedTimeStamp": new Date().getTime(),
                                                    "unfrinedRequestSentById": targetUserId,
                                                    "unfriendRequestSentByName": result.firstName,
                                                    "unfrinedRequestSentByPhoto": result.profilePic,
                                                    "unfriendFriendRequestById": _id,
                                                    "unfriendRequestByName": req.user.fileName,
                                                    "upfriendRequestByPhoto": req.user.profilePic,
                                                    "messageType": "Unfriend"
                                                };
                                                mqttClient.publish(req.auth.credentials._id, JSON.stringify(dataToSend), { qos: 1 }, (e, r) => { if (e) logger.error("SADASDASD : ", e) })
                                                mqttClient.publish(targetUserId, JSON.stringify(dataToSend), { qos: 1 }, (e, r) => { if (e) logger.error("SADASDAASDSD : ", e) });
                                            }
                                            return resolve(successResponse);
                                        })
                                    }
                                })


                            }

                        })
                        // return resolve(true);


                    })
            } else {
                return reject(new Error('Ooops, something broke!'))
            }
        });
    };

    checkFriend()
        .then((data) => { return postFriendship(data) })
        .then((data) => { return reply({ message: req.i18n.__('unfriend')['200'] }).code(200); })
        .catch((err) => {
            logger.err("get profile by FindMatchId API error : ", err);
            return reply({ message: err.message }).code(err.code);
        });
};


let response = {
    status: {
        200: { message: Joi.any().default(local['unfriend']['200']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) },

    }
}

module.exports = { handler, response, validator }
