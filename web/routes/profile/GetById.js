'use strict'
const Joi = require("joi");
const async = require("async");
const logger = require('winston');
const Promise = require('promise');
const GeoPoint = require('geopoint')
const ObjectID = require("mongodb").ObjectID
const userListCollection = require('../../../models/userList');
const userListType = require('../../../models/userListType');
const recentVisitorsCollection = require('../../../models/recentVisitors');
const friendShipCollection = require('../../../models/friendship');
const likedUserCollection = require('../../../models/likedUser');
const searchPreferencesCollection = require('../../../models/searchPreferences');
const userPrefrances = require('../../../models/userPrefrances');
const userBoostCollection = require('../../../models/userBoost');
const mqttClient = require("../../../library/mqtt");
const Config = process.env;
const local = require('../../../locales');
const Timestamp = require('mongodb').Timestamp;
var isFriend;
var isProfileLike;
/**
 * @function GET profileById
 * @description This API is used to get Profile by Id.

 * @property {string} authorization - authorization
 * @property {string} lang - language
 * @property {string}  targetUserId- targetUserId
  
 * @returns  200 : User found in database.
 * @example {
  "message": "User found in database.",
  "data": {
    "firstName": "Rahul",
    "countryCode": "+91",
    "contactNumber": "+919019810918",
    "gender": "Male",
    "profilePic": "................",
    "otherImages": [
      "1.......",
      "2..........",
      "3......."
    ],
    "email": "example@domain.com",
    "profileVideo": "..............",
    "otherVideos": [
      "1.......",
      "2..........",
      "3......."
    ],
    "dob": 1504867670000,
    "about": "about user",
    "currentLoggedInDevices": {
      "deviceId": "yourDeviceId",
      "pushToken": "yourPushToken"
    },
    "height": 190,
    "heightInFeet": "5'6\"",
    "firebaseTopic": "yourDeviceId",
    "userId": "5a0d494e2c501c37b0f3067a"
  }
}
 * @returns  400 : Value in any of the  mandatory field's is missing.
 * @returns  412 : User doesnot exist in the database. 
 * @returns  500 : An unknown error has occurred.
 **/
let handler = (req, res) => {

    let _id = req.auth.credentials._id;
    let targetUserId = req.params.targetUserId;
    let isTargetBoostProfile = false;
    let targetBoostId = "";
    let targetBoostDetails = {};
    let point1 = new GeoPoint(req.user.location.latitude, req.user.location.longitude);

    let requiredField = {
        _id: 0, firstName: 1, countryCode: 1, contactNumber: 1, myLikes: 1,
        gender: 2, profilePic: 1, otherImages: 1, email: 1, profileVideo: 1, otherVideos: 1,
        dob: 1, about: 1, height: 1, heightInFeet: 1, firebaseTopic: 1, currentLoggedInDevices: 1,
        dontShowMyAge: 1, dontShowMyDist: 1, onlineStatus: 1, instaGramUserName: 1, boost: 1, blockedBy: 1,
        matchedWith: 1, myunlikes: 1, mySupperLike: 1
    };
    let dataToSend = {};
    isFriendCheck()
        .then(() => { return isProfileLikedCheck(); })
        .then(() => { return getUser(); })
        .then(() => { return updateRecentVisitors(); })
        .then(() => { return processData(); })
        // .then(() => { return removeExtraUsers(); })
        .then(() => {
            console.log("dataToSend ", JSON.stringify(dataToSend))
            return res({ message: req.i18n.__('GetProfileById')['200'], data: dataToSend, isFriend: isFriend, isProfileLike: isProfileLike }).code(200);
        })
        .catch(function (err) {
            logger.info('Caught an error!', err);
            return res({ message: req.i18n.__('GetProfileById')['412'] }).code(412);
        });

    function isFriendCheck() {
        return new Promise(function (resolve, reject) {
            userListCollection.Select({ _id: new ObjectID(_id), friendShipId: new ObjectID(targetUserId) }, (err, result) => {
                if (err) {
                    return reject("-");
                }
                else if (result && result[0]) {
                    isFriend = 1;
                    return resolve(isFriend);
                }
                else {
                    isFriend = 0;
                    return resolve(isFriend);
                }
            });
        });
    }
    function isProfileLikedCheck() {
        return new Promise(function (resolve, reject) {

            userListCollection.Select({ _id: new ObjectID(_id), ProfileLikedBy: new ObjectID(targetUserId) }, (err, result) => {

                if (err) {
                    return reject("-");
                }
                else if (result && result[0]) {
                    isProfileLike = 1;
                    return resolve(isProfileLike);
                }
                else {
                    isProfileLike = 0;
                    return resolve(isProfileLike);

                }
            });
        });
    }
    function getUser() {
        return new Promise(function (resolve, reject) {
            userListCollection.SelectById({ _id: targetUserId }, requiredField, (err, result) => {
                if (err) return reject("-");

                if (result) {
                    result["emailId"] = result.email || "";
                    result["mobileNumber"] = result.contactNumber || "";
                    result["dateOfBirth"] = result.dob || 1;
                    if (result && result.boost && result.boost.expire >= new Date().getTime()) {
                        isTargetBoostProfile = true;
                        targetBoostId = result.boost._id;
                        targetBoostDetails = result.boost;
                    }

                    delete result.email;
                    delete result.contactNumber;
                    delete result.currentLoggedInDevices;
                }

                if (isTargetBoostProfile) {

                    userBoostCollection.Select({
                        "_id": ObjectID(targetBoostId.toString()),
                        "views": { "$elemMatch": { "userId": ObjectID(_id) } }
                    }, (e, r) => {
                        if (e) logger.error("  userBoostCollection.Select : ", e);

                        if (r) {
                            userBoostCollection.UpdateByIdWithPush({ _id: targetBoostId },
                                { views: { userId: ObjectID(_id), timestamp: new Date().getTime() } },
                                (e, r) => { if (e) logger.error(" userBoostCollection.UpdateByIdWithPush : ", e); });

                            userBoostCollection.UpdateWithIncrease({ _id: targetBoostId }, { "count.views": 1 },
                                (e, r) => { if (e) logger.error(" userBoostCollection.UpdateWithIncrease : ", e); });

                            userListCollection.UpdateWithIncrease({ _id: ObjectID(targetUserId) }, { "boost.views": 1 },
                                (e, r) => { if (e) logger.error(" userListCollection.UpdateWithIncrease : ", e); });

                            targetBoostDetails["views"] = targetBoostDetails["views"] + 1
                            console.log(" mqttClient.publish ", targetUserId)
                            mqttClient.publish(targetUserId, JSON.stringify({ "messageType": "boost", boost: targetBoostDetails }), { qos: 0 }, (e, r) => { if (e) logger.error("/boost/like", e) })
                        }
                    });
                }
                result["userId"] = targetUserId;
                dataToSend = result;
                userListCollection.SelectById({
                    _id: _id
                }, { firstName: 1, countryCode: 1, myBlock: 1, blockedBy: 1, myunlikes: 1, mySupperLike: 1, myLikes: 1, matchedWith: 1 }, (err, result) => {
                    if (err) return reject("-");

                    if (result && result.firstName) {
                        dataToSend["activityStatus"] = 0;
                        console.log("====>>>>", result["myLikes"])
                        dataToSend["liked"] = (result["myLikes"] && result["myLikes"].map(id => id.toString()).includes(targetUserId)) ? 1 : 0;
                        dataToSend["unliked"] = (result["myunlikes"] && result["myunlikes"].map(id => id.toString()).includes(targetUserId)) ? 1 : 0;
                        dataToSend["superliked"] = (result["mySupperLike"] && result["mySupperLike"].map(id => id.toString()).includes(targetUserId)) ? 1 : 0;
                        dataToSend["blocked"] = (result["blockedBy"] && result["blockedBy"].map(id => id.toString()).includes(targetUserId)) ? 1 : 0;
                        dataToSend["isBlocked"] = (result["blockedBy"] && result["blockedBy"].map(id => id.toString()).includes(targetUserId)) ? 1 : 0;
                        dataToSend["blockedByMe"] = (result["myBlock"] && result["myBlock"].map(id => id.toString()).includes(targetUserId)) ? 1 : 0;
                        dataToSend["isMatched"] = (result["matchedWith"] && result["matchedWith"].map(id => id.toString()).includes(targetUserId)) ? 1 : 0;
                    } else {
                        dataToSend["activityStatus"] = 1;
                    }
                    return resolve("--");
                });
            })
        });
    }
    function processData() {
        return new Promise(function (resolve, reject) {
            let condition = { "_id": ObjectID(targetUserId) };

            userListCollection.Select(condition, (err, result) => {
                if (err) return reject(new Error('Ooops, something broke! at checkOTP'));

                if (result && result[0]) {

                    dataToSend["_id"] = result[0]._id;
                    dataToSend["countryCode"] = result[0].countryCode;
                    dataToSend["contactNumber"] = result[0].contactNumber;
                    dataToSend["firstName"] = result[0].firstName;
                    dataToSend["dob"] = result[0].dob;
                    dataToSend["profilePic"] = result[0].profilePic;
                    dataToSend["gender"] = result[0].gender;
                    dataToSend["height"] = result[0].height;
                    dataToSend["email"] = result[0].email;
                    dataToSend["profileVideo"] = result[0].profileVideo;
                    dataToSend["profileVideoThumbnail"] = result[0].profileVideoThumbnail;
                    dataToSend["otherImages"] = result[0].otherImages;
                    dataToSend["about"] = result[0].about;
                    dataToSend["instaGramProfileId"] = result[0].instaGramProfileId || "";
                    dataToSend["instaGramToken"] = result[0].instaGramToken || "";
                    dataToSend["instaGramUserName"] = result[0].instaGramToken || "";
                    dataToSend["onlineStatus"] = result[0].onlineStatus || 0;
                    dataToSend["findMateId"] = result[0].findMateId;
                    dataToSend["ProfileLikedBy"] = result[0].ProfileLikedBy.length || 0;
                    dataToSend["MyProfileLiked"] = result[0].MyProfileLiked.length || 0;
                    dataToSend["country"] = result[0].address ? result[0].address.country : "";
                    dataToSend["countryShortName"] = result[0].address ? result[0].address.countryCode : "";
                    dataToSend["city"] = result[0].address ? result[0].address.city : "";

                    //  console.log("result-----",result[0].ProfileLikedBy.length);
                    let cur = new Date();
                    let diff = cur - result[0].dob; // This is the difference in milliseconds
                    let age = Math.floor(diff / 31557600000); // Divide by 1000*60*60*24*365.25
                    dataToSend["age"] = { value: age, isHidden: result[0].dontShowMyAge || 0 };
                    let point2 = new GeoPoint(result[0].location.latitude, result[0].location.longitude);
                    let distance = point1.distanceTo(point2, true).toFixed(2)//output in kilometers
                    dataToSend["distance"] = { value: parseFloat(distance), isHidden: result[0].dontShowMyDist || 0 };

                    let myPrefrances = [];
                    let prefrances = {}
                    let _id = result[0]._id;
                    let searchPreferencesObj = {};
                    let searchPreferences = [];

                    async.parallel([

                        function (callback) {

                            let point1 = new GeoPoint(req.user.location.latitude, req.user.location.longitude);
                            result.forEach(function (element) {
                                let point2 = new GeoPoint(element.location.latitude, element.location.longitude);
                                let distance = point1.distanceTo(point2, true).toFixed(2)//output in kilometers
                                element["distance"] = { value: distance, isHidden: element.dontShowMyDist || 0 };
                            }, this);
                            return callback(null, 1);

                        },
                        function (callback) {
                            userPrefrances.Select({}, (err, data) => {
                                myPrefrances = data;
                                for (let index = 0; index < myPrefrances.length; index++) {
                                    myPrefrances[index]["title"] = myPrefrances[index]["title"].replace("My ", "");
                                }

                                for (let index = 0; index < result[0].myPreferences.length; index++) {
                                    prefrances[result[0].myPreferences[index]["pref_id"]] = result[0].myPreferences[index];
                                }


                                for (let index = 0; index < myPrefrances.length; index++) {
                                    myPrefrances[index]["pref_id"] = myPrefrances[index]["_id"];
                                    myPrefrances[index]["isDone"] = prefrances[myPrefrances[index]["_id"]]["isDone"];
                                    myPrefrances[index]["selectedValues"] = prefrances[myPrefrances[index]["_id"]]["selectedValues"];

                                    if (myPrefrances[index]["_id"] == "5a30fda027322defa4a14638"
                                        && myPrefrances[index]["isDone"]) {
                                        dataToSend["work"] = myPrefrances[index]["selectedValues"][0];
                                    }
                                    if (myPrefrances[index]["_id"] == "5a30fdd127322defa4a14649"
                                        && myPrefrances[index]["isDone"]) {
                                        dataToSend["job"] = myPrefrances[index]["selectedValues"][0];
                                    }
                                    if (myPrefrances[index]["_id"] == "5a30fdfa27322defa4a14653"
                                        && myPrefrances[index]["isDone"]) {
                                        dataToSend["eduation"] = myPrefrances[index]["selectedValues"][0];
                                    }
                                }


                                let myPreferencesByGroupObj = {};
                                for (let index = 0; index < myPrefrances.length; index++) {
                                    if (myPrefrances[index]["isDone"]) {
                                        if (myPreferencesByGroupObj[myPrefrances[index]["title"]]) {
                                            myPreferencesByGroupObj[myPrefrances[index]["title"]].push(myPrefrances[index]);
                                        } else {

                                            myPreferencesByGroupObj[myPrefrances[index]["title"]] = [myPrefrances[index]];
                                        }
                                    }
                                }
                                let myPreferencesByGroupArray = [];
                                for (let key in myPreferencesByGroupObj) {
                                    myPreferencesByGroupArray.push({
                                        "title": key,
                                        "data": myPreferencesByGroupObj[key]
                                    });
                                }
                                myPrefrances = myPreferencesByGroupArray;
                                callback(err, data);
                            })
                        },
                        function (callback) {
                            searchPreferencesCollection.Select({}, (err, result) => {
                                if (result[0]) {
                                    let pref_id = "";
                                    for (let index = 0; index < result.length; index++) {
                                        pref_id = result[index]._id;
                                        searchPreferencesObj[pref_id] = result[index];
                                        searchPreferencesObj[pref_id]["selectedValue"] = searchPreferencesObj[pref_id]["OptionsValue"];
                                        delete searchPreferencesObj[pref_id]["ActiveStatus"];
                                        if (result[index]["TypeOfPreference_User"]) {
                                            delete searchPreferencesObj[pref_id]["TypeOfPreference_User"];
                                        }
                                    }
                                }
                                callback(null, 1);
                            })
                        },
                        function (callback) {
                            for (let index = 0; index < result[0].searchPreferences.length; index++) {
                                let pref_id = result[0].searchPreferences[index].pref_id;
                                if (searchPreferencesObj[pref_id]) {
                                    searchPreferencesObj[pref_id]["selectedValue"] = result[0].searchPreferences[index]["selectedValue"];
                                    if (result[0].searchPreferences[index]["selectedUnit"]) {
                                        searchPreferencesObj[pref_id]["selectedUnit"] = result[0].searchPreferences[index]["selectedUnit"];
                                    }
                                }
                            }
                            for (let pref_id in searchPreferencesObj) {
                                searchPreferences.push(searchPreferencesObj[pref_id])
                            }
                            callback(null, 1);
                        }
                    ], () => {
                        dataToSend["myPreferences"] = myPrefrances;
                        return resolve("--");
                    })
                } else {
                    return resolve("--");
                }

            });
        });
    }
    function updateRecentVisitors() {
        return new Promise(function (resolve, reject) {
            if (_id == targetUserId) return resolve("--");
            let condition = { _id: targetUserId };
            let data = { "recentVisitors": ObjectID(_id) };
            userListCollection.UpdateByIdWithAddToSet(condition, data, (e, r) => { });
            userListType.UpdateWithPush(targetUserId, "recentVisitors", _id, (e, r) => { });
            let dataToInsert = {
                "userId": ObjectID(_id),
                "targetUserId": ObjectID(targetUserId),
                "createdTimestamp": new Date().getTime(),
                creationTs: new Timestamp(),
                creationDate: new Date()
            };
            if (targetBoostId != "") dataToInsert["boostId"] = targetBoostId
            recentVisitorsCollection.Insert(dataToInsert, (e, r) => { });
            return resolve("---");

        });
    }
    function removeExtraUsers() {
        return new Promise(function (resolve, reject) {
            userListType.SelectAll((err, result) => {
                async.each(result.hits.hits, (element, callback) => {
                    if (element._id.length == 24) {
                        userListCollection.SelectOne({ _id: ObjectID(element._id) }, (err, data) => {
                            if (!data) {
                                userListType.Delete("userList", { _id: element._id }, (err, aaa) => {
                                    console.log(err)
                                    callback();
                                });
                            } else {
                                // userListType.Update(element._id,{"supperLikeByHistory":[]},()=>{});
                                callback();
                            }

                        })
                    } else {
                        callback();
                    }


                })
                return resolve("----");
            })
        });
    }
};

let validator = Joi.object({
    targetUserId: Joi.string().required().description("enter targetUserId").error(new Error('targetUserId is missing')),
}).required();

let response = {
    status: {
        200: { message: local['GetProfileById']['200'], data: Joi.any(), isFriend: Joi.any(), isProfileLike: Joi.any() },
        412: { message: local['GetProfileById']['412'] },
        400: { message: local['genericErrMsg']['400'] },
    }
}

module.exports = { handler, response, validator }