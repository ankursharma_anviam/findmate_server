'use strict'
let putAPI = require('./Put');
let postAPI = require('./Post');
let profileAPI = require('./Get');
let GetProfileByIdAPI = require('./GetById');
let DeleteAPI = require('./Delete');
let headerValidator = require("../../middleware/validator");

module.exports = [
    {
        method: 'Delete',
        path: '/profile',
        handler: DeleteAPI.APIHandler,
        config: {
            description: 'This API used to get profile Data',
            tags: ['api', 'profile'],
            auth: 'userJWT',
            validate: {
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: DeleteAPI.response
        }
    },
    {
        method: 'GET',
        path: '/profile/{targetUserId}',
        handler: GetProfileByIdAPI.handler,
        config: {
            description: 'This API used to get profile Data',
            tags: ['api', 'profile'],
            auth: 'userJWT',
            validate: {
                headers: headerValidator.headerAuthValidator,
                params: GetProfileByIdAPI.validator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetProfileByIdAPI.response
        }
    },
    {
        method: 'GET',
        path: '/profile',
        handler: profileAPI.APIHandler,
        config: {
            tags: ['api', 'profile'],
            description: 'This API is used to login an user in the app.',
            auth: 'userJWT',
            validate: {
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: profileAPI.APIResponse
        }
    },
    {
        method: 'POST',
        path: '/profile',
        handler: postAPI.handler,
        config: {
            description: 'This API will check otp is correct or not.',
            tags: ['api', 'profile'],
            auth: false,
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: postAPI.validator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: postAPI.response
        }
    },
    {
        method: 'PUT',
        path: '/profile',
        handler: putAPI.APIHandler,
        config: {
            description: 'This API will check otp is correct or not.',
            tags: ['api', 'profile'],
            auth: "userJWT",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: putAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: putAPI.response
        }
    }
];