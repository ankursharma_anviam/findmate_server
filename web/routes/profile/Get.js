'use strict'
const logger = require('winston');
const Promise = require('promise');
const db = require('../../../models/userList');
const userPrefrances = require("../../../models/userPrefrances");
const Joi = require("joi");
const local = require('../../../locales');

/**
 * @function GET profile
 * @description This API is used to get Profile.

 * @property {string} authorization - authorization
 * @property {string} lang - language
  
 * @returns  200 : User profile sent successfully.
 * @example {
  "message": "User profile sent successfully.",
  "data": {
    "firstName": "Rahul Sharma",
    "countryCode": "+91",
    "contactNumber": "+919182736451",
    "gender": "Male",
    "email": "example@domain.com",
    "dob": 1234567,
    "about": "You want app call to me",
    "height": 200
  }
}
 * @returns  400 : Value in any of the  mandatory field's is missing.
 * @returns  412 : Entered User Id(token) doesnot exist,please check the entered token. 
 * @returns  500 : An unknown error has occurred.
 **/
let APIHandler = (req, res) => {

    let _id = req.auth.credentials._id;
    let requiredField = {
        firstName: 1, countryCode: 1, contactNumber: 1, gender: 1,
        email: 1, dob: 1, about: 1, height: 1, profilePic: 1, profileVideo: 1, profileVideoThumbnail: 1,
        otherImages: 1, myPreferences: 1, instaGramProfileId: 1, instaGramToken: 1,
        dontShowMyAge: 1, dontShowMyDist: 1, boost: 1, count: 1, address:1
    };

    getUserData()
        .then(function (value) {
            return getPrefrances(value);
        })
        .then(function (value) {
            return res({ message: req.i18n.__('GetProfile')['200'], data: value }).code(200);
        })
        .catch(function (err) {
            logger.info('Caught an error!', err);
            return res({ message: req.i18n.__('GetProfile')['412'] }).code(412);
        });


    function getUserData() {
        return new Promise(function (resolve, reject) {
            db.SelectById({ _id: _id }, requiredField, (err, result) => {
                if (result) {
                    let cur = new Date();
                    let diff = cur - result.dob; // This is the difference in milliseconds
                    let age = Math.floor(diff / 31557600000); // Divide by 1000*60*60*24*365.25

                    result["emailId"] = result.email;
                    result["mobileNumber"] = result.contactNumber;
                    result["dateOfBirth"] = result.dob;
                    result["profilePic"] = result.profilePic || "";
                    result["otherImages"] = result.otherImages || [];
                    result["profileVideo"] = result.profileVideo || "";
                    result["profileVideoThumbnail"] = result.profileVideoThumbnail || "";
                    result["instaGramProfileId"] = result.instaGramProfileId || "";
                    result["instaGramToken"] = result.instaGramToken || "";
                    result["myPreferences"] = result.myPreferences || [];
                    result["about"] = result.about || "";
                    result["age"] = { value: age, isHidden: result.dontShowMyAge || 1 };
                    result["distance"] = { value: 0, isHidden: result.dontShowMyDist || 1 };
                    result["boostExpiryTime"] = (result.boost && result.boost.expiryTime) ? result.boost.expiryTime : 0;
                    result["country"] = (result.address)?result.address.country: "";
                     result["countryShortName"] = (result.address)?result.address.countryCode : "";
                     result["city"] = (result.address)?result.address.city : "";

                    delete result.email;
                    delete result.contactNumber;
                    resolve(result);
                }
                reject("-")
            })
        });
    }

    function getPrefrances(userData) {
        return new Promise(function (resolve, reject) {
            userPrefrances.Select({}, (err, result) => {
                if (err) reject("--");

                for (let index = 0; index < result.length; index++) {
                    for (let indexPref = 0; indexPref < userData["myPreferences"].length; indexPref++) {
                        if (result[index]._id.toString() == userData["myPreferences"][indexPref].pref_id.toString()) {
                            userData["myPreferences"][indexPref]["selectedValues"] = (userData["myPreferences"][indexPref]["selectedValues"]) ? userData["myPreferences"][indexPref]["selectedValues"] : [];
                            userData["myPreferences"][indexPref]["title"] = result[index]["title"];
                            userData["myPreferences"][indexPref]["label"] = result[index]["label"];
                            userData["myPreferences"][indexPref]["options"] = result[index]["options"];
                            userData["myPreferences"][indexPref]["type"] = result[index]["type"];
                            userData["myPreferences"][indexPref]["priority"] = result[index]["priority"];
                            userData["myPreferences"][indexPref]["iconNonSelected"] = result[index]["iconNonSelected"];
                            userData["myPreferences"][indexPref]["iconSelected"] = result[index]["iconSelected"];
                        }
                    }
                }

                let myPreferencesByGroupObj = {};
                for (let index = 0; index < userData["myPreferences"].length; index++) {
                    if (myPreferencesByGroupObj[userData["myPreferences"][index]["title"]]) {
                        myPreferencesByGroupObj[userData["myPreferences"][index]["title"]].push(userData["myPreferences"][index]);
                    } else {
                        myPreferencesByGroupObj[userData["myPreferences"][index]["title"]] = [userData["myPreferences"][index]];
                    }
                }
                let myPreferencesByGroupArray = [];
                for (let key in myPreferencesByGroupObj) {
                    myPreferencesByGroupArray.push({
                        "title": key,
                        "data": myPreferencesByGroupObj[key]
                    });
                }

                userData["myPreferences"] = myPreferencesByGroupArray;

                let values = {
                    "_id": userData["_id"],
                    "firstName": userData["firstName"],
                    "countryCode": userData["countryCode"],
                    "mobileNumber": userData["mobileNumber"],
                    "emailId": userData["emailId"],
                    "gender": userData["gender"],
                    "height": userData["height"],
                    "dateOfBirth": userData["dateOfBirth"],
                    "dob": userData["dob"],
                    "profilePic": userData["profilePic"],
                    "otherImages": userData["otherImages"],
                    "profileVideo": userData["profileVideo"],
                    "profileVideoThumbnail": userData["profileVideoThumbnail"],
                    "instaGramProfileId": userData["instaGramProfileId"],
                    "instaGramToken": userData["instaGramToken"],
                    "myPreferences": userData["myPreferences"],
                    "about": userData["about"],
                    "age": userData["age"],
                    "distance": userData["distance"],
                    "boostExpiryTime": userData["boostExpiryTime"],
                    "count": userData["count"],
                    "country": userData["country"],
                    "countryShortName": userData["countryShortName"],
                    "city": userData["city"]

                };
                resolve(values);
            });
        });
    }
};
let APIResponse = {
    status: {
        200: {
            message: local['GetProfile']['200'], data: Joi.any().example(
                {

                    "_id": "5ad099568cbafd68e03dad4f",
                    "firstName": "Rahul",
                    "countryCode": "+91",
                    "mobileNumber": "+919620826143",
                    "emailId": "rahul@mobifyi.com",
                    "gender": "Male",
                    "height": 165,
                    "dateOfBirth": 716730527604,
                    "dob": 716730527604,
                    "profilePic": "https://s3.amazonaws.com/datum-premium/datum-premium/profilePicture/IMAGE_20180413_171911.jpg",
                    "otherImages": [],
                    "profileVideo": "",
                    "profileVideoThumbnail": "",
                    "myPreferences": [
                        {
                            "pref_id": "5a30ff2627322defa4a146a8",
                            "isDone": true,
                            "selectedValues": [
                                "Hindu"
                            ],
                            "title": "My Virtues",
                            "label": "Religious beliefs",
                            "options": [
                                "Buddhist",
                                "Catholic",
                                "Christian",
                                "Hindu",
                                "Jewish",
                                "Muslim",
                                "Shinto",
                                "Agnostic",
                                "Athesist",
                                "Other"
                            ],
                            "type": 2,
                            "priority": 13,
                            "iconNonSelected": "http://107.170.105.134/datum_2.0-server/My_Virtues_Screen/religion_off@2x.png",
                            "iconSelected": "http://107.170.105.134/datum_2.0-server/My_Virtues_Screen/religion_on@2x.png"
                        },
                        {
                            "pref_id": "5a30fa6d27322defa4a14550",
                            "isDone": true,
                            "selectedValues": [
                                "5.5(165 cm)"
                            ],
                            "title": "My Vitals",
                            "label": "Height",
                            "options": [
                                "5.3(160 cm)",
                                "5.4(163 cm)",
                                "5.5(165 cm)",
                                "5.6(168 cm)",
                                "5.7(170 cm)"
                            ],
                            "type": 10,
                            "priority": 5,
                            "iconNonSelected": "http://107.170.105.134/datum_2.0-server/My_Vitals_Screen_Assets/height_off@2x.png",
                            "iconSelected": "http://107.170.105.134/datum_2.0-server/My_Vitals_Screen_Assets/height_on@2x.png"
                        }
                    ]
                }
            )
        },
        412: { message: local['GetProfile']['412'] },
        400: { message: local['genericErrMsg']['400'] },
        500: { message: local['genericErrMsg']['500'] },
    }
}

module.exports = { APIHandler, APIResponse }