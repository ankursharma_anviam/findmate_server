'use strict'
const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../locales');
const userListType = require('../../../models/userListType');
const userListCollection = require('../../../models/userList');
const friendshipCollection = require('../../../models/friendship');
const chatListCollection = require('../../../models/chatList');


let APIHandler = (req, res) => {
    let _id = req.auth.credentials._id;
    const dbErrResponse = { message: req.i18n.__('genericErrMsg')['500'], code: 500 };

    updateFriendShipStatus()
        .then((data) => { return pullFriendShipStatusFromUserList(data) })
        .then((data) => { return userListFriendShipStatus(data) })
        .then((data) => { return updateProfileStatus(data) })
        .then((data) => { return updateChatStatus(data) })
        .then(value => {
            return deleteChatHistory();
        })
        .then(value => {
            return res({ message: req.i18n.__('DeleteProfile')['200'] }).code(200);
        }).catch(function (err) {
            logger.error('Caught an error!', err);
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        });


    /**update all the friendship collection */
    function updateFriendShipStatus() {
        return new Promise(function (resolve, reject) {
            friendshipCollection.updateManyData({
                $or: [{ "friendRequestStatus": "Accepted" },
                { "friendRequestStatus": "Pending" }],
                $or: [{ "friendRequestSentByerId": new ObjectID(_id) },
                { "friendRequestSentTo": new ObjectID(_id) }]
            },
                {
                    friendDeletedOn: new Date().getTime(),
                    friendDeletedOnDate: new Date(),
                    friendRequestStatus: "Deleted"
                },
                (err, result) => {
                    if (err) {
                        logger.error("deleted profile API : ", err);
                        return reject(dbErrResponse);
                    }
                    else {
                        return resolve(true);

                    }

                });
        })
    }
    //pull all the friendShipId,pendingFriendShipId,ProfileLikedBy,MyProfileLiked from the userList
    function pullFriendShipStatusFromUserList() {
        return new Promise(function (resolve, reject) {
            userListCollection.UpdateFriendWithPull({}, { friendShipId: ObjectID(_id), pendingFriendShipId: ObjectID(_id), ProfileLikedBy: ObjectID(_id), MyProfileLiked: ObjectID(_id) }, (err, result) => {
                if (err) {
                    logger.error("delete userlist1 API : ", err);
                    return reject(dbErrResponse);
                }
                else {
                    return resolve(true);
                }
            });
        })
    };
    //assign [] friendShipId,pendingFriendShipId,MyProfileLiked, ProfileLikedBy
    function userListFriendShipStatus() {
        return new Promise(function (resolve, reject) {
            let data = { friendShipId: [], pendingFriendShipId: [], ProfileLikedBy: [], MyProfileLiked: [] };
            userListCollection.UpdateById(_id, data, (err, result) => {
                if (err) {
                    logger.error("delete profile userlist API : ", err);
                    return reject(dbErrResponse);
                }
                    return resolve(true);
            });
        })
    };
    function updateProfileStatus() {
        return new Promise(function (resolve, reject) {
            let dataToUpdate = {
                deleteStatus: 1,
                deleteTimeStamp: new Date().getTime()
            };
            userListType.Update(_id, dataToUpdate, (err, result) => {
                logger.error("userListType ", (err) ? err : "")
            });
            userListCollection.UpdateById(_id, dataToUpdate, (err, result) => {
                logger.error("userListCollection ", (err) ? err : "");
            });
            return resolve(true);
        });
    }
    //update the chat status
    function updateChatStatus() {
        return new Promise(function (resolve, reject) {
            let userId = req.auth.credentials._id;
            let memberUserId = "members." + userId;
            chatListCollection.Update({
                [memberUserId]: { "$exists": true },
            }, {
                    "deletedUser": ObjectID(userId),
                    "deletedUserTimeStamp": new Date().getTime()

                }, (err, result) => {
                    if (err) {
                        logger.error("post delete account API : ", err);
                        return reject(dbErrResponse);
                    }
                    else {

                        return resolve(true);
                    }

                })
        })
    };

}
function deleteChatHistory() {
    return new Promise(function (resolve, reject) {
        var userID = "members." + _id;
        let condition = {
            [userID]: { "$exists": true }
        }
        let dataToUpdate = {
            isDeleted: 1,
        };
        chatListCollection.Update(condition, dataToUpdate, (err, result) => {
            logger.error("userListType ", (err) ? err : "")
        });
        userListCollection.UpdateById(_id, dataToUpdate, (err, result) => {
            logger.error("userListCollection ", (err) ? err : "")
        });
        return resolve(true);
    });
}


let response = {
    status: {
        200: { message: local['DeleteProfile']['200'] },
        400: { message: local['genericErrMsg']['400'] },
        500: { message: local['genericErrMsg']['500'] }
    }
}

module.exports = { APIHandler, response }