'use strict'
const logger = require('winston');
// const userListCollection = require('../../../models/userList');
const connection = require('../../../models/connection');
const ObjectID = require('mongodb').ObjectID;
const Joi = require("joi");
const local = require('../../../locales');


let handler = (req, res) => {

    let _id = ObjectID(req.auth.credentials._id);
    // let _id = ObjectID("5ced06d45c40775eecc0dbbf")
    let limit = parseInt(req.query.limit) || 20;
    let offset = parseInt(req.query.offset) || 0;
    console.log(_id);
    //matcvh time id profile picture,address
   /*  let condition = [
        { "$match": { $or:[{"firstUser": _id},{"secondUser":_id} ] } },
        { "$lookup": { "from": "userList", "localField":  "secondUser", "foreignField": "_id", "as": "opponentData" } },
        { "$unwind": "$opponentData" },
        {
            "$project": {
                "countryOrRegionOfsecondUser": 1, "connectionStartedOn": 1, "connectionEndedOn": 1,
                "firstName": "$opponentData.firstName", "countryCode": "$opponentData.countryCode", "contactNumber": "$opponentData.contactNumber",
                "profilePic": "$opponentData.profilePic", "gender": "$opponentData.gender", "email": "$opponentData.email"
            }
        },
        { "$skip": offset },
        { "$limit": limit }
    ] */
    let condition = [
        { "$match": { $or:[{"firstUser": _id},{"secondUser":_id} ] } },
        { "$sort" : { "_id": -1 } },
        {
                    "$project": {
                        "secondUser":{ "$cond": { if: { "$eq": ["$firstUser",_id] }, "then": "$secondUser", else: "$firstUser" } },
                        "countryOrRegionOfsecondUser": 1, "connectionStartedOn": 1, "connectionEndedOn": 1
                    }
                },
                { "$lookup": { "from": "userList", "localField":  "secondUser", "foreignField": "_id", "as": "opponentData" } },
                { "$unwind": "$opponentData" },
                {
                    "$project": {
                        "countryOrRegionOfsecondUser": 1, "connectionStartedOn": 1, "connectionEndedOn": 1,"_id":"$secondUser",
                       "firstName": "$opponentData.firstName", "contactNumber": "$opponentData.contactNumber",
                       "profilePic": "$opponentData.profilePic", "gender": "$opponentData.gender", "email": "$opponentData.email","country":"$opponentData.address.country",
                       "countryCode":"$opponentData.address.countryCode","city":"$opponentData.address.city"
                   }
                },
              
                { "$skip": offset },
        { "$limit": limit }
        //         {      $count: "firstName"   }
    ]
    connection.Aggregate(condition, (err, result) => {
        console.log("result------", result);
        // console.log("condition------", condition);

        if (result.length) {

            return res({ message: req.i18n.__('GetRecentVisitors')['200'], data: result }).code(200);
        } else {
            return res({ message: req.i18n.__('GetRecentVisitors')['412'] }).code(412);
        }

    })
};

const queryValidator = Joi.object({
    offset: Joi.number().description("offset"),
    limit: Joi.number().description("limit"),
}).required();

let response = {
    status: {
        200: {
            message: Joi.any().default(local['GetRecentVisitors']['200']), data: Joi.any()
        },
        412: { message: Joi.any().default(local['GetRecentVisitors']['412']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
}

module.exports = { handler, response, queryValidator }