'use strict'
let headerValidator = require('../../middleware/validator');
let getAPI = require('./Get');

module.exports = [
    {
        method: 'GET',
        path: '/recentVisitors',
        handler: getAPI.handler,
        config: {
            description: 'This API will be used to get recentVisitors List',
            tags: ['api', 'match'],
            auth: 'userJWT',
            // auth:false,
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: getAPI.queryValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: getAPI.response
        }
    },
];