
'use strict'
const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const local = require('../../../locales');
const ObjectID = require('mongodb').ObjectID;
const userListMongoDB = require('../../../models/userList');
const userListES = require('../../../models/userListType');
const otps = require('../../../models/otps');

let APIHandler = (req, res) => {

    let _id = req.auth.credentials._id;
    let countryCode = req.payload.countryCode;
    let contactNumber = req.payload.countryCode + req.payload.phoneNumber;

    checkOTP()
        .then(function (value) {
            return setInMongo();
        }).then(function (value) {
            return setInES();
        }).then(function (value) {
            return res({ message: req.i18n.__('PatchMobileNumber')['200'] }).code(200);
        }).catch(function (err) {
            logger.error('Caught an error!', err);
            return res({ message: req.i18n.__('PatchMobileNumber')['412'] }).code(412);
        });

    function setInMongo() {
        return new Promise(function (resolve, reject) {

            let dataToUpdate = {
                "countryCode": countryCode,
                "contactNumber": contactNumber
            };
            userListMongoDB.UpdateById(_id, dataToUpdate, (err, result) => {
                if (err) {
                    return reject(new Error('Ooops, something broke!'));
                }
                else {
                    return resolve(result);
                }
            });
        });
    }
    function setInES() {
        return new Promise(function (resolve, reject) {

            let dataToUpdate = {
                "countryCode": countryCode,
                "contactNumber": contactNumber
            };

            userListES.Update(_id, dataToUpdate, (err, result) => {
                if (err) {
                    return reject(new Error('Ooops, something broke!'));
                }
                else {
                    return resolve(result);
                }
            });
        });
    }
    function checkOTP() {
        return new Promise(function (resolve, reject) {
            let condition = { "phone": contactNumber, "type": 3, "otp": req.payload.otp };
            otps.SelectOne(condition, (err, result) => {
                if (err) {
                    return reject(new Error('Ooops, something broke!'));
                } else if (result) {
                    return resolve(result);
                } else {
                    return reject(new Error('Ooops, OTP not matched!'));
                }
            });


        });
    }
};

let validator = Joi.object({
    countryCode: Joi.string().required().description("Country Code").error(new Error('countryCode is missing')),
    phoneNumber: Joi.string().required().description("Phonenumber(without space), Eg. 9620826142").error(new Error('phoneNumber is missing')),
    otp: Joi.number().required().description("ex : 123456 ?").example(123456).error(new Error('otp is missing')),
    deviceId: Joi.string().required().description("ex : 123456 ?").example("deviceId").error(new Error('deviceId is missing')),
}).required();

let response = {
    status: {
        200: { message: Joi.any().default(local['PatchMobileNumber']['200']) },
        412: { message: Joi.any().default(local['PatchMobileNumber']['412']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) },
    }
}

module.exports = { APIHandler, validator, response }