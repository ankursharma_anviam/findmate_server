'use strict'
const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;
const moment = require("moment");
const mongoTimestamp = require('mongodb').Timestamp;

const local = require('../../../locales');
const fcmPush = require("../../../library/fcm");
const myLibrary = require("../../../library/myLibrary");
const userListCollection = require('../../../models/userList');
const userMatchCollection = require('../../../models/userMatch');
const coinWalletCollection = require('../../../models/coinWallet');
const coinConfigCollection = require('../../../models/coinConfig');
const dateScheduleCollection = require('../../../models/dateSchedule');
const dateSchedule_TTL_Collection = require('../../../models/dateSchedule_TTL');
const walletCustomerCollection = require('../../../models/walletCustomer');

let validator = Joi.object({
    targetUserId: Joi.string().required().description("ex : 5a281337005a4e3b65bf12a8").example("5a281337005a4e3b65bf12a8").max(24).min(24).description("targetUserId").error(new Error('targetUserId is missing or incorrect it must be 24 char || digit only')),
    proposedOn: Joi.number().required().description("ex : 1512829147000").example(1512829147000).error(new Error('proposedOn is missing  or incorrect it must be 13 digit only')),
    dateType: Joi.number().required().description("ex : 1 : videoDate, 2 :  physicalDate, 3 : audioDate").min(1).max(3).example(1).default(1).error(new Error('dateType is missing  or incorrect it must be 1 || 2 || 3  only')),
    longitude: Joi.number().default(0).error(new Error('longitude is missing')),
    latitude: Joi.number().default(0).error(new Error('latitude is missing')),
    placeName: Joi.string().allow("").default("").error(new Error('placeName is missing')),
}).required();

let APIHandler = (req, res) => {

    const dateTypeObj = {
        1: "videoDate",
        2: "physicalDate",
        3: "audioDate"
    };
    let _id = req.auth.credentials._id;
    var targetUserId = req.payload.targetUserId;
    let dateType = req.payload.dateType;
    var timeStamp = new Date().getTime();
    let date_id = new ObjectID();
    var trigger = "videoDateInitiate";
    trigger = dateTypeObj[dateType];
    var data = {};

    let dataToSave = {
        "_id": date_id,
        "matchId": "",
        "expireAt": new Date(myLibrary.getExpiryTimeForMongoTTL(req.payload.proposedOn, 15)),
        "initiatedBy": ObjectID(_id),
        "initiatorName": "",
        "iniatorProfilePic": "",
        "opponentId": ObjectID(targetUserId),
        "opponentProfilePic": "",
        "opponentName": "",
        "createdTimestamp": timeStamp,
        "chatId": "",
        "status": "NotSet",
        "acceptedDateTimeStamp": 0,
        "negotiations": [{
            "proposerId": ObjectID(_id),
            "proposedOn": req.payload.proposedOn,
            "opponnedId": ObjectID(targetUserId),
            "requestedFor": dateTypeObj[dateType],
            "proposedTime": timeStamp,
            "opponentResponse": "",
            "placeName": req.payload.placeName,
            "longitude": req.payload.longitude,
            "latitude": req.payload.latitude,
        }],
        "dateFeedback": [],
        "callLogId": [],
        "placeName": req.payload.placeName,
        "location": {
            "longitude": req.payload.longitude,
            "latitude": req.payload.latitude,
        }
    }

    checkItValidForDate();

    function checkItValidForDate() {
        let condition = {
            $or: [{
                "initiatedBy": ObjectID(_id),
                "opponentId": ObjectID(targetUserId)
            },
            {
                "initiatedBy": ObjectID(targetUserId),
                "opponentId": ObjectID(_id)
            }
            ],
            "negotiations": {
                "$elemMatch": {
                    "proposedOn": {
                        "$gt": timeStamp
                    },
                    "opponentResponse": {
                        "$in": ["", "accepted"]
                    },
                }
            },
            "isComplete": {
                "$exists": false
            },
            "deActivate": {
                "$exists": false
            }
        };

        dateScheduleCollection.Select(condition, (err, result) => {
            if (err) {
                return res({
                    message: req.i18n.__('genericErrMsg')['500']
                }).code(500);
            }

            if (result[0]) {
                return res({
                    message: req.i18n.__('PostDate')['422']
                }).code(422);
            } else {

                getRequiredCoinForTrigger()
                    .then((value) => {
                        return getUsersCoinFromCoinWalletAndValidate(value);
                    })
                    // checkItmatched()
                    .then((value) => {
                        return checkItmatched();
                    })
                    .then((value) => {
                        return getOppnentData();
                    })
                    .then((value) => {
                        return getMatchData();
                    })
                    .then((value) => {
                        return saveAtDateSchedule();
                    })
                    .then((value) => {
                        return insertInWalletCusetomerAndUpdateInCoinWallet();
                    })
                    .then((value) => {
                        return res({
                            message: req.i18n.__('PostDate')['200'],
                            date_id: date_id,
                            coinWallet: data.coinWallet
                        }).code(200);
                    })
                    .catch((err) => {
                        logger.error('Caught an error!', err);
                        return res({
                            message: err.message
                        }).code(err.code);
                    });

            }

        })
    }

    function getRequiredCoinForTrigger() {
        return new Promise((resolve, reject) => {
            coinConfigCollection.SelectOne({}, (err, result) => {
                if (err) return reject(new Error('Ooops, no have data in coinConfig!'));
                return resolve(result[dateTypeObj[dateType]] || {});
            });
        });
    }

    function getUsersCoinFromCoinWalletAndValidate(requiredCoin) {
        return new Promise((resolve, reject) => {
            coinWalletCollection.SelectOne({
                _id: ObjectID(_id)
            }, (err, result) => {
                if (err) return reject({
                    code: 500,
                    message: req.i18n.__('genericErrMsg')['500']
                });

                logger.silly("requiredCoin ", requiredCoin)
                for (let key in requiredCoin) {
                    if (requiredCoin[key] == 0 || (result && result["coins"][key] && requiredCoin[key] <= result["coins"][key])) { } else {
                        return reject({
                            code: 402,
                            message: req.i18n.__('genericErrMsg')['402']
                        });
                    }
                }
                data["coinWallet"] = result["coins"] || {};
                data["requiredCoin"] = requiredCoin;
                return resolve(true);
            });
        });
    }

    function saveAtDateSchedule() {
        return new Promise(function (resolve, reject) {

            userListCollection.SelectOne({
                _id: ObjectID(targetUserId)
            }, (err, result) => {
                let deviceType = "1";
                if (result && result._id) {
                    deviceType = result.deviceType || "1";
                }

                let payload = {
                    notification: {
                        body: "your message"
                    },
                    data: {
                        field1: '7',
                        deviceType: deviceType
                    } // values must be string
                }

                payload["notification"]["body"] = `Hey ${dataToSave["opponentName"]}, ${dataToSave["initiatorName"]} has sent you a date request for ${moment.tz(req.payload.proposedOn, "Asia/Calcutta").format('Do MMM h:mm a')}.`;
                payload["notification"]["title"] = ""
                payload["data"]["type"] = "7";
                fcmPush.sendPushToTopic("/topics/" + targetUserId, payload, (e, r) => { });
            })

            dateSchedule_TTL_Collection.Insert(dataToSave, (err, result) => {
                if (err) return reject(new Error('Ooops, something broke! at dateSchedule_TTL_Collection'));
            });

            dateScheduleCollection.Insert(dataToSave, (err, result) => {
                if (err) return reject(new Error('Ooops, something broke!'));

                if (result) {
                    return resolve(result);
                } else {
                    return reject(new Error('Ooops, something broke!'));
                }
            });
        });
    }

    function getMatchData() {
        return new Promise(function (resolve, reject) {
            let condition = {
                "$or": [{
                    "initiatedBy": ObjectID(_id),
                    "opponentId": ObjectID(targetUserId)
                },
                {
                    "initiatedBy": ObjectID(targetUserId),
                    "opponentId": ObjectID(_id)
                }
                ],
                "unMatchedBy": {
                    "$exists": false
                }
            };
            userMatchCollection.SelectOne(condition, (err, result) => {
                if (err) return reject(new Error('Ooops, something broke!'));

                if (result) {
                    dataToSave["chatId"] = ObjectID(result.chatId);
                    dataToSave["matchId"] = ObjectID(result._id);
                    return resolve(result);
                } else {
                    return reject(new Error('Ooops, something broke!'));
                }
            });
        });
    }

    function getOppnentData() {
        return new Promise(function (resolve, reject) {
            let condition = {
                "_id": ObjectID(targetUserId)
            };
            userListCollection.SelectOne(condition, (err, result) => {
                if (err) return reject(new Error('Ooops, something broke!'));

                if (result) {
                    dataToSave["opponentName"] = result.firstName;
                    dataToSave["opponentProfilePic"] = result.profilePic || "";
                    return resolve(result);
                } else {
                    return reject(new Error('Ooops, something broke!'));
                }
            });
        });
    }

    function checkItmatched() {
        return new Promise(function (resolve, reject) {
            let condition = {
                "_id": ObjectID(_id),
                "matchedWith": ObjectID(targetUserId)
            };
            console.log("condition ", condition)
            userListCollection.SelectOne(condition, (err, result) => {
                if (err) return reject(new Error('Ooops, something broke!'));

                if (result) {
                    dataToSave["initiatorName"] = result.firstName;
                    dataToSave["iniatorProfilePic"] = result.profilePic || "";
                    return resolve(result);
                } else {
                    return reject(new Error('Ooops, something broke! targetUserId : ', targetUserId));
                }
            });
        });
    }

    function insertInWalletCusetomerAndUpdateInCoinWallet() {
        return new Promise((resolve, reject) => {
            let coinWallet = data.coinWallet;
            let requiredCoin = data.requiredCoin;

            let insertData = [];
            let txnId = "TXN-ID-" + Math.floor(Math.random() * 10000000000);
            var docTypeCode = 0;
            var docType = "";
            for (let key in requiredCoin) {

                if (coinWallet[key]) {
                    if (dateType == 1) {
                        //video Date
                        trigger = `Spent ${requiredCoin[key]} coins on scheduling a video date`;
                        docTypeCode = 6;
                        docType = "on video date";
                    } else if (dateType == 2) {
                        //inperson Date
                        trigger = `Spent ${requiredCoin[key]} coins on scheduling a inperson date`;
                        docTypeCode = 8;
                        docType = "on inperson date";
                    } else {
                        //audio Date
                        trigger = `Spent ${requiredCoin[key]} coins on scheduling a audio date`;
                        docTypeCode = 7;
                        docType = "on audio date";
                    }
                    insertData.push({
                        "txnId": txnId,
                        "userId": ObjectID(_id),
                        "txnType": "DEBIT",
                        "txnTypeCode": 2,
                        "trigger": trigger,
                        "docType": docType,
                        "docTypeCode": docTypeCode,
                        "currency": "N/A",
                        "currencySymbol": "N/A",
                        "coinType": key,
                        "coinOpeingBalance": coinWallet[key],
                        "cost": 0,
                        "coinAmount": requiredCoin[key],
                        "coinClosingBalance": (coinWallet[key] - requiredCoin[key]),
                        "paymentType": "N/A",
                        "timestamp": new Date().getTime(),
                        "transctionTime": new Date().getTime(),
                        "transctionDate": new Date().getTime(),
                        "paymentTxnId": "N/A",
                        "initatedBy": "customer"
                    });
                    coinWallet[key] = coinWallet[key] - requiredCoin[key];
                }
            }
            walletCustomerCollection.InsertMany(insertData, () => { });
            coinWalletCollection.Update({
                _id: ObjectID(_id)
            }, {
                    "coins": coinWallet
                }, () => { });
            coinWallet = data;
            return resolve(true);
        });
    }
};

let response = {
    status: {
        200: {
            message: Joi.any().default(local['PostDate']['200']),
            data: Joi.any(),
            coinWallet: Joi.any()
        },
        412: {
            message: Joi.any().default(local['PostDate']['412'])
        },
        422: {
            message: Joi.any().default(local['PostDate']['422'])
        },
        400: {
            message: Joi.any().default(local['genericErrMsg']['400'])
        },
        402: {
            message: Joi.any().default(local['genericErrMsg']['402'])
        },
    }
}

module.exports = {
    APIHandler,
    validator
}