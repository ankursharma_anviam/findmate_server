'use strict'
const Joi = require("joi");
const async = require("async");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;
const local = require('../../../locales');
const GeoPoint = require('geopoint')
const mqttClient = require("../../../library/mqtt");
const userListCollection = require('../../../models/userList');
const friendshipCollection = require('../../../models/friendship');
const fcmPush = require("../../../library/fcm");
const chatLikesCollection = require('../../../models/chatList');
const messagesCollection = require('../../../models/messages');
var friendShipId;
var deviceType = "1";

/**
 * @method POST friendRequestPesponse
 * @description This API is used to post friendRequestPesponse.
 * @param {*} req 
 * @param {*} res 
 * @property {*} authorization in header
 * @property {*} lang in header
 * @returns  200 : User found in database.
 * @returns  500 : An unknown error has occurred.
 */
let validator = Joi.object({
    targetUserId: Joi.string().required().description("targetUserId").error(new Error('targetUserId is missing')),
    statusCode: Joi.number().integer().min(1).max(2).required().description("statusCode 1 for accept,2 for deny").error(new Error('statusCode is missing'))
}).required();

const handler = (req, reply) => {
    const friendRequestAcceptResponse = { message: req.i18n.__('friendRequestResponseAccept')['200'], code: 200 };
    const friendRequestDeniedResponse = { message: req.i18n.__('friendRequestResponseDenied')['200'], code: 200 };
    const dbErrResponse = { message: req.i18n.__('genericErrMsg')['500'], code: 500 };
    var _id = new ObjectID(req.auth.credentials._id);
    var targetUserId = req.payload.targetUserId;
    var friendRequestStatus = ((req.payload.statusCode) == 1) ? "Accepted" : "Denied";
    var chatId = new ObjectID(), friendShipId;
    var msg_id = new ObjectID();

    var status = req.payload.statusCode;
    //add the friendship detail in friendship collection
    let postFriendship = () => {
        return new Promise((resolve, reject) => {
            //if accept request
            if (req.payload.statusCode == parseInt(1)) {
                friendshipCollection.Update({ "friendRequestSentByerId": new ObjectID(targetUserId), "friendRequestSentTo": _id, "friendRequestStatus": "Pending" },
                    {
                        friendRequestResponse: new Date().getTime(),
                        friendRequestResponseOnDate: new Date(),
                        friendRequestStatus: friendRequestStatus
                    }, (err, result) => {
                        if (err) {
                            logger.error("post friendship request response API : ", err);
                            return reject(dbErrResponse);
                        }
                        else if (result.result.nModified === parseInt(1)) {
                            //remove the pendingFriendShipId from targeruserId
                            userListCollection.UpdateFriendWithPull({ _id: new ObjectID(targetUserId) }, { pendingFriendShipId: _id }, (err12, result12) => {
                                if (err12) {
                                    logger.error("pull pendingFriendShipId API : ", err)
                                    return reject(dbErrResponse);
                                }
                            })
                            //remove the pendingFriendShipId from loggedin userList
                            userListCollection.UpdateFriendWithPull({ _id: _id }, { pendingFriendShipId: new ObjectID(targetUserId) }, (err13, result13) => {
                                if (err13) {
                                    logger.error("pull pendingFriendShipId of logged in user API : ", err)
                                    return reject(dbErrResponse);
                                }
                            })

                            //updating the friendShipId to targer user in the userlist 
                            let dataToUpdate = { friendShipId: new ObjectID(targetUserId) };
                            userListCollection.UpdateById1(_id, dataToUpdate, (err1, result1) => {
                                if (err1) return reject(new Error('Ooops, something broke!'));
                            });

                            //updating the own user in the userlist
                            let dataToUpdate1 = { friendShipId: _id };
                            userListCollection.UpdateById1(new ObjectID(targetUserId), dataToUpdate1, (err2, result2) => {
                                if (err2) return reject(new Error('Ooops, something broke!'));
                            });
                            //chat entry
                            friendshipCollection.Select({ "friendRequestSentByerId": new ObjectID(targetUserId), "friendRequestSentTo": _id, "friendRequestStatus": "Accepted" }, (err3, res3) => {
                                if (err3) return reject(new Error('Ooops, something broke!'));
                                friendShipId = res3[0]._id;
                                return resolve(true);

                            });

                        }
                        else {
                            return reject(new Error('Ooops, something broke!'))
                        }
                    })
                sendFCMAndMqttMassage()
            }
            //if ignored request
            if (req.payload.statusCode === parseInt(2)) {
                friendshipCollection.Update({ "friendRequestSentByerId": new ObjectID(targetUserId), "friendRequestSentTo": _id }, {
                    friendRequestResponse: new Date().getTime(),
                    friendRequestResponseOnDate: new Date(),
                    friendRequestStatus: friendRequestStatus
                }, (err, result) => {
                    if (err) {
                        logger.error("post shortList API : ", err)
                        return reject(dbErrResponse);
                    }
                    if (result.result.nModified == parseInt(1)) {
                        //remove userId from sentReqeuests[]
                        userListCollection.UpdateFriendWithPull({ _id: _id }, { pendingFriendShipId: new ObjectID(targetUserId) }, (err, result) => {
                            if (err) {
                                logger.error("patch userlist API : ", err);
                                return reject(dbErrResponse);
                            }
                            else {
                                //remove userId from friendRequest[]

                                userListCollection.UpdateFriendWithPull({ _id: new ObjectID(targetUserId) }, { pendingFriendShipId: new ObjectID(_id) }, (err, result) => {
                                    if (err) {
                                        logger.error("patch userlist API : ", err)
                                        return reject(dbErrResponse);
                                    }

                                    else {
                                        return resolve(friendRequestDeniedResponse);
                                    }
                                })

                            }

                        })

                    }
                    else {
                        reject(new Error('Ooops, something broke!'))
                    }
                })
            }
        });
    };

    function getChatId() {
        return new Promise(function (resolve, reject) {

            let userId = "members." + _id.toHexString();
            let targetId = "members." + targetUserId
            let condition = {
                // "isMatchedUser": { "$in": [2, 3] },
                "deletedUser": { "$exists": false },
                [userId]: { "$exists": true },
                [targetId]: { "$exists": true }
            };

            chatLikesCollection.SelectOne(condition, (err, result) => {
                console.log("========>", result)
                if (err) return reject(new Error('Ooops, something broke!'));

                if (result && result._id)  {
                    console.log("iffffffffffffffffffffffffffffffffffffffffff")

                    chatId = ObjectID(result._id);

                    let condition = { "_id": chatId };
                    let dataToPush = {
                        "message": [msg_id],
                    };
                    chatLikesCollection.UpdateByIdWithPush(condition, dataToPush, (e, r) => { if (e) logger.error("SODJKOSD ", e) });
                    let dataToUpdate = { "isMatchedUser": 2, "friendShipId": ObjectID(friendShipId), "matchedUserStatus": "chatWithFriend", };
                    chatLikesCollection.Update(condition, dataToUpdate, (e, r) => { if (e) logger.error("SODJKOSD ", e) });
                    let userId = "members." + _id + ".inactive";
                    let targetId = "members." + targetUserId + ".inactive";
                    let unsetInactiveStatus = {
                        [userId]: "",
                        [targetId]: ""
                    }
                    chatLikesCollection.UpdatewithUnset(condition, unsetInactiveStatus, (e, r) => { if (e) logger.error("SODJSDSSD ", e) });

                } else {
                    console.log("elseeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee")

                    let dataToInsert = {
                        "_id": chatId,
                        "message": [msg_id],
                        "members": {
                            [_id]: {
                                "status": "NormalMember"
                            },
                            [targetUserId]: {
                                "status": "NormalMember"
                            }
                        },
                        "initiatedBy": ObjectID(_id),
                        "friendShipId": ObjectID(friendShipId),
                        "createdAt": new Date().getTime(),
                        "chatType": "NormalChat",
                        "secretId": "",
                        "isMatchedUser": 2,
                        "matchUserStatus": "chatWithFriend",
                        creationDate: new Date()
                    };
                    chatLikesCollection.Insert(dataToInsert, (e, r) => { });

                }
                let newMessage = {
                    "_id": msg_id,
                    "messageId": "" + new Date().getTime(),
                    "secretId": "",
                    "dTime": -1,
                    "senderId": ObjectID(_id),
                    "receiverId": ObjectID(targetUserId),
                    "payload": "3embed test",
                    "messageType": "0",
                    "timestamp": new Date().getTime(),
                    "expDtime": 0,
                    "chatId": chatId,
                    "userImage": req.user.profilePic,
                    "toDocId": "test message",
                    "name": req.user.firstName,
                    "isWithoutMatch": true,
                    "members": {
                        [_id]: {

                        },
                        [targetUserId]: {
                            "status": 0
                        }
                    },
                    creationDate: new Date(),
                }
                messagesCollection.Insert(newMessage, (e, r) => { })
                return resolve({ chatId: chatId });
            })
        });
    }
    function sendFCMAndMqttMassage() {
        return new Promise(function (resolve, reject) {
            let payloadUser1 = {
                notification: {
                    body: "" + req.user.firstName + " just accepted your friend request,you can now chat and call freely with, " + req.user.firstName,
                    title: "Friend Request Accepted"
                },
                data: { type: "1", target_id: req.auth.credentials._id, deviceType: deviceType }
            }
            fcmPush.sendPushToTopic("/topics/" + req.payload.targetUserId, payloadUser1, (e, r) => {
                if (e) logger.error(e);
                //  console.log("r-----", r);
            });

            userListCollection.SelectOne({ _id: ObjectID(targetUserId) }, (err, result) => {
                if (err) logger.error("ASASDASASD : ", err);

                if (result && result._id) {
                    var targetUserName = result.firstName;
                    let dataToSend = {
                        "acceptTimeStamp": new Date().getTime(),
                        "friendRequestSentById": targetUserId,
                        "friendRequestSentByName": result.firstName,
                        "friendRequestSentByPhoto": result.profilePic,
                        "friendRequestSentByCountry": (result.address) ? result.address.country : "India",
                        "friendRequestSentByCountryShortName": (result.address) ? result.address.countryCode : "IN",
                        "friendRequestSentByCity": (result.address) ? result.address.city : Bangalore,
                        "acceptFriendRequestById": _id,
                        "acceptFriendRequestByName": req.user.firstName,
                        "acceptFriendRequestByPhoto": req.user.profilePic,
                        "acceptRequestSentByCountry": (req.user.address) ? req.user.address.country : "India",
                        "acceptRequestSentByCountryShortName": (req.user.address) ? req.user.address.countryCode : "IN",
                        "acceptRequestSentByCity": (req.user.address) ? req.user.address.city : Bangalore,
                        "messageType": "friendRequestAccept",
                        "chatId": chatId
                    };


                    mqttClient.publish(req.auth.credentials._id, JSON.stringify(dataToSend), { qos: 1 }, (e, r) => { if (e) logger.error("SADASDASD : ", e) })
                    mqttClient.publish(targetUserId, JSON.stringify(dataToSend), { qos: 1 }, (e, r) => { if (e) logger.error("SADASDAASDSD : ", e) });

                }
                return resolve(friendRequestAcceptResponse);
            })
        });
    }
    postFriendship()
        .then((data) => { return getChatId() })
        .then((data) => { return status == parseInt(1) ? reply({ message: req.i18n.__('friendRequestResponseAccept')['200'] }).code(200) : reply({ message: req.i18n.__('friendRequestResponseDenied')['200'] }).code(200); })
        .catch((err) => {
            logger.err("get profile by FindMatchId API error : ", err);
            return reply({ message: err.message }).code(err.code);
        });
};


let response = {

}

module.exports = { handler, response, validator }
