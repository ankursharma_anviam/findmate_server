'use strict'
let headerValidator = require('../../middleware/validator');
let friendRequestResponse= require('./Post');
module.exports = [
    {
        method: 'POST',
        path: '/friendRequestResponse',
        handler: friendRequestResponse.handler,
        config: {
            description: 'This API will be used to either accept / deny the friend request.',
            tags: ['api', 'friendRequestResponse'],
            auth: 'userJWT',
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: friendRequestResponse.validator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
             response: friendRequestResponse.response
        }
    }
    
];