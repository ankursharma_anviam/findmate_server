'use strict';
/**
 * @method GET likedProfile
 * @description This API allows user to get the Profile Like list
 * @author ikshita@mobifyi.com
 * @property {string} lan - in header
 * @property {string} authorization - in header
 * @returns 400:Mandatory field missing.
 * @returns 500:Internal server error.
 * @returns 200:Got The Details..
 */
const Joi = require('joi');
const logger = require('winston');
const errorMsg = require('../../../locales');
const ObjectID = require('mongodb').ObjectID;
const likedPostListCollection = require('../../../models/likedUser');
const userListCollection = require('../../../models/userList');
const APIHandler = (req, reply) => {

    let limit = parseInt(req.query.limit) || 20;
    let offset = parseInt(req.query.offset) || 0;
    var datoTosend = [];

    const dbErrResponse = { message: req.i18n.__('genericErrMsg')['500'], code: 500 };
    let userId = new ObjectID(req.auth.credentials._id);
    let getLikedPostList = () => {
        return new Promise((resolve, reject) => {
            userListCollection.OtherLikeDetailWithPagination(userId,limit,offset, (err, result) => {
                if (err) {
                    logger.error("get shortedlist API : ", err)
                    return reject(dbErrResponse);
                }
                else {
                    result.forEach(e => {
                        if(!e.userdetails.address){
                            e.userdetails["adderss"]["country"] = "India"
                            e.userdetails["adderss"]["countryCode"] = "IN"
                            e.userdetails["adderss"]["city"] = "Bangalore"
                        }
                        datoTosend.push(e.userdetails);
                    })       
                    return resolve(datoTosend);
                }
            })
        });
    };



    getLikedPostList()
        .then((data) => { return reply({ message: req.i18n.__('genericErrMsg')['200'], data: data }).code(200); })
        .catch((err) => {
            logger.error("get sortedlist error : ", err);
            return reply({ message: err.message }).code(err.code);
        });
};


const responseCode = {
    status: {
        200: { message: Joi.any().default(errorMsg['genericErrMsg']['200']), data: Joi.any() },
        400: { message: Joi.any().default(errorMsg['genericErrMsg']['400']) },
        500: { message: Joi.any().default(errorMsg['genericErrMsg']['500']) },

    }
}
//swagger response code

module.exports = { APIHandler, responseCode };