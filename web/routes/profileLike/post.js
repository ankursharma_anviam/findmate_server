'use strict';
/**
 * @method POST likeUser
 * @description This API allows user to get the favorite list
 * @author ikshita@mobifyi.com
 * @param {string} postId
 * @property {string} lan in header
 * @property {string} authorization in header
 * @returns  400:Mandatory field missing.
 * @returns  500:Internal server error.
 * @returns  200:Got The Details.
 */

const Joi = require('joi');
const logger = require('winston');
const errorMsg = require('../../../locales');
const ObjectID = require('mongodb').ObjectID;
const fcmPush = require("../../../library/fcm");
const likedUserCollection = require('../../../models/likedUser');
const userListCollection = require('../../../models/userList');
const payload = Joi.object({
    userId: Joi.string().max(24).min(24).required().description('userId').error(new Error('userId is missing or invalid')),
});

const APIHandler = (req, reply) => {
    const dbErrResponse = { message: req.i18n.__('genericErrMsg')['500'], code: 500 };
    var userId = new ObjectID(req.auth.credentials._id);
    var likedUserId = new ObjectID(req.query.userId);

    //add the Post to LikedPost
    let postLikedPostlist = () => {
        return new Promise((resolve, reject) => {
            let actionData = {
                actionBy: userId,
                actionOnProfile: likedUserId,
                actionType: "like",
                actionOn: new Date().getTime(),
                actionOnDate: new Date()
            }
            likedUserCollection.findOneAndUpdate({ actionBy: userId, actionOnProfile: likedUserId }, actionData, (err, result) => {
                if (err) {
                    logger.error("post action API : ", err)
                    return reject(dbErrResponse);
                } else if (result.result.nModified == 1) {
                    console.log("=====>>>>>", userId);
                    userListCollection.UpdateByIdWithAddToSet({ _id: userId.toString() }, { myLikes: likedUserId }, (e, r) => { if (e) logger.error(e) })
                    return reject({ code: 409, message: req.i18n.__('likedlistPost')['409'] });
                } else {
                    let deviceType = "1";
                    let payloadUser1 = {
                        notification: {
                            body: "You just got liked by " + req.user.firstName
                            , title: "Profile Like"
                        },
                        data: { type: "1", target_id: req.auth.credentials._id, deviceType: deviceType }
                    }
                    userListCollection.UpdateByIdWithAddToSet({ _id: userId.toString() }, { myLikes: likedUserId }, (e, r) => { if (e) logger.error(e) })
                    fcmPush.sendPushToTopic("/topics/" + req.query.userId, payloadUser1, (e, r) => {
                        if (e) {
                            logger.error("FCM error----", e);
                        }
                        else {
                            logger.silly("FCM respose---", r);
                        }
                    });

                    return resolve(true);
                }
            })
        });
    };

    //update the the userId of targetUserId to the UserList

    let updateUserlist = () => {
        return new Promise((resolve, reject) => {
            let likedPostListdata1 = {
                ProfileLikedBy: userId,
            }

            userListCollection.updateWithAddToSet(likedUserId, likedPostListdata1, (err, result) => {
                if (err) {
                    logger.error("update likedlist in userlist API : ", err);
                    return reject(dbErrResponse);
                }
                else {
                    return resolve(true);
                }
            })
        });
    };
    //update the the own liked to the UserList

    let updateUserlistForOwnLike = () => {
        return new Promise((resolve, reject) => {
            let likedPostListdata = {
                MyProfileLiked: likedUserId,
            }
            userListCollection.updateWithAddToSet(userId, likedPostListdata, (err, result) => {
                if (err) {
                    logger.error("update MyProfileLike in userlist API : ", err);
                    return reject(dbErrResponse);
                }
                else {
                    return resolve({ code: 200, message: req.i18n.__('likedlistPost')['200'] });
                }
            })
        });
    };

    postLikedPostlist()
        .then(() => { return updateUserlist() })
        .then(() => { return updateUserlistForOwnLike() })
        .then(() => { return reply({ message: req.i18n.__('likedlistPost')['200'] }).code(200); })
        .catch((err) => {
            logger.error("Post likedlist API error : ", err);
            return reply({ message: err.message }).code(err.code);
        });
};


const responseCode = {
    status: {
        200: { message: Joi.any().default(errorMsg['likedlistPost']['200']) },
        400: { message: Joi.any().default(errorMsg['likedlistPost']['400']) },
        409: { message: Joi.any().default(errorMsg['likedlistPost']['409']) },
        500: { message: Joi.any().default(errorMsg['genericErrMsg']['500']) },

    }
}//swagger response code

module.exports = { payload, APIHandler, responseCode };