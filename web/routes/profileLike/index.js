
'use strict';
const entity = "likedPost";
const postAPI = require('./post');
const getAPI= require('./getMy');
const getAPIOther= require('./getOther');

let headerValidator = require("../../middleware/validator");

module.exports = [
    /**
    * api to add new card
    */
    {
        method: 'POST',
        path: '/profileLike',
        handler: postAPI.APIHandler,
        config: {
            tags: ['api', 'profileLike'],
            description: 'This API will be used to post a like on the opposite user’s profile',
            auth: "userJWT",
            response: postAPI.responseCode,
            validate: {
                query: postAPI.payload,
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
    {
        method: 'GET',
        path: '/MyProfileLike',
        handler: getAPI.APIHandler,
        config: {
            tags: ['api', 'profileLike'],
            description: 'This API will be used to get user own like',
            auth: "userJWT",
            response: getAPI.responseCode,
            validate: {
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
    {
        method: 'GET',
        path: '/ProfileLikeBy',
        handler: getAPIOther.APIHandler,
        config: {
            tags: ['api', 'profileLike'],
            description: 'This API will be used to get who liked my profile',
            auth: "userJWT",
            response: getAPIOther.responseCode,
            validate: {
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
]