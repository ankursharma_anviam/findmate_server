const faceBookLogin = require('./faceBookLogin');
// const login = require('./login');
const preferences = require('./preferences');
const verificationCode = require('./verificationCode');
const verification = require('./verification');
const requestOtp = require('./requestOtp');
const profile = require('./profile');
const unCompleteUser = require('./unCompleteUser');
const searchPrefrances = require('./searchPrefrances');
const searchResult = require("./searchResult");
const like = require("./like");
const unLike = require("./unLike");
const match = require("./match");
const supperLike = require("./supperLike");
const location = require("./location");
const instagramId = require("./instagramId");
const recentVisitors = require("./recentVisitors");
const unMatch = require("./unMatch");
const currentDates = require("./currentDates");
const date = require("./date");
const dateResponse = require("./dateResponse");
const dateRating = require("./dateRating");
const pastDates = require("./pastDates");
const onlineUsers = require("./onlineUsers");
const askAQuestion = require("./askAQuestion");
const makeASuggestion = require("./makeASuggestion");
const rateToApp = require("./rateToApp");
const reportAnIssue = require("./reportAnIssue");
const reportUser = require("./reportUser");
const mobileNumber = require("./mobileNumber");
const subscription = require("./subscription");
const plan = require("./plan");
const coinPlan = require("./coinPlan");
const currentCoinBalance = require("./currentCoinBalance");
const trigger = require("./trigger");
const coinHistory = require("./coinHistory");
const planHistory = require("./planHistory");
const chatWithoutFriend = require("./chatWithOutFriend");
const block = require("./block");
const unBlock = require("./unBlock");
const chats = require("./chats");
const rewind = require("./rewind");
const boost = require("./boost");
const coinConfig = require("./coinConfig");
const messageWithoutMatch = require("./messageWithoutMatch");
const messages = require("./messages");
const activePlans = require("./activePlans");
const coinFromVideo = require("./coinFromVideo");
const checkFbId = require("./checkFbId");
const version = require("./version");
const completeDate = require("./completeDate");
const friends= require("./friends");
const friendRequestResponse= require("./friendRequestRespose");
const friendRequest= require("./friendRequest");
const searchFriend = require("./searchFriend");
const findMateId = require('./findMateId');
const unfriendUser = require('./unfriendUser');
const FriendsByName = require('./FriendsByName');
const profileLike = require('./profileLike');
const MyProfileLikeCount= require('./MyLikeCount');
const messageWithoutFriend= require('./messageWithoutFirend');
const chatWithoutMatch = require("./chatWithoutMatch");
const LiveUsersByPreferences = require("./LiveUsersByPreferences");
const cloudinary = require("./cloudinary");

const findProspect = require('./findProspects');
const dissconnectUser = require('./disconnectUser');

module.exports = [].concat(verification, faceBookLogin, verificationCode, preferences, requestOtp,
    profile, unCompleteUser, searchPrefrances, searchResult, like, unLike, match,
    supperLike, location, instagramId, recentVisitors, unMatch, currentDates, date,
    dateResponse, dateRating, pastDates, onlineUsers, askAQuestion, makeASuggestion,
    rateToApp, reportAnIssue, reportUser, mobileNumber, subscription, coinPlan,
    currentCoinBalance, trigger, coinHistory, planHistory, plan,
    block, unBlock, chats, rewind, boost, coinConfig, messageWithoutMatch,
     activePlans, coinFromVideo, checkFbId, version, completeDate,findMateId,searchFriend,friendRequest,friends,friendRequestResponse,
    unfriendUser,FriendsByName,profileLike,MyProfileLikeCount,chatWithoutFriend,chatWithoutMatch,
    messageWithoutFriend,LiveUsersByPreferences,
    messages, cloudinary,findProspect,dissconnectUser
);