'use strict'
var GetAPI = require('./Get');
let headerValidator = require('../../middleware/validator');

module.exports = [
    {
        method: 'GET',
        path: '/cloudinaryDetails',
        handler: GetAPI.APIHandler,
        config: {
            description: 'This API will return latest version of the APP',
            tags: ['api', 'version'],
            auth: false,
            // validate: {
            //     headers: headerValidator.headerAuthValidator
            // },
            response: GetAPI.APIResponse
        }
    }
];
