'use strict'
const Joi = require("joi");
const local = require('../../../locales');
const cloudinary = require("../../../models/cloudinary")
/**
 * @method GET version
 * @description This API use to GET version.
 
 * @param {*} req 
 * @param {*} res 
 
 * @returns  200 : retun latest version.
 * @returns  500 : An unknown error has occurred.
 */
let APIHandler = (req, res) => {

    cloudinary.SelectOne({}, (err, result) => {
        if (err) {
            console.log("error ", err)
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else {
            return res({ message: req.i18n.__('GetCloudinary')['200'], data: result }).code(200);
        }
    })
};

let APIResponse = {
    status: {
        200: { message: local['GetCloudinary']['200'], data: Joi.any() },
        500: { message: local['genericErrMsg']['500'] }
    }
}

module.exports = { APIHandler, APIResponse }