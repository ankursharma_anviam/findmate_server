'use strict'
let headerValidator = require('../../middleware/validator');
let disconnectUser = require('./Post');
module.exports = [
    {
        method: 'POST',
        path: '/disconnectUser',
        handler: disconnectUser.handler,
        config: {
            description: 'This API is used to find users on right swipe',
            tags: ['api', 'swipeToConnect'],
            auth: 'userJWT',
            // auth: false,
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: disconnectUser.validator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: disconnectUser.response
        }
    }
];