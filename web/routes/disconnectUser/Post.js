'use strict'
const local = require('../../../locales');
const Joi = require("joi");
// const Promise = require('promise');
const Promise = require('bluebird');

const redisOperation = require('../../../models/redisOperation');


const connection = Promise.promisifyAll(require('../../../models/connection'));
const ObjectID = require('mongodb').ObjectID;

const validator = Joi.object({
    callEndedByThisUser: Joi.boolean().required().description("should be true or false").error(new Error('filed is required')),

})

//* @param {string} phoneNumber this will be 10 digit mobile numbe rof the user

/**
 * @method POST disconnectUser
 * @description This API is use to disconnect user.
 * @author karthikb@mobifyi.com
* @property {string} language in header
* @property {string} authorization in header
* @returns 200:user sucessfully disconnected
* @returns  500:internal server error 
* @returns  404:user already disconnected
 */
const handler = (req, res) => {

    let currentUser = req.auth.credentials._id
    // let currentUser = "5cb87f2048b7c44e7cc8a5d0";

    //now we will change the status of user to "disconnected".
    let changeStatusOfUser = () => {

        return new Promise(async (resolve, reject) => {


            let changedUserStatus = await redisOperation.changeUserStatusInRedis(currentUser, "dissconnected");

            console.log('changedUserStatus', changedUserStatus);
            changedUserStatus == 'OK' ? resolve('OK') : reject();

        })

    }

    //change the status of the user to dissconnected in redis and mongodb
    let changeUserStatusInDb = (changedUserStatus) => {

        return new Promise(async (resolve, reject) => {

            //userdetails of connected user
            let userConnection;

            if (changedUserStatus) {
                // get connection object with userId and connectionEndedOn is not set 

                let getConnectionCondition = {
                    $or: [
                        { firstUser: ObjectID(currentUser) },
                        { secondUser: ObjectID(currentUser) }
                    ],
                    connectionEndedOn: 0,
                    Duration: 0,
                }

                //update prospect connection ended on

                try {
                    userConnection = await connection.SelectOneAsync(getConnectionCondition);

                    console.log('userConnection updated in db?',userConnection);

                    if(userConnection == null) return res({ message: req.i18n.__('disconnectUser')['404'] }).code(404);

                } catch (err) {
                    res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
                }
                console.log('userConnection========>', userConnection);

                //set connectionEndedOn and duration in connection object and update
                let changeInConnectionData = {

                    callEndedBy:req.payload.callEndedByThisUser ? ObjectID(currentUser):"",
                    connectionEndedOn : new Date().getTime(),
                    Duration : Math.floor(((new Date().getTime()-userConnection.connectionStartedOn) % 86400000)%3600000)%60000
                }
                console.log('ppppppppp',changeInConnectionData);

                try{

                    //update duration of chat and callEndedBy in database
                    let updatedConnection = await connection.findOneAndUpdateAsync({_id:userConnection._id}, {$set:changeInConnectionData});
                    res({ message: req.i18n.__('disconnectUser')['200'] }).code(200);
                }catch(err){
                    res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
                }

                // console.log('updated',updatedConnection);


            }
        })


    }





    changeStatusOfUser()
        .then((changedUserStatus) => changeUserStatusInDb(changedUserStatus))
        .catch((err)=> res({"message": err.message}) )

}




let response = {
    status: {
        200: { message: Joi.any().default(local['disconnectUser']['200']) },
        404: { message: Joi.any().default(local['disconnectUser']['404']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}


module.exports = { validator, handler, response };