'use strict'
const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const userListCollection = require('../../../models/userList');
const userListType = require('../../../models/userListType');
const local = require('../../../locales');


let validator = Joi.object({
    instagramId: Joi.string().required().description("instagramId").example("751730862").error(new Error('instagramId is missing')),
    instagramName: Joi.string().required().description("instagramName").example("instagramName").error(new Error('instagramName is missing')),
    instaGramToken: Joi.string().allow("").description("instaGramToken").example("751730862").error(new Error('instaGramToken is missing')),
}).required();



let handler = (req, res) => {

    let _id = req.auth.credentials._id;

    updateInMongo()
        .then(value => {
            return updateInElasticSearch();
        }).then(value => {
            return res({ message: req.i18n.__('PatchInstagramId')['200'] }).code(200);
        })
        .catch(function (err) {
            logger.info('Caught an error!', err);
            return res({ message: req.i18n.__('PatchInstagramId')['412'] }).code(412);
        })


    function updateInMongo() {
        return new Promise(function (resolve, reject) {
            let data = {
                "instaGramProfileId": req.payload.instagramId,
                "instaGramToken": req.payload.instaGramToken || "",
                "instagramName": req.payload.instagramName
            };
            userListCollection.UpdateById(_id, data, (err, result) => {
                return (err) ? reject(new Error('Ooops, something broke! 101')) : resolve("--");
            });
        });
    }
    function updateInElasticSearch() {
        return new Promise(function (resolve, reject) {

            let data = {
                "instaGramProfileId": req.payload.instagramId,
                "instaGramToken": req.payload.instaGramToken || "",
                "instagramName": req.payload.instagramName
            };
            userListType.Update(_id, data, (err, result) => {

                return (err) ? reject(new Error('Ooops, something broke! 102')) : resolve("--");
            });
        });
    }

};

let response = {
    status: {
        200: { message: Joi.any().default(local['PatchInstagramId']['200']) },
        412: { message: Joi.any().default(local['PatchInstagramId']['412']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
}

module.exports = { handler, validator, response }