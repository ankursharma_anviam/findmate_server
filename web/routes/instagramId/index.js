'use strict'
let headerValidator = require('../../middleware/validator');
let patchAPI = require('./Patch');

module.exports = [
    {
        method: 'PATCH',
        path: '/instagramId',
        handler: patchAPI.handler,
        config: {
            description: 'This API will be used by the passport feature to update the user’s location so that the future search runs off this new location',
            tags: ['api', 'location'],
            auth: 'userJWT',
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: patchAPI.validator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: patchAPI.response
        }
    },
];