'use strict'
let headerValidator = require('../../middleware/validator');
let getAPI = require('./Get');

module.exports = [
    {
        method: 'GET',
        path: '/deactivateReasons',
        handler: getAPI.handler,
        config: {
            description: 'This API will be used by the passport feature to update the user’s location so that the future search runs off this new location',
            tags: ['api', 'login'],
            auth: 'userJWT',
            validate: {
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: getAPI.response
        }
    }
];