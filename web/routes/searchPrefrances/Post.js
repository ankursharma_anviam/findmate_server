'use strict'
const Joi = require("joi");
const async = require("async");
const Cryptr = require('cryptr');
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;


const userListType = require('../../../models/userListType');
const userListCollection = require('../../../models/userList');
const searchPreferences = require('../../../models/searchPreferences');


const payloadValidator = Joi.object({
    preferences: Joi.array().required().description(`[{"pref_id":"57231014e8408f292d8b4567","selectedValue":["Male"]},{"pref_id":"55d48f9bf6523cc552c51c52","selectedValue":[0,500],"selectedUnit":"km"}, {"pref_id":"58bfb210849239b062ca9db4","selectedValue":[100,300],"selectedUnit":"centimetre"},{"pref_id":"55d486a5f6523c582bc51c53","selectedValue":[20,75],"selectedUnit":"year"}]`).error(new Error('preferences is missing')),
    longitude: Joi.number().description("longitude").allow("").example(77.3086251).error(new Error('longitude is missing')),
    latitude: Joi.number().description("latitude").allow("").example(13.0983762).error(new Error('latitude is missing')),
    isPassportLocation: Joi.boolean().default(false).description("isPassportLocation true/false").example(true).error(new Error('isPassportLocation is missing')),
}).required();
/**
 * @method POST  preferences
 * @description This API used to update preferences.
 
 * @param {*} req 
 * @param {*} res 
 
 * @property {*} authorization in header
 * @property {*} lang in header
 * @property {array} preferences pref_id
 * @property {*} type type : 1:searchPreferences,2:myPreferences
 */
let APIHandler = (req, res) => {
    let _id = req.auth.credentials._id;
    let preferences = [];
    let favoritePreObj = {};
    let searchPrefObj = {};

    let modifiy = 0, newCreate = 0, unModified = 0, notInPayload = 0;
    
    getPrefrances()
        .then(function (value) { return margePrefranceFromPayload(); })
        .then(function (value) { return getPrefrancesFromDB(); })
        .then(function (value) { return convertObjectToArrayForupdate(); })
        .then(function (value) { return updatePreferences(); })
        .then(function (value) { return updatePreferences_ES(); })
        .then(function (value) { return updateLocation(); })
        .then(function (value) {
            return res({
                message: req.i18n.__('genericErrMsg')['200'],
                data: { modifiy: modifiy, unModified: unModified, newCreate: newCreate, notInPayload: notInPayload }
            }).code(200);
        })
        .catch(function (err) {
            logger.info('Caught an error!', err);
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        });

    function getPrefrances() {
        return new Promise(function (resolve, reject) {
            userListCollection.SelectById({ _id: _id }, { _id: 0, searchPreferences: 1 }, (err, result) => {
                if (err) return reject(new Error('Ooops, something broke!'));

                if (!result || !result.searchPreferences) return reject(new Error('Ooops, something broke!'));

                for (let index = 0; index < result.searchPreferences.length; index++) {
                    let pref_id = result.searchPreferences[index].pref_id;
                    favoritePreObj[pref_id] = { selectedValue: result.searchPreferences[index]["selectedValue"] };
                    
                    if (result.searchPreferences[index]["selectedUnit"]) {
                        favoritePreObj[pref_id]["selectedUnit"] = result.searchPreferences[index]["selectedUnit"];
                    }
                }
                notInPayload = result.searchPreferences.length;
                return resolve("---");
            })
        });
    }
    function margePrefranceFromPayload() {

        return new Promise(function (resolve, reject) {
            for (let index = 0; index < req.payload.preferences.length; index++) {
                let pref = req.payload.preferences[index];
                let pref_id = pref.pref_id;
                if (favoritePreObj[pref_id]) {
                    if (pref.selectedValue) {
                        favoritePreObj[pref_id]["selectedValue"] = pref.selectedValue
                    }
                    if (pref.selectedUnit) {
                        favoritePreObj[pref_id]["selectedUnit"] = pref.selectedUnit
                    }
                    modifiy++;
                    notInPayload--;
                } else {
                    unModified++;
                }
            }
            resolve("--");
        });

    }
    function getPrefrancesFromDB() {

        return new Promise(function (resolve, reject) {
            searchPreferences.Select({ "liveMode" : true}, (err, result) => {
                if (err) reject(new Error('Ooops, something broke!'));

                for (let index = 0; index < result.length; index++) {
                    let pref = result[index];
                    let pref_id = pref._id;
                    if (!favoritePreObj[pref_id]) {
                        if (pref.TypeOfPreference == 1) {
                            favoritePreObj[pref_id] = { selectedValue: pref.OptionsValue[0] };
                        } else {
                            favoritePreObj[pref_id] = { selectedValue: pref.OptionsValue };
                        }
                        if (pref["optionsUnits"]) {
                            favoritePreObj[pref_id]["selectedUnit"] = pref["optionsUnits"][0];
                        }
                        newCreate++;
                    }

                }
                resolve("---");
            })
        });
    }
    function convertObjectToArrayForupdate() {
        return new Promise(function (resolve, reject) {
            for (let key in favoritePreObj) {
                let dataToPsh = {
                    pref_id: ObjectID(key),
                    selectedValue: favoritePreObj[key]["selectedValue"]
                }
                if (favoritePreObj[key]["selectedUnit"]) {
                    dataToPsh["selectedUnit"] = favoritePreObj[key]["selectedUnit"];
                }
                searchPrefObj[key] = favoritePreObj[key]["selectedValue"];
                preferences.push(dataToPsh);
            }
            resolve("--");
        });

    }
    function updatePreferences() {
        return new Promise(function (resolve, reject) {

            let preferencesType = "searchPreferences";

            userListCollection.Update(
                { _id: ObjectID(_id) },
                { [preferencesType]: preferences },
                (err, result) => {
                    if (err) {
                        return reject(new Error('Ooops, something broke! at 33 updateVideoLink_MONGO ', err));
                    } else {
                        return resolve("update successfully. at 22 updateVideoLink_MONGO");
                    }
                });
        });
    }
    function updatePreferences_ES() {
        return new Promise(function (resolve, reject) {

            let preferencesType = "searchPreferences";

            userListType.Update(_id, {
                [preferencesType]: preferences,
                searchPrefObj:searchPrefObj
            }, function (err, result) {
                if (err) {
                    logger.error("ERR",err)
                    reject(new Error('Ooops, something broke! at 11 updateVideoLink_MONGO'));
                } else {
                    resolve("update successfully. at updatePreferences_ES");
                }
            });

        });
    }
    function updateLocation() {
        return new Promise(function (resolve, reject) {

            if (req.payload.latitude != null && req.payload.longitude != null) {
                userListType.Update(_id, { "location": { "lat": req.payload.latitude, "lon": req.payload.longitude }, isPassportLocation: req.payload.isPassportLocation }, function (err, result) { });
                userListCollection.UpdateById(_id, {
                    "location": { "longitude": req.payload.longitude, "latitude": req.payload.latitude },
                    "isPassportLocation": req.payload.isPassportLocation
                }, () => { })
            }
            return resolve("--");
        });
    }
};

let APIResponse = {

}

module.exports = { APIHandler, payloadValidator, APIResponse }