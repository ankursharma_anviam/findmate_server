'use strict'
const Joi = require("joi");
const async = require("async");
const Cryptr = require('cryptr');
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require("mongodb").ObjectID
const userListCollection = require('../../../models/userList');
const searchPreferencesCollection = require('../../../models/searchPreferences');

let APIHandler = (req, res) => {

    let _id = req.auth.credentials._id;
    let heigthUnit = "";
    let distanceUnit = "";
    let dataToSend = [];
    let searchPreferencesObj = {};
    let searchPreferences = [];
    let myPreferences = [];

    GetPreferences()
        .then(function (value) {
            return getUser();
        }).then(function (value) {
            return res({ message: req.i18n.__('genericErrMsg')['200'], data: dataToSend }).code(200);
        }).catch(function (err) {
            logger.info('Caught an error!', err);
            return res({ message: req.i18n.__('genericErrMsg')['412'] }).code(412);
        });


    function GetPreferences() {
        return new Promise(function (resolve, reject) {

            searchPreferencesCollection.Select({"liveMode":true}, (err, result) => {
                if (result[0]) {
                    let pref_id = "";
                    for (let index = 0; index < result.length; index++) {
                        pref_id = result[index]._id;
                        searchPreferencesObj[pref_id] = result[index];
                        searchPreferencesObj[pref_id]["selectedValue"] = searchPreferencesObj[pref_id]["OptionsValue"];
                        delete searchPreferencesObj[pref_id]["ActiveStatus"];
                        if (result[index]["TypeOfPreference_User"]) {
                            delete searchPreferencesObj[pref_id]["TypeOfPreference_User"];
                        }
                    }

                    resolve("Found");
                } else {
                    reject(new Error('Ooops, something broke! at getUser'));
                }
            })
        });
    }

    function getUser() {
        return new Promise(function (resolve, reject) {

            userListCollection.Select({ _id: ObjectID(_id) }, (err, result) => {
                if (result[0]) {

                    for (let index = 0; index < result[0].searchPreferences.length; index++) {
                        let pref_id = result[0].searchPreferences[index].pref_id;
                        if (searchPreferencesObj[pref_id]) {
                            searchPreferencesObj[pref_id]["selectedValue"] = result[0].searchPreferences[index]["selectedValue"];
                            if (result[0].searchPreferences[index]["selectedUnit"]) {
                                searchPreferencesObj[pref_id]["selectedUnit"] = result[0].searchPreferences[index]["selectedUnit"] || searchPreferencesObj[pref_id]["OptionsValue"][0];
                            } else if (searchPreferencesObj[pref_id]["TypeOfPreference"] == "3" || searchPreferencesObj[pref_id]["TypeOfPreference"] == "4") {
                                searchPreferencesObj[pref_id]["selectedUnit"] = searchPreferencesObj[pref_id]["optionsUnits"][0];
                            }
                        }
                    }
                    for (let pref_id in searchPreferencesObj) {
                        searchPreferences.push(searchPreferencesObj[pref_id])
                    }


                    dataToSend.push({ searchPreferences: searchPreferences });

                    resolve("Found");
                } else {
                    reject(new Error('Ooops, something broke! at getUser'));
                }
            })
        });
    }
};

let APIResponse = {
}

module.exports = { APIHandler, APIResponse }