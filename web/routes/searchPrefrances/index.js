'use strict'
let getPreferencesAPI = require('./Get');
let postPreferencesAPI = require('./Post');
let patchPreferencesAPI = require("./Patch");
let headerValidator = require('../../middleware/validator');


module.exports = [
    {
        method: 'POST',
        path: '/searchPreferences',
        handler: postPreferencesAPI.APIHandler,
        config: {
            description: 'This API used to update all preferences.',
            tags: ['api', 'searchPreferences'],
            auth: 'userJWT',
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: postPreferencesAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            //response: postPreferencesAPI.APIResponse
        }
    },
    {
        method: 'Get',
        path: '/searchPreferences',
        handler: getPreferencesAPI.APIHandler,
        config: {
            description: 'This API used to get Preferences.',
            tags: ['api', 'searchPreferences'],
            auth: 'userJWT',
            validate: {
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: getPreferencesAPI.APIResponse
        }
    },
    //  {
    //     method: 'PATCH',
    //     path: '/searchPreferences',
    //     handler: patchPreferencesAPI.APIHandler,
    //     config: {
    //         description: 'This API used to update particuler one preferences value.',
    //         tags: ['api', 'searchPreferences'],
    //         auth: 'userJWT',
    //         validate: {
    //             headers: headerValidator.headerAuthValidator,
    //             payload: patchPreferencesAPI.payloadValidator,
    //             failAction: (req, reply, source, error) => {
    //                 failAction: headerValidator.faildAction(req, reply, source, error)
    //             }
    //         },
    //         response: patchPreferencesAPI.APIResponse
    //     }
    // }
];