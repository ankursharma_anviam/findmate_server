'use strict'
const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const userListCollection = require('../../../models/userList');
const userListType = require('../../../models/userListType');
const userLocationLogsCollection = require("../../../models/userLocationLogs");
const rabbitmqUtil = require('../../../models/rabbitMq/latLong');
const rabbitMq = require('../../../models/rabbitMq');
const rabbitLatLongToCity = require('../../../models/rabbitMq/latLongToCity');
const Timestamp = require('mongodb').Timestamp;
const local = require('../../../locales');
const ObjectID = require('mongodb').ObjectID;


let validator = Joi.object({
    longitude: Joi.number().description("longitude").example("77.3086251").error(new Error('longitude is missing')),
    latitude: Joi.number().description("latitude").example("13.0983762").error(new Error('latitude is missing')),
    ip: Joi.string().required().description("IP Address,Eg. 106.51.66.44").example("106.51.66.44").error(new Error('ip is missing')),
    timeZone: Joi.string().required().description("timeZone,Eg. Atlantic Daylight Time || Indian Ocean Time || ....").example(" Indian Ocean Time").error(new Error('timeZone is missing')),
    isPassportLocation: Joi.boolean().default(false).description("isPassportLocation true/false").example(true).error(new Error('isPassportLocation is missing')),
}).required();

/**
 * @method PATCH location
 * @description This API will be used by the passport feature to update the user’s location so that the future search runs off this new location.
 
 * @param {*} req 
 * @param {*} res 
 
 * @property {*} authorization in header
 * @property {*} lang in header
 * @property {number} longitude longitude,Eg. 77.3086251
 * @property {number} latitude latitude,Eg. 13.0983762
 
 * @returns  200 : User's location updated successfully. 
 * @returns  400 : Value in any of the  mandatory field's is missing.
 * @returns  500 : An unknown error has occurred.
 */

let handler = (req, res) => {

    let _id = req.auth.credentials._id;
    console.log("location payload :", req.payload)
    // return res({ message: req.i18n.__('PatchLocation')['200'] }).code(200);
    updateInMongo()
        .then(value => {
            return updateInElasticSearch();
        }).then(value => {
            return addInUserLocationLogs();
        }).then(value => {
            return res({ message: req.i18n.__('PatchLocation')['200'] }).code(200);
        })
        .catch(function (err) {
            logger.info('Caught an error!', err);
            return res({ message: req.i18n.__('PatchLocation')['412'] }).code(412);
        })


    if (typeof req.payload.latitude == 'undefined' || typeof req.payload.longitude == 'undefined') {

        rabbitmqUtil.InsertQueue(rabbitMq.getChannelLatLong(), rabbitMq.queueLatLong,
            { isVerified: true, data: { ip: req.payload.ip, _id: _id } },
            (err, doc) => { });
    } else {
        rabbitLatLongToCity.InsertQueue(rabbitMq.getChannelLatLongToCity(), rabbitMq.queueLatLongToCity,
            { lat: req.payload.latitude, lon: req.payload.longitude, _id: _id },
            (err, doc) => { });
    }

    function updateInMongo() {
        return new Promise(function (resolve, reject) {
            let data = {
                "location.longitude": req.payload.longitude || 0,
                "location.latitude": req.payload.latitude || 0,
                "timeZone": req.payload.timeZone,
                "isPassportLocation": req.payload.isPassportLocation
            };
            userListCollection.UpdateById(_id, data, (err, result) => {
                return (err) ? reject(new Error('Ooops, something broke! 101')) : resolve("--");
            });
        });
    }
    function updateInElasticSearch() {
        return new Promise(function (resolve, reject) {

            let data = {
                location: {
                    "lat": req.payload.latitude || 0,
                    "lon": req.payload.longitude || 0
                },
                "isPassportLocation": req.payload.isPassportLocation
            };
            userListType.Update(_id, data, (err, result) => {

                return (err) ? reject(new Error('Ooops, something broke! 102')) : resolve("--");
            });
        });
    }
    function addInUserLocationLogs() {
        return new Promise(function (resolve, reject) {
            let data = {
                "userId": ObjectID(_id),
                "location": {
                    "longitude": req.payload.longitude || 0,
                    "latitude": req.payload.latitude || 0,
                },
                "timeZone": req.payload.timeZone,
                creationTs: new Timestamp(),
                creationDate: new Date(),
                "isPassportLocation": req.payload.isPassportLocation
            };
            userLocationLogsCollection.Insert(data, (err, result) => {
                return (err) ? reject(new Error('Ooops, something broke! 102')) : resolve("--");
            });
        });
    }
};

let response = {
    status: {
        200: { message: Joi.any().default(local['PatchLocation']['200']) },
        412: { message: Joi.any().default(local['PatchLocation']['412']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
}

module.exports = { handler, validator, response }