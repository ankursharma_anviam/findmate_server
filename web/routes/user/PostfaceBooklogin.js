'use strict'
var Joi = require('joi');
var async = require("async");
var moment = require("moment");
var Promise = require('promise');
var request = require("request");
const logger = require('winston');
var rp = require('request-promise');
var jsonwebtoken = require('jsonwebtoken');
var ObjectID = require('mongodb').ObjectID

var allMessage = require('../../../language');
var Auth = require('../../middleware/authentication');
const userListCollection = require('../../../models/userList');
const userDevicesCollection = require("../../../models/userDevices")
const preferenceCollection = require("../../../models/preferenceTable")

var Config = process.env;
var inMylanguage = "1";


let validator = Joi.object({
    facebookToken: Joi.string().required().description("facebook Access Token").error(new Error('facebookToken is missing')),
    faceBookId: Joi.string().required().description("facebook Id").error(new Error('faceBookId is missing')),
    pushToken: Joi.string().required().description("Push Token").error(new Error('pushToken is missing')),
    deviceId: Joi.string().required().description("deviceId").error(new Error('deviceId is missing')),
    deviceMake: Joi.string().required().description("Device Make").error(new Error('deviceMake is missing')),
    deviceModel: Joi.string().required().description("Device Model").error(new Error('deviceModel is missing')),
    deviceType: Joi.string().required().description("Device Type").error(new Error('deviceType is missing')),
}).required();

let handler = (req, res) => {
    var condition = { fbId: req.payload.faceBookId };
    logger.info("111")
    userListCollection.Select(condition, (err, result) => {
        if (err) {
            return res({ message: allMessage.genericErrMsg["500"][inMylanguage] }).code(503);

        } else if (result[0]) {
            logger.info("222")
            var userId = result[0]._id;


            var userDevices = {
                userId: userId,
                pushToken: req.payload.pushToken,
                deviceId: req.payload.deviceId,
                deviceMake: req.payload.deviceMake,
                deviceModel: req.payload.deviceModel,
                deviceType: req.payload.deviceType,
                createRegisteredTimestamp: new Date().getTime(),
                currentlyActive: true
            }

            async.series([
                function (callback) {
                    /* insert into userList collection*/
                    var userData = {
                        "currentLoggedInDevices.deviceId": req.payload.deviceId,
                        "currentLoggedInDevices.pushToken": req.payload.pushToken
                    };
                    userListCollection.Update(condition, userData, (err, result) => {
                        callback(err, result);
                    })
                },
                // function (callback) {
                //     /* insert into userList type --for Elastic Search*/
                //     condition = { fbId: req.payload.faceBookId };
                //     var data = {
                //         "currentLoggedInDevices": {
                //             "deviceId": req.payload.deviceId,
                //             "pushToken": req.payload.pushToken
                //         },
                //         "firebaseTopic": req.payload.deviceId
                //     }
                //     middleWare.Update("userList", data, userId, "ESearch", function (err, result) {
                //         console.log("error : ", err);
                //         callback(err, result);
                //     })
                // },
                function (callback) {
                    /* update  userDevices.currentlyActive=false collection */
                    condition = { userId: ObjectID(userId), "currentlyActive": true };
                    var data = { "currentlyActive": false }
                    userDevicesCollection.Update(condition, data, (err, result) => {
                        callback(err, result);
                    });
                },
                function (callback) {
                    /* insert into userDevices collection*/
                    userDevicesCollection.Insert(userDevices, (err, result) => {
                        callback(err, result);
                    })
                }
            ], function (err, result) {
                if (err) return res({ message: allMessage.UNKNOWN_ERROR_OCCURRED[inMylanguage] }).code(503);
                token = Auth.SignJWT({ _id: userId.toString(), key: 'acc' }, 'user',6000000);
                return res({ message: allMessage.fbLoginAPI["200"][inMylanguage], data: { token: token } }).code(200);
            });

        } else {
            logger.info("222")

            const userFieldSet = 'id, name, about, email,work,picture, photos,birthday,education,likes';
            const options = {
                method: 'GET',
                uri: `https://graph.facebook.com/v2.11/` + req.payload.faceBookId,
                qs: {
                    access_token: req.payload.facebookToken,
                    fields: userFieldSet
                }
            };

            rp(options)
                .then(function (body) {
                    // Process html like you would with jQuery... 
                    body = JSON.parse(body);

                    var userId = new ObjectID();
                    var userDevices_id = new ObjectID();
                    var profilePic_id = new ObjectID();
                    var profileVideo_id = new ObjectID();

                    var userData = {};
                    userData._id = userId;
                    userData.firstName = body.name;
                    userData.fbId = body.id;
                    userData.about = body.about;
                    userData.email = body.email;
                    userData.dob = new Date(body.birthday).getTime();
                    // userData.profilePic = body.picture.data.url || "";
                    userData.work = body.work;
                    userData.education = body.education;
                    userData.likes = body.likes;

                    /**
                     * now take the proper description for work,education and likes
                     */

                    /**
                     * Take Details for WORK
                     */
                    var work = [];
                    if (userData.work) {


                        for (i = 0; i < userData.work.length; i++) {
                            startDate = (userData.work[i].start_date) ? userData.work[i].start_date : "";
                            endDate = (userData.work[i].end_date) ? userData.work[i].end_date : "";
                            startDateTime = (userData.work[i].start_date) ? new Date(userData.work[i].start_date).getTime() : "";
                            endDateTime = (userData.work[i].end_date) ? new Date(userData.work[i].end_date).getTime() : "";
                            description = (userData.work[i].description) ? userData.work[i].description : "";
                            employer = (userData.work[i].employer.name) ? userData.work[i].employer.name : "";
                            location = (userData.work[i].location.name) ? userData.work[i].location.name : "";
                            position = (userData.work[i].position.name) ? userData.work[i].position.name : "";

                            work.push({
                                startDate: startDate,
                                endDate: endDate,
                                startDateTime: startDateTime,
                                endDateTime: endDateTime,
                                description: description,
                                employer: employer,
                                location: location,
                                position: position
                            });

                        }
                    }
                    userData.work = work;
                    /**
                     * proccess for take WORK details is done
                     * 
                     * Now take Details for EDUCATION
                     * 
                     */


                    var education = [];
                    // for (i = 0; i < userData.education.length; i++) {
                    //     type = (userData.education[i].type) ? userData.education[i].type : "";
                    //      year = (userData.education[i].year.name) ? userData.education[i].year.name : "";
                    //     school = (userData.education[i].school.name) ? userData.education[i].school.name : "";
                    //     education.push({
                    //         type: type, year: year, school: school, employer: employer
                    //     })
                    // }
                    userData.education = education;

                    /**
                    * proccess for take education details is done
                    * 
                    * Now take Details for Likes
                    */

                    var likes = [];
                    if (userData.likes) {
                        for (i = 0; i < userData.likes.data.length; i++) {

                            likes.push(userData.likes.data[i].name);
                        }
                    }
                    userData.likes = likes;

                    userData["favoritePreferences"] = [];
                    userData["myPreferences"] = [];
                    userData["registeredTimestamp"] = new Date().getTime();
                    userData["profilePic"] = [
                        profilePic_id
                    ];
                    userData["otherImages"] = [];
                    userData["profileVideo"] = [
                        // profileVideo_id
                    ];
                    userData["otherVideos"] = [];
                    userData["instaGramProfileId"] = "";
                    userData["currentLoggedInDevices"] = {
                        "deviceId": req.payload.deviceId,
                        "pushToken": req.payload.pushToken
                    };
                    userData["onlineStatus"] = 0;
                    userData["profileStatus"] = 0;
                    userData["loggedInLocations"] = [
                        // ObjectId("59b6431b596c9c5cde0b4c0e")
                    ];
                    userData["likedBy"] = [];
                    userData["matchedWith"] = [];
                    userData["lastUnlikedUser"] = [];
                    userData["recentVisitors"] = [];
                    userData["balance"] = 0;
                    userData["WalletTxns"] = [];
                    userData["BlockedBy"] = [];
                    userData["DisLikedUSers"] = [];
                    userData["ProfileTotalViewCount"] = 0;
                    userData["textMessageCount"] = 0;
                    userData["imgCount"] = 0;
                    userData["videoCount"] = 0;
                    userData["videoCallDuration"] = 0;
                    userData["audioCallDuration"] = 0;
                    userData["fbPermission"] = {};
                    userData["height"] = 0;
                    userData["location"] = {
                        "longitude": 0,
                        "latitude": 0
                    }


                    var userDevices = {
                        _id: userDevices_id,
                        userId: userId,
                        pushToken: req.payload.pushToken,
                        deviceId: req.payload.deviceId,
                        deviceMake: req.payload.deviceMake,
                        deviceModel: req.payload.deviceModel,
                        deviceType: req.payload.deviceType,
                        createRegisteredTimestamp: new Date().getTime(),
                        currentlyActive: true

                    }
                    var profilePic_media = {
                        _id: profilePic_id,
                        userId: userId,
                        mediaType: 0,//( 0 – img, 1 – vid )
                        mediaFor: "profile",//(Profile, Other)
                        lowIQ: body.picture.data.url,
                        mediumIQ: body.picture.data.url,
                        highIQ: body.picture.data.url
                    };

                    async.series([
                        function (callback) {
                            /* Get Preferences to save In userList collection */
                            preferenceCollection.Select({}, (err, result) => {
                                if (err) callback(err, result);

                                userData.favoritePreferences = [];
                                userData.myPreferences = [];
                                for (index = 0; index < result.length; index++) {

                                    /* add in my favoritePreferences */
                                    userData.favoritePreferences.push({
                                        pref_id: result[index]._id,
                                        selectedValue: (result[index].TypeOfPreference == 1) ? [result[index].OptionsValue[0]] : result[index].OptionsValue
                                    });


                                    /* add in my Preferences */
                                    userData.myPreferences.push({
                                        pref_id: result[index]._id,
                                        selectedValue: (result[index].TypeOfPreference == 1) ? [result[index].OptionsValue[0]] : result[index].OptionsValue
                                    })

                                }
                                callback(err, result);

                            })
                        },
                        function (callback) {
                            /* insert into userList collection*/
                            userListCollection.Insert(userData, (err, result) => {
                                callback(err, result);
                            })
                        },
                        function (callback) {
                            /* insert into userDevices collection*/
                            userDevicesCollection.Insert(userDevices, (err, result) => {
                                callback(err, result);
                            })
                        },
                        /* insert In Elastic Search */
                        // function (callback) {
                        //     /* insert into userList type --for Elastic Search*/
                        //     userListCollection.Insert(userData, {}, "ESearch", function (err, result) {
                        //         console.log("error : ", err);
                        //         callback(err, result);
                        //     })
                        // }

                    ], function (err, result) {
                        if (err) return res({ message: allMessage.genericErrMsg["500"][inMylanguage] }).code(503);
                        token = Auth.SignJWT({ _id: userId.toString(), key: 'acc' }, 'user',6000000);
                        return res({ message: allMessage.fbLoginAPI["201"][inMylanguage], data: { token: token } }).code(201);
                    });

                })
                .catch(function (err) {
                    // Crawling failed or Cheerio choked... 
                    logger.info("aaaaaa ", err)
                    return res({ message: allMessage.genericErrMsg["500"][inMylanguage] }).code(500);
                });

        }
    })
}

let response = {
    status: {
        200: { message: allMessage.fbLoginAPI["200"]["1"], data: { token: Joi.any() } },
        201: { message: allMessage.fbLoginAPI["201"]["1"], data: { token: Joi.any() } },
        400: { message: allMessage.genericErrMsg["400"]["1"] },
        500: { message: allMessage.genericErrMsg["500"]["1"] },

    }
}

module.exports = { handler, validator, response }