'use strict'
let PostFBLoginAPI = require('./PostfaceBooklogin');
let headerValidator = require("../../middleware/validator")

module.exports = [
    {
        method: 'POST',
        path: '/faceBooklogin',
        handler: PostFBLoginAPI.handler,
        config: {
            description: `This API use for signup or login from facebook with facebook token`,
            tags: ['api', 'Login'],
            auth: false,
            validate: {
                payload: PostFBLoginAPI.validator,
                // headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PostFBLoginAPI.response
        }
    }
];