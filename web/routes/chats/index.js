'use strict'
let headerValidator = require('../../middleware/validator');
let getAPI = require('./Get');
let DeleteAPI = require('./Delete');

module.exports = [
    {
        method: 'GET',
        path: '/Chats/{pageNo}',
        handler: getAPI.handler,
        config: {
            description: 'This API is used to like user',
            tags: ['api', 'Chats'],
            auth: 'userJWT',
            validate: {
                params: getAPI.validator,
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            // response: getAPI.response
        }
    },
    {
        method: 'Delete',
        path: '/Chats',
        handler: DeleteAPI.APIHandler,
        config: {
            description: 'This API is used to like user',
            tags: ['api', 'Chats'],
            auth: 'userJWT',
            validate: {
                payload: DeleteAPI.validator,
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: DeleteAPI.response
        }
    },
];