'use strict'
const Joi = require("joi");
const moment = require("moment");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;
const local = require('../../../locales');
const plansCollection = require('../../../models/plans');
const userListCollection = require('../../../models/userList');
const subscriptionCollection = require('../../../models/subscription');

/**
 * @method POST subscription
 * @description This API use to  POST unmatch.
 
 * @param {*} req 
 * @param {*} res 
 
 * @property {*} authorization in header
 * @property {*} lang in header
 * @property {*} targetUserId targetUserId
 
 * @returns  200 : User unmatched successfully.
 * @returns  400 : Value in any of the  mandatory field's is missing.
 * @returns  422 : targetUserId  doesnot exist in the database,try with the different ID.
 * @returns  500 : An unknown error has occurred.
 */
let handler = (req, res) => {

    let _id = req.auth.credentials._id;

    checkPlanExists()
        .then(function (value) { return subscribePlan(value); })
        .then(function (value) { return res({ message: req.i18n.__('PostSubscription')['200'], data: value }).code(200); })
        .catch(function (err) { return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500); });

    function checkPlanExists() {
        return new Promise(function (resolve, reject) {
            plansCollection.SelectOne({ _id: ObjectID(req.payload.planId), statusCode: 1 }, (err, result) => {
                if (err) return reject(new Error('Ooops, something broke!'));

                if (result && result._id) {
                    return resolve(result);
                }
                return reject(new Error('Ooops, no have plan!'));
            })
        });
    }
    function subscribePlan(planDetails) {
        return new Promise(function (resolve, reject) {

            let purchaseDate = moment().startOf('day').valueOf();
            let expiryTime = 0;
            if (planDetails.durationInMonths) {
                expiryTime = moment().add(planDetails.durationInMonths, "months").startOf('day').valueOf();
            } else {
                expiryTime = moment().add(planDetails.durationInDays, "days").startOf('day').valueOf();
            }
            let purchaseTime = new Date().getTime();
            let subscriptionId = new ObjectID();
            let dataToInsert = {
                "_id": subscriptionId,
                "planId": ObjectID(planDetails._id),
                "purchaseDate": purchaseDate,
                "purchaseTime": purchaseTime,
                "userPurchaseTime": req.payload.userPurchaseTime,
                "durationInMonths": planDetails.durationInMonths,
                "planName": planDetails.planName,
                "expiryTime": expiryTime,
                "userId": ObjectID(_id),
                "statusCode": 1,
                "status": "active",
                "cost": planDetails.cost,
                "currencySymbol": planDetails.currencySymbol,
                "currencyCode": planDetails.currencyCode,
                "paymentGateway": req.payload.paymentGateway,
                "paymentGatewayTxnId": req.payload.paymentGatewayTxnId
            };

            subscriptionCollection.Insert(dataToInsert, (err, result) => {
                if (err) return reject(new Error('Ooops, something broke!'));

                let dataToPush = {
                    "subscription": [{
                        "planId": ObjectID(planDetails._id),
                        "subscriptionId": subscriptionId,
                        "productId": planDetails.actualId,
                        "purchaseDate": purchaseDate,
                        "purchaseTime": purchaseTime,
                        "userPurchaseTime": req.payload.userPurchaseTime,
                        "durationInMonths": planDetails.durationInMonths,
                        "expiryTime": expiryTime,
                        "likeCount": planDetails.likeCount,
                        "rewindCount": planDetails.rewindCount,
                        "superLikeCount": planDetails.superLikeCount,
                        "whoLikeMe": planDetails.whoLikeMe,
                        "whoSuperLikeMe": planDetails.whoSuperLikeMe,
                        "recentVisitors": planDetails.recentVisitors,
                        "readreceipt": planDetails.readreceipt,
                        "passport": planDetails.passport,
                        "noAdds": planDetails.noAdds,
                        "hideDistance": planDetails.hideDistance,
                        "hideAge": planDetails.hideAge,
                    }]
                };
                userListCollection.UpdateById(_id, dataToPush, () => { });
                return resolve(dataToPush);
            })
        });
    }
};

let response = {
    status: {
        200: { message: local['PostSubscription']['200'], data: Joi.any() },
        412: { message: local['PostSubscription']['412'] },
        400: { message: local['genericErrMsg']['400'] },
    }
}

let validator = Joi.object({
    planId: Joi.string().required().min(24).max(24).description("planId, Eg. 5b08158770c84b13a29c95cf ").error(new Error('planId is missing')),
    paymentGatewayTxnId: Joi.string().required().description("paymentGatewayTxnId ").error(new Error('paymentGatewayTxnId is missing')),
    paymentGateway: Joi.string().required().description("paymentGateway ").error(new Error('paymentGateway is missing')),
    userPurchaseTime: Joi.string().required().description("userPurchaseTime ").error(new Error('userPurchaseTime is missing')),
    // cost: Joi.number().required().description("cost, Eg. 10.00").error(new Error('cost is missing')),
    // currencySymbol: Joi.string().required().description("currencySymbol, Eg. $").error(new Error('currencySymbol is missing')),
    // currencyCode: Joi.string().required().description("currencyCode, Eg. USD").error(new Error('currencyCode is missing')),
}).required();

module.exports = { handler, response, validator }