'use strict'
let headerValidator = require('../../middleware/validator');
let findProspects = require('./post');
module.exports = [
    {
        method: 'POST',
        path: '/findProspects',
        handler: findProspects.handler,
        config: {
            description: 'This API is used to find users on right swipe',
            tags: ['api', 'swipeToConnect'],
            auth: 'userJWT',
            // auth: false,
            validate: {
                headers: headerValidator.headerAuthValidator,
               payload: findProspects.validator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: findProspects.response
        }
    }
];