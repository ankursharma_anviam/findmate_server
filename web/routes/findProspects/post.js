'use strict'
const Config = process.env;
const Joi = require("joi");
// const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;//to convert stringId to mongodb's objectId
const Promise = require('bluebird');
const local = require('../../../locales');

const redisOperation = require('../../../models/redisOperation');

// const mqtt = Promise.promisifyAll(require('./../../../library/mqtt'));
const mqtt = require('./../../../library/mqtt');
const prospect = Promise.promisifyAll(require('../../../models/prospect'));
const userList = Promise.promisifyAll(require('../../../models/userList'));
const connection = Promise.promisifyAll(require('../../../models/connection'));
const userList_Es = require('../../../models/userListType');



const validator = Joi.object({
    rangeOfAge: Joi.array().description("age preference eg: [105436100000,905436200000]").error(new Error('rangeOfAge field is required')),
    rangeOfHeight: Joi.array().description("height preference,eg:[23,200]").error(new Error(' rangeOfHeight field is required')),
    interest: Joi.array().description("interests ,eg:[58bfb210849239b062ca9db4,5c5af0df2f705d135ad72f81]")
    // distance: Joi.array().description("distance preference,eg:[200,300]").error(new Error(' distance field is required'))
})


/**
 * @method POST findProspect
 * @description This API is used to find users with mathcing preference.
 * @author karthikb@mobifyi.com
* @property {string} language in header
* @property {string} authorization in header
* @returns 200:user sucessfully added
* @returns  500:internal server error 
* @returns  404:no user found,please try again
 */



const handler = (req, res) => {

    let currentUser = req.auth.credentials._id;
    let matchedUser;
    // change the status of user as "searching"
    let changeStatusOfUser = () => {
        return new Promise(async (resolve, reject) => {
            // redis operation status will be stored in this variable
            let changedUserStatus = await redisOperation.changeUserStatusInRedis(currentUser, "searching");

            changedUserStatus == 'OK' ? resolve() : res({ message: req.i18n.__('findProspect')['500'] }).code(500);
        });
    }

    let searchUsersByStatus = () => {

        //users who's status is searching in redis
        let searchingUsers = [];
        return new Promise(async (resolve, reject) => {
            //stores _id of users who are online in redis
            let users = await redisOperation.searchUsersByStatusInRedis();
            if (users) {
                //if users exist its keys are iterated to find _id with status searching
                Object.keys(users).forEach((key) => {
                    if (users[key] == "searching") {
                        //push the user to searchingUsers array
                        searchingUsers.push(key);
                    }
                });
                //if searchingUsers array contains user other than myself proceed
                return (searchingUsers.length > 1) ? resolve(searchingUsers) : res({ message: req.i18n.__('findProspect')['404'] }).code(404);
                // if (searchingUsers.length > 1) {
                //     return resolve(searchingUsers);
                // }
                // else {
                //     return  res({ message: req.i18n.__('findProspect')['404'] }).code(404);
                // }
            }
            else {
                return res({ message: req.i18n.__('findProspect')['404'] }).code(404);
            }
        })
    }

    // this function finds users who are not in prospect redis
    let checkIfUserIsInProspect = (searchingUsers) => {
        return new Promise(async (resolve, reject) => {
            //users who are not already connected
            searchingUsers.splice(searchingUsers.indexOf(currentUser), 1);

            let usersNotInProspect = []
            let usersInProspect = []

            for (let searchingUser of searchingUsers) {
                let userId = [currentUser, searchingUser];
                let userIsInProspect = await redisOperation.checkIfUserIsInProspectInRedis(userId);

                if (userIsInProspect == null) {
                    usersNotInProspect.push(searchingUser)
                } else {
                    usersInProspect.push({
                        id: searchingUser,
                        time: userIsInProspect
                    })
                }
            }
            //usersInProspect sort by using timestamp ascending
            let sortedUsersInProspect = usersInProspect.sort(function (a, b) {
                return (a.time < b.time) ? 1 : -1
            })
            //push all id in usersNotInProspect array
            sortedUsersInProspect.map((item) => {
                usersNotInProspect.push(item.id);
            });
            if (usersNotInProspect.length < 1) {
                res({ message: req.i18n.__('findProspect')['404'] }).code(404);
            }
            // console.log('users not in prospect',usersNotInProspect);
            return resolve(usersNotInProspect);
        })
    }

    //this function searches user for matching in elastic search with preference
    let searchWithPrefrences = async (usersNotInProspect) => {
        try {
            //gets user detail of the current user from database
            let user = req.user
 
            let payload = {
                rangeOfAge: req.payload.rangeOfAge,
                rangeOfHeight: req.payload.rangeOfHeight,
                interests: req.payload.interest
                //    distance : req.payload.distance
            }

            //matches the preference of the current user with a user in elastic search
            let maxScoreUsers = await userList_Es.searchWithPrefrencesInES(usersNotInProspect, user, payload);



            if (maxScoreUsers['hits']['total'] != '0') {

                console.log('-------------------------------------------------------->',maxScoreUsers.took)

                //add both users into chat room
                matchedUser = maxScoreUsers['hits']['hits'][0];

                // let matchedUserDetail = await userList.SelectOneAsync({ _id: ObjectID(matchedUser._id) });
                let matchedUserDetail = matchedUser._source;
                let requestSent = user.pendingFriendShipId.map(e => e.toString()).includes(matchedUser._id.toString())
                let isFriend = user.friendShipId.map(e => e.toString()).includes(matchedUser._id.toString())
                let isProfileLiked = user.ProfileLikedBy.map(e => e.toString()).includes(matchedUser._id.toString())

                console.log('hererer');
                //data to be sent in mqtt
                let userAdded = await addUsersToChatRoom(currentUser, matchedUser, {
                    opponentId: currentUser,
                    opponentName: user.firstName,
                    opponentProfilePicture: user.profilePic,
                    isOpponentFriend: isFriend,
                    countryCode: user.address.countryCode,
                    likedCount: user.ProfileLikedBy.length,
                    lat: user.address.latitude,
                    log: user.address.longitude,
                    country: user.address.country,
                    city: user.address.city,
                    // isFriendRequestSent:isRequestSent?true:false, 
                    isFriendRequestSent: requestSent,
                    isProfileLiked: isProfileLiked 

                }, {
                        opponentId: matchedUser._id,
                        opponentName: matchedUserDetail.firstName,
                        opponentProfilePicture: matchedUserDetail.profilePic,
                        isOpponentFriend: isFriend,
                        countryCode: matchedUserDetail.address.countryCode,
                        likedCount: matchedUserDetail.ProfileLikedBy.length,
                        lat: matchedUserDetail.address.latitude,
                        log: matchedUserDetail.address.longitude,
                        country: user.address.country,
                        city: user.address.city,
                        isFriendRequestSent: requestSent,
                        isProfileLiked: isProfileLiked
                    }, user);

                if (userAdded) {
                    res({ message: req.i18n.__('findProspect')['200'] }).code(200);
                }


            } else {
                console.log('check preference,gender');
                
                res({ message: req.i18n.__('findProspect')['404'] }).code(404);
                
                /* 
                //search user when preference doesnot match,just match with random user with same gender

                console.log('am I being executed??');
                //stores best match user
                let maxScoreUsersRandom = await userList_Es.searchUsersForChatInESAsync(usersNotInProspect, user);

                if (maxScoreUsers['hits']['total'] == '0')
                    return res({ message: req.i18n.__('findProspect')['404'] }).code(404);

                let matchedUser = maxScoreUsers['hits']['hits'][0];

                console.log("matchedUser     =====> ", maxScoreUsers['hits']['total']);

                //find detail of the matched user
                // let matchedUserDetail = await userList.SelectOneAsync({ _id: ObjectID(matchedUser._id) });
                let matchedUserDetail = matchedUser._source
                // console.log('final data1 ===================>', JSON.stringify(maxScoreUsersRandom['hits']['total']));

                //add both users into chat room
                if (maxScoreUsersRandom['hits']['total'] != '0') {
                    matchedUser = maxScoreUsersRandom['hits']['hits'][0];
                    console.log("matchedUser ===========> ", matchedUser);

                    console.log('1111111111111111111111111111111111111111111111111111', user._id, matchedUser._id, user.pendingFriendShipId, user.pendingFriendShipId.map(e => e.toString())
                        .includes(matchedUser._id.toString()))

                    console.log('likeCountme', user.ProfileLikedBy.length);
                    console.log('likeCountyou', matchedUserDetail.ProfileLikedBy.length);
                    console.log('isFriendRequestSent', user.pendingFriendShipId.map(e => e.toString()).includes(matchedUser._id.toString()))
                    console.log('isOpponentFriend', user.friendShipId.map(e => e.toString()).includes(matchedUser._id.toString()))
                    console.log('isProfileLiked', user.ProfileLikedBy.map(e => e.toString()).includes(matchedUser._id.toString()))

                    let requestSent = user.pendingFriendShipId.map(e => e.toString()).includes(matchedUser._id.toString())
                    let isFriend = user.friendShipId.map(e => e.toString()).includes(matchedUser._id.toString())
                    let isProfileLiked = user.ProfileLikedBy.map(e => e.toString()).includes(matchedUser._id.toString())


                    let userAdded = await addUsersToChatRoom(currentUser, matchedUser, {
                        opponentId: currentUser,
                        opponentName: user.firstName,
                        opponentProfilePicture: user.profilePic,
                        isOpponentFriend: isFriend,
                        countryCode: user.address.countryCode,
                        likedCount: user.ProfileLikedBy.length,
                        lat: user.address.latitude,
                        log: user.address.longitude,
                        country: user.address.country,
                        city: user.address.city,
                        isFriendRequestSent: requestSent,
                        isProfileLiked: isProfileLiked

                    }, {
                            opponentId: matchedUser._id,
                            opponentName: matchedUserDetail.firstName,
                            opponentProfilePicture: matchedUserDetail.profilePic,
                            isOpponentFriend: isFriend,
                            countryCode: matchedUserDetail.address.countryCode,
                            likedCount: matchedUserDetail.ProfileLikedBy.length,
                            lat: matchedUserDetail.address.latitude,
                            log: matchedUserDetail.address.longitude,
                            country: user.address.country,
                            city: user.address.city,
                            isFriendRequestSent: requestSent,
                            isProfileLiked: isProfileLiked
                        }, user);
                    if (userAdded) {
                        res({ message: req.i18n.__('findProspect')['200'] }).code(200);
                    }
                } else {
                    res({ message: req.i18n.__('findProspect')['404'] }).code(404);
                } */

            }

        } catch (error) {
            console.log("searchWithPrefrences  error  =========> ", error);

        }

    }

    changeStatusOfUser()
        .then(searchUsersByStatus)
        .then(checkIfUserIsInProspect)
        .then(searchWithPrefrences)
        .catch((err) => {
            console.log('finale error', err);
            res(`${err.message}`);
        })

};


//this function creates random id and sends matched user that room id in mqtt
async function addUsersToChatRoom(currentUser, matchedUser, userDetail, opponentDetail, user) {
    try {
        // get chatRoomId
        let chatRoomId = Date.now();
        let connectionData = {
            chatRoomId: chatRoomId,
            // user:user
        };

        if (chatRoomId) {
            //push chatRoomId to mqtt of both users


            //send matched user data and chatRoomId to current user through mqtt in currentUserId topic
            mqtt.publish(currentUser, JSON.stringify({ "chatRoomId": chatRoomId, "userDetail": opponentDetail }), { qos: 1 }, (err, result) => {

                if (err) {
                    console.log('mqtt error', err);
                } else {
                    console.log('sent to this topic', currentUser)
                    console.log('resultmqtt', result);
                }
            });

            //send user data and chatRoomId to matched user through mqtt in matcherUserId topic
            mqtt.publish(matchedUser._id, JSON.stringify({ "chatRoomId": chatRoomId, userDetail }), { qos: 1 }, (err, result) => {

                if (err) {
                    console.log('mqtt error', err);
                } else {
                    console.log('sen to topic match user', matchedUser._id);
                    console.log('resultmqtt', result);
                }
            });
            // await mqtt.publishAsync( matchedUser._id.toString(), connectionData, { qos: 1 });

            //adding matched user to prospect
            // let usersToChangeStatus = [currentUser.toString(), matchedUser._id.toString()];
            //change status of users to connected
            // let userStatusToConnected = await redisOperation.changeUserStatusInRedis(usersToChangeStatus, "connected");
             redisOperation.changeUserStatusInRedis(currentUser.toString(), "connected");
             redisOperation.changeUserStatusInRedis(matchedUser._id.toString(), "connected");

            //getting current user detail from database

            // currentUser = await userList.SelectOneAsync({ _id: ObjectID(currentUser) })
            currentUser = user


                // create connection in mongodb
                connectionData = {
                    firstUser: currentUser._id,
                    countryOrRegionOfFirstUser: currentUser.address.country,
                    secondUser: ObjectID(matchedUser._id),
                    countryOrRegionOfsecondUser: matchedUser._source.address.country,
                    connectionStartedOn: new Date().getTime(),
                    connectionEndedOn: 0,
                    Duration: 0

                }
            let newConnection = await connection.InsertAsync(connectionData);
            if (newConnection) {

                // create prospect in mongodb and redis
                let prospectData = {
                    firstUser: currentUser._id,
                    secondUser: ObjectID(matchedUser._id),
                    connectionStartedOn: new Date().getTime(),
                    connectionEndedOn: 0,
                    coolOffTimeStamp: 60,
                    coolOffTimeStampExpired: false
                }
                let newProspect = await createProspect(prospectData);
                return newProspect;
            }
        }
    } catch (error) {
        //change the user status to searching if something goes wrong
        console.log("addUserToChatRoom error =====> ", error);

        // let usersToChangeStatus = [currentUser._id.toString(), matchedUser._id.toString()]
         redisOperation.changeUserStatusInRedis(currentUser._id.toString(), "searching");
         redisOperation.changeUserStatusInRedis(matchedUser._id.toString(), "searching");
        throw error;
    }
}

async function createProspect(prospectData) {
    try {
        //This function will insert prospectData in mongodb and also in 
        //redis( with coolOffTime as expire Time) and returns newProspect object      
        let newProspect = await prospect.InsertAsync(prospectData);
        if (newProspect) {
            let redisProspect = [prospectData.firstUser, prospectData.secondUser];
            let prospectInRedis = await redisOperation.insertUserInProspectRedis(redisProspect)
            if (prospectInRedis) {
                return newProspect;
            }
        }
    } catch (error) {
        console.log("createProspect error ========> ", error);
        throw error;

    }
}



let response = {
    status: {
        200: { message: Joi.any().default(local['findProspect']['200']) },
        404: { message: Joi.any().default(local['findProspect']['404']) },
        500: { message: Joi.any().default(local['findProspect']['500']) }
    }
}


module.exports = { handler, response, validator };


/* rabbitmq:
    image: "rabbitmq:3-management"
    hostname: "rabbit"
    ports:
      - "15672:15672"
      - "5672:5672"
    labels:
      NAME: "rabbitmq"
    volumes:
      - ./rabbitmq-isolated.conf:/etc/rabbitmq/rabbitmq.config */