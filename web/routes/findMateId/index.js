'use strict'
let headerValidator = require('../../middleware/validator');
let findMateId = require('./Patch');
let GetFindMateId= require('./Get');
module.exports = [
    {
        method: 'PATCH',
        path: '/findMateId',
        handler: findMateId.handler,
        config: {
            description: 'This API will update findMateId',
            tags: ['api', 'findMateId'],
            auth: 'userJWT',
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: findMateId.validator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: findMateId.response
        }
    },
    {
        method: 'GET',
        path: '/findMateId',
        handler: GetFindMateId.handler,
        config: {
            description: 'This API will give findMateId',
            tags: ['api', 'findMateId'],
            auth: 'userJWT',
            validate: {
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            },
             response: GetFindMateId.response
        }
    },
];