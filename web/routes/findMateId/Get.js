'use strict'
const Joi = require("joi");
const async = require("async");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;
const local = require('../../../locales');
const GeoPoint = require('geopoint')
const userListCollection = require('../../../models/userList');


/**
 * @method GET like
 * @description This API is used to get likedlist.
 * @param {*} req 
 * @param {*} res 
 * @property {*} authorization in header
 * @property {*} lang in header
 * @returns  200 : User found in database.
 * @returns  500 : An unknown error has occurred.
 */

const handler = (req, reply) => {
    let _id = new ObjectID(req.auth.credentials._id);

    const dbErrResponse = { message: req.i18n.__('genericErrMsg')['500'], code: 500 };
    let getFindMatchId = () => {
        return new Promise((resolve, reject) => {
            userListCollection.FindMateIdOne(_id, (err, result) => {
                if (err) {
                    logger.error("get FindMatchId API : ", err)
                    return reject(dbErrResponse);
                }
                else {
                    return resolve(result);
                }

            })
        });
    };

    getFindMatchId()
        .then((data) => { return reply({ message: req.i18n.__('genericErrMsg')['200'], data: data }).code(200); })
        .catch((err) => {
            logger.err("getAll findMatchId details error : ", err);
            return reply({ message: err.message }).code(err.code);
        });
};




let response = {
    status: {
        200: { message: Joi.any().default(local['genericErrMsg']['200']), data: Joi.any() },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) },
    }
}

module.exports = { handler, response }