'use strict'
const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const local = require('../../../locales');
const userListCollection = require('../../../models/userList');
const userListType = require('../../../models/userListType');

let validator = Joi.object({
    findMateId: Joi.string().required().description("findMateId").error(new Error('findMateId is missing'))
}).required();

/**
 * @method PATCH findMatchId
 * @description This API use to  PATCH findMatchId.
 
 * @param {*} req 
 * @param {*} res 
 
 * @property {*} authorization in header
 * @property {*} lang in header
 * @property {*} findMateId findMatchId
 
 * @returns  200 : User unmatched successfully.
 * @returns  400 : Value in any of the  mandatory field's is missing.
 * @returns  422 : targetUserId  doesnot exist in the database,try with the different ID.
 * @returns  500 : An unknown error has occurred.
 */
let handler = (req, res) => {
    var findMateId = req.payload.findMateId.toLowerCase();
    let _id = req.auth.credentials._id;
    console.log('cccccccccccccccc',_id);
    const findMateResponse = { message: req.i18n.__('FindMateId')['412'], code: 412 };
    const dbErrResponse = { message: req.i18n.__('genericErrMsg')['500'], code: 500 };
    // console.log(findMateId);

    /**check findMatchId exists or not */
    function checkFindMateId() {
        return new Promise(function (resolve, reject) {
            let condition = { "findMateId": findMateId };
            userListCollection.Select(condition, (err, result) => {
                if (err)
                    return reject(dbErrResponse);
                else if (result.length) {
                    console.log('hey',result);
                    return reject(findMateResponse);
                }
                else
                    return resolve(true);
            });
        });
    }
    /**update findMatchId in mongodb */
    function updateFindMateId() {
        return new Promise(function (resolve, reject) {
            let dataToUpdate = { "findMateId": findMateId };
            userListCollection.UpdateById(_id, dataToUpdate, (err, result) => {
                if (err) return reject(new Error('Ooops, something broke!'));
                else
                    return resolve(true);
            });
        });
    }
    /**update findMatchId in elasticsearch */
    function updateESFindMateId() {
        return new Promise(function (resolve, reject) {
            let dataToUpdateInES = { "findMateId": findMateId };
            userListType.Update(_id, dataToUpdateInES, (err, result) => {
                console.log("es-err", err);
                console.log("es-res", result);
                return (err) ? reject(new Error('Ooops, something broke!')) : resolve(findMateId);
            });
        });
    }
    checkFindMateId()
        .then(() => { return updateFindMateId() })
        .then(() => { return updateESFindMateId() })
        .then((data) => { return res({ message: req.i18n.__('genericErrMsg')['200'], data: data }).code(200); })
        .catch((err) => {
            logger.error("patch findMateId details error : ", err);
            return res({ message: err.message }).code(err.code);
        })

};

let response = {
    status: {
        200: { message: Joi.any().default(local['genericErrMsg']['200']), data: Joi.any() },
        412: { message: Joi.any().default(local['FindMateId']['412']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },

    }
}
module.exports = { handler, response, validator }