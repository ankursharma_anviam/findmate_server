'use strict'
let GetAPI = require('./Get');
let headerValidator = require('../../middleware/validator');

module.exports = [
    {
        method: 'GET',
        path: '/plan',
        handler: GetAPI.handler,
        config: {
            description: 'This API will be used to get a plans',
            tags: ['api', 'plans'],
            auth: 'userJWT',
            response: GetAPI.response,
            validate: {
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                }
            }
        }
    }
];