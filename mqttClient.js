"use strict"
const mqtt = require('mqtt');
var async = require("async");
var moment = require('moment');
var myLibrary = require('./library/myLibrary');
const objectId = require('mongodb').ObjectID;
const config = require('./config');
var userList = require("./models/userList");
var userListType = require("./models/userListType");
const db = require('./models/mongodb');
const fcmPush = require('./library/fcm');
var middleWare = require("./Controller/dbMiddleware.js");
const elasticSearch = require('./models/elasticSearch');
var dateScheduleCollection = require("./models/dateSchedule")
var dateSchedule_TTLCollection = require("./models/dateSchedule_TTL")
var messageTypes = ["text", "image", "video", "location", "concat", "audio", "sticker", "doodle", "gify"];
var redisClient = require("./models/redisDB").client
db.connect(() => { });
elasticSearch.connect(() => { });
var msglength = 0;


var os = require('os');
var interfaces = os.networkInterfaces();
var addresses = [];
var calls = {};
for (var k in interfaces) {
    for (var k2 in interfaces[k]) {
        var address = interfaces[k][k2];
        if (address.family === 'IPv4' && !address.internal) {
            addresses.push(address.address + "_DatumClient");
        }
    }
}

var MqttClientId = "MQTT_CLIENT_12_" + addresses[0];
/**
 * options:
 *  - clientId
 *  - username
 *  - password
 *  - keepalive: number
 *  - clean:
 *  - will: 
 */
var mqtt_options = {
    clientId: MqttClientId,
    keepalive: 0,
    clean: true,
    // username: config.mqtt.MQTT_USERNAME,
    // password: config.mqtt.MQTT_PASSWORD
    will: { topic: "test1", payload: "offline", qos: 1, retain: 1 }
};

const client = mqtt.connect(config.mqtt.MQTT_URL, mqtt_options);

// debuggin code
client.on("error", function (error) {
    console.log("ERROR: ", error);
});

client.on('offline', function (error) {
    console.log("offline", error);
});

client.on('reconnect', function (error) {
    console.log("reconnect", error);
});
/**
 * here client is connect to server as assign
 */
client.on('connect', () => {
    console.log('MQTT Connected');
    client.subscribe("searchResult/#", {
        qos: 2
    });
    client.subscribe("match/user/#", {
        qos: 1
    });
    client.subscribe("GetChats/#", {
        qos: 2
    });
    client.subscribe("unMatch/#", {
        qos: 2
    });
    client.subscribe("OnlineStatus/#", {
        qos: 2
    });
    client.subscribe("Acknowledgement/#", {
        qos: 2
    });
    client.subscribe("Message/#", {
        qos: 1
    });
    client.subscribe("GetMessages/#", {
        qos: 2
    });
    client.subscribe("boost/#", {
        qos: 1
    });
    client.subscribe("#", {
        qos: 1
    });
    // client.subscribe("#", { qos: 1 });
    console.log("Client Connected...");
});
/**
 * here client listin chennels which is subscribe
 * In any channel user must be send fId : fiberBaseUserId 
 */
client.on('message', (topic, message) => {
    try {
        var targetId = "";
        message = JSON.parse(message.toString());
        switch (message.messageType) {
            case "onlineStatus":
                {

                    if (message && message._id) {
                        try {
                            userList.UpdateById(message._id, {
                                onlineStatus: message.onlineStatus || 0,
                                lastOnline: new Date().getTime()
                            }, () => { });

                            userListType.Update(message._id, {
                                onlineStatus: message.onlineStatus || 0,
                                lastOnline: new Date().getTime()
                            }, () => { });

                            if (!message.onlineStatus) {
                                redisClient.del(message._id, (e, r) => {
                                    console.log("remove Data from redis : ", e);
                                })
                            }
                        } catch (error) {
                            console.log("catch : ", error);
                        }
                    }
                    break;
                }
        }
        switch (topic) {
            case String(topic.match(/^OnlineStatus.*/)):
                {
                    if (message && message._id) {
                        try {
                            userList.UpdateById(message._id, {
                                onlineStatus: message.onlineStatus || 0,
                                lastOnline: new Date().getTime()
                            }, () => { });

                            userListType.Update(message._id, {
                                onlineStatus: message.onlineStatus || 0,
                                lastOnline: new Date().getTime()
                            }, () => { });

                            if (!message.onlineStatus) {
                                redisClient.del(message._id, (e, r) => {
                                    console.log("remove Data from redis : ", e);
                                })
                            }
                        } catch (error) {
                            console.log("catch : ", error);
                        }
                    }
                    break;
                }
            case String(topic.match(/^\/match\/user\/.*/)):
                {
                    console.log("got a msg ", message.toString())
                    break;
                }
            case String(topic.match(/^Acknowledgement.*/)):
                {
                    Acknowledgement(message);
                    break;
                }

            case String(topic.match(/^CallsAvailability.*/)):
            case String(topic.match(/^Calls.*/)):
                {

                    try {
                        console.log("message call : ", message);
                        if (message.type == 1 && message.dateId) {

                            dateScheduleCollection.Update(
                                { _id: objectId(message.dateId) },
                                { "expireAt": new Date(myLibrary.getExpiryTimeForMongoTTL(new Date().getTime(), (60 * 24))) },
                                (e, r) => {
                                    if (e) logger.error(e);
                                });
                            dateSchedule_TTLCollection.Update(
                                { _id: objectId(message.dateId) },
                                { "expireAt": new Date(myLibrary.getExpiryTimeForMongoTTL(new Date().getTime(), (60 * 24))) },
                                (e, r) => {
                                    if (e) logger.error(e);
                                });

                        } else if (message.type == 2 && message.dateId) {
                            dateScheduleCollection.Update(
                                { _id: objectId(message.dateId) },
                                { "expireAt": new Date(myLibrary.getExpiryTimeForMongoTTL(new Date().getTime(), 5)) },
                                (e, r) => {
                                    if (e) logger.error(e);
                                });
                            dateSchedule_TTLCollection.Update(
                                { _id: objectId(message.dateId) },
                                { "expireAt": new Date(myLibrary.getExpiryTimeForMongoTTL(new Date().getTime(), 5)) },
                                (e, r) => {
                                    if (e) logger.error(e);
                                });
                        }

                    } catch (error) {
                        logger.error(e)
                    }
                    break;
                }
            case String(topic.match(/^Message.*/)):
                {
                    console.log("received message  topic : " + topic);
                    insertMessage(message);
                    break;
                }
        }
        // console.log("message ", JSON.stringify(message))
        console.log("received message  topic : " + topic);
    } catch (e) {
        console.log("Exception : ", e);
    }
});

function insertMessage(message) {

    var userId, targetUserId, payload, type, messageId, creation, secretId, thumbnail, dTime,
        userImage, toDocId, name, dataSize, extension, fileName, mimeType, isMatchedUser,
        previousReceiverIdentifier, previousFrom, previousPayload, previousType, previousId, replyType, previousFileType;

    userId = message.from;
    targetUserId = message.to;
    payload = message.payload;
    type = message.type;
    messageId = message.id;
    secretId = (message.secretId) ? message.secretId : "";
    dTime = (message.dTime || message.dTime == 0) ? message.dTime : -1;
    creation = new Date().getTime();
    thumbnail = message.thumbnail;
    mimeType = message.mimeType;
    extension = message.extension;
    fileName = message.fileName;
    previousReceiverIdentifier = message.previousReceiverIdentifier;
    previousFrom = message.previousFrom;
    previousPayload = message.previousPayload;
    previousType = message.previousType;
    previousId = message.previousId;
    replyType = message.replyType;
    previousFileType = message.previousFileType;
    userImage = message.userImage;
    toDocId = message.toDocId;
    name = message.name;
    dataSize = message.dataSize
    isMatchedUser = (message.isMatchedUser) ? 1 : 0

    async.waterfall([
        function (callback) {

            var userExists = "members." + userId;
            var targetExists = "members." + targetUserId;
            var condition = {
                [userExists]: {
                    $exists: true
                },
                [targetExists]: {
                    $exists: true
                },
                secretId: secretId,
                chatType: "NormalChat",
                "$or": [{ "isUnMatched": { "$ne": 1 } }, { "isFriend": 1 }]
                ,
                "deletedUser": { "$exists": false },
            };
            middleWare.Select("chatList", condition, "Mongo", {}, function (err, result) {
                if (err) {
                    return callback(err, null);
                } else {
                    return (result[0]) ? callback(null, result[0]._id) : callback(null, 0);
                }
            });

        },
        function (chatId, callback) {

            var messageIdDB = new objectId();

            if (chatId && type != "11" && type != "12") {
                console.log("********************************in if**********************************************");
                var data = {
                    $unset: {
                        ["members." + userId + ".inactive"]: "",
                        ["members." + targetUserId + ".inactive"]: ""
                    },
                    $push: {
                        "message": messageIdDB
                    }
                };
                console.log("messageIdDB  ", messageIdDB.toString())
                var condition = {
                    _id: objectId(chatId.toString())
                };
                middleWare.FindAndModify("chatList",
                    data,
                    condition,
                    "Mongo", {}, {},
                    function (getChatErr, getChatRes) {
                        if (getChatErr) console.log('error 2030 : ', getChatErr)
                    })

            } else {
                console.log("********************************in else**********************************************");
                chatId = objectId();
                var chatDB = {
                    "_id": chatId,
                    "message": [messageIdDB],
                    "members": {
                        [userId]: {
                            "memberId": objectId(userId),
                            "status": "NormalMember"
                        },
                        [targetUserId]: {
                            "memberId": objectId(targetUserId),
                            "status": "NormalMember"
                        }
                    },
                    "senderId": objectId(userId),
                    "targetId": objectId(targetUserId),
                    "initiatedBy": objectId(userId),
                    "createdAt": new Date().getTime(),
                    "chatType": "NormalChat",
                    "secretId": secretId,
                    "isMatchedUser": isMatchedUser
                }
                middleWare.Insert("chatList", chatDB, {}, "Mongo", function (err, res) {
                    if (err) console.log("error 202 : ", err);
                });
                var chatType = (secretId == "") ? "singleChat" : "secretChat";
                // dasboardModule.dashboard({ "subject": "chatLog", "type": chatType });
            }

            var messageDB = {
                "_id": messageIdDB,
                "messageId": messageId,
                "secretId": secretId,
                "dTime": dTime,
                "senderId": objectId(userId),
                "receiverId": objectId(targetUserId),
                "payload": payload,
                "messageType": type,
                "timestamp": new Date().getTime(),
                "expDtime": 0,
                "chatId": objectId(chatId.toString()),
                "userImage": userImage,
                "toDocId": toDocId,
                "name": name,
                "dataSize": dataSize,
                "thumbnail": thumbnail,
                "mimeType": mimeType,
                "extension": extension,
                "fileName": fileName,
                "members": {
                    [userId]: {
                        "memberId": objectId(userId),
                        // "status": ""
                        // "deleteAt": 0
                    },
                    [targetUserId]: {
                        "memberId": objectId(targetUserId),
                        "status": 1,
                        // "readAt": 0,
                        // "deliveredAt": new Date().getTime()
                        // "deleteAt": 0
                    }
                }

            };

            if (type == '10') {
                messageDB.previousReceiverIdentifier = previousReceiverIdentifier;
                messageDB.previousFrom = previousFrom;
                messageDB.previousPayload = previousPayload;
                messageDB.previousType = previousType;
                messageDB.previousId = previousId;
                messageDB.replyType = replyType;
                if (previousType == '9') {
                    messageDB.previousFileType = previousFileType;
                }

            } else if (type == "11") {
                let removedAt = new Date().getTime();
                if (message.removedAt) {
                    removedAt = message.removedAt;
                }
                let condition = {
                    messageId: messageId,
                    senderId: objectId(userId)
                };
                let dataToUpdate = {
                    payload: payload,
                    messageType: "11",
                    removedAt: removedAt
                }
                middleWare.Update("messages", dataToUpdate, condition, "Mongo", () => { })
            } else if (type == "12") {
                let editedAt = new Date().getTime();
                if (message.editedAt) {
                    editedAt = message.editedAt;
                }
                let condition = {
                    messageId: messageId,
                    senderId: objectId(userId)
                };
                let dataToUpdate = {
                    payload: payload,
                    wasEdited: true,
                    editedAt: editedAt
                }
                middleWare.Update("messages", dataToUpdate, condition, "Mongo", () => { })
            } else {
                /** to avoid duplicate messages when the internet is not fast */
                // middleWare.UpdateAdvanced("messages", {
                //     $set: messageDB
                // }, {
                //         chatId: chatId,
                //         senderId: objectId(userId),
                //         messageId: messageId
                //     }, {
                //         upsert: true
                //     }, "Mongo", function (err, res) { if (err) console.log("error 2003 : ", err); })
                try {

                    middleWare.Insert("messages", messageDB, {}, "Mongo", function (err, res) { if (err) console.log("error 2043 : ", err); })
                } catch (error) {
                    console.log("error ASASAS: ", error)
                }
            }
            return callback(null, "done2");
        },
        function (data, callback) {
            userList.SelectOne({
                _id: objectId(targetUserId)
            }, (err, result) => {
                if (err) logger.error(err)


                if (result && result._id && result.currentLoggedInDevices.deviceType == "1") {
                    let deviceType = "1";

                    let payloadForFCM = {
                        notification: {
                            "body": "You got a new message from " + name,
                            "title": "message",
                            "receiverId": result._id,
                            "receiverName": result.firstName,
                            "receiverIdentifier": result.contactNumber,
                            "receiverImage": result.profilePic,
                            "isMatchUser": isMatchedUser,
                            "initiated": false,
                            "apns-collapse-id": userId,
                        },
                        data: {
                            "type": "41",
                            "target_id": userId,
                            "deviceType": deviceType,
                            "receiverId": result._id,
                            "receiverName": result.firstName,
                            "receiverIdentifier": result.contactNumber,
                            "receiverImage": result.profilePic,
                            "isMatchUser": isMatchedUser,
                            "initiated": false
                        } // values must be string
                    }
                    fcmPush.sendPushToTopic("/topics/" + targetUserId, payloadForFCM, (e, r) => { });
                    console.log("**********************************sendPushToTopic********************************************")
                }
                return callback(null, 1)
            })
        }
    ], () => { });
}
/** 
 * single chat message acknowledgement 
 */
function Acknowledgement(data) {

    data = data;
    // data = JSON.parse(data);
    /**
     { 
         to: '5988253b50e23910a3bd5a83',
         msgIds: [ '1502375529082' ],
         dTime: -1,
         secretId: 'wGtTnIJaYO80ZbuVFynJPnPLabs3Embed',
         from: '59847dbd3fe3042557c0745b',
         status: '2',
         doc_id: '3efb89ae-29cf-4242-b62d-4a7f1d5b0f97' 
      }
     */

    var userId = data.from;
    var messageId = data.msgIds;
    var status = parseInt(data.status);
    var dtime = (data.dTime) ? parseInt(data.dTime) : null;
    var targetId = data.to;
    var updaetStatus = "members." + userId + ".status";
    var secretId = data.secretId;

    /**
     * status: 2- delivered, 3- read
     */
    switch (parseInt(status)) {
        case "2":
        case 2:
            {
                /**
                 * Message Delivered to the recipients
                 */
                var deliveredAt = "members." + userId + ".deliveredAt";

                var dataToUpdate = {
                    [updaetStatus]: 2
                }
                dataToUpdate[deliveredAt] = new Date().getTime();

                for (var index = 0; index < messageId.length; index++) {
                    middleWare.Update("messages", dataToUpdate, {
                        messageId: messageId[index]
                    }, "Mongo", function (err, result) {
                        if (err) {
                            console.log("Error : messages 101 ", err);
                        }
                    });

                }
                break;
            }
        case "3":
        case 3:
            {
                /**
                 * Message Read by the recipients
                 */
                if (data.secretId && data.secretId != "") {
                    /**
                     * secret chat
                     * get all the documents to update and update thier expiry time
                     */
                    var condition = {
                        $and: [{
                            $and: [{
                                "receiverId": objectId(userId)
                            }, {
                                "senderId": objectId(targetId)
                            }, {
                                secretId: secretId
                            }]
                        },
                        // { ["members." + userId + ".status"]: { $exists: true } },
                        {
                            ["members." + userId + ".status"]: {
                                $ne: 3
                            }
                        },
                        {
                            "timestamp": {
                                "$lte": new Date().getTime()
                            }
                        },
                        {
                            "payload": {
                                $ne: ""
                            }
                        }
                        ]
                    }

                    middleWare.Select("messages", condition, "Mongo", {}, function (getMsgErr, getMsgResult) {
                        if (getMsgErr) {

                        } else if (getMsgResult.length > 0) {

                            async.each(getMsgResult, function (msgObj, msgCB) {
                                /**
                                 * update the each doc
                                 * get the 'dTime' and then calculate the 'expDtime'
                                 */
                                var dataToUpdate = {
                                    ["members." + userId + ".status"]: 3,
                                    ["members." + userId + ".readAt"]: new Date().getTime()
                                }
                                if (msgObj.dTime > 0) {
                                    dataToUpdate['expDtime'] = new Date().getTime() + (msgObj.dTime * 1000)
                                }

                                middleWare.Update("messages", dataToUpdate, {
                                    _id: msgObj._id
                                }, "Mongo", function (updtErr, updtResult) {
                                    if (updtErr) {
                                        console.log("Error : messages 101 ", err);
                                    }
                                });

                                msgCB(null);
                            })
                        }
                    })

                } else {
                    /**
                     * normal chat
                     * update all the document in single query
                     */

                    var condition = {
                        $and: [{
                            $and: [{
                                "receiverId": objectId(userId)
                            }, {
                                "senderId": objectId(targetId)
                            }, {
                                secretId: ""
                            }]
                        },
                        //{ ["members." + userId + ".status"]: { $exists: true } },
                        {
                            ["members." + userId + ".status"]: {
                                $ne: 3
                            }
                        },
                        {
                            "timestamp": {
                                "$lte": new Date().getTime()
                            }
                        }
                        ]
                    }

                    var dataToUpdate = {
                        ["members." + userId + ".status"]: 3,
                        ["members." + userId + ".readAt"]: new Date().getTime()
                    }

                    middleWare.Update("messages", dataToUpdate, condition, "Mongo", function (err, result) {
                        if (err) {
                            console.log("Error : messages 102 ", err);
                        }
                    });
                }
                break;
            }

    }
}