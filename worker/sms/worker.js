const logger = require('winston');
const ipc = require('node-ipc');
const rabbitMq = require('../../models/rabbitMq');
const SMS = require("../../library/twilio");
const smsLog = require('../../models/smsLog');



ipc.config.id = 'rabbitmqWorkerSMS';
ipc.config.retry = 1000;
ipc.config.silent = true;

ipc.connectTo('rabbitmqserverSMS', () => {
    ipc.of.rabbitmqserverSMS.on('connect', () => {
        ipc.of.rabbitmqserverSMS.emit('consumer.connected', {});
        logger.info("RabbitMQ consumer connected");
    });
});


logger.info('step 4: child Process Started.......' + process.pid + " ");

rabbitMq.connect(() => {
    prepareConsumer(rabbitMq.getChannelSMS(), rabbitMq.queueSMS, rabbitMq.get());
});



function prepareConsumer(channel, queue_arg, amqpConn) {

    channel.assertQueue(queue_arg, { durable: false }
        , function (err, queue) {
            logger.info("RabbitMQ Message count", queue.messageCount + " " + queue.consumerCount);
            if (queue.messageCount > 0) {
                channel.consume(queue_arg, function (msg) {

                    logger.info("msg   ", msg.content.toString());
                    var data = JSON.parse(msg.content.toString());
                    SMS.sendSms(data, function (err, result) { 
                        ipc.of.rabbitmqserverSMS.emit('message.consumed',result);
                    });
                    //   console.log("consume end"+new Date());
                }, { noAck: true }, function (err, ok) {
                    //To check if need to exit worker
                    // exitWorker(channel, amqpConn, queue_arg);
                });
            }
        });
}