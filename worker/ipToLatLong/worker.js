const logger = require('winston');
const ipc = require('node-ipc');
const rabbitMq = require('../../models/rabbitMq');
const mqtt = require('../../library/mqtt');
const unverfiedUsers = require('../../models/unverfiedUsers');
const userList = require('../../models/userList');
const ObjectID = require("mongodb").ObjectID
const elasticSearchDB = require('../../models/elasticSearch');
const cluster = require('cluster');
const db = require('../../models/mongodb');
elasticSearchDB.connect(() => { });
db.connect(() => { });//create a connection to mongodb
const userListType = require('../../models/userListType');
var geoip = require('geoip-lite');
const userListCollection = require("../../models/userList");
var NodeGeocoder = require('node-geocoder');




ipc.config.id = 'rabbitmqWorkerIptoLatLong';
ipc.config.retry = 1000;
ipc.config.silent = true;

ipc.connectTo('rabbitmqserverIptoLatLong', () => {
    ipc.of.rabbitmqserverIptoLatLong.on('connect', () => {
        ipc.of.rabbitmqserverIptoLatLong.emit('consumer.connected', {});
        logger.info("RabbitMQ consumer connected");
    });
});

rabbitMq.connect(() => {
    prepareConsumer(rabbitMq.getChannelLatLong(), rabbitMq.queueLatLong, rabbitMq.get());
});



function prepareConsumer(channel, queue_arg, amqpConn) {

    channel.assertQueue(queue_arg, { durable: false }
        , function (err, queue) {

            if (queue.messageCount > 0) {
                channel.consume(queue_arg, function (msg) {

                    let data = JSON.parse(msg.content.toString());

                    let _id = data.data._id;
                    let ip = data.data.ip;
                    let geo = geoip.lookup(ip);

                    try {
                        if (geo) {
                            geo = { longitude: geo.ll[1], latitude: geo.ll[0] };

                            userList.UpdateById(_id, { location: geo }, (e, r) => { });

                            let data_ES = {
                                location: {
                                    "lat": geo.latitude,
                                    "lon": geo.longitude
                                }
                            };
                            userListType.Update(_id, data_ES, (err, result) => { });



                            try {
                                var options = {
                                    provider: 'google',
                                    httpAdapter: 'https', // Default
                                    apiKey: "AIzaSyC5DWlx6UqE9TO26ToFYqC_8Jkjoqw4eAA", // for Mapquest, OpenCage, Google Premier
                                    formatter: null         // 'gpx', 'string', ...
                                };
                                var geocoder = NodeGeocoder(options);
                                
                                geocoder.reverse({ lat: geo.latitude, lon: geo.longitude }, function (err, res) {
                                    logger.error(err)
                                    logger.info(res)
                                    if (res && res[0]) {
                                        userListCollection.UpdateById(_id, {
                                            "address": res[0]
                                        }, () => { })
                                    }
                                });
                            } catch (error) {

                            }


                            // mqtt.publish("ipToLatLong", JSON.stringify(geo), { qos: 1 }, () => { });


                        } else {
                            geo = { longitude: 77.58, latitude: 13.02 };

                            userList.UpdateById(_id, { location: geo }, (e, r) => { });

                            let data_ES = {
                                location: {
                                    "lat": geo.latitude,
                                    "lon": geo.longitude
                                }
                            };
                            userListType.Update(_id, data_ES, (err, result) => { });

                        }


                    } catch (error) {

                    }

                    // ipc.of.rabbitmqserverIptoLatLong.emit('message.consumed', geo);
                    //   console.log("consume end"+new Date());
                }, { noAck: false }, function (err, ok) {

                    //To check if need to exit worker

                });
            }


        });

}


// function exitWorker(channel, amqpConn, queue_arg) {

//     channel.assertQueue(queue_arg, { durable: false }, function (err, queue) {
//         logger.info("RabbitMQ Message count", queue.messageCount);
//         if (queue.messageCount == 0) {
//             logger.info('step -5: exeection done')
//             logger.info('RabbitMQ Shutdown command, exiting...' + process.pid);
//             ipc.of.rabbitmqserverIptoLatLong.emit('consumer.exiting', {});
//             channel.connection.close();
//             amqpConn.close();
//             logger.info('Process end.......' + process.pid + " ");
//             process.exit();
//         }
//     });
// }