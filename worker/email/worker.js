const logger = require('winston');
const rabbitMq = require('../../models/rabbitMq');
const sendMail = require('../../library/mailgun');


rabbitMq.connect(() => {
    prepareConsumer(rabbitMq.getChannelEmail(), rabbitMq.queueEmail, rabbitMq.get());
});



function prepareConsumer(channelEmail, queueEmail, amqpConn) {

    channelEmail.assertQueue(queueEmail, { durable: false }
        , function (err, queue) {


            channelEmail.consume(queueEmail, function (msg) {


                var data = JSON.parse(msg.content.toString());
                let param = {
                    from: data.from,
                    to: data.to,
                    subject: data.subject,
                    html: data.html,
                    text: data.html
                }
                sendMail.sendMail(param, (err, res) => {
                    if (res) {
                        logger.silly("mail sent to ", data.to);
                    }
                });

            }, { noAck: true }, function (err, ok) {
                //To check if need to exit worker
            });

        });
}