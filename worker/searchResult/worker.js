const logger = require('winston');
const config = require("../../config")
const ipc = require('node-ipc');
const rabbitMq = require('../../models/rabbitMq');
const mqtt = require('../../library/mqtt');
const Joi = require("joi");
const async = require("async");
const Promise = require('promise');
const ObjectID = require("mongodb").ObjectID
const userListType = require('../../models/userListType');
const elasticSearchDB = require('../../models/elasticSearch');
const cluster = require('cluster');
elasticSearchDB.connect(() => { });

logger.default.transports.console.colorize = true
logger.default.transports.console.timestamp = true
logger.default.transports.console.prettyPrint = config.env === 'development'
logger.level = config.logger.level

rabbitMq.connect(() => {
    prepareConsumer(rabbitMq.getChannelSearchResult(), rabbitMq.queueSearchResult, rabbitMq.get());
});

function prepareConsumer(channel, queue_arg, amqpConn) {
    channel.assertQueue(queue_arg, { durable: false }, function (err, queue) {

        channel.consume(queue_arg, function (msg) {

            var data = JSON.parse(msg.content.toString());
            var _id = data._id;
            var skip = data.skip || 0;
            var limit = data.limit || 20;


            let favoritePreferences = [], dataToSend = [], matcheResult = [];
            let gender = "Male,Female", heightMin = 100, heightMax = 300, dobMin = 1504867670000, dobMax = 1804867670000, distanceMin = 1,
                distanceMax = 500, latitude = 77.589864, longitude = 13.028712, contactNumber = "123", fbId = "123", must_not = [];
            let myLikes = [], matches = [], myDisLikes = [], myBlock = [];

            try {

                async.series([
                    function (callback) {

                        userListType.Select({ "_id": _id }, (err, result) => {
                            // console.log("_sorces : ", result.hits.hits[0]._source.contactNumber);
                            if (result.hits.hits && result.hits.hits[0]) {
                                favoritePreferences = result.hits.hits[0]._source.searchPreferences;
                                latitude = result.hits.hits[0]._source.location.lat || 77;
                                longitude = result.hits.hits[0]._source.location.lon || 13;
                                contactNumber = result.hits.hits[0]._source.contactNumber || result.hits.hits[0]._source.fbId;
                                fbId = result.hits.hits[0]._source.fbId;
                                myLikes = result.hits.hits[0]._source.myLikes || [];
                                matches = result.hits.hits[0]._source.matchedWith || [];
                                myDisLikes = result.hits.hits[0]._source.myunlikes || [];
                                myBlock = result.hits.hits[0]._source.myBlock || [];
                            } else {
                                favoritePreferences = [];
                                latitude = 77;
                                longitude = 13;
                                contactNumber = "XXXXXXXXXXX";
                                fbId = "XXXXXX";
                                myLikes = [];
                                matches = [];
                                myDisLikes = [];
                                myBlock = [];
                            }

                            callback(err, result);
                        });
                    },
                    function (callback) {

                        for (let index = 0; index < favoritePreferences.length; index++) {
                            if (favoritePreferences[index].pref_id == "57231014e8408f292d8b4567") {
                                gender = favoritePreferences[index].selectedValue.toString();
                            } else if (favoritePreferences[index].pref_id == "58bfb210849239b062ca9db4") {
                                heightMin = (favoritePreferences[index].selectedValue[0] < 100) ? 100 : favoritePreferences[index].selectedValue[0];
                                heightMax = (favoritePreferences[index].selectedValue[1] < 100) ? 300 : favoritePreferences[index].selectedValue[1];
                            } else if (favoritePreferences[index].pref_id == "55d486a5f6523c582bc51c53") {
                                dobMin = favoritePreferences[index].selectedValue[0];
                                dobMax = favoritePreferences[index].selectedValue[1];

                                let today = new Date();
                                let yyyy = today.getFullYear();
                                let MininmumAge = yyyy - dobMin;
                                let MixinmumAge = yyyy - dobMax;
                                let MinDob = '01/01/' + MininmumAge;
                                let MaxDob = '01/01/' + MixinmumAge;
                                let dateOfBirth1 = new Date(MinDob);
                                let dateOfBirth2 = new Date(MaxDob);
                                let MaxTimeStamp = dateOfBirth1.getTime();
                                let MinTimeStamp = dateOfBirth2.getTime();

                                dobMin = (favoritePreferences[index].selectedValue[1] == 55) ? -2179594491000 : MinTimeStamp;
                                dobMax = MaxTimeStamp
                            } else if (favoritePreferences[index].pref_id == "55d48f9bf6523cc552c51c52") {
                                distanceMin = (favoritePreferences[index].selectedValue[0] || 1) + "km";
                                distanceMax = (favoritePreferences[index].selectedValue[1] || 100) + "km";
                            }
                        }

                        callback(null, 1);
                    },
                    function (callback) {

                        must_not.push({ "match": { "contactNumber": contactNumber || "12345" } });
                        must_not.push({ "match": { "fbId": fbId || "-11111" } });
                        must_not.push({ "match": { "isDeactive": true } });
                        must_not.push({ "match": { "profileStatus": 1 } });

                        for (let index = 0; index < myLikes.length; index++) {
                            must_not.push({ "match": { "_id": myLikes[index] } });
                        }
                        for (let index = 0; index < matches.length; index++) {
                            must_not.push({ "match": { "_id": matches[index] } });
                        }
                        for (let index = 0; index < myDisLikes.length; index++) {
                            must_not.push({ "match": { "_id": myDisLikes[index] } });
                        }
                        for (let index = 0; index < myBlock.length; index++) {
                            must_not.push({ "match": { "_id": myBlock[index] } });
                        }
                        callback(null, 5);
                    },
                    function (callback) {
                        userListType.findMatch({
                            gender: gender, heightMin: heightMin, heightMax: heightMax, dobMin: dobMin,
                            dobMax: dobMax, longitude: longitude, latitude: latitude, distanceMin: distanceMin,
                            distanceMax: distanceMax, contactNumber: contactNumber, must_not: must_not,
                            skip: skip, limit: limit
                        }, (err, result) => {
                            if (err) logger.error(err)
                            matcheResult = (result.hits) ? result.hits.hits : [];
                            logger.silly("matcheResult total ", matcheResult.length)
                            callback(err, result);
                        });
                    },


                    function (callback) {
                        for (let index = 0; index < matcheResult.length; index++) {
                            let birthdate = new Date(matcheResult[index]["_source"]["dob"]);
                            let cur = new Date();
                            let diff = cur - birthdate; // This is the difference in milliseconds
                            let age = Math.floor(diff / 31557600000); // Divide by 1000*60*60*24*365.25

                            dataToSend.push({
                                "opponentId": matcheResult[index]["_id"],
                                "firstName": matcheResult[index]["_source"]["firstName"],
                                "mobileNumber": matcheResult[index]["_source"]["contactNumber"],
                                "emailId": matcheResult[index]["_source"]["email"],
                                "height": matcheResult[index]["_source"]["height"],
                                "gender": matcheResult[index]["_source"]["gender"],
                                "dateOfBirth": matcheResult[index]["_source"]["dob"],
                                "onlineStatus": matcheResult[index]["_source"]["onlineStatus"],
                                "about": matcheResult[index]["_source"]["about"],
                                "profilePic": matcheResult[index]["_source"]["profilePic"],
                                "profileVideo": matcheResult[index]["_source"]["profileVideo"],
                                "otherImages": matcheResult[index]["_source"]["otherImages"],
                                "firebaseTopic": matcheResult[index]["_source"]["firebaseTopic"],
                                "instaGramProfileId": matcheResult[index]["_source"]["instaGramProfileId"] || "",
                                "instaGramToken": matcheResult[index]["_source"]["instaGramToken"] || "",
                                "deepLink": matcheResult[index]["_source"]["deepLink"],
                                "profileVideoThumbnail": matcheResult[index]["_source"]["profileVideoThumbnail"],

                                "distance": parseFloat(matcheResult[index]["sort"][0].toFixed(2)),
                                "age": age
                            })
                        }
                        callback(null, 2);
                    }
                ], (err, result) => {
                    mqtt.publish("searchResult/" + _id, JSON.stringify({ data: dataToSend }), { qos: 2 }, () => { });
                })

            } catch (error) {
                logger.error("error : ", error);
                mqtt.publish("searchResult/" + _id, JSON.stringify({ data: dataToSend }), { qos: 2 }, () => { });
            }
        }, { noAck: true }, function (err, ok) {
            //To check if need to exit worker
            logger.silly("no have message");
        });
    });
}