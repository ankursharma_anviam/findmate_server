const logger = require('winston');
const ipc = require('node-ipc');
const rabbitMq = require('../../models/rabbitMq');
const mqtt = require('../../library/mqtt');
const unverfiedUsers = require('../../models/unverfiedUsers');
const userList = require('../../models/userList');
const ObjectID = require("mongodb").ObjectID
const elasticSearchDB = require('../../models/elasticSearch');
const cluster = require('cluster');
const db = require('../../models/mongodb');
elasticSearchDB.connect(() => { });
db.connect(() => { });//create a connection to mongodb
// const userListType = require('../../models/userListType');
const userListCollection = require("../../models/userList");
const userListType_ES = require("../../models/userListType");
var NodeGeocoder = require('node-geocoder');


rabbitMq.connect(() => {
    prepareConsumer(rabbitMq.getChannelLatLongToCity(), rabbitMq.queueLatLongToCity, rabbitMq.get());
});



function prepareConsumer(channel, queue_arg, amqpConn) {

    channel.assertQueue(queue_arg, { durable: false }
        , function (err, queue) {
                
                channel.consume(queue_arg, function (msg) {
                    
                    let data = JSON.parse(msg.content.toString());
                    
                    try {
                        var options = {
                            provider: 'google',
                            httpAdapter: 'https', // Default
                            apiKey: "AIzaSyD6gFN9a_X73ihsvMBXCKPSh5SuC58U2fw", // for Mapquest, OpenCage, Google Premier
                            formatter: null         // 'gpx', 'string', ...
                        };
                        var geocoder = NodeGeocoder(options);
                        
                        geocoder.reverse({ lat: data.lat, lon: data.lon }, function (err, res) {
                            console.log("resssss",res,"errrrr",err)

                            if (res && res[0]) {
                                
                                console.log("update city successfully")
                                let address = {};

                                if (res[0]["formattedAddress"]) address["formattedAddress"] = res[0]["formattedAddress"]
                                if (res[0]["latitude"]) address["latitude"] = res[0]["latitude"]
                                if (res[0]["longitude"]) address["longitude"] = res[0]["longitude"]
                                if (res[0]["streetNumber"]) address["streetNumber"] = res[0]["streetNumber"]
                                if (res[0]["streetName"]) address["streetName"] = res[0]["streetName"]
                                if (res[0]["country"]) address["country"] = res[0]["country"]
                                if (res[0]["countryCode"]) address["countryCode"] = res[0]["countryCode"]
                                if (res[0]["city"]) address["city"] = res[0]["city"]

                                userListCollection.UpdateById(data._id, {
                                    "address": address
                                }, () => { })

                                userListType_ES.Update(data._id,{address:address},(e,r)=>{if(e)console.log("error : address es  ",e)});
                                
                            }else{
                                console.log("not got city : ")        
                            }
                        });
                    } catch (error) {
                        console.log("latlong to city : ", error)
                    }
                }, { noAck: false }, function (err, ok) {

                    //To check if need to exit worker

                });
            
        });
}