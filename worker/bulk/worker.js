const logger = require('winston');
const config = require("../../config")
const rabbitMq = require('../../models/rabbitMq');
const userListType = require('../../models/userListType');
const elasticSearchDB = require('../../models/elasticSearch');
var bulkMsg = [];
var timeOut;


logger.default.transports.console.colorize = true
logger.default.transports.console.timestamp = true
logger.default.transports.console.prettyPrint = config.env === 'development'
logger.level = config.logger.level

rabbitMq.connect(() => {
    elasticSearchDB.connect(() => {
        prepareConsumer(rabbitMq.getChannelForBulkQuerys(), rabbitMq.queueForBulkQuerys, rabbitMq.get());
    });
});


function prepareConsumer(channel, queue_arg, amqpConn) {
    channel.assertQueue(queue_arg, { durable: false }, function (err, queue) {
        channel.consume(queue_arg, function (msg) {
            try {
                var dataInArray = JSON.parse(msg.content.toString());
                dataInArray.forEach(element => {
                    bulkMsg.push(element);
                });
                if (bulkMsg.length > 100) {
                    updateInES();
                } else {
                    timeOut = setTimeout(updateInES, 1000 * 10);
                }
            } catch (error) {
                logger.error("error : ", error);
            }
        }, { noAck: false }, function (err, ok) {

            //To check if need to exit worker

        });
    });
}

function updateInES() {
    if (bulkMsg.length) {
        userListType.Bulk(bulkMsg, (e, r) => { if (e) logger.error(e) });
    }
    bulkMsg = [];
    clearTimeout(timeOut);
}