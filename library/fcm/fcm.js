const FCM = require('fcm-push');
const config = require('../../config');
//const fcm = new FCM(process.env.FCM_SERVER_KEY);
const fcm = new FCM(config.fcm.FCM_SERVER_KEY);

const sendPushToTopic = (userId, request, callback) => {

    let payload = {};
    let resData = {
        action: 1,
        pushType: 2,
        title: 'Match',
        categoryIdentifier: "chat.category",
        msg: 'Hey You Got Match',
        "type": request.data.type,
        "target_id": request.data.target_id,
        "title": request.notification.title || "findmate",
        "message": request.notification.body,
        "sound": "default",
        data: request.data

    }
    payload = {
        to: `${userId}`, // required fill with device token or topics
        // to: `/topics/${userId}`, // required fill with device token or topics
        collapse_key: '1',
        priority: 'high',
        apns: {
            headers: {
                'collapse_key': "1"
            }
        },
        "delay_while_idle": true,
        "dry_run": false,
        "time_to_live": 3600,
        "content_available": true,
        badge: 1,
        data: resData,
        categoryIdentifier: "chat.category",
    };
    if (request.data.deviceType == "2") {
        payload.notification = {
            title: request.notification.title || "findmate",
            body: request.notification.body,
            sound: "default",
            categoryIdentifier: "chat.category"
        };
    }

    console.log("fcm data ", payload)
    fcm.send(payload)
        .then(function (response) {
            console.log("notification sent payload ", payload)
            console.log("notification sent ", response)
            callback(null, 1);
        })
        .catch(function (err) {
            console.log("Something has gone wrong ! ", err);
            callback(err, null);
        });



    /** 
    * Function to send push notification to a specified topic 
    * topic: should follow [a-zA-Z0-9-_.~%] format
    * payload: must be object 
           format: { 
               notification : { body : "string", title : "string", icon : "string" },
               data: { field1: 'value1', field2: 'value2' } // values must be string
           }
    
           * Send a message to devices subscribed to the provided topic.
         */
    // new Promise((resolve, reject) => {

    //     admin.messaging().sendToTopic(topic, payload)
    //         .then(function (response) {
    //             console.log("notification sent")
    //             // resolve({ err: 0, message: "notification sent" });
    //         })
    //         .catch(function (error) {
    //             /* Error sending message  */
    //             console.log("notification not  sent", error)
    //             // reject({ err: 1, message: error.message, error: error });
    //         });
    // })
}




module.exports = {
    sendPushToTopic

}