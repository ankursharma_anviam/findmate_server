var moment = require("moment");
var logger = require('winston');
var ObjectID = require('mongodb').ObjectID;

var config = require('./config');
var fcm = require('./library/fcm')
var mqtt = require('./library/mqtt')
var rabbitMq = require('./models/rabbitMq');
var userList = require("./models/userList");
var planCollection = require('./models/plans');
var userListType = require("./models/userListType");
var subscription = require("./models/subscription");
var userBoost = require("./models/userBoost");
var rabbitmqUtil = require('./models/rabbitMq/utils');

const likeNotificationAt = "likeNotificationAt:";
const likeRefreshInMillisecond = 1000 * 60  * 5;
// const likeRefreshInMillisecond = 1000 * 60 * 60 * 12;
const redisKeyForBoost = "redisKeyForBoost:";
const boostTime = 1000 * 60 * 5;
// const boostTime = 1000 * 60 * 30;

const heightInCMObj = {
    "4.0(121 cm)": 121,
    "4.1(124 cm)": 124,
    "4.2(127 cm)": 127,
    "4.3(129 cm)": 129,
    "4.4(132 cm)": 132,
    "4.5(134 cm)": 134,
    "4.6(137 cm)": 137,
    "4.7(139 cm)": 139,
    "4.8(142 cm)": 142,
    "4.9(144 cm)": 144,
    "4.10(147 cm)": 147,
    "4.11(149 cm)": 149,
    "5.0(152 cm)": 152,
    "5.1(154 cm)": 154,
    "5.2(157 cm)": 157,
    "5.3(160 cm)": 160,
    "5.4(162 cm)": 162,
    "5.5(165 cm)": 165,
    "5.6(167 cm)": 167,
    "5.7(170 cm)": 170,
    "5.8(172 cm)": 172,
    "5.9(175 cm)": 175,
    "5.10(177 cm)": 177,
    "5.11(180 cm)": 180,
    "6.0(182 cm)": 182,
    "6.1(185 cm)": 185,
    "6.2(187 cm)": 187,
    "6.3(190 cm)": 190,
    "6.4(193 cm)": 193,
    "6.5(195 cm)": 195,
    "6.6(198 cm)": 198,
    "6.7(200 cm)": 200,
    "6.8(203 cm)": 203,
    "6.9(205 cm)": 205,
    "6.10(208 cm)": 208,
    "6.11(210 cm)": 210,
    "7.0(213 cm)": 213
}
const heightInFeetObj = {
    "4.0(121 cm)": `4'0"`,
    "4.1(124 cm)": `4'1"`,
    "4.2(127 cm)": `4'2"`,
    "4.3(129 cm)": `4'3"`,
    "4.4(132 cm)": `4'4"`,
    "4.5(134 cm)": `4'5"`,
    "4.6(137 cm)": `4'6"`,
    "4.7(139 cm)": `4'7"`,
    "4.8(142 cm)": `4'8"`,
    "4.9(144 cm)": `4'9"`,
    "4.10(147 cm)": `4'10"`,
    "4.11(149 cm)": `4'11"`,
    "5.0(152 cm)": `5'0"`,
    "5.1(154 cm)": `5'1"`,
    "5.2(157 cm)": `5'2"`,
    "5.3(160 cm)": `5'3"`,
    "5.4(162 cm)": `5'4"`,
    "5.5(165 cm)": `5'5"`,
    "5.6(167 cm)": `5'6"`,
    "5.7(170 cm)": `5'7"`,
    "5.8(172 cm)": `5'8"`,
    "5.9(175 cm)": `5'9"`,
    "5.10(177 cm)": `5'10"`,
    "5.11(180 cm)": `5'11"`,
    "6.0(182 cm)": `6'0"`,
    "6.1(185 cm)": `6'1"`,
    "6.2(187 cm)": `6'2"`,
    "6.3(190 cm)": `6'3"`,
    "6.4(193 cm)": `6'4"`,
    "6.5(195 cm)": `6'5"`,
    "6.6(198 cm)": `6'6"`,
    "6.7(200 cm)": `6'7"`,
    "6.8(203 cm)": `6'8"`,
    "6.9(205 cm)": `6'9"`,
    "6.10(208 cm)": `6'10"`,
    "6.11(210 cm)": `6'11"`,
    "7.0(213 cm)": `7'0"`
}
const findAndRemoveExpiredInAppPurchasePlans = () => {
    try {
        var dataToPlanUpdateFreePlan = {};
        planCollection.SelectOne({ _id: ObjectID("5b52d9901582421dee1bde50") }, (err, planDetails) => {
            if (planDetails) {
                let purchaseDate = moment().startOf('day').valueOf();
                let expiryTime = 0;
                if (planDetails.durationInMonths) {
                    expiryTime = moment().add(planDetails.durationInMonths, "months").startOf('day').valueOf();
                } else {
                    expiryTime = moment().add(planDetails.durationInDays, "days").startOf('day').valueOf();
                }
                let purchaseTime = new Date().getTime();
                dataToPlanUpdateFreePlan = {
                    "planId": ObjectID(planDetails._id),
                    "subscriptionId": "Free Plan",
                    "purchaseDate": purchaseDate,
                    "purchaseTime": purchaseTime,
                    "userPurchaseTime": purchaseTime,
                    "durationInMonths": planDetails.durationInMonths,
                    "expiryTime": expiryTime,
                    "likeCount": planDetails.likeCount,
                    "rewindCount": planDetails.rewindCount,
                    "superLikeCount": planDetails.superLikeCount,
                    "whoLikeMe": planDetails.whoLikeMe,
                    "whoSuperLikeMe": planDetails.whoSuperLikeMe,
                    "recentVisitors": planDetails.recentVisitors,
                    "readreceipt": planDetails.readreceipt,
                    "passport": planDetails.passport,
                    "noAdds": planDetails.noAdds,
                    "hideDistance": planDetails.hideDistance,
                    "hideAge": planDetails.hideAge,
                };
            }
        });
        let condition = [
            { "$match": { "statusCode": 1, "expiryTime": { "$lte": new Date().getTime() } } },
            { "$lookup": { "from": "userList", "localField": "userId", "foreignField": "_id", "as": "Data" } },
            { "$unwind": "$Data" },
            { "$project": { "planName": 1, "userId": 1, "firstName": "$Data.firstName", "email": "$Data.email" } }
        ]
        subscription.Aggregate(condition, (err, result) => {
            if (err) { logger.error(err) }
            else if (result) {
                let plansToSetExpired = [];

                result.forEach(element => {
                    plansToSetExpired.push(ObjectID(element._id));

                    let dataToSend = {
                        "from": "noreply@appscrip.com",
                        "to": element.email,
                        "subject": `Plan expired on “${config.server.APPNAME}”`,
                        "html": `Hello <b>${element.firstName.toUpperCase()}</b>,
                                <br><br>Your paid plan on <b>${config.server.APPNAME}</b> has expired.
                                <br><br>
                                You will now stop enjoying all the paid privileges on <b>${config.server.APPNAME}</b>.
                                <br><br><br><br>
                                Cheers,<br>
                                <b>${config.server.TEAMNAME}.</b>`
                    };
                    rabbitmqUtil.InsertQueue(rabbitMq.getChannelEmail(), rabbitMq.queueEmail, dataToSend, (err, doc) => { });
                    userList.UpdateByIdWithPull({ _id: element.userId }, { "subscription": { "subscriptionId": ObjectID(element._id) } },
                        (err, result) => { });
                    userList.UpdateById(element.userId, { "subscription": [dataToPlanUpdateFreePlan] }, (err, result) => {
                        if (err) logger.error(err);
                    });
                });
                subscription.Update({ "_id": { "$in": plansToSetExpired } }, { "statusCode": 0, "status": "expired" }, () => { });

            }
        });
    } catch (error) {
        logger.error("in cronJobTask.js : ", error);
    }
}
const sendWellComeMailOnSignUp = (data) => {
    try {
        let dataToSend = {
            "from": "noreply@appscrip.com",
            "to": data.email,
            "subject": `Welcome to ${config.server.APPNAME}!`,
            "html": `<!DOCTYPE html>
            <html>
            <title>customerNewSignup</title>
            <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
            <style>
                @import url('https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900');
                .contTab {
                    padding: 15px 35px;
                    background: #fff!important;
                    border-radius: 4px;
                }
            
                .titleHeader {
                    text-align: center;
                    text-transform: capitalize;
                    font-weight: 300;
                    color: #588dc9 !important;
                    letter-spacing: 0.6px;
                    background: -webkit-linear-gradient(120deg, #FF7B43,#F53170) !important;
                    background-clip: border-box;
                    -webkit-background-clip: text;
                    -webkit-text-fill-color: transparent;
                }
            
                .recptTitle {
                    font-weight: 500;
                    letter-spacing: 0.3px;
                    margin-top: 0px;
                    margin-bottom: 10px;
                    font-size: 16px;
                }
            
                .recptNum {
                    margin-top: 0px;
                    font-size: 14px;
                    color: #666;
                    font-weight: 400;
                    margin-bottom: 10px;
                    letter-spacing: 0.3px;
                }
            
                .paidCat {
                    color: #666;
                    margin: 0;
                    font-weight: 500;
                    letter-spacing: 0.3px;
                    margin-bottom: 5px;
                    font-size: 12px;
                }
            
                .detailsSpan {
                    /* letter-spacing: 0.3px;
                    font-size: 15px !important;
                    color: #666;
                    margin: 0; */
                    letter-spacing: 0.5px;
                    font-size: 12px !important;
                    color: #666;
                    margin: 0;
                    text-align: left !important;
                    line-height: 1.6;
                    min-height: 76px;
                    padding: 0 10px;
                    opacity: 0.8;
                }
            
                .payMethod {
                    font-weight: 600 !important;
                }
            
                .anchClass {
                    /* color: #2f98d8; */
                    color: #fff !important;
                    font-weight: 500 !important;
                }
            
                @media screen and (max-width: 767px) and (min-width: 240px) {
                    .contTab {
                        padding: 15px;
                        background: #fff;
                    }
                    .recptTitle {
                        font-weight: 500;
                    }
                    .recptNum {
                        font-size: 12px;
                        font-weight: 500;
                    }
                    .paidCat {
                        text-align: center;
                        font-weight: 400;
                    }
                    .detailsSpan {
                        text-align: center;
                        font-size: 14px !important;
                        font-weight: 500;
                    }
                    .imgLayoutTd {
                        display: block;
                    }
                }
            </style>
            
            <body style="margin:0px; font-family: 'Roboto', sans-serif;">
            
                <table width="100%" cellspacing="0" cellpadding="0" border="0" style="font-family: 'Roboto', sans-serif;">
                    <tr>
                        <td style="    background: -moz-linear-gradient(top, #FF7B43 0%, #F53170 100%);
                        background: -webkit-linear-gradient(top, #FF7B43 0%, 49%,#F53170 100%);
                        background: linear-gradient(to bottom, #FF7B43 0%, 49%,#F53170 100%);padding-top: 30px;">
                            <table width="100%" cellspacing="0" cellpadding="0" border="0" style="max-width:700px;margin:0 auto;box-shadow: 0 4px 12px 0 rgba(0,0,0,0.3);">
                                <tr>
                                    <td style="">
            
                                        <!--header-->
                                        <table width="100%" cellspacing="0" cellpadding="0" border="0" style="background: #fff;padding-top: 25px;
                                        padding-bottom: 10px;padding-left: 25px;padding-right: 25px;">
                                            <tr>
                                                <td style="width:100%;">
                                                    <h1 style="text-align: center;
                                                    text-transform: capitalize;
                                                    font-weight: 300;
                                                    color: #FF7B43;
                                                    letter-spacing: 0.6px;
                                                    background-clip: border-box;
                                                    -webkit-background-clip: text;
                                                    -webkit-text-fill-color: transparent;">Welcome to the leading
                                                        <br> online dating website</h1>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width:100%;text-align: center;">
                                                    <img src="https://res.cloudinary.com/dbjiphldq/image/upload/v1534244164/datum-mail-template/welcome-template/datum_logo.png" style="max-width: 100px;padding: 5px 75px;padding-bottom: 5px;border-radius: 4px;padding-bottom: 10px !important;cursor: pointer;">
                                                </td>
                                            </tr>
            
                                        </table>
            
                                        <!--header-->
                                        <!--content-->
                                        <table width="100%" cellspacing="0" cellpadding="0" border="0" style="">
                                            <tr>
                                                <td>
                                                    <table width="100%" cellspacing="0" cellpadding="0" border="0" class="contTab" style="background: #fff;">
                                                        <tr>
                                                            <td>
                                                                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                                                    <tr>
                                                                        <td style="letter-spacing: 0.3px;color: #666;">
                                                                            <p class="" style="    text-align: center;
                                                                            margin: 0;
                                                                            font-size: 15px;
                                                                            line-height: 25px;
                                                                            padding-top: 0px;
                                                                            padding-bottom: 30px;
                                                                            padding-right:calc(10%);
                                                                            padding-left: calc(10%);">
                                                                                <span style="font-weight: 500">Congratulations!</span> You've joined the most dynamic
                                                                                group of singles out there.
                                                                                <a href="#" class="anchClass">
                                                                                    We're responsible for more dates, relationships and marriages than any other site. Period.
                                                                                </a>
                                                                            </p>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <table border="0" cellspacing="0" cellpadding="0" style="width: 100%;text-align: center;">
                                                                    <thead>
                                                                        <tr>
                                                                            <td style="" class="imgLayoutTd">
                                                                                <img src="https://res.cloudinary.com/dbjiphldq/image/upload/v1534237118/datum-mail-template/welcome-template/create_profilexxxhdpi.png" width="" style="height: 120px;max-width: 90%;padding-bottom: 10px;" alt="mail1">
                                                                                <p class="detailsSpan">
                                                                                    Take a minute to create your profile and let everyone know exactly who you are.
                                                                                </p>
                                                                            </td>
                                                                            <td style="" class="imgLayoutTd">
                                                                                <img src="https://res.cloudinary.com/dbjiphldq/image/upload/v1534237118/datum-mail-template/welcome-template/share_photoxxxhdpi.png" width="" style="height: 120px;max-width: 90%;padding-bottom: 10px;" alt="mail2">
                                                                                <p class="detailsSpan">
                                                                                    Get to call your match and meet them in person if you wish.And get into some action.
                                                                                    <!-- <strong style="word-spacing:3px;"> 15 times more likely to be viewed </strong>
                                                                                    and three times more likely to receive winks. -->
                                                                                </p>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="" class="imgLayoutTd">
                                                                                <img src="https://res.cloudinary.com/dbjiphldq/image/upload/v1534237118/datum-mail-template/welcome-template/messagexxxhdpi.png" width="" style="height: 120px;max-width: 90%;padding-bottom: 10px;" alt="mail3">
                                                                                <p class="detailsSpan">
                                                                                    Search,like,chat and date,Or,jump right into the action and start chatting real time with other members.
                                                                                </p>
                                                                            </td>
                                                                            <td style="" class="imgLayoutTd">
                                                                                <img src="https://res.cloudinary.com/dbjiphldq/image/upload/v1534237118/datum-mail-template/welcome-template/video_chatxxxhdpi.png" width="" style="height: 120px;max-width: 90%;padding-bottom: 10px;" alt="mail3">
                                                                                <p class="detailsSpan">
                                                                                    Add videos of you in your profile.Profile with video are hundreds times more likely to receive more likes.
                                                                                </p>
                                                                            </td>
                                                                        </tr>
                                                                    </thead>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <!--content-->
                                        <!--footer-->
            
                                        <!--footer-->
                                    </td>
                                </tr>
                                <br>
                                <br>
                            </table>
                        </td>
                    </tr>
                </table>          
            </body>
            </html>`
        };
        rabbitmqUtil.InsertQueue(rabbitMq.getChannelEmail(), rabbitMq.queueEmail, dataToSend, (err, doc) => { });

    } catch (error) {
        logger.error(error)
    }
}
const removeBoostIfRequired = () => {
    try {
        let condition = { "$and": [{ "boost.expiryTime": { $lt: new Date().getTime() } }, { "boost.expiryTime": { "$ne": 0 } }] };
        userList.Select(condition, (err, result) => {
            if (result) {
                result.forEach(element => {
                    userListType.Update(element._id.toString(), { "boost": { "start": 0, "expiryTime": 0 } }, () => { });
                    userList.UpdateById(element._id, { "boost.start": 0, "boost.expiryTime": 0 }, () => { });
                });
            }
        })

    } catch (error) {
        logger.error("in cronJobTask.js : ", error);
    }
}
const updateOnlineUsers = () => {
    try {
        // console.log("-5 min ", moment().add(-5, "minute").valueOf())
        // console.log("cunt min ", moment().valueOf())
        userList.Update({ onlineStatus: 1, "lastOnline": { "$lte": moment().add(-5, "minute").valueOf() } }, { "onlineStatus": 0 }, (error, result) => {
            if (error) logger.error("checkOnlineUsers update  Mongo : ", error)
        });
        userList.Update({ "lastOnline": { "$exists": false } }, { "onlineStatus": 0 }, (error, result) => {
            if (error) logger.error("checkOnlineUsers update  Mongo : ", error)
        });
        userListType.setOffline(moment().add(-5, "minute").valueOf(), (error, result) => {
            if (error) logger.error("checkOnlineUsers update  ES : ", error)
        });

    } catch (error) {
        logger.error("checkOnlineUsers : ", (error) ? error : "")
    }
}
const sendLikeNotification = (_id) => {
    /*      here we are fire a query becuse, between refeshlike  user can change device(device type) at that time push will not work 
             so every time we are fire a query to get correct deviceType
             */
    userList.SelectOne({ _id: ObjectID(_id) }, (err, result) => {
        if (err) logger.error("select user ", err);

        if (result) {
            let payload = {
                notification: {
                    body: "your free likes are refreshed login to datum to find your date"
                    , title: "Match"
                },
                data: {
                    type: "31", target_id: _id, deviceType: result.deviceType,
                    newLikes: result.subscription[0].likeCount
                }
            }
            fcm.sendPushToTopic("/topics/" + _id, payload, (e, r) => { if (e) logger.error("fcm publish on like : ", e) })
            mqtt.publish(_id, JSON.stringify({ messageType: "newLikes", newLikes: result.subscription[0].likeCount }), { qos: 2 },
                (e, r) => { if (e) logger.error("mqtt publish on like : ", e) })

        }
    })
}
const onBoostExpire = (_id) => {
    userListType.removeField(_id, "boostTimeStamp", (e, r) => { if (e) logger.error("boost remove field from es ", e) });
    userList.Unset({ _id: ObjectID(_id) }, { boost: 1 }, (e, r) => { if (e) logger.error("boost remove field from mongo ", e) });
    userBoost.Update({ userId: ObjectID(_id) }, { isActive: 0 }, (e, r) => { if (e) logger.error("boost remove field from mongo userboost ", e) });
    mqtt.publish(_id, JSON.stringify({ "type": "boost" }), { qos: 0 }, () => { })
    /*      here we are fire a query becuse, between boost  user can change device(device type) at that time push will not work 
             so every time we are fire a query to get correct deviceType
             */
    userList.SelectOne({ _id: ObjectID(_id) }, (err, result) => {
        if (err) logger.error("select user ", err);

        if (result) {
            let payload = {
                notification: {
                    body: "your boost is over"
                    , title: "Match"
                },
                data: {
                    type: "31", target_id: _id, deviceType: result.deviceType,
                }
            }
            fcm.sendPushToTopic("/topics/" + _id, payload, (e, r) => { if (e) logger.error("fcm publish on boost : ", e) })
            mqtt.publish(_id, JSON.stringify({ "messageType": "boostExpire", message: "your boost is over" }), { qos: 2 },
                (e, r) => { if (e) logger.error("mqtt publish on boost : ", e) })

        }
    })
}

module.exports = {
    boostTime, redisKeyForBoost, onBoostExpire,
    likeNotificationAt, sendLikeNotification, likeRefreshInMillisecond,
    findAndRemoveExpiredInAppPurchasePlans, sendWellComeMailOnSignUp,
    removeBoostIfRequired, heightInCMObj, heightInFeetObj, updateOnlineUsers
}