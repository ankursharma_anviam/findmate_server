
'use strict'

const joi = require('joi')

const envVarsSchema = joi.object({
    MQTT_WEBSOCKET_URL: joi.string().required(),
    MQTT_URL: joi.string().required(),
    MQTT_USERNAME: joi.string().required(),
    MQTT_PASSWORD: joi.string().required(),
}).unknown()
    .required()

const { error, value: envVars } = joi.validate(process.env, envVarsSchema)
if (error) {
    throw new Error(`Config validation error: ${error.message}`)
}

const config = {
    mqtt: {
        MQTT_URL: envVars.MQTT_URL,
        MQTT_WEBSOCKET_URL: envVars.MQTT_WEBSOCKET_URL,
        MQTT_USERNAME: envVars.MQTT_USERNAME,
        MQTT_PASSWORD: envVars.MQTT_PASSWORD
    }
}

module.exports = config