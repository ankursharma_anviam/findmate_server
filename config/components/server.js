'use strict'

const joi = require('joi')

const envVarsSchema = joi.object({
  PORT_azar: joi.number().required(),
  API_URL_azar: joi.string().required(),
  APPNAME_azar: joi.string().required(),
  TEAMNAME: joi.string().required(),
  LATEST_VERSION: joi.string().required(),
}).unknown()
  .required()

const { error, value: envVars } = joi.validate(process.env, envVarsSchema)
if (error) {
  throw new Error(`Config validation error: ${error.message}`)
}

const config = {
  server: {
    port: envVars.PORT_azar,
    API_URL: envVars.API_URL_azar,
    APPNAME: envVars.APPNAME_azar,
    TEAMNAME: envVars.TEAMNAME,
    LATEST_VERSION: envVars.LATEST_VERSION
  }
}

module.exports = config